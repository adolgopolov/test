<?php

use yii\db\Migration;

/**
 * Class m200725_110710_init
 */
class m200725_110710_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->text(),
            'priority' => $this->integer()->defaultValue(1),
            'status' => $this->smallInteger()->defaultValue(1),
            'date_done' => $this->date(),
            'created_at' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200725_110710_init cannot be reverted.\n";

        return false;
    }
    */
}
