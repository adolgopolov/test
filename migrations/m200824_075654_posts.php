<?php

use yii\db\Migration;

/**
 * Class m200824_075654_posts
 */
class m200824_075654_posts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sqlCreate = <<<SQL
CREATE TABLE user (
  user_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY(user_id)
);

CREATE TABLE post (
  post_id INT NOT NULL AUTO_INCREMENT,
  user_id INT NOT NULL,
  title VARCHAR(255) NOT NULL,
  content TEXT,
  PRIMARY KEY(post_id),
  INDEX(user_id),
  FOREIGN KEY(user_id) REFERENCES user(user_id)
);
SQL;

        $this->execute($sqlCreate);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
        $this->dropTable('post');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200824_075654_posts cannot be reverted.\n";

        return false;
    }
    */
}
