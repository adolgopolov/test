Start the container

    docker-compose up -d
    
    docker-compose exec php bash
            - chmod -R 777 migrations/ docker/dbdata 
            - chmod -R 777 models/ runtime/ web/assets/ controllers/ views/ 
            - php yii migrate
            
    
You can then access the application through the following URL:

    http://127.0.0.1:8000  сайт
    http://127.0.0.1:8080 админер
    
    
    Тестовое задание:
    Написать простой функционал по работе со списком дел (To Do List)
     с использованием фреймворка Yii2 и PostgreSQL.
    Необходимо реализовать вывод списка задач, добавление, редактирование,
       удаление и выставление флага «выполнено».
    Задача должна иметь поля: заголовок, описание, дата выполнения, приоритет (низкий, средний, высокий). 
    
    
    1. PHP
    
    Реализуйте функцию, проверяющую, можно ли из отрезков составить треугольник. Описание функции следующее:
    
    function isValidTriangle(int $a, int $b, int $c): bool {
    // Ваш код.
    }
    
    Диапазон значений у входящих параметров 1..PHP_INT_MAX.
    
    2. SQL
    
    Сделайте запрос к БД, возвращающий имена пользователей, входящих в топ-10 по количеству постов.
    
    Схема данных - https://www.db-fiddle.com/f/bGRwVVUySsBH1XyrrKL87d/0
    
    3. JS
    
    Измените скрипт https://jsfiddle.net/h9wc0ons/ так, чтобы всплывающее окно, открываемое по клику на кнопке, было заблокировано браузером.
    
    Как будет готово - дайте нам знать: по всем вопросам свое решение выложите в свой github.com и ссылку пришлите нам.
    