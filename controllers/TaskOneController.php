<?php

namespace app\controllers;

use app\models\PostAR;
use app\models\UserAR;
use Yii;
use app\models\TaskAR;
use app\models\TaskAS;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskController implements the CRUD actions for TaskAR model.
 */
class TaskOneController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaskAR models.
     * @return mixed
     */
    public function actionIndex()
    {

        $models = UserAR::find()->mostPublish()->all();

        return $this->render('index', [
            'models' => $models
        ]);
    }

    /**
     * @return string
     */
    public function actionTriangle()
    {
        $model = new \app\models\TaskOne();
        $result = null;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $result = $model->isValidTriangle()?'Правильные длины треугольника':'Треугольник не сложить';
            }
        }

        return $this->render('triangle', [
            'model' => $model,
            'result' => $result,
        ]);
    }

    public function actionGenerate($u=1,$p=10)
    {
        UserAR::generate($u);
        PostAR::generate($p);

        return $this->redirect(['index']);
    }

}
