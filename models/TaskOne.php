<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 24.08.2020 09:35
 */

namespace app\models;

/**
 * Class TaskOne
 * @package app\models
 * @property int $line1
 * @property int $line2
 * @property int $line3
 */
class TaskOne extends \yii\base\Model
{
    public $line1;
    public $line2;
    public $line3;

    public function rules()
    {
        return [
            [['line1','line2','line3'],'integer']
        ];
    }


    public function isValidTriangle()
    {
        return self::validTriangle($this->line1,$this->line2,$this->line3);
    }

    /**
     * @param int $a
     * @param int $b
     * @param int $c
     * @return bool
     */
    public static function validTriangle( $a,  $b,  $c)
    {
        $triangle = [$a, $b, $c];
        $maxLine = max([$a, $b, $c]);
        $other = array_diff($triangle,[$maxLine]);

        return ($maxLine - $other[0] < $other[1] );
    }
}