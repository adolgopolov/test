<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserAR]].
 *
 * @see UserAR
 */
class UserAQ extends \yii\db\ActiveQuery
{
    /**
     * @param int $limit
     * @return UserAQ
     */
    public function mostPublish($limit=10)
    {
        $this->alias('u');
        $this->select([
            'u.*',
            'p_count'=> 'COUNT(p.post_id)',
        ]);
        $this->leftJoin(PostAR::tableName().' p','p.user_id=u.user_id');
        $this->groupBy('u.user_id');
        $this->orderBy(['p_count'=>SORT_DESC]);

        return $this->limit($limit);
    }

    /**
     * {@inheritdoc}
     * @return UserAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
