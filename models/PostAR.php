<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $post_id
 * @property int $user_id
 * @property string $title
 * @property string|null $content
 *
 * @property UserAR $user
 */
class PostAR extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'title'], 'required'],
            [['user_id'], 'integer'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserAR::class, 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'post_id' => 'Post ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'content' => 'Content',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery | UserAQ
     */
    public function getUser()
    {
        return $this->hasOne(UserAR::class, ['user_id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return PostAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new PostAQ(get_called_class());
    }

    public static function generate($count)
    {
        $userIds = UserAR::find()->column();
        for ($i = 0; $i < $count; $i++) {
            $post = new self([
                'user_id' => array_rand($userIds),
                'title' => 'Title' . $i,
                'content' => 'Content' . $i,
            ]);
            $post->save();
        }
    }
}
