<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $user_id
 * @property string $name
 * @property int $p_count
 * @property Post[] $posts
 */
class UserAR extends \yii\db\ActiveRecord
{
    public $p_count;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }
    public function scenarios()
    {
        $parent = parent::scenarios();
        $parent[self::SCENARIO_DEFAULT] = array_merge($parent[self::SCENARIO_DEFAULT],['p_count']);

        return $parent;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Posts]].
     *
     * @return \yii\db\ActiveQuery|PostQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['user_id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new UserAQ(get_called_class());
    }

    public static function generate($count)
    {
        for($i=0;$i<$count;$i++)
        {
            $user = new self(['name' => 'random'.$i]);
            $user->save();
        }
    }
}
