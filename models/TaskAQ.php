<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TaskAR]].
 *
 * @see TaskAR
 */
class TaskAQ extends \yii\db\ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return self
     */
    public function oldPriority($priority)
    {
        return $this->andWhere(['>=','priority', (int) $priority]);
    }
    /**
     * {@inheritdoc}
     * @return self
     */
    public function notSelf($id)
    {
        return $this->andWhere(['not',['id'=> $id]]);
    }

    /**
     * {@inheritdoc}
     * @return TaskAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TaskAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
