<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $priority
 * @property int $status
 * @property string $date_done
 * @property string $created_at
 * @property int $countTasks
 * @property [] $listTasks
 */
class TaskAR extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_DONE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => TimestampBehavior::class,
                'value' => date('Y-m-d H:i:s'),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['priority', 'status'], 'default', 'value' => null],
            [['priority', 'status'], 'integer'],
            [['priority', 'status'], 'default', 'value' => 1],
            [['date_done', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'priority' => 'Приоритет',
            'status' => 'Статус',
            'date_done' => 'Дата выполнения',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TaskAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskAQ(get_called_class());
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        self::changePriority($this->id, $this->priority ?: 1);
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function isDone()
    {
        return $this->status == self::STATUS_DONE;
    }

    public function toggleStatus()
    {
        $status = ($this->status == self::STATUS_DONE) ? self::STATUS_NEW : self::STATUS_DONE;
        $this->updateAttributes(['status' => $status]);
    }

    public static function getStatuses($status = null)
    {
        $list = [
            self::STATUS_NEW => 'Новый',
            self::STATUS_DONE => 'Выполнен',
        ];
        if (!empty($status)) {
            if (!empty($list[$status])) {
                return $list[$status];
            }
            return 'Не определен';
        }
        return $list;
    }

    public static function getListTasks()
    {
        $list = [];
        for ($i = 1; $i <= self::getCountTasks(); $i++) {
            $list[$i] = $i;
        }
        $list[$i] = $i;
        return $list;
    }

    public static function getCountTasks()
    {
        return self::find()->count();
    }

    /**
     * изменить порядок остальных записей
     * @param $priority
     */
    public static function changePriority($selfID, $priority)
    {
        $query = self::find()->oldPriority($priority);
        if ($selfID) {
            $query->notSelf($selfID);
        }
        $records = $query
            ->orderBy(['priority' => SORT_ASC])
            ->all();

        foreach ($records as $eachRecord) {
            $eachRecord->updateAttributes(['priority' => ++$priority]);
        }
    }
}
