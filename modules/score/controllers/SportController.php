<?php

namespace common\modules\score\controllers;

use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\components\S3Core;
use common\doings\score\ChangeScoreKindDoing;
use common\modules\game\models\SportMatchAR;
use common\modules\score\forms\CalcSportAS;
use common\modules\score\forms\CalcSportSmanFM;
use common\modules\score\forms\ScoreKindAS;
use common\modules\score\models\CalcItemAR;
use common\modules\score\models\CalcSportAR;
use common\modules\score\models\ScoreKindAR;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportPositionAR;
use common\modules\sport\forms\SportTeamFM;
use Exception;
use kartik\select2\Select2;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

class SportController extends AdminController
{
    use CRUDTrait;

    public const TAB_MATCHES = 0;
    public const TAB_SCORINGS = 1;

    public $modelClass = CalcSportAR::class;

    /**
     * @param int $tab
     * @return string
     */
    public function actionIndex(int $tab = self::TAB_MATCHES)
    {
        Url::remember();

        $query = CalcSportAS::querySearchGroup();

        $searchModel = new CalcSportAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $query);

        $scoreKindSearch = new ScoreKindAS();
        $scoreKindProvider = $scoreKindSearch->search(Yii::$app->request->queryParams);

        $leagues = SportLeagueAR::find()
            ->indexBy('id')
            ->all();

        $scoreKindStatuses = ScoreKindAR::getStatusesList();
        $leagueList = SportLeagueAR::getLeaList();
        $statuses = S3Core::code2status();
        $sports = SportAR::getAsOptsList();

        return $this->render('index', [
            'tab' => $tab,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'scoreKindProvider' => $scoreKindProvider,
            'scoreKindSearch' => $scoreKindSearch,
            'scoreKindStatuses' => $scoreKindStatuses,
            'leagues' => $leagues,
            'statuses' => $statuses,
            'sports' => $sports,
            'leagueList' => $leagueList,
        ]);
    }

    /**
     * @param $match_id
     * @return string
     * @throws \Exception
     */
    public function actionView($match_id)
    {
        $matchModel = SportMatchAR::findOne($match_id);

        $searchModel = new CalcSportAS();
        $dataProvider = $searchModel->searchSmen(Yii::$app->request->queryParams);

        $teams = SportTeamFM::getTeams();
        $positions = SportPositionAR::getPositions();

        $columns = $this->generateColumns($match_id, false, $teams, $positions, $searchModel);

        ColumnHiderWidget::setColumnsSession();

        return $this->render('matches/view', [
            'match_id' => $match_id,
            'matchModel' => $matchModel,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns' => $columns,
        ]);
    }

    /**
     * @param $match_id
     * @return string
     * @throws \Exception
     */
    public function actionUpdate($match_id)
    {
        $calcSportSmanModel = new CalcSportSmanFM();

        if ($calcSportSmanModel->load(Yii::$app->request->post())) {
            if ($calcSportSmanModel->saveCalcSport()) {
                Yii::$app->session->addFlash('success', 'Скоринги матча успешно обновлены.');

                return $this->refresh();
            }

            Yii::$app->session->addFlash('error', 'Произошла ошибка при обновлении скорингов.');
        }

        $matchModel = SportMatchAR::findOne($match_id);

        $query = CalcSportAS::queryCalcSmen();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => false,
        ]);

        $columns = $this->generateColumns($match_id, true);

        return $this->render('matches/update', [
            'match_id' => $match_id,
            'matchModel' => $matchModel,
            'dataProvider' => $dataProvider,
            'columns' => $columns,
        ]);
    }

    /**
     * @throws Exception
     */
    public function actionCreate()
    {
        throw new Exception('Не реализовано!');
    }

    /**
     * @throws Exception
     */
    public function actionDelete()
    {
        throw new Exception('Не реализовано!');
    }

    /**
     * @param integer $matchId
     * @param bool $forUpdate
     * @param array $teams
     * @param array $positions
     * @param null|CalcSportAS $searchModel
     * @return array
     * @throws \Exception
     */
    public function generateColumns($matchId, $forUpdate = false, $teams = [], $positions = [], $searchModel = null)
    {
        $columns = [
            [
                'attribute' => 'sman_id',
                'options' => [
                    'class' => 'small-grid-column',
                ],
            ],
            [
                'attribute' => 'sman_name',
                'options' => [
                    'class' => 'small-grid-column',
                ],
            ],
            [
                'attribute' => 'pos_id',
                'options' => [
                    'class' => 'small-grid-column',
                ],
                'value' => static function (CalcSportAS $model) {
                    return $model->pos_name;
                },
                'filter' => $searchModel ? Select2::widget([
                    'attribute' => 'pos_id',
                    'model' => $searchModel,
                    'data' => $positions,
                    'pluginOptions' => [
                        'placeholder' => 'Выберите амплуа',
                        'allowClear' => true,
                    ],
                    'pjaxContainerId' => 'score-sport-pjax',
                ]) : false,
            ],
            [
                'attribute' => 'team_id',
                'options' => [
                    'class' => 'small-grid-column',
                ],
                'filter' => $searchModel ? Select2::widget([
                    'attribute' => 'team_id',
                    'model' => $searchModel,
                    'data' => $teams,
                    'pluginOptions' => [
                        'placeholder' => 'Выберите команду',
                        'allowClear' => true,
                    ],
                    'pjaxContainerId' => 'score-sport-pjax',
                ]) : false,
            ],
        ];

        $matchCalcs = CalcSportAR::find()
            ->select(['calc_id'])
            ->andWhere(['match_id' => $matchId])
            ->groupBy('calc_id')
            ->column();

        $calcNames = CalcItemAR::find()
            ->select(['r_name'])
            ->andWhere(['id' => $matchCalcs])
            ->indexBy('id')
            ->column();

        asort($matchCalcs);

        $hidingColumns = [];

        $calcSportSmanModel = new CalcSportSmanFM();

        foreach ($matchCalcs as $calcId) {
            $attribute = 'calc_' . $calcId;
            $label = $calcNames[$calcId] ?? null;
            $hidingColumns[$attribute] = $label;

            $columns[] = [
                'attribute' => $attribute,
                'label' => $label,
                'value' => static function (CalcSportAS $model) use ($calcId, $forUpdate, $calcSportSmanModel) {
                    $condition = [
                        'sman_id' => $model->sman_id,
                        'match_id' => $model->match_id,
                        'calc_id' => $calcId,
                    ];

                    if ($forUpdate) {
                        $calcModels = CalcSportAS::getCalcs($condition);
                        $inputs = null;

                        foreach ($calcModels as $calcModel) {
                            $attribute = "calc_result[$calcModel->id]";

                            $inputs .= Html::tag('div', Html::activeInput('number', $calcSportSmanModel, $attribute, [
                                'value' => $calcModel->calc_result,
                                'class' => 'form-control',
                                'data' => [
                                    'calc-result' => $model->sman_id,
                                ],
                            ]), ['class' => 'input-text-grid-column']);
                        }

                        if ($inputs) {
                            return $inputs;
                        }
                    }

                    return CalcSportAS::getCalcSum($condition);
                },
                'format' => 'raw',
            ];
        }

        $attributeSum = 'calc_sum';
        $labelSum = 'Сумма';
        $hidingColumns[$attributeSum] = $labelSum;

        $columns[] = [
            'attribute' => $attributeSum,
            'label' => $labelSum,
            'value' => static function (CalcSportAS $model) use ($forUpdate, $attributeSum) {
                $value = CalcSportAS::getCalcSum([
                    'sman_id' => $model->sman_id,
                    'match_id' => $model->match_id,
                ]);

                if ($forUpdate) {
                    return Html::activeInput('number', $model, $attributeSum, [
                        'value' => $value,
                        'class' => 'form-control',
                        'disabled' => true,
                        'data' => [
                            'calc-result-sum' => $model->sman_id,
                        ],
                    ]);
                }

                return $value;
            },
            'format' => 'raw',
        ];

        if ($searchModel) {
            $searchModel->hidingColumns = $hidingColumns;
        }

        return $columns;
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreateScoreKind()
    {
        $model = new ScoreKindAR();

        if ($model->load(Yii::$app->request->post())) {
            /** @var ChangeScoreKindDoing $doing */
            $doing = ChangeScoreKindDoing::new();

            $result = $doing->doIt([
                'model' => $model,
            ]);

            if ($result) {
                Yii::$app->session->addFlash('success', 'Тип скоринга успешно создан!');

                return $this->redirect(['update-score-kind', 'id' => $model->id]);
            }

            Yii::$app->session->addFlash('error', $doing->errors);

            return $this->redirect(['index', 'tab' => self::TAB_SCORINGS]);
        }

        $sports = SportAR::getAsOptsList();

        return $this->render('scorings/create', [
            'model' => $model,
            'sports' => $sports,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionViewScoreKind($id)
    {
        Url::remember();

        /** @var ScoreKindAR $model */
        $model = ScoreKindAR::findOrFail($id);

        return $this->render('scorings/view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdateScoreKind($id)
    {
        /** @var ScoreKindAR $model */
        $model = ScoreKindAR::findOrFail($id);

        if ($model->load(Yii::$app->request->post())) {
            /** @var ChangeScoreKindDoing $doing */
            $doing = ChangeScoreKindDoing::new();

            $result = $doing->doIt([
                'model' => $model,
            ]);

            if ($result) {
                Yii::$app->session->addFlash('success', 'Тип скоринга успешно изменён!');

                return $this->refresh();
            }

            Yii::$app->session->addFlash('error', $doing->errors);

            return $this->redirect(['index', 'tab' => self::TAB_SCORINGS]);
        }

        $sports = SportAR::getAsOptsList();

        return $this->render('scorings/update', [
            'model' => $model,
            'sports' => $sports,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSwitchStatusScoreKind($id)
    {
        /** @var ScoreKindAR $model */
        $model = ScoreKindAR::findOrFail($id);

        /** @var ChangeScoreKindDoing $doing */
        $doing = ChangeScoreKindDoing::new();

        $result = $doing->doIt([
            'model' => $model,
            'mode' => ChangeScoreKindDoing::MODE_SWITCH_STATUS,
        ]);

        if ($result) {
            Yii::$app->session->addFlash('success', 'Статус типа скоринга успешно изменён.');
        } else {
            Yii::$app->session->addFlash('error', $doing->errors);
        }

        return $this->goBack();
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCloneScoreKind($id)
    {
        /** @var ScoreKindAR $model */
        $model = ScoreKindAR::findOrFail($id);

        /** @var ChangeScoreKindDoing $doing */
        $doing = ChangeScoreKindDoing::new();

        $result = $doing->unsetRules(['checkStatus'])
            ->doIt([
                'model' => $model,
                'mode' => ChangeScoreKindDoing::MODE_CLONE,
            ]);

        if ($result) {
            Yii::$app->session->addFlash('success', 'Копия типа скоринга успешно создана.');
        } else {
            Yii::$app->session->addFlash('error', $doing->errors);
        }

        return $this->goBack();
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGridActions()
    {
        if (Yii::$app->request->isPost) {
            $_post = Yii::$app->request->post();
            $ids = $_post['selection'] ?? [];

            if (!$ids) {
                Yii::$app->session->addFlash('error', 'Вы не выбрали ни одну запись.');

                return $this->redirect(['index', 'tab' => self::TAB_SCORINGS]);
            }

            if (isset($_post['next-status'])) {
                if ($this->gridNextStatus($ids)) {
                    Yii::$app->session->addFlash('success', 'Массовый перевод статусов произошёл успешно.');
                } else {
                    Yii::$app->session->addFlash('error', 'Произошла ошибка при переводе статусов.');
                }
            } elseif (isset($_post['clone'])) {
                if ($this->gridClone($ids)) {
                    Yii::$app->session->addFlash('success', 'Массовое созание копий произошло успешно.');
                } else {
                    Yii::$app->session->addFlash('error', 'Произошла ошибка при создании копий.');
                }
            }
        }

        return $this->redirect(['index', 'tab' => self::TAB_SCORINGS]);
    }

    /**
     * @param $ids
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function gridNextStatus($ids)
    {
        /** @var ChangeScoreKindDoing $doing */
        $doing = ChangeScoreKindDoing::new();
        $models = ScoreKindAR::findAll($ids);

        foreach ($models as $model) {
            $doing->doIt([
                'model' => $model,
                'mode' => ChangeScoreKindDoing::MODE_SWITCH_STATUS,
            ]);
        }

        return !$doing->hasErrors();
    }

    /**
     * @param $ids
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function gridClone($ids)
    {
        /** @var ChangeScoreKindDoing $doing */
        $doing = ChangeScoreKindDoing::new();
        $models = ScoreKindAR::findAll($ids);

        foreach ($models as $model) {
            $doing->unsetRules(['checkStatus'])
                ->doIt([
                    'model' => $model,
                    'mode' => ChangeScoreKindDoing::MODE_CLONE,
                ]);
        }

        return !$doing->hasErrors();
    }
}
