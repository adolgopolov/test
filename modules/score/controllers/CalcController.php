<?php

namespace common\modules\score\controllers;

use backend\controllers\AdminController;
use backend\controllers\DictCRUDTrait;
use common\components\S3Kind;
use common\modules\score\models\CalcItemAR;
use yii\base\Exception;
use yii\data\ActiveDataProvider;

/**
 * CalcController implements the CRUD actions for CalcItemAR model.
 */
class CalcController extends AdminController
{
    use DictCRUDTrait;

    public $modelClass = CalcItemAR::class;
    public $kind = S3Kind::KIND_CALC;

    /**
     * Lists all CalcItemAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CalcItemAR::find()
                ->andWhere('id > 0'),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @throws Exception
     */
    public function actionCreate()
    {
        throw new Exception('Не реализовано!!!');
    }

    /**
     * @throws Exception
     */
    public function actionUpdate()
    {
        throw new Exception('Не реализовано!!!');
    }

    /**
     * @throws Exception
     */
    public function actionDelete()
    {
        throw new Exception('Не реализовано!!!');
    }
}
