<?php

namespace common\modules\score\controllers;

use backend\controllers\AdminController;
use backend\controllers\DictCRUDTrait;
use common\components\S3Kind;
use common\modules\score\models\GamerLevelAR;
use yii\data\ActiveDataProvider;

/**
 * GamerLevelController implements the CRUD actions for GamerLevelAR model.
 */
class GamerLevelController extends AdminController
{
    use DictCRUDTrait;

    public $modelClass = GamerLevelAR::class;
    public $kind = S3Kind::KIND_GAMER_LEVEL;

    /**
     * Lists all GamerLevelAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => GamerLevelAR::find(),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
