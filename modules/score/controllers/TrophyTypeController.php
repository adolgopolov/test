<?php

namespace common\modules\score\controllers;

use backend\controllers\AdminController;
use common\modules\game\forms\TrophyTypeAS;
use common\modules\game\models\TrophyTypeAR;
use common\modules\money\models\MoneyCurrencyAR;
use Yii;

class TrophyTypeController extends AdminController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TrophyTypeAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $currencyModels = MoneyCurrencyAR::find()
            ->indexBy('id')
            ->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currencyModels' => $currencyModels,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        /** @var TrophyTypeAR $model */
        $model = TrophyTypeAR::findOrFail($id);

        $currencyModels = MoneyCurrencyAR::find()
            ->indexBy('id')
            ->all();

        return $this->render('view', [
            'model' => $model,
            'currencyModels' => $currencyModels,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var TrophyTypeAR $model */
        $model = TrophyTypeAR::findOrFail($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Запись успешно обновлена.');

                return $this->redirect(['index']);
            }

            Yii::$app->session->addFlash('error', $model->errors);
        }

        $currencies = MoneyCurrencyAR::getList();

        return $this->render('update', [
            'model' => $model,
            'currencies' => $currencies,
        ]);
    }
}
