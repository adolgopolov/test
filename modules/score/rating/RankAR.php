<?php

namespace common\modules\score\rating;

use common\components\FindOrFailTrait;
use common\components\S3DictionaryAR;
use common\components\S3Kind;

/**
 * Class RankAR
 * @package common\modules\rating\models
 */
class RankAR extends S3DictionaryAR
{
    use FindOrFailTrait;

    // TODO взять из с донными из ss_dictionary
    /* public const WOOD_RANGE = ['left' => 0, 'right' => 149900];
    public const STEEL_RANGE = ['left' => 150000, 'right' => 349900];
    public const BRONZE_RANGE = ['left' => 350000, 'right' => 749900];
    public const SILVER_RANGE = ['left' => 750000, 'right' => 1349900];
    public const GOLD_RANGE = ['left' => 1350000, 'right' => 2149900];
    public const PLATINUM_RANGE = ['left' => 2150000, 'right' => 3149900];
    public const DIAMOND_RANGE = ['left' => 3150000, 'right' => 4349900];
    public const TITAN_RANGE = ['left' => 4350000, 'right' => 5760000];

    public const MAX_POINTS = self::TITAN_RANGE['right'];
    */

    public const WOOD = 1;
    public const STEEL = 2;
    public const BRONZE = 3;
    public const SILVER = 4;
    public const GOLD = 5;
    public const PLATINUM = 6;
    public const DIAMOND = 7;
    public const TITAN = 8;

    /**
     * @return int|null
     */
    public static function kind()
    {
        return S3Kind::KIND_RANK;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'ent_code' => 'Код',
            'ent_value' => 'Значение'
        ];
    }

    /**
     * TODO
     * Метод рассчитывает 'rank_id' => 1, 'rank_progress' => 0, 'elite_id' для единственного интервала на дату
     * обход интервалов организовывает вызывающая сторона, требуется разбивка
     * @param int $exp_points
     * @param array $start_ranks
     * @param string|\DateInterval|\DateTime $interval_date
     * @return array
     */
    public static function calcRank(int $exp_points, array $start_ranks = [], $date_interval = null) : array
    {
        $start_ranks = array_merge(['rank_id' => 1, 'rank_progress' => 0, 'elite_id' => 0], $start_ranks);

        return ['rank_id' => 1, 'rank_progress' => 0, 'elite_id' => 0]; // TODO $start_ranks;
    }
}
