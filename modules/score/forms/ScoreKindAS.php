<?php

namespace common\modules\score\forms;

use common\modules\score\models\ScoreKindAR;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class ScoreKindAS extends ScoreKindAR
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'r_name'], 'string'],
            [['r_status', 'sport_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [

        ]);
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->alias('score_kind');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        $query->andFilterWhere([
            'AND',
            ['id' => $id],
            ['like', 'r_name', $this->r_name],
            ['r_status' => $this->r_status],
            ['sport_id' => $this->sport_id],
        ]);

        return $dataProvider;
    }
}
