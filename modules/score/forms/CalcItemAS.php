<?php

namespace common\modules\score\forms;

use common\modules\score\models\CalcItemAR;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class CalcItemAS extends CalcItemAR
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'string'],
            [['ent_value'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'ent_value' => 'Вид спорта',
        ]);
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->alias('calc');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => ArrayHelper::merge($dataProvider->sort->attributes, [

            ]),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        $query->andFilterWhere([
            'AND',
            ['id' => $id],
            ['ent_value' => $this->ent_value],
        ]);

        return $dataProvider;
    }
}
