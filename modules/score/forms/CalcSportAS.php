<?php

namespace common\modules\score\forms;

use common\components\S3Kind;
use common\modules\game\models\SportMatchAR;
use common\modules\score\models\CalcSportAQ;
use common\modules\score\models\CalcSportAR;
use common\modules\sount\widgets\buffer\interfaces\ColumnHiderModelInterface;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportMemberAR;
use common\modules\sport\models\SportPositionAR;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class CalcSportAS extends CalcSportAR implements ColumnHiderModelInterface
{
    public const SCENARIO_SMEN = 'smen';

    public $sport_id;
    public $lea_id;
    public $match_name;
    public $match_status;
    public $team_id;
    public $sman_name;
    public $pos_name;
    public $pos_id;
    public $sum_calc_result;

    public $columns;
    public $hidingColumns = [];

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_SMEN => ['calc_id', 'sman_id', 'team_id', 'sman_name', 'pos_id'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sport_id', 'lea_id', 'match_id', 'match_status', 'calc_id', 'team_id', 'pos_id'], 'integer'],
            [['id', 'updated_at', 'match_name', 'sman_name', 'pos_name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'sport_id' => 'Вид спорта',
            'lea_id' => 'Лига',
            'match_name' => 'Название матча',
            'match_status' => 'Статус матча',
            'calc_id' => 'ID расчета',
            'sman_id' => 'ID Спортсмена',
            'team_id' => 'Команда',
            'sman_name' => 'Спортсмен',
            'pos_id' => 'Амплуа спортсмена',
        ], $this->hidingColumns);
    }

    /**
     * @param $params
     * @param CalcSportAQ|null $query
     * @return ActiveDataProvider
     */
    public function search($params, $query = null)
    {
        $query = $query ?: self::querySearch();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                'id' => SORT_DESC,
            ],
            'attributes' => ArrayHelper::merge($dataProvider->sort->attributes, [
                'sport_id',
                'lea_id',
                'match_name',
                'match_status',
            ]),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        $query->andFilterWhere([
            'AND',
            ['calc_sport.id' => $id],
            ['like', 'calc_sport.updated_at', $this->updated_at],
        ]);

        $query->andFilterHaving([
            'AND',
            ['sport_id' => $this->sport_id],
            ['lea_id' => $this->lea_id],
            ['like', 'match_name', $this->match_name],
            ['match_status' => $this->match_status],
        ]);

        return $dataProvider;
    }

    /**
     * @return CalcSportAQ
     */
    public static function querySearch()
    {
        return self::find()
            ->alias('calc_sport')
            ->select(['calc_sport.*', 'sport.id as sport_id', 'lea.id as lea_id', 'match.r_name as match_name', 'match.r_status as match_status'])
            ->leftJoin(SportMatchAR::tableName() . ' match', 'match.id = calc_sport.match_id')
            ->leftJoin(SportAR::tableName() . ' sport', 'match.sport_id = sport.id AND sport.ent_kind = :sport_kind', [
                'sport_kind' => S3Kind::KIND_SPORT,
            ])
            ->leftJoin(SportLeagueAR::tableName() . ' lea', 'lea.id = match.lea_id');
    }

    /**
     * @return CalcSportAQ
     */
    public static function querySearchGroup()
    {
        $fromQuery = self::find()
            ->groupBy(['`match_id` DESC']);

        return self::querySearch()
            ->from('(' . $fromQuery->createCommand()->rawSql . ') calc_sport');
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function searchSmen($params)
    {
        $query = self::queryCalcSmen();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => ArrayHelper::merge($dataProvider->sort->attributes, [
                'team_id',
                'sman_name',
                'pos_id',
            ]),
        ]);

        $this->scenario = self::SCENARIO_SMEN;
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $calc_id = str_replace(' ', '', trim($this->calc_id));
        if (strpos($calc_id, ',')) {
            $calc_id = explode(',', $calc_id);
        }

        $query->andFilterWhere([
            'AND',
            ['calc_sport.calc_id' => $calc_id],
        ]);

        $query->andFilterHaving([
            'AND',
            ['team_id' => $this->team_id],
            ['like', 'sman_name', $this->sman_name],
            ['pos_id' => $this->pos_id],
        ]);

        return $dataProvider;
    }

    /**
     * TODO неправильный выбор  SportMemberAR неправильная выборка -
     * TODO не фильтрует по матчу делает выборку всех расчетов по спортсмену
     * @return CalcSportAQ
     */
    public static function queryCalcSmen()
    {
        $queryMember = SportMemberAR::find()
            ->select(['MAX(id)'])
            ->andWhere('sman_id = calc_sport.sman_id')
            ->orderBy(['id' => SORT_DESC])
            ->limit(1);

        $queryPosition = SportPositionAR::find()
            ->select(['r_name'])
            ->andWhere('id = team_member.pos_id');

        return self::find()
            ->alias('calc_sport')
            ->select([
                'calc_sport.id',
                'calc_sport.match_id',
                'calc_sport.calc_id',
                'calc_sport.sman_id',
                'team_member.team_id as team_id',
                'team_member.pos_id as pos_id',
                'sman.r_name as sman_name',
                '(' . $queryPosition->createCommand()->rawSql . ') as pos_name',
            ])
            ->leftJoin(SportMemberAR::tableName() . ' team_member', 'team_member.id = (' . $queryMember->createCommand()->rawSql . ')')
            ->leftJoin(SmanAR::tableName() . ' sman', 'sman.id = calc_sport.sman_id')
            ->groupBy('calc_sport.sman_id');
    }

    /**
     * @param array|string $condition
     * @return integer|string|null
     */
    public static function getCalcSum($condition)
    {
        $model = self::find()
            ->select(['SUM(calc_result) as sum_calc_result'])
            ->andWhere($condition)
            ->one();

        return $model->sum_calc_result ?? '';
    }

    /**
     * @param $condition
     * @return array|CalcSportAS[]|CalcSportAR[]
     */
    public static function getCalcs($condition)
    {
        return self::find()
            ->select(['id', 'calc_result'])
            ->andWhere($condition)
            ->all();
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return array_keys($this->hidingColumns);
    }
}
