<?php

namespace common\modules\score\forms;

use common\modules\score\models\CalcSportAR;
use yii\base\Model;

class CalcSportSmanFM extends Model
{
    /** @var array */
    public $calc_result;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['calc_result'], 'safe'],
        ];
    }

    /**
     * @return bool|int
     */
    public function saveCalcSport()
    {
        if (!$this->validate()) {
            return false;
        }

        $saved = 0;

        foreach ($this->calc_result as $id => $calc_result) {
            $calcSport = CalcSportAR::findOne($id);

            if (!$calcSport) {
                continue;
            }

            $calcSport->calc_result = $calc_result;
            if ($calcSport->save()) {
                $saved++;
            }
        }

        return $saved;
    }
}
