<?php

namespace common\modules\score;

use Yii;
use yii\mutex\MysqlMutex;
use common\modules\score\models\SportCoef;
use common\components\S3Core;
use common\modules\game\models\{
    GameTouMatchAR, GameTourneyAR
};
use common\modules\score\models\ScoreKindAR;
use common\modules\score\operations\{DynamicScoring, ExperienceScoring, MatchScoring, TourneyScoring, TrophyScoring};
use backend\components\aboard\MenuEntryInterface;

/**
 * Class ScoreModule
 * @package common\modules\score
 */
class ScoreModule extends \yii\base\Module implements MenuEntryInterface
{
    public $provider;
    const MUTEX_NAME = 'scoring';

    /**
     * @param $match
     * @param array $opts
     * @return bool
     */
    public function calcMatch($match, &$opts = [])
    {
        $success = false;

        $mutex = new MysqlMutex();
        if ($mutex->acquire(self::MUTEX_NAME)) {
            $transaction = Yii::$app->db->beginTransaction('REPEATABLE READ');

            try {
                $opMatch = MatchScoring::generateScoring($match);

                $opMatch->calculate();

                $success = $opMatch->save();

                if ($success){
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                    $opts['errors'] = $opMatch->errors;
                    Yii::warning('calcMatch ' . serialize($opts['errors']), 'ss3');
                }
            } catch (\Exception $ex) {
                $success = false;
                Yii::error($ex->getMessage() . PHP_EOL . $ex->getTraceAsString(), 'ss3');
                $opts['errors']['core'] = $ex->getMessage();
            }
        }

        return $success;
    }

    public function calcExperience($tourney, &$opts = [])
    {
        $success = false;

        $mutex = new MysqlMutex();
        if ($mutex->acquire(self::MUTEX_NAME)) {
            $transaction = Yii::$app->db->beginTransaction('REPEATABLE READ');
            try {
                $opTourney = ExperienceScoring::generateScoring($tourney);

                $opTourney->calculate();
                $success = $opTourney->save();

                if ($success){
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                    $opts['errors'] = $opTourney->errors;
                    Yii::warning('calcTourney ' . serialize($opts['errors']), 'ss3');
                }
            } catch (\Exception $ex) {
                $success = false;
                Yii::error($ex->getMessage() . PHP_EOL . $ex->getTraceAsString(), 'ss3');
                $opts['errors']['core'] = $ex->getMessage();
            }
        }

        return $success;
    }

    /**
     * @param $tourney
     * @param array $opts
     * @return bool
     */
    public function calcTourney($tourney, &$opts = [])
    {
        $success = false;

        $mutex = new MysqlMutex();
        if ($mutex->acquire(self::MUTEX_NAME)) {
            $transaction = Yii::$app->db->beginTransaction('REPEATABLE READ');
            try {
                $opTourney = TourneyScoring::generateScoring($tourney);

                $opTourney->calculate();

                $success = $opTourney->save();

                if ($success) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                    $opts['errors'] = $opTourney->errors;
                    Yii::warning('calcTourney ' . serialize($opts['errors']), 'ss3');
                }
            } catch (\Exception $ex) {
                $success = false;
                Yii::error($ex->getMessage() . PHP_EOL . $ex->getTraceAsString(), 'ss3');
                $opts['errors']['core'] = $ex->getMessage();
            }
        }

        return $success;
    }

    /**
     * @param $tourney
     * @param array $opts
     * @return bool
     */
    public function calcTrophy($tourney, &$opts = [])
    {
        $success = false;

        $mutex = new MysqlMutex();
        if ($mutex->acquire(self::MUTEX_NAME)) {
            $transaction = Yii::$app->db->beginTransaction('REPEATABLE READ');

            try {
                $opTourney = TrophyScoring::generateScoring($tourney);

                if ($opTourney->tourney->isStatus(S3Core::STATUS_COMPLETED)) {
                    $opts['errors'] = 'Tourney is completed ' . $opTourney->tourney->id;
                    $transaction->rollBack();

                    return false;
                }

                $success = $opTourney->calculate() && $opTourney->save();

                if ($success) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                    $opts['errors'] = $opTourney->errors;
                    Yii::warning('calcTrophy ' . serialize($opts['errors']), 'ss3');
                }
            } catch (\Exception $ex) {
                $success = false;
                Yii::error($ex->getMessage() . PHP_EOL . $ex->getTraceAsString(), 'ss3');
                $opts['errors']['core'] = $ex->getMessage();
            }
        }

        return $success;
    }

    /**
     * @param $tourney
     * @param array $opts
     * @return bool
     */
    public function calcDynamic($tourney, &$opts = [])
    {
        // TODO оформить вызов как остальные скоринги

        $opDynamic = DynamicScoring::generateScoring($tourney);

        $opDynamic->calculate();

        $opDynamic->save();

        return true;//$opTourney->save();
        // TODO вернуть успешность или ошибку
    }

    /**
     *  все типы расчетов по турнирам для матча
     * @param $matchId
     * @return models\ScoreKindAQ .
     */
    public static function getScoreKinds($matchId)
    {
        $tableTourney = GameTourneyAR::tableName();
        $tableSKind = ScoreKindAR::tableName();
        $tableTMatch = GameTouMatchAR::tableName();

        return ScoreKindAR::find()
                          ->groupBy("$tableSKind.id")
                          ->innerJoin($tableTourney, "$tableTourney.score_kind=$tableSKind.id")
                          ->innerJoin($tableTMatch, "$tableTMatch.tou_id=$tableTourney.id")
                          ->andWhere(["$tableTMatch.match_id" => $matchId]);
    }

    /**
     * @param $sportId
     * @param null $date
     * @return int
     */
    public function getSportCoef($sportId, $date): int
    {
        return SportCoef::coefBySport($sportId);
    }

    public static function getMenuItems(): ?array
    {
        return [
            [
                'label' => 'Скоринг',
                'icon' => 'panel',
                'items' => [
                    [
                        'label' => 'Спортивный скоринг',
                        'url' => ['/score/sport/index'],
                    ],
                    [
                        'label' => 'Calcs',
                        'url' => ['/score/calc/index'],
                    ],
                    [
                        'label' => 'Уровни игроков',
                        'url' => ['/score/gamer-level/index'],
                    ],
                    [
                        'label' => 'Типы скорингов выигрышей',
                        'url' => ['/score/trophy-type/index'],
                    ],
                ],
            ],
        ];
    }
}
