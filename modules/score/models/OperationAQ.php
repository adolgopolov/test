<?php

namespace common\modules\score\models;

use common\extensions\ExtAQ;

/**
 * This is the ActiveQuery class for [[OperationAR]].
 *
 * @see OperationAR
 */
class OperationAQ extends ExtAQ
{
    /**
     * {@inheritdoc}
     * @param int|array $match_id номер матча или массив номеров
     * @return OperationAQ
     */
    public function whereMatch($match_id)
    {
        return $this->andWhere(['ent_id' => $match_id,'op_type' => OperationAR::TYPE_MATCH]);
    }

    /**
     * {@inheritdoc}
     * @param int|array $tou_id номер турнира или массив номеров
     * @return OperationAQ
     */
    public function whereTourney($tou_id)
    {
        return $this->andWhere(['ent_id' => $tou_id,'op_type' => OperationAR::TYPE_TOURNEY]);
    }

    /**
     * {@inheritdoc}
     * @param int|array $tou_id номер турнира или массив номеров
     * @return OperationAQ
     */
    public function whereTrophyTourney($tou_id)
    {
        return $this->andWhere(['ent_id' => $tou_id,'op_type' => OperationAR::TYPE_TROPHY]);
    }

    /**
     * {@inheritdoc}
     * @param int|array $tou_id номер турнира или массив номеров
     * @return OperationAQ
     */
    public function whereDynamicTourney($tou_id)
    {
        return $this->andWhere(['ent_id' => $tou_id,'op_type' => OperationAR::TYPE_DYNAMIC]);
    }

    /**
     * @param $tou_id
     * @return OperationAQ
     */
    public function whereExpTourney($tou_id)
    {
        return $this->andWhere(['ent_id' => $tou_id,'op_type' => OperationAR::TYPE_EXP_RANG]);
    }
}
