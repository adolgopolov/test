<?php

namespace common\modules\score\models;

use common\modules\sport\models\SportAR;

/**
 * Class SportCoef
 * @package common\modules\score\models
 */
class SportCoef
{
    const COEF_SOCCER = 1;
    const COEF_HOCKEY = 0.5;
    const COEF_BASKET = 0.5;
    const COEF_DOTA2 = 0.5;
    const COEF_CSGO = 0.5;

    public static function coefBySport($id)
    {
        static $cache = [
            SportAR::SOCCER => self::COEF_SOCCER,
            SportAR::HOCKEY => self::COEF_HOCKEY,
            SportAR::BASKET => self::COEF_BASKET,
            SportAR::DOTA2 => self::COEF_DOTA2,
            SportAR::CSGO => self::COEF_CSGO,
        ];

        return $cache[$id] ?? 1;
    }
}
