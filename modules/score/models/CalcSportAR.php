<?php

namespace common\modules\score\models;

use common\extensions\ExtInfoAR;

/**
 * This is the model class for table "ss_calc_sport".
 *
 * @property int $id
 * @property int $sman_id
 * @property int $match_id
 * @property int $score_kind
 * @property int $calc_id
 * @property int $calc_result
 * @property int $occa_amount
 * @property int $is_deleted
 * @property int $is_fixed
 * @property array $ext_info
 * @property string $updated_at
 */
class CalcSportAR extends ExtInfoAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ss_calc_sport';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sman_id', 'match_id', 'score_kind', 'calc_id', 'calc_result', 'occa_amount', 'is_deleted', 'is_fixed'], 'integer'],
            [['match_id'], 'required'],
            [['ext_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sman_id' => 'Sman ID',
            'match_id' => 'ID Матча',
            'score_kind' => 'Score Kind',
            'calc_id' => 'Calc ID',
            'calc_result' => 'Calc Result',
            'occa_amount' => 'Количество расчетных единиц',
            'is_deleted' => 'Is Deleted',
            'is_fixed' => 'Is Fixed',
            'ext_info' => 'Ext Info',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @return CalcSportAQ
     */
    public static function find()
    {
        return new CalcSportAQ(static::class);
    }

    /**
     * @return void
     */
    public function clearData()
    {
        if ($this->is_fixed) {
            return;
        }

        $this->updateAttributes([
            'sman_id' => 0,
            'score_kind' => 0,
            'calc_id' => 0,
            'calc_result' => 0,
            'occa_amount' => 0,
        ]);
    }

    /**
     * @return bool
     */
    public function isCleared()
    {
        return $this->sman_id === 0 && $this->score_kind === 0 && $this->calc_id === 0;
    }
}
