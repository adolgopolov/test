<?php

namespace common\modules\score\models;

use yii\helpers\ArrayHelper;
use common\components\S3Core;
use common\components\S3DictionaryAR;
use common\extensions\ExtInfoAR;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportPositionAR;
use common\validators\JsonValidator;

/**
 * This is the model class for table "ss_score_kind".
 *
 * @property int $id
 * @property int $sport_id
 * @property string $r_name
 * @property int $r_status
 * @property array $ext_info
 * @property array $scorings массив конфигов скорингов
 * @property SportAR $sport
 * @property array $scoring
 * @property int|string|null $parent
 * @property CalcItemAR[] $calcs
 * @property SportPositionAR[] $positions
 */
class ScoreKindAR extends ExtInfoAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%score_kind}}';
    }

    /**
     * @return array
     */
    protected function jsonFields(): array
    {
        return [
            'scoring',
            'parent',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_name', 'sport_id'], 'required'],
            [['r_status'], 'integer'],
            [['sport_id'], 'integer'],
            [['ext_info'], JsonValidator::class],
            [['r_name'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sport_id' => 'Вид спорта',
            'r_name' => 'Название',
            'r_status' => 'Статус',
            'ext_info' => 'Ext Info',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ScoreKindAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new ScoreKindAQ(static::class);
    }

    /**
     * @param $sport_id
     * @return int
     */
    public static function getDefaultKindId($sport_id)
    {
        $score_kind = self::find()
            ->select('id')
            ->where(['sport_id' => $sport_id])
            ->orderBy(['id' => SORT_ASC])->limit(1)->scalar();

        return $score_kind ?: 0;
    }

    /**
     * @param $sport_id
     * @return array|ScoreKindAR|null
     */
    public static function getDefaultKind($sport_id)
    {
        return self::find()
            ->where(['sport_id' => $sport_id])
            ->orderBy(['id' => SORT_ASC])->limit(1)->one();
    }

    /**
     * два вида массива по кодам и по ключам
     * @param null $replaceCode
     * @return array ['ent_code'=>['config']]
     *  Universal ['ent_code'=>['amplua' => 'points*100']]
     */
    public function getScorings($replaceCode = null)
    {
        //TODO закешировать получение массива правил расчета через свойство по номерам и по кодам ?
        $scorings = $this->joGet('scoring');
        $scorings = $scorings ?: [];

        if ($replaceCode) {
            $scorings = $this->replaceCode($scorings);
        }

        return $scorings;
    }

    /**
     * @param $array
     * @param null $replaceCode
     */
    public function setScorings($array, $replaceCode = null)
    {
        if ($replaceCode) {
            $array = $this->replaceCode($array);
        }

        $this->joSet(['scoring' => $array]);
    }

    /**
     * @brief заменить коды на ID
     * @brief заменить коды на ID массиве
     * @param array $array
     * @return array
     */
    protected function replaceCode($array)
    {
        $newArray = [];

        foreach ($array as $key => $value) {
            $obj = S3DictionaryAR::findByCode($key);
            $newKey = $obj->id ?? 0;
            if (is_array($value)) {
                $newArray[$newKey] = $this->replaceCode($value);
            } else {
                $newArray[$newKey] = $value;
            }
        }

        return $newArray;
    }

    /**
     * получить массив для расчета скоринга
     * ['amplua' => 'points*100']
     * @param $scoreId
     * @return null
     */
    public function getScoringByScoreId($scoreId)
    {
        return $this->scorings[$scoreId] ?: null;
    }

    /**
     * @brief возвращает количество очков по амплуа, или количество очков если есть любая позиция или 0
     * @param int $positionId
     * @param int $calcId
     * @return int
     */
    public function pointsFor($positionId, $calcId)
    {
        static $any;

        if (empty($any)) {
            $any = SportPositionAR::findByCode('any.position')->id;
        }

        return $this->scorings[$calcId][$positionId] ?? $this->scorings[$calcId][$any] ?? 0;
    }

    /**
     * @param $sportId
     * @return array
     */
    public static function getScoresList($sportId)
    {
        $kinds = self::find()
            ->whereSport($sportId)
            ->andWhere(['r_status' => S3Core::STATUS_LIVE])
            ->all();

        return ArrayHelper::map($kinds, 'id', 'r_name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(SportAR::class, ['id' => 'sport_id']);
    }

    /**
     * @return array
     */
    public static function getStatusesList()
    {
        return [
            S3Core::STATUS_DRAFT => 'draft',
            S3Core::STATUS_LIVE => 'live',
            S3Core::STATUS_CLOSED => 'closed',
        ];
    }

    /**
     * @return CalcItemAR[]
     */
    public function getCalcs()
    // TODO метод приведен к Yii way - getCalcs() должен возращать ActiveQuery
    {
        return CalcItemAR::find()
            ->andWhere([
                'OR',
                ['ent_value' => $this->sport_id],

                [ 'and',
                    ['ent_value' => ScoreKindAQ::getAnySport($this->sport_id)],
                    ['>=','id',70],
                ],
            ])
            ->indexBy('id')
            ->all();
    }

    /**
     * @return SportPositionAR[]
     */
    public function getPositions()
    // TODO 1. должно быть перенесено в SportPositionAQ,
    // 2. либо метод приведен к Yii way - getPositions() должен возращать ActiveQuery
    {
        return SportPositionAR::find()
            ->andWhere([
                'OR',
                ['ent_value' => $this->sport_id],
                [ 'and',
                    ['ent_value' => ScoreKindAQ::getAnySport($this->sport_id)],
                    ['>=','id',50],
                ],
                ['id' => 1]
            ])
            ->indexBy('id')
            ->all();
    }
}
