<?php

namespace common\modules\score\models;

use common\extensions\ExtInfoAR;
use common\validators\JsonValidator;

/**
 * This is the model class for table "ss_operation".
 *
 * @property int $id
 * @property int $r_status
 * @property int $op_type
 * @property string $r_name
 * @property int $ent_id
 * @property string $updated_at
 * @property array $ext_info
 */
class OperationAR extends ExtInfoAR
{
    const TYPE_MATCH = 1;
    const TYPE_TOURNEY = 2;
    const TYPE_TROPHY = 3;
    const TYPE_DYNAMIC = 4;
    const TYPE_EXP_RANG = 5;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%operation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge($this->strongRules([
            [['r_status', 'op_type', 'ent_id'], 'integer'],
            [['op_type', 'ent_id'], 'required'],
            ['updated_at', 'safe'],
            [['r_name'], 'string', 'max' => 250]
        ],[
            [['ent_id', 'op_type'], 'unique', 'targetAttribute' => ['ent_id', 'op_type']]
        ]), $this->extInfoRules());
    }

    /**
     * @return array
     */
    protected function extInfoRules()
    {
        return [
            ['ext_info', JsonValidator::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_status' => 'R Status',
            'op_type' => 'Op Type',
            'r_name' => 'R Name',
            'ent_id' => 'Ent ID',
            'updated_at' => 'Updated At',
            'ext_info' => 'Ext Info',
        ];
    }

    /**
     * {@inheritdoc}
     * @return OperationAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new OperationAQ(get_called_class());
    }
}
