<?php

namespace common\modules\score\models;

use common\components\S3DictionaryAR;
use common\components\S3Kind;
use common\validators\JsonValidator;

class CalcItemAR extends S3DictionaryAR
{
    /**
     * {@inheritdoc}
     */
    public static function kind()
    {
        return S3Kind::KIND_CALC;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'ent_kind'  => 'Kind',
            'ent_code'  => 'Код',
            'ent_value' => 'Значение',
            'r_name'    => 'Название'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [['ent_code', 'r_name', 'ent_value'], 'string'],
            ['id', 'unique', 'targetAttribute' => ['id', 'ent_kind']],
            [['ext_info'], JsonValidator::class],
        ];
    }
}
