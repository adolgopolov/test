<?php

namespace common\modules\score\models;

use Yii;

/**
 * This is the model class for table "ss_dynamic_price".
 *
 * @property int $tou_id
 * @property int $sman_id
 * @property int $sman_pow сила спортсмена умноженная на 100
 * @property int $sman_price стоимость спортсмена
 * @property array $ext_info
 */
class DynPriceAR extends \common\extensions\ExtInfoAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%dynamic_price}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return $this->strongRules([
            [['tou_id', 'sman_id'], 'required'],
            [['tou_id', 'sman_id', 'sman_pow', 'sman_price'], 'integer'],
            [['ext_info'], 'safe'],
            ],[
            [['tou_id', 'sman_id'], 'unique', 'targetAttribute' => ['tou_id', 'sman_id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tou_id' => 'Tou ID',
            'sman_id' => 'Sman ID',
            'sman_pow' => 'Sman Pow',
            'sman_price' => 'Sman Price',
            'ext_info' => 'Ext Info',
        ];
    }

    /**
     * {@inheritdoc}
     * @return DynPriceAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new DynPriceAQ(get_called_class());
    }
}
