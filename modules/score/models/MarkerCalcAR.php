<?php

namespace common\modules\score\models;

use common\components\{S3DictionaryAR, S3Kind};

/**
 *
 */
class MarkerCalcAR extends S3DictionaryAR
{
    public $params;
    /**
     * виды расчетов для одного события может быть несколько
     * @return int|null
     */
    public static function kind()
    {
        return S3Kind::KIND_CALC;
    }

    public static function getAll($sport_id)
    {
        return self::find()->whereSport($sport_id)->all();
    }

}
