<?php

namespace common\modules\score\models;

use common\extensions\ExtAQ;

/**
 * This is the ActiveQuery class for [[CalcSportAR]].
 *
 * @see CalcSportAR
 */
class CalcSportAQ extends ExtAQ
{
    public function whereMatch($match_id)
    {
        return $this->andWhere(['match_id' => $match_id]);
    }

    public function whereKind($kind_id)
    {
        return $this->andWhere(['score_kind' => $kind_id]);
    }
}
