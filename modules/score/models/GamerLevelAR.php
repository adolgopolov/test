<?php

namespace common\modules\score\models;

use common\components\{S3DictionaryAR, S3Kind};

class GamerLevelAR extends S3DictionaryAR
{
    public static function kind()
    {
        return S3Kind::KIND_GAMER_LEVEL;
    }

    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'ent_kind'  => 'Kind',
            'ent_code'  => 'Код',
            'ent_value' => 'Значение',
            'r_name'    => 'Название'
        ];
    }

    public function rules()
    {
        return [
            ['id', 'integer'],
            [['ent_code', 'r_name', 'ent_value'], 'string'],
            ['id', 'unique', 'targetAttribute' => ['id', 'ent_kind']]
        ];
    }
}
