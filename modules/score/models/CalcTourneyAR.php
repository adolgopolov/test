<?php

namespace common\modules\score\models;

use common\extensions\ExtInfoAR;

/**
 * This is the model class for table "ss_calc_tourney".
 *
 * @property int $id
 * @property int $sman_id
 * @property int $tou_id
 * @property int $calc_id
 * @property int $calc_amount
 * @property int $calc_points points * 100
 * @property int $is_deleted
 * @property array $ext_info
 */
class CalcTourneyAR extends ExtInfoAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%calc_tourney}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sman_id', 'tou_id', 'calc_id', 'calc_amount', 'calc_points', 'is_deleted'], 'integer'],
            [['tou_id'], 'required'],
            [['ext_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sman_id' => 'Sman ID',
            'tou_id' => 'Tou ID',
            'calc_id' => 'Calc ID',
            'calc_amount' => 'Calc Amount',
            'calc_points' => 'Calc Points',
            'is_deleted' => 'Is Deleted',
            'ext_info' => 'Ext Info',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CalcTourneyAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new CalcTourneyAQ(get_called_class());
    }

    public function clearData()
    {

        $this->updateAttributes([
            'sman_id' => 0,
            'calc_id' => 0,
            'calc_amount' => 0,
            'calc_points' => 0,
            'ext_info' => NULL,
        ]);
    }

    public function isCleared()
    {
        return $this->sman_id == 0 && $this->calc_id == 0 && $this->calc_points == 0;
    }

}
