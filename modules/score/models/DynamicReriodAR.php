<?php

namespace common\modules\score\models;

use common\modules\sport\models\SportPositionAR;
use common\components\{S3DictionaryAR, S3Kind};

/**
 * @property-read int $ratio
 * @property-read int $matchesCount
 */
class DynamicReriodAR extends S3DictionaryAR
{
    public $params;

    /**
     * периоды для расчета динамической стоимости
     * @return int|null
     */
    static function kind()
    {
        return S3Kind::DYNAMIC_PERIODS;
    }

    public static function getAll()
    {
        return self::find()->andWhere(['>', 'id', 0])->all();
    }

    public function getRatio()
    {
        return $this->joGet('w_ratio');
    }

    public function getMatchesCount()
    {
        return $this->joGet('count_match');
    }
}
