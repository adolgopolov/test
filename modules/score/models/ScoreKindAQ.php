<?php

namespace common\modules\score\models;

use common\modules\sport\models\SportAR;

/**
 * This is the ActiveQuery class for [[ScoreKindAR]].
 *
 * @see ScoreKindAR
 */
class ScoreKindAQ extends \common\extensions\ExtAQ
{
    /**
     * @param $sportId
     * @param null $mode
     * @return \common\extensions\ExtAQ|ScoreKindAQ
     */
    public function whereSport($sportId, $mode = null)
    {
        return $sportId ? $this->andWhere([
            'OR',
            ['sport_id' => $sportId],
            ['sport_id' => self::getAnySport($sportId)],
        ]) : $this;
    }

    /**
     * @param int $sportId
     * @return int
     */
    public static function getAnySport($sportId)
    {
        return $sportId < 100 ? SportAR::ANY_SPORT : SportAR::CYBER_SPORT;
    }
}
