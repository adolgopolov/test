<?php

namespace common\modules\score\models;

/**
 * This is the ActiveQuery class for [[DynPriceAR]].
 *
 * @see DynPriceAR
 */
class DynPriceAQ extends \common\extensions\ExtAQ
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return DynPriceAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return DynPriceAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
