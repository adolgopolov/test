<?php

namespace common\modules\score\calcs;

use common\components\ErrorsTrait;
use common\modules\game\models\SportMatchAR;
use common\modules\score\models\CalcSportAR;
use common\modules\score\models\ScoreKindAR;
use common\modules\score\models\MarkerCalcAR;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportOccaAR;
use yii\base\Exception;
use yii\db\ActiveRecordInterface;

/**
 * Description of AbstractCalc
 *
 * @author Skynin <sohay_ua@yahoo.com>
 * created: 19-Apr-2019
 */
abstract class BaseSportCalc
{
    use ErrorsTrait;
    protected $calcId;
    protected $scoreKind;
    protected $occaAR;
    protected $isFinalize;

    /**
     * BaseSportCalc constructor.
     * @param ScoreKindAR $scoreKind
     * @param SportOccaAR $occaAR
     * @param int $scoreId
     */
    public function __construct(ScoreKindAR $scoreKind, SportOccaAR $occaAR, int $scoreId)
    {
        $this->calcId = $scoreId;
        $this->scoreKind = $scoreKind;
        $this->occaAR = $occaAR;
    }

    /**
     * В $calcResultAR записывается результат расчета (скоринга)
     * @param CalcSportAR $calcResultAR
     * @return CalcSportAR|null
     * @throws Exception
     */
    public function calc(CalcSportAR $calcResultAR)
    {
        if ($this->isFinalize) {
            throw new Exception('Повторный расчет производить запрещено');
        }
        $this->isFinalize = true;
        return $calcResultAR;
    }

    public function getCalcId()
    {
        return $this->calcId;
    }

    /**
     * @param SmanAR $sman
     * @param SportMatchAR $match
     * @return int |null
     */
    protected function positionFor(SmanAR $sman, SportMatchAR $match)
    {
        // TODO подготовить провериь кеш для спортсменов матча?
        $member = $match->findPositionOfMan($sman->id);
        // TODO ищем в $memberCache, перенести выборку позиций в командах в кеш
        /*сделать массив [
                       'guest' => ['sman_id' => $position_id],
                       'owner' => ['sman_id' => $position_id],
                     ]*/
        return $member->pos_id ?: null;
    }
}
