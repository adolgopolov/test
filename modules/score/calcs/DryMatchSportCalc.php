<?php

namespace common\modules\score\calcs;

use common\modules\game\models\SportMatchAR;
use common\modules\score\models\CalcSportAR;
use common\modules\sport\models\SmanAR;
use yii\base\Exception;


class DryMatchSportCalc extends BaseSportCalc
{

    public function calc(CalcSportAR $calcResultAR)
    {
        $amountOcca = $this->occaAR->occa_amount;

        $sman = $this->occaAR->sman;
        $match = $this->occaAR->match;
        $smanPosition = $this->positionFor($sman, $match);

        // scoreObj для амлуа спортсмена по спортивному факту
        $occaPoints = $this->scoreKind->pointsFor($smanPosition, $this->calcId);
        if ($occaPoints === 0) {
            return null;
        }

        $result = ($amountOcca ?? 1) * $occaPoints;

        $calcResultAR->calc_result = $result;
        $calcResultAR->occa_amount = $amountOcca;
        $calcResultAR->sman_id = $sman->id;
        $calcResultAR->score_kind = $this->scoreKind->id;
        $calcResultAR->match_id = $this->occaAR->match_id;
        $calcResultAR->calc_id = $this->calcId;

        return parent::calc($calcResultAR);
    }

    public function isDryMatch()
    {
        $sman = $this->occaAR->sman;
        $match = $this->occaAR->match;
        if ($match->isOwnerTeamSman($sman->id)) {
            return (($match->total_owner > $match->total_guest) && $match->total_guest == 0);
        } else {
            return (($match->total_owner < $match->total_guest) && $match->total_owner == 0);
        }
    }
}
