<?php

namespace common\modules\score\calcs;

use common\modules\game\models\SportMatchAR;
use common\modules\score\models\CalcSportAR;
use common\modules\sport\models\SmanAR;
use yii\base\Exception;

/**
 * расчте урона по строениям,
 * событие-факт "урон по строениям"
 * начисление очков должно происходить за каждые полные 100 очков урона
 *
 * @author adolgopolov
 * created: 15-Jan-2020
 */
class DotaDamageBuildingCalc extends BaseSportCalc
{
    /**
     * @param CalcSportAR $calcResultAR
     * @return CalcSportAR|null
     * @throws Exception
     */
    public function calc(CalcSportAR $calcResultAR)
    {

        $amountOcca = (int)floor($this->occaAR->occa_amount / 1000);
        // если не хватает очков урона для расчета(менее 100), то не считаем
        if ($amountOcca === 0) {
            return null;
        }
        $sman = $this->occaAR->sman;
        $match = $this->occaAR->match;
        $smanPosition = $this->positionFor($sman, $match);

        // scoreObj для амлуа спортсмена по спортивному факту
        $occaPoints = $this->scoreKind->pointsFor($smanPosition, $this->calcId);
        // не найдено очков для расчета
        if ($occaPoints === 0) {
            return null;
        }

        $result = ($amountOcca ?? 1) * $occaPoints;

        $calcResultAR->calc_result = $result;
        $calcResultAR->occa_amount = $this->occaAR->occa_amount;
        $calcResultAR->sman_id = $sman->id;
        $calcResultAR->score_kind = $this->scoreKind->id;
        $calcResultAR->match_id = $this->occaAR->match_id;
        $calcResultAR->calc_id = $this->calcId;

        return parent::calc($calcResultAR);
    }
}
