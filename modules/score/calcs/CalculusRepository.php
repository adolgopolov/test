<?php

namespace common\modules\score\calcs;

use common\components\ErrorsTrait;
use common\components\S3DictionaryAR;
use common\modules\score\models\ScoreKindAR;
use common\modules\sport\models\SportFactAR;
use common\modules\sport\models\SportOccaAR;
use Yii;
use yii\db\Exception;

/**
 * Description of CalculusRepository
 *
 * @author Skynin <sohay_ua@yahoo.com>
 * created: 19-Apr-2019
 */
class CalculusRepository
{
    use ErrorsTrait;
    const PREFIX_CALC = 'calc.sport.';
    private static $obj;
    protected $calcs;
    protected $complexityCalcs;
    protected $occa;
    protected $scoreKind;
    protected $match;

    /**
     * классы сложных расчетов определяются по dictionary-коду расчета
     * @var array [codeCalc=>ClassNameCalc]
     */
    private static $codesCalc = [

        'dota2.damage_buildings' => '\common\modules\score\calcs\DotaDamageBuildingCalc',
    ];

    public static function getInstance(): CalculusRepository
    {
        if (self::$obj) {
            return self::$obj;
        }
        return self::$obj = new CalculusRepository();
    }

    /**
     * @brief выбирается класс в зависимости от названия расчета
     *        данные для расчета формируются из связных данных события или матча
     *        расчет может использовать данные текущего события либо данные предыдущих расчетов
     * @param SportOccaAR $occa
     * @param ScoreKindAR $scoreKind
     * @return BaseSportCalc | null
     */
    //todo $scoreKind подставлять из свойства
    public function findForOcca($occa): ?BaseSportCalc
    {
        // сопоставление события и маркера расчета, если маркера нету в расчете, то не считать
        $calcId = $this->detectSimpleOcca($occa->fact->ent_code);
        if (!$calcId) return null;

        if (array_key_exists($occa->fact->ent_code, self::$codesCalc)) {
            $calcClass = self::$codesCalc[$occa->fact->ent_code];
            $calc = new $calcClass($this->scoreKind, $occa, $calcId);
        } else {
            $calc = new UniversalSportCalc($this->scoreKind, $occa, $calcId);
        }
        return $calc;
    }

    /**
     * реализовать сбор сложных событий в массив и возвращать по запросу
     * @param $match
     * @param $scoreKind
     * @param $flagFinisMatch
     * @return array
     */
    public function getComplexityCalcs()
    {

        if (!empty($this->complexityCalcs)) {
            return $this->complexityCalcs;
        } else {
            return [];
        }

    }

    public function initComplexityCalcsFor($match, ScoreKindAR $scoreKind)
    {
        $this->calcs = [];
        $this->occa = [];
        $this->complexityCalcs = [];
        $this->match = $match;
        $this->scoreKind = $scoreKind;
    }

    public function prepareComplexityCalcs(SportOccaAR $occa, $calc)
    {
        $this->calcs[] = $calc;
        $this->occa[] = $occa;
        if ($occa->fact_id == SportFactAR::FACT_MATCH_PRESENCE) {
            $fakeOcca = new SportOccaAR([
                'match_id' => $this->match->id,
                'occa_amount' => 1,
                'sman_id' => $occa->sman_id,
            ]);
            // todo сделать универсальный детектор для сухих матчей футбола хокея (вратать гостей 2:0)
            if ($calcId = $this->detectSimpleOcca('soccer.dry_match')) {

                $dryCalc = new DryMatchSportCalc($this->scoreKind, $fakeOcca, $calcId);
                if ($dryCalc->isDryMatch()) {
                    $this->complexityCalcs[] = $dryCalc;
                }
            }
            // детектор победы или поражения (гости победили 2:0)
            if ($calcId = $this->detectSimpleOcca('any.win')) {

                $winCalc = new WinLoseSportCalc($this->scoreKind, $fakeOcca, $calcId);
                if ($winCalc->isWin()) {
                    $this->complexityCalcs[] = $winCalc;
                }
            }
            if ($calcId = $this->detectSimpleOcca('any.lose')) {

                $lossCalc = new WinLoseSportCalc($this->scoreKind, $fakeOcca, $calcId);
                if ($lossCalc->isLose()) {
                    $this->complexityCalcs[] = $lossCalc;
                }
            }
        }
    }

    /**
     * проверка наличия скоринга по коду события cyber.murder == 'calc.sport.'cyber.murder
     * @param string $factCode
     * @return int | false
     */
    protected function detectSimpleOcca($factCode)
    {
        $obj = S3DictionaryAR::findByCode(self::PREFIX_CALC . $factCode);

        if ($obj && isset($this->scoreKind->scorings[$obj->id])) {
            return $obj->id;
        }
        return false;
    }

}
