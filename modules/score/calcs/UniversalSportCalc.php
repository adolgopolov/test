<?php

namespace common\modules\score\calcs;

use common\modules\game\models\SportMatchAR;
use common\modules\score\models\CalcSportAR;
use common\modules\sport\models\SmanAR;
use yii\base\Exception;

/**
 *
 * @author Skynin <sohay_ua@yahoo.com>
 * created: 19-Apr-2019
 */
class UniversalSportCalc extends BaseSportCalc
{

    /**
     * @param CalcSportAR $calcResultAR
     * @return CalcSportAR|null
     * @throws Exception
     */
    public function calc(CalcSportAR $calcResultAR)
    {
        $amountOcca = $this->occaAR->occa_amount;

        $sman = $this->occaAR->sman;
        $match = $this->occaAR->match;
        $smanPosition = $this->positionFor($sman, $match);

        // scoreObj для амлуа спортсмена по спортивному факту
        $occaPoints = $this->scoreKind->pointsFor($smanPosition, $this->calcId);
        // не найдено очков для расчета
        if ($occaPoints === 0) {
            return null;
        }

        $result = ($amountOcca ?? 1) * $occaPoints;

        $calcResultAR->calc_result = $result;
        $calcResultAR->occa_amount = $amountOcca;
        $calcResultAR->sman_id = $sman->id;
        $calcResultAR->score_kind = $this->scoreKind->id;
        $calcResultAR->match_id = $this->occaAR->match_id;
        $calcResultAR->calc_id = $this->calcId;


        return parent::calc($calcResultAR);
    }
}
