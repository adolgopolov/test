<?php

namespace common\modules\score\operations;

use Yii;
use common\helpers\ArrayHelper;
use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\user\models\UserAR;
use common\modules\score\rating\RankAR;

/**
 * Class ExperienceScoring
 * @package common\modules\score\operations
 */
class ExperienceScoring extends BaseScoring
{
    public $users = [];

    public function calculate()
    {
        parent::calculate();

        $tourney = GameTourneyAR::findOne($this->ent_id);

        if (empty($tourney)) {
            $this->addError ('ent_id', "Tourney $this->ent_id not found");
            return false;
        }

        $this->users = [];

        /*
         * TODO
         * получить по турниру, добавить из 'exp.prev.'
         *
         * пока - просто по турниру
         */
        $touUsers = $tourney->getUsers()->forUpdate()->indexBy('id')->all();

        // Суммируем exp_points пользователей в $totalExp
        $preparedUserIds = implode(',', array_map('intval', array_keys($touUsers)));

        $tblLineup = GameLineupAR::tableName();

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
        SELECT SQL_NO_CACHE user_id, SUM(exp_points) exp_total
        FROM $tblLineup
        WHERE user_id in ($preparedUserIds)
        AND exp_points > 0 AND is_actual_award = 1 AND isnt_reg = 0
        GROUP BY user_id LOCK IN SHARE MODE");

        $totalExp = ArrayHelper::index($command->queryAll(), 'user_id');

        $userFields = ['exp_points', 'rank_id', 'rank_progress', 'elite_id'];

        foreach ($touUsers as $user) {
            /** @var UserAR $user */

            $infoUser = $user->getAttributes($userFields);
            $this->joSet('exp.prev.' . $user->id, $infoUser);

            $userExpTotal = $totalExp[$user->id]['exp_total'] ?? 0;
            $this->joSet('exp.total.' . $user->id, $userExpTotal);

            if ($user->exp_points >= $userExpTotal) continue;

            $user->exp_points = $userExpTotal;

            /* расчет по рейтингу-элите
             * см описание в common\modules\score\rating\RankAR::calcRank
             */
            $user->setAttributes(
                    RankAR::calcRank($user->exp_points, $infoUser)
                );

            $this->joSet('exp.done.' . $user->id, $user->getAttributes($userFields));

            $user->joSet('exp_op', $this->id);

            $this->users[] = $user;
        }
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        // реальная запись в БД
        foreach ($this->users as $user) {
            if ($user && !$user->save()) {
                $this->addErrors($user->errors);
            }
        }

        $save = !$this->hasErrors() && parent::save($runValidation, $attributeNames);

        if ($save) {
        // TODO sender для обновления рангов
        // сравнить 'exp.prev.' и 'exp.done.' и отправить только значимые изменения
        //
        // И
        // https://conflu.kitfan.ru/pages/viewpage.action?pageId=22741006
        // После достижения последнего ранга, пользователь получает 5 паков по всем видам спорта (если они у нас будут),
        // получает ачивку и возле его фотографии в рейтинге появляется отображение
        // Короны, Кубка или чего-нибудь еще, после чего все начинается заново,
        // если пользователь проходит дважды всё, он получает 2 Корону, Кубок.
        }

        return $save;
    }

    /**
     * @param $tourney
     * @return array|ExperienceScoring|\yii\db\ActiveRecord|null
     */
    public static function generateScoring($tourney)
    {
        $touId = is_object($tourney) ? $tourney->id : $tourney;

        if (($result = self::find()->whereExpTourney($touId)->forUpdate()->one()) === null) {
            $result = new self(['op_type' => self::TYPE_EXP_RANG, 'ent_id' => $touId]);
            $result->save();
        }

        return $result;
    }
}
