<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 27.06.19 15:21
 */

namespace common\modules\score\operations;


use common\components\S3DictionaryAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\score\models\DynamicReriodAR;
use common\modules\score\models\DynPriceAR;
use common\modules\score\models\ScoreKindAR;
use common\modules\score\TourneySmanProvider;
use common\modules\sount\models\SouOccaAR;
use common\modules\sount\models\SouSmanAR;
use yii\helpers\ArrayHelper;

/**
 * Description of TourneyScoring
 *
 * @author Andrey Dolgopolov
 * created: 01-july-2019
 *
 * @property GameTourneyAR $tourney
 */
class DynamicScoring extends BaseScoring
{
    protected $arrayOfOccaByMatch = [];

    protected $arrayOfSmanPointsByMatch = [];

    protected $poolRecords;
    /** @var DynPriceAR[] */
    protected $newResults;

    protected $repository;

    const PREFIX_CALC = 'calc.sport.';


    public function calculate()
    {
        parent::calculate();
        //todo  для игрового турнира выбираются все спортсмены
        $this->getSmansOccaByMatch();
        //print_r($this->getAttributes());die;

        $this->calcPointsByMatch();

        $this->calcSmenPower();

        $this->calcDynamicPrice();

        $this->clearPrevARs();
    }

    /**
     * @brief расчет события по скорингу учитывая тип скоринга
     * @param $occa
     * @param $scoreKind
     * @return integer
     */
    protected function calculateOcca(SouOccaAR $occa, ScoreKindAR $scoreKind)
    {
        $queue = [];
        //TODO репозиторий инициализируется $occa,$scoreKind типом расчета
        $amountOcca = $occa->fact_value;

        $sman = $occa->sman;
        $match = $occa->match;
        $calcId = self::detectSimpleOcca($occa, $scoreKind);
        if(!$calcId) {
            //для событий у которых нету скоринга возвращаем 0 очков
            return 0;
        }
        $smanPosition = $this->positionFor($sman, $match);
        // scoreObj для амлуа спортсмена по спортивному факту
        if ($smanPosition) {
            $occaPoints = $scoreKind->pointsFor($smanPosition, $calcId);
            return ($amountOcca ?? 1) * $occaPoints;
        }
        return 0;
    }

    protected static function detectSimpleOcca(SouOccaAR $occa, ScoreKindAR $scoreKind)
    {
        if ($occa->fact->innerDict) {

            $obj = S3DictionaryAR::findByCode(self::PREFIX_CALC . $occa->fact->innerDict->ent_code);

            if (isset($scoreKind->scorings[$obj->id])) {
                return $obj->id;
            }
        }

        return false;
    }

    private function positionFor(SouSmanAR $sman)
    {
        // TODO подготовить провериь кеш для спортсменов матча?
        // FIXME не надо полготавливать кеш
        // $member = SportTeamAR::find()->whereTourney($this->tourney)->andWhere(['sman_id'=>$sman->sman_our_id])
        // и можно в SportTeamAQ упрятать кеширование по whereTourney:
        // при первом запросе - получаем отдельно по турниру
        // и накладываем все остальные условия на полученные по турниру
        $member = $this->tourney->findPositionOfMan($sman->sman_our_id);
        // TODO ищем в $memberCache, перенести выборку позиций в командах в кеш
        return $member->pos_id ?? null;
    }

    /**
     * @param int | GameTourneyAR $tourney
     * @return DynamicScoring
     */
    public static function generateScoring($tourney)
    {
        $touId = is_object($tourney) ? $tourney->id : $tourney;
        if (($result = self::find()->whereDynamicTourney($touId)->one()) === null) {
            $result = new self(['op_type' => self::TYPE_DYNAMIC, 'ent_id' => $touId]);
            //TODO блокировать запись
            $result->save();
        }
        return $result;
    }

    protected function getSmansOccaByMatch()
    {
        return $this->arrayOfOccaByMatch = TourneySmanProvider::listSmanMatchPoints($this->ent_id);
    }

    /**
     * return GameTourneyAQ
     */
    public function getTourney()
    {
        return $this->hasOne(GameTourneyAR::class, ['id' => 'ent_id']);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->newResults) {
            foreach ($this->newResults as $eachScoring) {
                if ($eachScoring)
                    if(!$eachScoring->save())$this->addErrors($eachScoring->errors);
                $eachScoring->errors;
            }
        }
        if($this->hasErrors()) {
            return false;
        }

        return parent::save($runValidation = true, $attributeNames = null);
    }

    public function getArFor($smanId)
    {
        if (empty($this->poolRecords)) {
            $this->poolRecords = ArrayHelper::index(DynPriceAR::findAll(['tou_id' => $this->tourney->id]), 'sman_id');
        }
        if (array_key_exists($smanId, $this->poolRecords)) {
            $dynamicPriceAR = $this->poolRecords[$smanId];
            unset($this->poolRecords[$smanId]);
            return $dynamicPriceAR;
        }
        $dynamicPriceAR = new DynPriceAR([
            'tou_id' => $this->tourney->id,
            'sman_id' => $smanId,
        ]);
        return $dynamicPriceAR;
    }

    /**
     * Очищает неиспользованные записи
     */
    protected function clearPrevARs(): void
    {
        /** @var DynPriceAR $freeAr */
        if ($this->poolRecords) {
            foreach ($this->poolRecords as $freeAr) {
                $freeAr->delete();
            }
        }
    }

    /**
     *  формирование массива игроков с очками поматчево
     *
     * @return array
     */
    protected function calcPointsByMatch()
    {
        $scoreKind = $this->tourney->scoreKind;
        foreach ($this->arrayOfOccaByMatch as $SmanMatchOcca) {
            if ($SmanMatchOcca['sman_id']) {
                $occa = new SouOccaAR([
                    'match_sou_id' => $SmanMatchOcca['match_sou_id'],
                    'sman_sou_id' => $SmanMatchOcca['sman_sou_id'],
                    'fact_sou_id' => $SmanMatchOcca['fact_sou_id'],
                    'fact_value' => $SmanMatchOcca['occa_val'],
                ]);
                $result = $this->calculateOcca($occa, $scoreKind);
                // перебор массива с пересчетом в спортсмен - матч - количество очков
                if (isset($this->arrayOfSmanPointsByMatch[$SmanMatchOcca['sman_id']][$SmanMatchOcca['match_sou_id']])) {
                    $userPoint = $this->arrayOfSmanPointsByMatch[$SmanMatchOcca['sman_id']][$SmanMatchOcca['match_sou_id']];
                    //todo
                    $newResult = $userPoint + $result ?? 0;
                    $this->arrayOfSmanPointsByMatch[$SmanMatchOcca['sman_id']][$SmanMatchOcca['match_sou_id']] = $newResult;
                } else {
                    $newResult = $result ?? 0;
                    $this->arrayOfSmanPointsByMatch[$SmanMatchOcca['sman_id']][$SmanMatchOcca['match_sou_id']] = $newResult;
                }
            }
        }
        return $this->arrayOfOccaByMatch;
    }

    /**
     * расчет средней силы спортсменов для последних 50 матчей
     */
    protected function calcSmenPower()
    {
        $periods = DynamicReriodAR::getAll();
        foreach ($this->arrayOfSmanPointsByMatch as $userId => $userMatches) {
            // todo записать всех пользователей турнира с их силой в таблицу спортсмены турнира
            krsort($userMatches);
            $resultPow = 0;
            foreach ($periods as $period) {
                $arr = array_slice($userMatches, 0, $period->matchesCount);
                $userPoints = array_sum($arr) / count($arr);
                $resultPow += $period->ratio * $userPoints / 100;
            }
            $arDynPrice = $this->getArFor($userId);
            $arDynPrice->sman_pow = (int)round($resultPow);
            $this->newResults[] = $arDynPrice;
        }
    }

    /**
     * расчет динамической стоимости спортсменов в турнире
     */
    protected function calcDynamicPrice()
    {
        $deltaN = 66.67;
        if(empty($this->newResults)){
            //todo обработать ошибку пустых списков спортсменов
            return ;
        }
        $minPow = min(ArrayHelper::getColumn($this->newResults, 'sman_pow'));
        foreach ($this->newResults as &$dynPriceRec) {
            $dynPriceRec->joPower = $dynPriceRec->sman_pow + abs($minPow) + 100;
        }

        $powers = ArrayHelper::getColumn($this->newResults, 'joPower');
        $maxPow = max($powers) / 100;
        $avgPow = array_sum($powers) / (count($powers)*100);
        $delta50 = ($maxPow - $avgPow) ;
        $delta = $delta50 / (50 / ($deltaN - 50));
        $sman67Pow = $avgPow + $delta;
        $smenCountLineup = 6; //TODO перенести из состава 2 GameLayoutAR
        $budget = $this->tourney->lup_budget;

        $lineupLimitPow = $sman67Pow * $smenCountLineup;
        $powerKoef = $budget / ($lineupLimitPow);
        $powerKoef = round($powerKoef,0);

        foreach ($this->newResults as &$dynPriceRec) {
            $dynPriceRec->sman_price = (int) round($dynPriceRec->joPower * $powerKoef / 100,0);
        }


    }

}