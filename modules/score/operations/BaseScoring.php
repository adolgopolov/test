<?php

namespace common\modules\score\operations;

use common\modules\score\models\OperationAR;

/**
 * Class BaseScoring
 * @package common\modules\score\operations
 */
class BaseScoring extends OperationAR
{
    public function calculate()
    {
        $nameCount = 'run';
        $this->joSet($nameCount, $this->joGet($nameCount, 0) + 1);
    }
}
