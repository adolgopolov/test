<?php

namespace common\modules\score\operations;

use common\modules\game\models\SportMatchAR;
use common\modules\score\calcs\CalculusRepository;
use common\modules\score\models\CalcSportAR;
use common\modules\score\models\ScoreKindAR;
use common\modules\score\ScoreModule;
use common\modules\sport\models\SportFactAR;
use common\modules\sport\models\SportOccaAR;
use yii\helpers\ArrayHelper;

/**
 * Description of MatchScoring
 *
 * @author Skynin <sohay_ua@yahoo.com>
 * created: 19-Apr-2019
 *
 * @property SportMatchAR $match
 */
class MatchScoring extends BaseScoring
// наследуется от абстрактного скоринга у которого есть calculate()
// а абстрактный скоринг наследуется от операции
{

    protected $poolCalcSports = [];
    protected $newResults = [];

    protected $occasions;
    /** @var CalculusRepository */
    protected $repository;


    public function calculate()
    {
        parent::calculate();

        // выбрать турниры в которых есть этот матч взять все уникальные типы скорингов
        $scoreKinds = ScoreModule::getScoreKinds($this->ent_id)->all();

        foreach ($scoreKinds as $scoreKind) {
            $this->calculateMatch($this->match, $scoreKind);
        }

        $this->clearPrevARs(); // тут или в save
        //TODO вернуть успешность или ошибку
    }

    /**
     *
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        // TODO реальная запись в БД
        foreach ($this->newResults as $eachScoring) {
            if (!$eachScoring->save()) {
                $this->addErrors($eachScoring->getErrors());
                return false;
            }
        }
        return parent::save($runValidation = true, $attributeNames = null);
    }

    /**
     * @brief расчет событий матча по набору правил
     * @param $match
     * @param ScoreKindAR $scoreKind
     * @throws \yii\db\Exception
     */
    protected function calculateMatch($match, ScoreKindAR $scoreKind)
    {
        static $occaCache = [];
        $this->repository = CalculusRepository::getInstance();
        if (empty($occaCache[$match->id])) {
            $occaCache[$match->id] = SportOccaAR::find()->whereMatch($match->id)->onlyCalcOcca()->forUpdate()->all();
        }

        $occasions = $occaCache[$match->id];
        $this->repository->initComplexityCalcsFor($match, $scoreKind);
        // простые события  из буфера
        /** @var SportOccaAR $eachOcca */
        foreach ($occasions as $eachOcca) {
            // по каждому виду расчета произвести расчет события
            $calc =  $this->calculateOcca($eachOcca, $scoreKind);
            $this->repository->prepareComplexityCalcs($eachOcca,$calc);
            // TODO вернуть успешность или ошибку
        }

        //todo ? взять сложные события из репозитория если не срабатывает $this->calculateOcca в цикле, то репозиторий не инициализирован
        $calcs = $this->repository->getComplexityCalcs();
        foreach ($calcs as $complexityCalc) {
            $arSport = $this->getARfor($match->id, $scoreKind, $complexityCalc->getCalcId());
            $result = $complexityCalc->calc($arSport);
            if (!empty($result)) {
                //TODO вызвать обработку очереди паралльных расчетов $queue
                $this->newResults[] = $result;
            }
        }
    }

    /**
     * @brief расчет события по скорингу учитывая тип скоринга
     * @param $occa
     * @param $scoreObj
     * @param $scoreKind
     * @return CalcSportAR|null
     */
    protected function calculateOcca($occa, $scoreKind)
    {

        //TODO репозиторий инициализируется $occa,$scoreKind типом расчета
        /** @var SportOccaAR $occa */
        $occaCalculate = $this->repository->findForOcca($occa);

        if ($occaCalculate) {
            $arSport = $this->getARfor($occa->match_id, $scoreKind, $occaCalculate->getCalcId());
            if ($arSport->is_fixed) {
                return null;
            }
            /**
             * @var CalcSportAR $result
             */
            $result = $occaCalculate->calc($arSport);
            //TODO в результат записать количество очков из $occa->ext_info['points']
            if (is_numeric($occa->joPoints)) {
                $result->calc_result = $occa->joPoints;
            }

        } else {
            // расчета для такого события нету в $scoreKind
            //!!! это не ошибка для разных скорингов наборы расчетов могут отличатся
            return null;
        }


        // ? $occaCalculate->finis()
        // перед возвратом записываем в буфер для сохранения
        if ($result) {
            //TODO вызвать обработку очереди паралльных расчетов $queue
            $this->newResults[] = $result;
        }
        return $result;

    }

    /**
     * @param $matchId
     * @param $scoreKind
     * @return CalcSportAR
     */
    protected function getARfor($matchId, $scoreKind, $scoreObjId): CalcSportAR
    {
        static $poolFlag = null;
        if ($poolFlag === null) {
            $this->poolCalcSports = CalcSportAR::find()->andWhere(['match_id' => $this->match->id])->forUpdate()->all();
            $poolFlag = true;
        }
        /** @var CalcSportAR $result */
        $result = null;
        assert($this->match->id == $matchId, 'Событие не соответствует матчу операции');
        // если там уже есть для этой комбинации $occa, $eachScoreKind - чистим и отдаем
        $result = $this->grabPool($matchId, $scoreKind->id, $scoreObjId);

        return $result;

    }

    /**
     * Очищает неиспользованные записи
     */
    protected function clearPrevARs()
    {
        /** @var CalcSportAR $freeAr * */
        foreach ($this->poolCalcSports as $freeAr) {
            $freeAr->clearData();
            $freeAr->save();
        }

    }

    /**
     * return SportMatchAQ
     */
    public function getMatch()
    {
        return $this->hasOne(SportMatchAR::class, ['id' => 'ent_id'])->forUpdate();
    }

    /**
     * @return CalcSportAR
     */
    protected function createNewCalcResult(): CalcSportAR
    {
        $result = new CalcSportAR();
        return $result;
    }

    /**
     * @param $match_id
     * @param $scoreKindId
     * @param $scoreObjId
     * @return CalcSportAR | null
     */
    protected function grabPool($match_id, $scoreKindId, $scoreObjId): ?CalcSportAR
    {

        /** сравнить все результаты по ключам, вернуть совпавший
         *  очистить данные в возвращаемом.
         * @var CalcSportAR $calcResult
         */
        if (!empty($this->poolCalcSports)) {
            foreach ($this->poolCalcSports as $key => $calcResult) {
                if ($calcResult->match_id == $match_id && $calcResult->score_kind == $scoreKindId && $calcResult->calc_id == $scoreObjId) {
                    $calcResult->clearData();
                    unset($this->poolCalcSports[$key]);
                    return $calcResult;
                }
            }
            // выбираем первый очищенный
            foreach ($this->poolCalcSports as $key => $calcResult) {
                if ($calcResult->isCleared()) {
                    unset($this->poolCalcSports[$key]);
                    return $calcResult;
                }
            }
        }

        // создаем новую запись под расчет
        return $this->createNewCalcResult();
    }

    /**
     * @param int|SportMatchAR $match
     * @return MatchScoring
     */
    public static function generateScoring($match)
    {
        $matchId = is_object($match) ? $match->id : $match;
        if (($result = self::find()->whereMatch($matchId)->forUpdate()->one()) === null) {
            $result = new self(['op_type' => self::TYPE_MATCH, 'ent_id' => $matchId]);
            $result->save();
        }
        return $result;
    }
}
