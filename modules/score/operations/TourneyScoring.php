<?php

namespace common\modules\score\operations;

use yii\helpers\ArrayHelper;
use common\components\S3Core;
use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameTouMatchAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\score\models\CalcSportAR;
use common\modules\score\models\CalcTourneyAR;

/**
 * Description of TourneyScoring
 *
 * @author Andrey Dolgopolov
 * created: 01-may-2019
 *
 * @property GameTourneyAR $tourney
 */
class TourneyScoring extends BaseScoring
{
    protected $poolCalcTourneys = [];
    protected $cellRecords = [];

    /**
     * записи сгрупированные по спортсмену и маркеру расчета
     * @var CalcTourneyAR[] with keys ['sman_id'.'-'.'calc_id']
     */
    protected $calcRecords = [];

    /**
     * записи составов
     * @var GameLineupAR[] with keys ['lineup_id']
     */
    protected $lineUpRecords = [];
    protected $repository;


    public function calculate()
    {
        parent::calculate();

        // взять все спортивные расчеты матчей для данного турнира
        foreach ($this->getSportCalcsForTourney() as $sportCalc) {
            $calcTouRec = $this->getARfor($sportCalc->sman_id, $sportCalc->calc_id);
            $calcTouRec->calc_amount = $sportCalc->occa_amount;
            $calcTouRec->calc_points = $sportCalc->calc_result;
            $this->calcRecords[] = $calcTouRec;
        }
        $this->calculateSpecialPoints();

        $this->calculateLineUps();

        $this->clearPrevARs();
        //TODO вернуть успешность или ошибку
    }

    /**
     * сохранить все турнирные расчеты и измененные ячейки составов
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        // реальная запись в БД
        foreach ($this->calcRecords as $eachCalc) {
            if ($eachCalc && !$eachCalc->save()) {
                $this->addErrors($eachCalc->errors);
            }
        }

        foreach ($this->cellRecords as $eachCell) {
            if ($eachCell && !$eachCell->save()) {
                $this->addErrors($eachCell->errors);
            }
        }

        foreach ($this->lineUpRecords as $eachLineUp) {
            if ($eachLineUp && !$eachLineUp->save()) {
                $this->addErrors($eachLineUp->errors);
            }
        }

        $save = !$this->hasErrors() && parent::save($runValidation, $attributeNames);

        if ($save) {
            $this->tourney->sendUpdateLineupsPush();
        }

        return $save;
    }

    /**
     * @brief расчет каких то специальных надбавок по турниру
     */
    protected function calculateSpecialPoints()
    {
        // TODO провести дополнительные действия расчетов в турнире, если есть
        foreach ($this->calcRecords as $record) {

        }
    }

    /**
     * @brief расчет составов турнира по турнирным результатам
     */
    protected function calculateLineUps()
    {
        /**
         * модификация по кажому скорингу, модификация по спортсмену
         */
        $smansPoints = [];

        // сгрупировать расчеты по спортсменам
        foreach ($this->calcRecords as $record) {
            $smansPoints[$record->sman_id][$record->calc_id] = $record;

            //$smansPoints[$record->sman_id] = $record->calc_points;
        }
        // взять все составы турнира
        foreach ($this->getLineUpCells() as $lineup) {
            $cellPoints = 0;
            $lineup->scenario = 'scoring';
            $lineup->l_points = 0;
            $lineup->exp_points = 0;

            foreach ($lineup->lineUpCells as $cell) {

                if (array_key_exists($cell->sman_id, $smansPoints)) {
                    $cell = S3Core::mGoods()->modifyCalc($cell, $smansPoints[$cell->sman_id]);
                    // обновить очки спортсмена в составе

                    $cell = S3Core::mGoods()->modifyCell($cell);
                    // добавить очки спортсмена к составу, в котором он находится
                    $cellPoints += $cell->sman_points;
                } else {
                    $cell->sman_points = 0;
                }
                $this->cellRecords[] = $cell;
            }

            $lineup->l_points += $cellPoints;
            $lineup->exp_points += $cellPoints * S3Core::mScore()->getSportCoef($lineup->tourney->sport_id, $lineup->award_day);
            
            $lineup = S3Core::mGoods()->modifyLineup($lineup);
            $this->lineUpRecords[$lineup->id] = $lineup;
        }
        $this->prepareLineUps();

    }

    /**
     * подготовка составов к записи, установка позиций в турнире, дополнительные записи в ext_info
     */
    protected function prepareLineUps()
    {
        /**
         * автоматическая сортировка по очкам для получения позиции состава
         * ['количество очков' => 'призовое место']
         * @var array $positions
         */
        $points = ArrayHelper::getColumn($this->lineUpRecords, 'l_points');
        $points = array_unique($points);
        rsort($points);
        $points = array_flip($points);
        foreach ($this->lineUpRecords as $lineupId => $lineup) {
            $this->lineUpRecords[$lineupId]->l_place = $points[$lineup->l_points] + 1;
        }

    }

    /**
     * @param $smanId
     * @param $calcId
     * @return CalcTourneyAR
     */
    protected function getARfor($smanId, $calcId): CalcTourneyAR
    {
        static $poolFlag = null;
        if ($poolFlag === null) {
            $this->poolCalcTourneys = CalcTourneyAR::find()->andWhere(['tou_id' => $this->tourney->id])->forUpdate()->all();
            $poolFlag = true;
        }
        /** @var CalcTourneyAR $result */
        $result = null;

        // если там уже есть для этой комбинации турнир, спортсмен , маркер расчета
        $result = $this->grabPool($this->tourney->id, $smanId, $calcId);
        $result->setAttributes([
            'sman_id' => $smanId,
            'tou_id' => $this->tourney->id,
            'calc_id' => $calcId,
        ]);
        return $result;
    }

    /**
     * Очищает неиспользованные записи
     */
    protected function clearPrevARs()
    {
        /** @var CalcTourneyAR $freeAr * */
        foreach ($this->poolCalcTourneys as $freeAr) {
            $freeAr->clearData();
        }

    }

    /**
     * return GameTourneyAQ
     */
    public function getTourney()
    {
        return $this->hasOne(GameTourneyAR::class, ['id' => 'ent_id'])->forUpdate();
    }

    /**
     * @return CalcTourneyAR
     */
    protected function createNewCalcResult(): CalcTourneyAR
    {
        $result = new CalcTourneyAR();
        $result->clearData();
        return $result;
    }

    /**
     * @param $touId
     * @param $smanId
     * @param $calcId
     * @return CalcTourneyAR | null
     */
    protected function grabPool($touId, $smanId, $calcId): ?CalcTourneyAR
    {

        /** сравнить все результаты по ключам, вернуть совпавший
         *  очистить данные в возвращаемом.
         * @var CalcTourneyAR $calcResult
         */
        if (!empty($this->poolCalcTourneys)) {
            foreach ($this->poolCalcTourneys as $key => $calcResult) {
                if ($calcResult->tou_id == $touId && $calcResult->sman_id == $smanId && $calcResult->calc_id == $calcId) {
                    $calcResult->clearData();
                    unset($this->poolCalcTourneys[$key]);
                    return $calcResult;
                }
            }
            // выбираем первый очищенный
            foreach ($this->poolCalcTourneys as $key => $calcResult) {
                if ($calcResult->isCleared()) {
                    unset($this->poolCalcTourneys[$key]);
                    return $calcResult;
                }
            }
        }

        // создаем новую запись под расчет
        return $this->createNewCalcResult();
    }

    /**
     * @param int|GameTourneyAR $tourney
     * @return TourneyScoring
     */
    public static function generateScoring($tourney)
    {
        $touId = is_object($tourney) ? $tourney->id : $tourney;

        if (($result = self::find()->whereTourney($touId)->forUpdate()->one()) === null) {
            $result = new self(['op_type' => self::TYPE_TOURNEY, 'ent_id' => $touId]);
            $result->save();
        }

        return $result;
    }

    /**
     * @return array|CalcSportAR[]
     */
    protected function getSportCalcsForTourney()
    {
        $touMatchTable = GameTouMatchAR::tableName();
        $calcSportTable = CalcSportAR::tableName();
        // групировка рассчетов по sman_id, calc_id, расчеты только матчей относящихся к турниру, и только типа расчета турнира
        return CalcSportAR::find()
            ->select([
                'sman_id' => "$calcSportTable.sman_id",
                'match_id' => "$calcSportTable.match_id",
                'score_kind' => "$calcSportTable.score_kind",
                'calc_id' => "$calcSportTable.calc_id",
                'calc_result' => "sum($calcSportTable.calc_result)",
                'occa_amount' => "sum($calcSportTable.occa_amount)",
            ])
            ->innerJoin($touMatchTable, "$touMatchTable.match_id=$calcSportTable.match_id")
            ->where([
                "$touMatchTable.tou_id" => $this->tourney->id,
                "$calcSportTable.score_kind" => $this->tourney->score_kind,
            ])
            ->groupBy(["$calcSportTable.sman_id", "$calcSportTable.calc_id"])
            ->forRead()
            ->all();
    }

    /**
     * @return array|GameLineupAR[]|GameTourneyAR[]
     */
    protected function getLineUpCells()
    {
        return $this->tourney->getLineups()->registered()->with('lineUpCells')->forRead()->all();
    }
}
