<?php

namespace common\modules\score\operations;

use common\components\S3Core;
use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\score\TourneyResultTrait;
use yii\helpers\ArrayHelper;

/**
 * Description of TourneyScoring
 *
 * @author Andrey Dolgopolov
 * created: 01-may-2019
 *
 * @property GameTourneyAR $tourney
 */
class TrophyScoring extends BaseScoring
{
    use TourneyResultTrait;
    /**
     * записи составов
     * @var GameLineupAR[] with keys ['lineup_id']
     */
    protected $lineUpRecords = [];

    public function calculate()
    {
        parent::calculate();

        $this->lineUpRecords = $this->grabLineUpRecords();
        $points = $this->pointsLineUp($this->tourney);
        $table = $this->awardTable($this->tourney);

        if (empty($table)) {
            return false;
        }

        $awards = $this->groupAwardEqualResults($points, $table, 'prize_list');
        $this->prepareLineUps($awards);

        return true;
    }

    /**
     * сохранить все турнирные расчеты и измененные ячейки составов
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        foreach ($this->lineUpRecords as $eachLineUp) {
            $clonedModel = clone $eachLineUp; // TODO зачем клонировали если $this->tourney->sendUpdateLineupsPush();

            if (!$eachLineUp->save()) {
                $this->addErrors($eachLineUp->getErrors());

                return false;
            }
        }

        // только для турнира в статусе процессинг и выше
        if ($this->tourney->isStatus(S3Core::STATUS_PROCESSING) && !empty($this->lineUpRecords)) {
            if (!$this->calcTransactions()) {
                return false;
            }
        }

        $save = parent::save($runValidation, $attributeNames);

        if ($save) {
            $this->tourney->sendUpdateLineupsPush();
        }

        return $save;
    }

    /**
     * подготовка составов к записи, распределение фишек
     */
    protected function prepareLineUps($awards)
    {
        $awards = ArrayHelper::index($awards, 'subject_id');

        foreach ($this->lineUpRecords as $lineupId => $lineup) {
            $this->lineUpRecords[$lineupId]->l_award = $awards[$lineupId]['award'] ?? 0;

        }
    }

    /**
     * return GameTourneyAQ
     */
    public function getTourney()
    {
        return $this->hasOne(GameTourneyAR::class, ['id' => 'ent_id'])->forUpdate();
    }

    /**
     * @param int | GameTourneyAR $tourney
     * @return TrophyScoring
     */
    public static function generateScoring($tourney)
    {
        $touId = is_object($tourney) ? $tourney->id : $tourney;
        if (($result = self::find()->whereTrophyTourney($touId)->forUpdate()->one()) === null) {
            $result = new self(['op_type' => self::TYPE_TROPHY, 'ent_id' => $touId]);
            $result->save();
        }

        return $result;
    }

    /**
     * @return array
     */
    public function prepareUserAwards()
    {
        foreach ($this->lineUpRecords as $lineUp) {
            if ($lineUp->l_award) {
                $data[$lineUp->user_id] = [
                    //TODO параметр счета фишек из турнира
                    'user_id' => $lineUp->user_id,
                    'award' => $lineUp->l_award,
                ];
            }
        }

        return $data ?? [];
    }

    /**
     * @return bool
     */
    protected function calcTransactions()
    {
        $touExtInfo = [
            'tou_id' => $this->ent_id,
            'info' => 'trophy',
            'op' => ['id' => $this->id, 'type' => $this->op_type]
        ];
        $data = $this->prepareUserAwards();

        return S3Core::mMoney()->transactionsByScoring($this->id, 1, $data, $touExtInfo);
    }

}
