<?php

/**
 * @var $this \yii\web\View
 * @var $model ScoreKindAR
 */

use common\components\S3Core;
use common\modules\score\controllers\SportController;
use common\modules\score\models\ScoreKindAR;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Просмотр типа скоринга: ' . $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Спортивный скоринг', 'url' => ['index', 'tab' => SportController::TAB_SCORINGS]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update-score-kind', 'id' => $model->id]];

$statuses = ScoreKindAR::getStatusesList();

$statusName = $statuses[S3Core::STATUS_LIVE];
if ($model->r_status === S3Core::STATUS_LIVE) {
    $statusName = $statuses[S3Core::STATUS_CLOSED];
}

$tableHead = null;
$tableHeadTh = Html::tag('th', null, ['scope' => 'row']);
$tableBody = null;

$calcs = $model->calcs;
$positions = $model->positions;
$scoring = $model->scoring;
?>
<div class="score-kind-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-arrow-left"></span> К списку', ['index', 'tab' => SportController::TAB_SCORINGS], [
            'class' => 'btn btn-outline-primary pull-left',
        ]) ?>
        <?= Html::a('Создать копию', ['clone-score-kind', 'id' => $model->id], [
            'class' => 'btn btn-primary',
            'data' => [
                'confirm' => 'Вы действительно хотите создать копию типа скоринга?',
                'method' => 'post',
            ],
        ]) ?>
        <?php if ($model->r_status !== S3Core::STATUS_CLOSED) { ?>
            <?= Html::a('Перевести в ' . $statusName, ['switch-status-score-kind', 'id' => $model->id], [
                'class' => ['btn', $model->r_status === S3Core::STATUS_LIVE ? 'btn-danger' : 'btn-success'],
                'data' => [
                    'confirm' => 'Вы действительно хотите изменить статус типа скоринга?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php } ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'sport_id',
                'value' => static function (ScoreKindAR $model) {
                    return $model->sport->r_name ?? null;
                }
            ],
            'r_name',
            [
                'attribute' => 'r_status',
                'value' => static function (ScoreKindAR $model) {
                    return S3Core::code2status($model->r_status);
                }
            ],
        ],
    ]) ?>

    <?= Html::beginTag('table', ['class' => 'table-wrapper-scroll-y table']) ?>
    <?php foreach ($calcs as $calc) {
        /** @var \common\modules\score\models\CalcItemAR $calc */
        $tableRow = Html::tag('td', $calc->r_name);
        ?>
        <?php
        foreach ($positions as $position) {
            /** @var \common\modules\sport\models\SportPositionAR $position */
            $fieldValue = $scoring[$calc->id][$position->id] ?? null;

            $tableHeadTh .= Html::tag('th', $position->r_name, ['scope' => 'row']);
            $tableRow .= Html::tag('td', $fieldValue);
        }

        $tableHead = Html::tag('tr', $tableHeadTh);
        $tableBody .= Html::tag('tr', $tableRow);
        $tableHeadTh = Html::tag('th', null, ['scope' => 'row']);
        ?>
    <?php } ?>
    <?php if ($tableHead && $tableBody) { ?>
        <?= Html::tag('thead', Html::tag('tr', $tableHead)) ?>
        <?= Html::tag('tbody', $tableBody) ?>
        <?= Html::endTag('table') ?>
    <?php } ?>
</div>
