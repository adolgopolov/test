<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\modules\score\models\ScoreKindAR
 * @var $sports array
 */

use common\modules\score\controllers\SportController;
use yii\helpers\Html;

$this->title = 'Создание типа скоринга';
$this->params['breadcrumbs'][] = ['label' => 'Спортивный скоринг', 'url' => ['index', 'tab' => SportController::TAB_SCORINGS]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="score-kind-ar-create">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
        'sports' => $sports,
    ]) ?>
</div>
