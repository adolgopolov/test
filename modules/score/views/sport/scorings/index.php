<?php

/**
 * @var $this \yii\web\View
 * @var $scoreKindProvider \yii\data\ActiveDataProvider
 * @var $scoreKindSearch ScoreKindAS
 * @var $scoreKindStatuses array
 * @var $sports array
 */

use backend\components\aboard\ActionColumn;
use common\components\S3Core;
use common\modules\score\forms\ScoreKindAS;
use common\widgets\GridActions\GridActionsWidget;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

?>
<div class="btn btn-"></div>
<?php Pjax::begin(['id' => 'pjax-all-scorings']) ?>
<?php GridActionsWidget::begin([
    'contentInsideForm' => true,
    'createUrl' => ['create-score-kind'],
    'enableDeleteButton' => false,
]) ?>
<?= Html::submitButton('Создать копию', [
    'class' => 'btn btn-info mL-5',
    'name' => 'clone',
    'data' => [
        'confirm' => 'Вы действительно хотите создать копии выбранных скорингов?',
    ],
]) ?>
<?= Html::submitButton('Следующий статус', [
    'class' => 'btn btn-info mL-5',
    'name' => 'next-status',
    'data' => [
        'confirm' => 'Вы действительно хотите перевести статусы выбранных скорингов?',
    ],
]) ?>
<?php GridActionsWidget::end() ?>
<?= GridView::widget([
    'dataProvider' => $scoreKindProvider,
    'filterModel' => $scoreKindSearch,
    'columns' => [
        GridActionsWidget::$checkboxColumn,
        'id',
        [
            'attribute' => 'sport_id',
            'filter' => Select2::widget([
                'attribute' => 'sport_id',
                'model' => $scoreKindSearch,
                'data' => $sports,
                'pluginOptions' => [
                    'placeholder' => 'Выберите вид спорта',
                    'allowClear' => true,
                ],
            ]),
            'value' => static function (ScoreKindAS $model) {
                return $model->sport->r_name ?? null;
            },
        ],
        'r_name',
        [
            'attribute' => 'r_status',
            'filter' => Select2::widget([
                'attribute' => 'r_status',
                'model' => $scoreKindSearch,
                'data' => $scoreKindStatuses,
                'pluginOptions' => [
                    'placeholder' => 'Выберите статус',
                    'allowClear' => true,
                ],
            ]),
            'value' => static function (ScoreKindAS $model) {
                return S3Core::code2status($model->r_status);
            },
        ],
        [
            'class' => ActionColumn::class,
            'template' => '<div class="btn-group">{view}{update}{clone}{switch-status}</div>',
            'urlCreator' => static function ($action, ScoreKindAS $model) {
                switch ($action) {
                    case 'create':
                        return ['create-score-kind'];
                    case 'view':
                        return ['view-score-kind', 'id' => $model->id];
                    case 'update':
                        return ['update-score-kind', 'id' => $model->id];
                    case 'clone':
                        return ['clone-score-kind', 'id' => $model->id];
                    case 'switch-status':
                        return ['switch-status-score-kind', 'id' => $model->id];
                }

                return null;
            },
            'buttons' => [
                'clone' => static function ($url) {
                    return Html::a('<span class="fas fa-clone"></span>', $url, [
                        'class' => 'btn btn-outline-primary',
                        'data' => [
                            'confirm' => 'Вы действительно хотите создать копию типа скоринга?',
                            'toggle' => 'tooltip',
                            'title' => 'Создать копию',
                        ],
                    ]);
                },
                'switch-status' => static function ($url, ScoreKindAS $model) use ($scoreKindStatuses) {
                    if ($model->r_status === S3Core::STATUS_CLOSED) {
                        return null;
                    }

                    $link = $model->r_status === S3Core::STATUS_LIVE ? '<span class="fas fa-close"></span>' : '<span class="fas fa-heart"></span>';
                    $statusName = $model->r_status === S3Core::STATUS_LIVE ? $scoreKindStatuses[S3Core::STATUS_CLOSED] : $scoreKindStatuses[S3Core::STATUS_LIVE];
                    $tooltip = 'Перевести в ' . $statusName;

                    return Html::a($link, $url, [
                        'class' => ['btn', $model->r_status === S3Core::STATUS_LIVE ? 'btn-outline-danger' : 'btn-outline-success'],
                        'data' => [
                            'confirm' => 'Вы действительно хотите изменить статус типа скоринга?',
                            'toggle' => 'tooltip',
                            'title' => $tooltip,
                        ],
                    ]);
                },
            ],
        ],
    ],
]) ?>
<?php Pjax::end() ?>
