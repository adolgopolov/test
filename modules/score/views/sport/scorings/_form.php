<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\modules\score\models\ScoreKindAR
 * @var $sports array
 */

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$tableHead = null;
$tableHeadTh = Html::tag('th', null, ['scope' => 'row']);
$tableBody = null;

$calcs = $model->calcs;
$positions = $model->positions;
$scoring = $model->scoring;
?>
<?php $form = ActiveForm::begin() ?>

<?= $form->field($model, 'r_name')->textInput() ?>

<?php if ($model->isNewRecord) { ?>
    <?= $form->field($model, 'sport_id')->widget(Select2::class, [
        'data' => $sports,
        'pluginOptions' => [
            'placeholder' => 'Выберите вид спорта',
        ],
    ]) ?>
<?php } else { ?>
    <?= Html::beginTag('table', ['class' => 'table-wrapper-scroll-y table']) ?>
    <?php foreach ($calcs as $calc) {
        /** @var \common\modules\score\models\CalcItemAR $calc */
        $tableRow = Html::tag('td', $calc->r_name);
        ?>
        <?php
        foreach ($positions as $position) {
            /** @var \common\modules\sport\models\SportPositionAR $position */
            $fieldValue = $scoring[$calc->id][$position->id] ?? null;
            $fieldInput = $form->field($model, "scoring[$calc->id][$position->id]")->input('number', [
                'value' => $fieldValue,
            ])->label(false);

            $tableHeadTh .= Html::tag('th', $position->r_name, ['scope' => 'row']);
            $tableRow .= Html::tag('td', $fieldInput);
        }

        $tableHead = Html::tag('tr', $tableHeadTh);
        $tableBody .= Html::tag('tr', $tableRow);
        $tableHeadTh = Html::tag('th', null, ['scope' => 'row']);
        ?>
    <?php } ?>
    <?php if ($tableHead && $tableBody) { ?>
        <?= Html::tag('thead', Html::tag('tr', $tableHead)) ?>
        <?= Html::tag('tbody', $tableBody) ?>
        <?= Html::endTag('table') ?>
    <?php } ?>
<?php } ?>

<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>
