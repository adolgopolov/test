<?php

/**
 * @var $this \yii\web\View
 * @var $tab integer
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel \common\modules\score\forms\CalcSportAS
 * @var $scoreKindProvider \yii\data\ActiveDataProvider
 * @var $scoreKindSearch \common\modules\score\forms\ScoreKindAS
 * @var $scoreKindStatuses array
 * @var $leagues array
 * @var $statuses array
 * @var $sports array
 * @var $leagueList array
 */

use common\modules\score\controllers\SportController;
use kartik\tabs\TabsX;
use yii\helpers\Html;

$this->title = 'Спортивный скоринг';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="score-sport-index">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= TabsX::widget([
        'items' => [
            [
                'label' => '<span class="ti ti-layout-grid3"></span> Все матчи',
                'active' => $tab === SportController::TAB_MATCHES,
                'content' => $this->render('matches/index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'leagues' => $leagues,
                    'statuses' => $statuses,
                    'sports' => $sports,
                    'leagueList' => $leagueList,
                ]),
            ],
            [
                'label' => '<span class="ti ti-layout-grid3"></span> Все скоринги',
                'active' => $tab === SportController::TAB_SCORINGS,
                'content' => $this->render('scorings/index', [
                    'scoreKindProvider' => $scoreKindProvider,
                    'scoreKindSearch' => $scoreKindSearch,
                    'scoreKindStatuses' => $scoreKindStatuses,
                    'sports' => $sports,
                ]),
            ],
        ],
        'encodeLabels' => false,
    ]) ?>
</div>
