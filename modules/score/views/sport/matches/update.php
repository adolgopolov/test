<?php

/**
 * @var $this \yii\web\View
 * @var $match_id integer
 * @var $matchModel \common\modules\game\models\SportMatchAR|null
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $columns array
 */

use common\modules\score\controllers\SportController;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$titleMatch = $matchModel->r_name ?? $match_id;
$this->title = 'Редактирование скоринга матча ' . $titleMatch;
$this->params['breadcrumbs'][] = ['label' => 'Спортивный скоринг', 'url' => ['index', 'tab' => SportController::TAB_MATCHES]];
$this->params['breadcrumbs'][] = ['label' => $titleMatch, 'url' => ['view', 'match_id' => $match_id]];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'match_id' => $match_id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
$this->registerJsFile('/js/score/calc_sport.js');
?>
<div class="score-sport-update-ar">
    <?php $form = ActiveForm::begin() ?>
    <div class="d-flex flex-row justify-content-between mT-10 mB-10">
        <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
        <div>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
    ]) ?>
    <?php ActiveForm::end() ?>
</div>
