<?php

/**
 * @var $this View
 * @var $match_id integer
 * @var $matchModel null|\common\modules\game\models\SportMatchAR
 * @var $searchModel CalcSportAS
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $columns array
 */

use common\modules\score\controllers\SportController;
use common\modules\score\forms\CalcSportAS;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

$this->title = $matchModel->r_name ?? $match_id;
$this->params['breadcrumbs'][] = ['label' => 'Спортивные скоринг', 'url' => ['index', 'tab' => SportController::TAB_MATCHES]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'match_id' => $match_id]];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="score-sport-view-ar">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#score-sport-pjax',
        'pjaxUrl' => Url::to(['view', 'match_id' => $match_id]),
        'model' => $searchModel,
    ]) ?>
    <?php Pjax::begin([
        'id' => 'score-sport-pjax',
        'enablePushState' => true,
    ]) ?>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-arrow-left"></span> К списку', ['index', 'tab' => SportController::TAB_MATCHES], [
            'class' => 'btn btn-outline-primary pull-left',
        ]) ?>
    </p>
    <?= GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => $columns,
    ]) ?>
    <?php Pjax::end() ?>
</div>
