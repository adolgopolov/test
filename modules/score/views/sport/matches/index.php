<?php

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel CalcSportAS
 * @var $leagues array
 * @var $statuses array
 * @var $sports array
 * @var $leagueList array
 */

use backend\components\aboard\ActionColumn;
use common\modules\score\forms\CalcSportAS;
use common\modules\sport\models\SportLeagueAR;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>
<?php Pjax::begin(['id' => 'pjax-all-matches']) ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'updated_at',
        [
            'attribute' => 'sport_id',
            'filter' => Select2::widget([
                'attribute' => 'sport_id',
                'model' => $searchModel,
                'data' => $sports,
                'pluginOptions' => [
                    'placeholder' => 'Выберите вид спорта',
                    'allowClear' => true,
                ],
            ]),
            'value' => static function (CalcSportAS $model) use ($sports) {
                return $sports[$model->sport_id] ?? null;
            }
        ],
        [
            'attribute' => 'lea_id',
            'filter' => Select2::widget([
                'attribute' => 'lea_id',
                'model' => $searchModel,
                'data' => $leagueList,
                'pluginOptions' => [
                    'placeholder' => 'Выберите лигу',
                    'allowClear' => true,
                ],
            ]),
            'value' => static function (CalcSportAS $model) use ($leagues) {
                /** @var SportLeagueAR $league */
                $league = $leagues[$model->lea_id] ?? null;

                return $league ? $league->getTitle() : null;
            },
        ],
        'match_id',
        'match_name',
        [
            'attribute' => 'match_status',
            'filter' => Select2::widget([
                'attribute' => 'match_status',
                'model' => $searchModel,
                'data' => $statuses,
                'pluginOptions' => [
                    'placeholder' => 'Выберите статус',
                    'allowClear' => true,
                ],
            ]),
            'value' => static function (CalcSportAS $model) use ($statuses) {
                return $statuses[$model->match_status] ?? null;
            },
        ],
        [
            'class' => ActionColumn::class,
            'template' => '<div class="btn-group">{view}{update}</div>',
            'urlCreator' => static function ($action, CalcSportAS $model) {
                switch ($action) {
                    case 'view':
                        return ['view', 'match_id' => $model->match_id];
                        break;
                    case 'update':
                        return ['update', 'match_id' => $model->match_id];
                        break;
                }

                return null;
            },
        ],
    ],
]) ?>
<?php Pjax::end() ?>
