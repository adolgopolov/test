<?php

use common\widgets\AceEditor;
use common\widgets\DetailForm\DetailFormWidget;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\score\models\CalcItemAR $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<div class="calc-item-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputType' => 'number',
            ],
            'ent_kind' => [
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'ent_code' => [
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'ent_value',
            'r_name',
            'ext_info' => AceEditor::widget([
                'model' => $model,
                'attribute' => 'ext_info',
                'mode' => 'json',
                'theme' => 'github',
            ]),
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
