<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model common\modules\score\models\CalcItemAR
 */

$this->title = $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Calc Item Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id, 'ent_kind' => $model->ent_kind]];

YiiAsset::register($this);
?>
<div class="calc-item-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-arrow-left"></span> К списку', ['index'], [
            'class' => 'btn btn-outline-primary pull-left',
        ]) ?>
        <?= Html::a('<span class="fa fa-trash"></span> Удалить', ['delete', 'id' => $model->id, 'ent_kind' => $model->ent_kind], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно желаете уделить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ent_kind',
            'ent_code',
            'ent_value',
            'r_name',
        ],
    ]) ?>
</div>
