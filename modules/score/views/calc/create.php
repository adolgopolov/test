<?php

use yii\helpers\Html;

/** @var yii\web\View $this*/
/** @var common\modules\score\models\CalcItemAR $model */

$this->title = 'Добавление Calc Item Ar';
$this->params['breadcrumbs'][] = ['label' => 'Calc Item Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calc-item-ar-create">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
