<?php

use yii\helpers\Html;

/** @var yii\web\View $this*/
/** @var common\modules\score\models\CalcItemAR $model */

$this->title = 'Update Calc Item Ar: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Calc Item Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'ent_kind' => $model->ent_kind]];
$this->params['breadcrumbs'][] = 'Update';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id, 'ent_kind' => $model->ent_kind]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
?>
<div class="calc-item-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
