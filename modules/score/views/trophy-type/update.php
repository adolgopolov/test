<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\modules\game\models\TrophyTypeAR
 * @var $currencies array
 */

use yii\helpers\Html;

$this->title = 'Редактирование: ' . $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Типы скорингов выигрышей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->r_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
?>
<div class="triphy-type-ar-update">
    <h3 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', [
        'model' => $model,
        'currencies' => $currencies,
    ]) ?>
</div>
