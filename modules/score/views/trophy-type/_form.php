<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\modules\game\models\TrophyTypeAR
 * @var $currencies array
 */

use common\widgets\DetailForm\DetailFormWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin() ?>

<?= DetailFormWidget::widget([
    'model' => $model,
    'inputs' => [
        'id' => [
            'inputHidden' => $model->isNewRecord,
            'inputOptions' => [
                'disabled' => true,
            ],
        ],
        'r_name',
        'info' => [
            'inputType' => 'textarea',
        ],
        'is_free' => [
            'inputType' => 'checkbox',
        ],
        'is_auto_pfund' => [
            'inputType' => 'checkbox',
        ],
        'buyin_currency' => Select2::widget([
            'model' => $model,
            'attribute' => 'buyin_currency',
            'data' => $currencies,
            'pluginOptions' => [
                'placeholder' => 'Список валют',
                'multiple' => true,
                'allowClear' => true,
            ],
        ]),
    ],
]) ?>

<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>
