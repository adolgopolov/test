<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\modules\game\models\TrophyTypeAR
 * @var $currencyModels \common\modules\money\models\MoneyCurrencyAR[]
 */

use common\modules\game\models\TrophyTypeAR;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Типы скорингов выигрышей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];

$booleanStatuses = TrophyTypeAR::getBooleanStatuses();
?>
<div class="trophy-type-ar-view">
    <h3 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h3>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-arrow-left"></span> К списку', ['index'], [
            'class' => 'btn btn-outline-primary pull-left mB-10',
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'r_name',
            'info',
            [
                'attribute' => 'is_free',
                'filter' => $booleanStatuses,
                'value' => static function (TrophyTypeAR $model) use ($booleanStatuses) {
                    return Html::tag('span', $model->is_free ? $booleanStatuses[1] : $booleanStatuses[0], [
                        'class' => [
                            'badge',
                            'badge-pill',
                            $model->is_free ? 'badge-success' : 'badge-danger',
                        ]
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'is_auto_pfund',
                'filter' => TrophyTypeAR::getBooleanStatuses(),
                'value' => static function (TrophyTypeAR $model) use ($booleanStatuses) {
                    return Html::tag('span', $model->is_auto_pfund ? $booleanStatuses[1] : $booleanStatuses[0], [
                        'class' => [
                            'badge',
                            'badge-pill',
                            $model->is_auto_pfund ? 'badge-success' : 'badge-danger',
                        ],
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'buyin_currency',
                'value' => static function (TrophyTypeAR $model) use ($currencyModels) {
                    $list = [];

                    foreach ((array)$model->buyin_currency as $currencyId) {
                        if (isset($currencyModels[$currencyId])) {
                            $list[] = $currencyModels[$currencyId]->r_name;
                        }
                    }

                    return implode(', ', $list);
                }
            ],
        ],
    ]) ?>
</div>
