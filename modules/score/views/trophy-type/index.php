<?php

/**
 * @var $this \yii\web\View
 * @var $searchModel TrophyTypeAS
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $currencyModels \common\modules\money\models\MoneyCurrencyAR[]
 */

use backend\components\aboard\ActionColumn;
use common\modules\game\forms\TrophyTypeAS;
use common\modules\game\models\TrophyTypeAR;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Типы скорингов выигрышей';
$this->params['breadcrumbs'][] = $this->title;

$booleanStatuses = TrophyTypeAR::getBooleanStatuses();
?>
<div class="trophy-type-ar-index">
    <h3 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h3>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            'r_name',
            [
                'attribute' => 'is_free',
                'filter' => $booleanStatuses,
                'value' => static function (TrophyTypeAS $model) use ($booleanStatuses) {
                    return Html::tag('span', $model->is_free ? $booleanStatuses[1] : $booleanStatuses[0], [
                        'class' => [
                            'badge',
                            'badge-pill',
                            $model->is_free ? 'badge-success' : 'badge-danger',
                        ]
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'is_auto_pfund',
                'filter' => TrophyTypeAR::getBooleanStatuses(),
                'value' => static function (TrophyTypeAS $model) use ($booleanStatuses) {
                    return Html::tag('span', $model->is_auto_pfund ? $booleanStatuses[1] : $booleanStatuses[0], [
                        'class' => [
                            'badge',
                            'badge-pill',
                            $model->is_auto_pfund ? 'badge-success' : 'badge-danger',
                        ],
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'buyin_currency',
                'value' => static function (TrophyTypeAS $model) use ($currencyModels) {
                    $list = [];

                    foreach ((array)$model->buyin_currency as $currencyId) {
                        if (isset($currencyModels[$currencyId])) {
                            $list[] = $currencyModels[$currencyId]->r_name;
                        }
                    }

                    return implode(', ', $list);
                }
            ],
            [
                'class' => ActionColumn::class,
                'template' => '<div class="btn-group">{view}{update}</div>'
            ],
        ],
    ]) ?>
</div>
