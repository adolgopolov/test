<?php

use yii\helpers\Html;

/** @var yii\web\View $this*/
/** @var common\modules\score\models\GamerLevelAR $model */

$this->title = 'Добавление Gamer Level Ar';
$this->params['breadcrumbs'][] = ['label' => 'Gamer Level Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gamer-level-ar-create">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
