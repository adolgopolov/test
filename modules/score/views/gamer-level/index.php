<?php

use backend\components\aboard\ActionColumn;
use common\widgets\GridActions\GridActionsWidget;
use yii\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Уровни игроков';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gamer-level-ar-index">
    <?= GridActionsWidget::widget([
        'title' => $this->title,
    ]) ?>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            GridActionsWidget::$checkboxColumn,
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'ent_kind',
                'options' => [
                    'class' => 'kind-grid-column',
                ],
            ],
            [
                'attribute' => 'ent_code',
                'options' => [
                    'class' => 'code-grid-column',
                ],
            ],
            'ent_value',
            'r_name',
            //'ext_info',
            ['class' => ActionColumn::class],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
