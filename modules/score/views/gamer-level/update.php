<?php

use yii\helpers\Html;

/** @var yii\web\View $this*/
/** @var common\modules\score\models\GamerLevelAR $model */

$this->title = 'Редактирование уровня игрока: ' . $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Gamer Level Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'ent_kind' => $model->ent_kind]];
$this->params['breadcrumbs'][] = 'Update';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id, 'ent_kind' => $model->ent_kind]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
?>
<div class="gamer-level-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
