<?php

namespace common\modules\score;

use Yii;
use yii\helpers\ArrayHelper;
use common\modules\game\models\{GameLineupAR, GameTourneyAR, PrizeFundDTO};
/**
 *
 * @author Skynin <sohay_ua@yahoo.com>
 * created: 21-Nov-2018
 */
trait TourneyResultTrait
{
    private $pointsLineUp;
    /**
     * @brief выбрать составы турнира
     */
    protected function grabLineUpRecords()
    {
        $lineUps = GameLineupAR::find()
            ->whereTourney($this->ent_id)
            ->registered()
            ->orderBy(['l_points' => SORT_DESC])
            ->forUpdate()
            ->all();
        foreach ($lineUps as $eachLineUp) {
            $eachLineUp->scenario = 'scoring';
        }
        $this->pointsLineUp = $lineUps;
        return ArrayHelper::index($lineUps,'id');
    }

    /**
     *  собрать очки составов в массив , обязательно сортировать по убыванию очков!!!
     * @param GameTourneyAR $tourney
     * @return array
     */
    function pointsLineUp (GameTourneyAR $tourney)
    {
        $pointsLineUp = [];
        foreach ($this->pointsLineUp as $eachLineUp){
            $pointsLineUp[] = [
                'subject_id' => $eachLineUp->id,
                'subject_result' => $eachLineUp->l_points,
            ];
        }

        assert($tourney->reg_lineups == count($pointsLineUp), 'Не совпадает количество составов (tourney->reg_lineups != count($pointsLineUp))');

        return $pointsLineUp;
    }

    /**
     * @param GameTourneyAR $tourney
     * @return array|bool|void
     */
    function awardTable (GameTourneyAR $tourney)
    {
        if($trophyAR = $tourney->trophyType) {
            $table = $trophyAR->calcAwards( new PrizeFundDTO([
                'prize_fund'		 => $tourney->prizefund,
                'buyin'			 => $tourney->getBuyInGold(),
                'gamer_count'	 => $tourney->reg_lineups,
            ]));

            if($table) {
                return $table;
            }
            else {
                $this->addErrors($trophyAR->errors);
                return false;
            }
        }
        $this->addError('trophy_kind', 'trophyType in tourney is empty ' . $tourney->id);
        return false;
    }

    /**
     * возвращает массив ['id состава' => 'количество выигранных фишек']
     * распределяет равномерно фишки между учасниками с одинаковым количеством очков
     * @param $pointsLineUp
     * @param $awardTable
     * @param $fieldAward
     * @param int $defaultAward
     * @return mixed
     */
	function groupAwardEqualResults ( $pointsLineUp, $awardTable, $fieldAward, $defaultAward = 0 )
	{
		$budgets = [];
		/*
		 * бюджеты для позиций
		 * ключ - очки
		 * list = количество для этого бюджета
		 * award сумма бюджета
		 */

		$currAwardInd = 0;
		$currAwardTable = $awardTable[$fieldAward];
        //TODO алгоритм для массива отсортированного по убыванию очков

		// собираем бюджет для одинаковых очков
		foreach ( $pointsLineUp as $keyPoint => $grResult ) {

			$cuPoints = $grResult['subject_result'];

			if (isset($budgets[$cuPoints])) {
				$budgets[$cuPoints]['award'] += $currAwardTable[$currAwardInd+1] ?? 0;
				$budgets[$cuPoints]['list'][] = $grResult;
			}
			elseif(isset($currAwardTable[$currAwardInd+1])) {
				$budgets[$cuPoints] = [
					'award' => $currAwardTable[$currAwardInd+1] ?? 0,
					'list' => [$grResult]
				];
			}

			++$currAwardInd;
		}

		// распределение призового при равном кол-ве баллов
		foreach ( $pointsLineUp as &$grResult ) {

			$cuPoints = $grResult['subject_result'];
			$grResult['award'] = $defaultAward;

			if (isset($budgets[$cuPoints])) {
				$grResult['award'] = (int) ($budgets[$cuPoints]['award'] / count($budgets[$cuPoints]['list']));
			}
		}

		return $pointsLineUp;
	}
}
