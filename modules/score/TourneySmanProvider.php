<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 27.06.19 15:29
 */

namespace common\modules\score;


use common\components\S3Component;
use common\modules\game\models\GameTourneyAR;
use common\modules\sount\models\SouSmanAR;
use common\modules\sport\models\SmanAR;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class TourneySmanProvider extends S3Component
{
    /**
     * возвращает список событий и их количество по матчам и спортсменам
     * для данного турнира
     */
    public static function listSmanMatchPoints ($touId):array
    {

        $queryStr = '';
        //TODO забрать всех спортсменов турнира
        $tourney = GameTourneyAR::findOne($touId);
        $smenTorney = $tourney->getSmen()->all();
        $smanIds = ArrayHelper::getColumn($smenTorney,'id');
        $smans = SouSmanAR::find()->where(['sman_our_id' => $smanIds])->all();
        $start = 1;
        foreach ($smans as $key => $sman){
            if(!$start){
                $queryStr .= " UNION \n";
            }
            $start = 0;
            $queryStr .= " (select t.match_sou_id , smatch.match_our_id match_id, t.sman_sou_id, ssman.sman_our_id sman_id,
                                 t.fact_sou_id, sfact.ent_id fact_id, sum(t.fact_value) occa_val
       from ss_sou_occasion t
       inner join ss_sou_match smatch ON smatch.id=t.match_sou_id
       inner join ss_sou_sman ssman ON ssman.id=t.sman_sou_id
       inner join ss_sou_dict sfact ON sfact.id=t.fact_sou_id AND sfact.ent_kind = 3
 where  t.sman_sou_id = $sman->id AND t.match_sou_id in (select tc.match_sou_id match_ids from ss_sou_match_sman tc where tc.sman_sou_id = $sman->id) AND ssman.sman_our_id IS NOT NULL
 group by t.match_sou_id, t.fact_sou_id ) \n";

        }
        if(empty($queryStr)) return [];
        $queryStr .= "order by match_sou_id desc";
        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand($queryStr);

        return $command->queryAll();

    }
}
