<?php

namespace common\modules\sport\controllers;

use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\doings\sport\ChangeMatchOccasionDoing;
use common\modules\game\models\SportMatchAR;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SportOccaAS;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportFactAR;
use common\modules\sport\models\SportOccaAR;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * OccaController implements the CRUD actions for SportOccaAR model.
 */
class OccaController extends AdminController
{
    use CRUDTrait;

    public $modelClass = SportOccaAR::class;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SportOccaAR models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember(['index'], 'occaReturn');

        $searchModel = new SportOccaAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null, [
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SportOccaAR model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        Url::remember(['view', 'id' => $id], 'occaReturn');

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SportOccaAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new SportOccaAR();
        if ($model->load(Yii::$app->request->post())) {
            $doing = ChangeMatchOccasionDoing::new();

            $result = $doing->doIt([
                'model' => $model,
            ]);

            if ($result) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'matches' => SportMatchAR::getMatches(),
            'facts' => SportFactAR::getFacts(),
            'smen' => SmanAR::getSmen(),
        ]);
    }

    /**
     * Updates an existing SportOccaAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($id)
    {
        Url::remember(['update', 'id' => $id], 'occaReturn');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $doing = ChangeMatchOccasionDoing::new();

            $result = $doing->doIt([
                'model' => $model,
            ]);

            if ($result) {
                return $this->goBack();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'matches' => SportMatchAR::getMatches(),
            'facts' => SportFactAR::getFacts(),
            'smen' => SmanAR::getSmen(),
        ]);
    }

    /**
     * Deletes an existing SportOccaAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $occaReturn = Yii::$app->session->get('occaReturn');

        if ($occaReturn) {
            return $this->redirect($occaReturn);
        }

        return $this->goBack();
    }

    /**
     * Deletes an existing SportOccaAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSoftDelete($id)
    {
        $model = $this->findModel($id);

        $model->markDeleted();

        $doing = ChangeMatchOccasionDoing::new();

        $doing->doIt([
            'models' => [
                $model
            ],
        ]);

        return $this->goBack();
    }

    /**
     * Finds the SportOccaAR model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SportOccaAR the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SportOccaAR::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
