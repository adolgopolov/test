<?php

namespace common\modules\sport\controllers;

use Yii;
use yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\doings\sport\ChangeTeamMemberDoing;
use common\models\Many2Many;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SportTeamAS;
use common\modules\sport\forms\SportTeamFM;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportMemberAR;
use common\modules\sport\models\SportPositionAR;
use Throwable;

/**
 * TeamController implements the CRUD actions for SportTeamFM model.
 */
class TeamController extends AdminController
{
    use CRUDTrait;

    protected $modelClass = SportTeamFM::class;

    /**
     * Lists all SportTeamFM models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SportTeamAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $sports = SportAR::getAsOptsList();
        $leagues = SportLeagueAR::getLeaList('r_name');

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'sports' => $sports,
            'leagues' => $leagues,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionView($id)
    {
        /** @var SportTeamFM $model */
        $model = SportTeamFM::findOrFail($id);

        $leaDataProvider = new ActiveDataProvider([
            'query' => $model->getLeagues()->with(['sport']),
        ]);

        $smanDataProvider = new ActiveDataProvider([
            'query' => $model->getSmen(),
        ]);

        return $this->render('view', [
            'model' => $model,
            'leaDataProvider' => $leaDataProvider,
            'smanDataProvider' => $smanDataProvider,
        ]);
    }

    /**
     * Creates a new SportTeamFM model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws ExitException
     */
    public function actionCreate()
    {
        $model = new SportTeamFM();
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'sports' => $sports,
        ]);
    }

    /**
     * Updates an existing SportTeamFM model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ExitException
     * @throws InvalidConfigException
     */
    public function actionUpdate($id)
    {
        /** @var SportTeamFM $model */
        $model = SportTeamFM::findOrFail($id);
        $sports = SportAR::getAsOptsList();

        $leaDataProvider = new ActiveDataProvider([
            'query' => $model->getLeagues(),
        ]);

        $availableLeagues = $this->getAvailableLeagues($model);

        $lastTeamMembers = $model->getSmen()
            ->alias('sman')
            ->select(['team_member.id', 'MAX(team_member.last_day) AS last_day'])
            ->joinWith(['teamMembers AS team_member'])
            ->andWhere([
                '>', 'team_member.last_day', date('Y-m-d'),
            ])
            ->groupBy(['team_member.sman_id'])
            ->column();

        $query = $model->getSmen()
            ->alias('sman')
            ->joinWith(['teamMembers as team_member' => static function (ActiveQuery $query) use ($lastTeamMembers) {
                return $query
                    ->andWhere([
                        'team_member.id' => $lastTeamMembers,
                    ]);
            }]);

        $smenDataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $id]);
        }

        $modelMember = new SportMemberAR();

        $smen = SmanAR::getSmen();
        $positions = ArrayHelper::map(SportPositionAR::getPositionsByTeam($model->id), 'id', 'name');

        return $this->render('update', [
            'model' => $model,
            'sports' => $sports,
            'leaDataProvider' => $leaDataProvider,
            'availableLeagues' => $availableLeagues,
            'smenDataProvider' => $smenDataProvider,
            'modelMember' => $modelMember,
            'smen' => $smen,
            'positions' => $positions,
        ]);
    }

    /**
     * @param string $action
     * @return array
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionAttachLeague(string $action = 'attach')
    {
        $id = (int)Yii::$app->request->post('id');
        $child_id = (int)Yii::$app->request->post('child_id');

        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = ['success' => true, 'message' => 'Лига успешно добавлена в команду'];

        /**
         * @var SportTeamFM $model
         * @var SportLeagueAR $league
         */
        $model = SportTeamFM::findOne($id);
        $league = SportLeagueAR::findOne($child_id);

        $teamAttach = Many2Many::find()
            ->andWhere([
                'id_right' => $child_id,
                'id_left' => $id,
                'rel_kind' => Many2Many::TYPE_TEAM_LEAGUE,
            ])
            ->exists();

        if (!$model || !$league) {
            $response['success'] = false;
            $response['message'] = 'Ошибка прикрепления лиги к команде';

        } elseif ($action === 'attach' && !$teamAttach) {
            $model->link('leagues', $league, ['rel_kind' => Many2Many::TYPE_TEAM_LEAGUE]);

        } elseif ($action === 'detach' && $teamAttach) {
            $link = Many2Many::findOne([
                'id_right' => $child_id,
                'id_left' => $id,
                'rel_kind' => Many2Many::TYPE_TEAM_LEAGUE,
            ]);

            if ($link) {
                $link->delete();
                $response['message'] = 'Лига успешно удалена с команды';
            }
        }

        return $response;
    }

    /**
     * @return array|Response
     * @throws InvalidConfigException
     */
    public function actionCreateMember()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $error_message = ['success' => false, 'message' => 'Ошибка при создании позиции!'];

        if (Yii::$app->request->isPost) {

            $model = new SportMemberAR();
            if ($model->load(Yii::$app->request->post())) {

                $doing = ChangeTeamMemberDoing::new();
                $result = $doing->doIt([
                    'model' => $model,
                ]);

                if ($result) {
                    return ['success' => true, 'message' => 'Позиция успешно создана!'];
                }

                $error_message['message'] = $doing->errors;
            }
        }

        return $error_message;
    }

    /**
     * @return array|Response
     * @throws Throwable
     */
    public function actionDropMember()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $error_message = ['success' => false, 'message' => 'Ошибка при создании позиции!'];

        if (Yii::$app->request->isPost) {
            $id = (int)Yii::$app->request->post('id');

            $sman = SmanAR::findOne(['id' => $id]);

            if ($sman) {
                $model = $sman->lastTeamMember;

                if ($model) {
                    $doing = ChangeTeamMemberDoing::new();
                    $result = $doing->unsetRules(['checkPrevPosition'])
                        ->doIt([
                            'model' => $model,
                        ]);

                    if ($result) {
                        return ['success' => true, 'message' => 'Позиция успешно создана!'];
                    }
                }
            }
        }

        return $error_message;
    }

    /**
     * @param SportTeamFM $model
     * @return array
     * @throws InvalidConfigException
     */
    private function getAvailableLeagues($model): array
    {
        $myLeagueIds = $model->getLeagues()
            ->select(['id'])
            ->asArray()
            ->column();

        return SportLeagueAR::find()
            ->asArray()
            ->select(['r_name'])
            ->whereSport($model->sport_id)
            ->andWhere(['NOT IN', 'id', $myLeagueIds])
            ->indexBy('id')
            ->orderBy('r_name ASC')
            ->column();
    }
}
