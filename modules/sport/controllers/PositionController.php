<?php

namespace common\modules\sport\controllers;

use backend\controllers\AdminController;
use backend\controllers\DictCRUDTrait;
use common\components\S3Kind;
use common\modules\sport\models\SportPositionAR;
use yii\base\Exception;
use yii\data\ActiveDataProvider;

/**
 * PositionController implements the CRUD actions for SportPositionAR model.
 */
class PositionController extends AdminController
{
    use DictCRUDTrait;

    protected $modelClass = SportPositionAR::class;
    protected $kind = S3Kind::KIND_POSITION;

    /**
     * Lists all SportPositionAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SportPositionAR::find()->andWhere(['>', 'id', 0]),
        ]);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return void
     * @throws Exception
     */
    public function actionCreate()
    {
        throw new Exception('Не реализовано!!!');
    }

    /**
     * @return void
     * @throws Exception
     */
    public function actionUpdate()
    {
        throw new Exception('Не реализовано!!!');
    }

    /**
     * @return void
     * @throws Exception
     */
    public function actionDelete()
    {
        throw new Exception('Не реализовано!!!');
    }
}
