<?php

namespace common\modules\sport\controllers;

use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\modules\sport\models\SportAR;
use Exception;
use yii\data\ActiveDataProvider;

/**
 * SportController implements the CRUD actions for SportAR model.
 */
class SportController extends AdminController
{
    use CRUDTrait;

    public $modelClass = SportAR::class;

    /**
     * Lists all SportAR models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SportAR::find()->andWhere(['>', 'id', 0]),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return void
     * @throws Exception
     */
    public function actionCreate()
    {
        throw new Exception('Не реализовано!!!');
    }

    /**
     * @return void
     * @throws Exception
     */
    public function actionUpdate()
    {
        throw new Exception('Не реализовано!!!');
    }

    /**
     * @return void
     * @throws Exception
     */
    public function actionDelete()
    {
        throw new Exception('Не реализовано!!!');
    }
}
