<?php

namespace common\modules\sport\controllers;

use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SmanAS;
use common\modules\sport\forms\SmanFM;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportMemberAR;
use common\modules\sport\models\SportPositionAR;
use common\modules\sport\forms\SportTeamFM;
use Yii;
use yii\base\ExitException;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * AdminController implements the CRUD actions for SmanFM model.
 */
class SmanController extends AdminController
{
    use CRUDTrait;

    protected $modelClass = SmanFM::class;

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SmanAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $sports = SportAR::getAsOptsList();
        $positions = SportPositionAR::getPositions();

        $teams = SportTeamFM::getTeams();

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'teams' => $teams,
            'sports' => $sports,
            'positions' => $positions,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws ExitException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var SmanFM $model */
        $model = SmanFM::findOrFail($id);
        $teamMemberProvider = new ActiveDataProvider([
            'query' => $model->getTeamMembers(),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        $teams = SportTeamFM::getTeams();
        $modelMember = $model->lastTeamMember;
        $modelMember = $modelMember ?: new SportMemberAR();
        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно обновлена');

            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelMember' => $modelMember,
            'teams' => $teams,
            'teamMemberProvider' => $teamMemberProvider,
        ]);
    }

    /**
     * @param $sman_id
     * @return void
     */
    public function actionCreateMember($sman_id)
    {
//        Yii::$app->response->format = Response::FORMAT_JSON;
//        $error_message = ['success' => false, 'message' => 'Ошибка при создании позиции!'];
//        $sman = SmanFM::findOne($sman_id);
//
//        if (!$sman) {
//            return $error_message;
//        }
//
//        $model = new SportMemberAR();
//
//        if ($model->load(Yii::$app->request->post())) {
//            $model->sman_id = $sman_id;
//
//            $doing = ChangeTeamMemberDoing::new();
//            $result = $doing->doIt([
//                'model' => $model,
//            ]);
//
//            if ($result) {
//                return ['success' => true, 'message' => 'Позиция успешно создана!'];
//            }
//        }
//
//        return $error_message;
    }

    /**
     * @param null $id
     * @return array
     */
    public function actionAjaxPosition($id = null): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $selected = null;
        $team_id = Yii::$app->request->post('depdrop_all_params')['team-id'];
        $positions = SportPositionAR::getPositionsByTeam($team_id);

        if ($id) {
            $model = SmanFM::findOne($id);
            if ($model) {
                $lastTeamMember = $model->lastTeamMember;
                if ($lastTeamMember) {
                    $selected = $lastTeamMember->pos_id;
                }
            }
        }

        return ['output' => $positions, 'selected' => $selected];
    }
}
