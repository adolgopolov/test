<?php

namespace common\modules\sport\controllers;

use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SportMemberAS;
use common\modules\sport\models\SportMemberAR;
use Exception;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * MemberController implements the CRUD actions for SportMemberAR model.
 */
class MemberController extends AdminController
{
    use CRUDTrait;

    public $modelClass = SportMemberAR::class;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SportMemberAR models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SportMemberAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Url::remember();
        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function actionCreate()
    {
        throw new Exception('Не реализовано!!!');
    }

    /**
     * @return void
     * @throws Exception
     */
    public function actionUpdate()
    {
        throw new Exception('Не реализовано!!!');
    }

    /**
     * @return void
     * @throws Exception
     */
    public function actionDelete()
    {
        throw new Exception('Не реализовано!!!');
    }
}
