<?php

namespace common\modules\sport\controllers;

use backend\controllers\AdminController;
use backend\controllers\DictCRUDTrait;
use common\components\S3Kind;
use common\modules\sport\models\SportFactAR;
use yii\base\Exception;
use yii\data\ActiveDataProvider;

/**
 * FactController implements the CRUD actions for SportFactAR model.
 */
class FactController extends AdminController
{
    use DictCRUDTrait;

    protected $modelClass = SportFactAR::class;
    protected $kind = S3Kind::KIND_FACT;

    /**
     * Lists all SportFactAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SportFactAR::find(),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @return void
     * @throws Exception
     */
    public function actionCreate()
    {
        throw new Exception('Не реализовано!!!');
    }

    /**
     * @return void
     * @throws Exception
     */
    public function actionUpdate()
    {
        throw new Exception('Не реализовано!!!');
    }

    /**
     * @return void
     * @throws Exception
     */
    public function actionDelete()
    {
        throw new Exception('Не реализовано!!!');
    }
}
