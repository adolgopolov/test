<?php

namespace common\modules\sport\controllers;

use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\models\Many2Many;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SportLeagueAS;
use common\modules\sport\forms\SportLeagueFM;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportTeamAQ;
use common\modules\sport\models\SportTeamAR;
use Throwable;

/**
 * LeagueController implements the CRUD actions for SportLeagueFM model.
 */
class LeagueController extends AdminController
{
    use CRUDTrait;

    protected $modelClass = SportLeagueFM::class;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'attach-team' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all SportLeagueFM models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SportLeagueAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $sports = SportAR::getAsOptsList();

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'sports' => $sports,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws Yii\web\NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionView($id)
    {
        /** @var SportLeagueFM $model */
        $model = SportLeagueFM::findOrFail($id);

        $teamDataProvider = new ActiveDataProvider([
            'query' => $model->getTeams(),
        ]);

        return $this->render('view', [
            'model' => $model,
            'teamDataProvider' => $teamDataProvider,
        ]);
    }

    /**
     * Creates a new SportLeagueFM model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws Yii\base\ExitException
     */
    public function actionCreate()
    {
        $model = new SportLeagueFM();
        $sports = SportAR::getAsOptsList();
        $statusList = SportLeagueAR::getStatusList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'sports' => $sports,
            'statusList' => $statusList,
        ]);
    }

    /**
     * Updates an existing SportLeagueFM model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws Yii\base\ExitException
     * @throws InvalidConfigException
     * @throws Yii\web\NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        /** @var SportLeagueFM $model */
        $model = SportLeagueFM::findOrFail($id);
        $sports = SportAR::getAsOptsList();
        $statusList = SportLeagueAR::getStatusList();

        $teamsProvider = new ActiveDataProvider([
            'query' => $model->getTeams(),
        ]);

        $availableTeams = $this->getAvailableTeams($model)
            ->asArray()
            ->select(['r_name'])
            ->indexBy('id')
            ->orderBy('r_name ASC')
            ->column();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('update', [
            'model' => $model,
            'sports' => $sports,
            'teamsProvider' => $teamsProvider,
            'availableTeams' => $availableTeams,
            'statusList' => $statusList,
        ]);
    }

    /**
     * Attach team to league
     *
     * @param string $action
     * @return array
     * @throws Throwable
     * @throws Yii\db\StaleObjectException
     */
    public function actionAttachTeam(string $action = 'attach')
    {
        $id = Yii::$app->request->post('id');
        $child_id = Yii::$app->request->post('child_id');

        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = ['success' => true, 'message' => 'Команда успешно добавлена в лигу'];

        $model = SportLeagueFM::findOrFail($id);
        $team = SportTeamAR::findOrFail($child_id);

        $teamAttach = Many2Many::find()
            ->andWhere([
                'id_right' => $id,
                'id_left' => $child_id,
                'rel_kind' => Many2Many::TYPE_TEAM_LEAGUE,
            ])
            ->exists();

        if (!$model || !$team) {
            $response['success'] = false;
            $response['message'] = 'Ошибка прикрепления команды к лиге';

        } else if ($action === 'attach' && !$teamAttach) {
            $model->link('teams', $team, ['rel_kind' => Many2Many::TYPE_TEAM_LEAGUE]); // FRAGILE  $model->attachTeam($team)

        } else if ($action === 'detach' && $teamAttach) {
            $link = Many2Many::findOne(['id_right' => $id, 'id_left' => $child_id, 'rel_kind' => Many2Many::TYPE_TEAM_LEAGUE]);  // FRAGILE  $model->detachTeam($team)
            if ($link) {
                $link->delete();
                $response['message'] = 'Команда успешно удалена с лиги';
            }
        }

        return $response;
    }

    /**
     * @param SportLeagueFM $model
     * @return SportTeamAQ
     * @throws InvalidConfigException
     */
    private function getAvailableTeams(SportLeagueFM $model)
    {
        $myTeamIds = $model->getTeams()->select('id')->asArray()->column();

        return SportTeamAR::find()->andWhere(['sport_id' => $model->sport_id])
            ->andWhere(['NOT IN', 'id', $myTeamIds]);
    }
}
