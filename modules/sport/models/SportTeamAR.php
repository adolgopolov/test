<?php

namespace common\modules\sport\models;

use Yii;
use yii\db\ActiveQuery;
use backend\modules\xitimport\models\XitBufferAR;
use common\components\FindOrFailTrait;
use common\components\S3DictionaryAQ;
use common\components\S3Kind;
use common\components\S3Core;
use common\extensions\ExtInfoAR;
use common\models\Many2Many;
use common\modules\game\models\GameTouMatchAR;
use common\modules\game\models\SportMatchAR;
use common\modules\sount\models\SouTeamAR;
use common\traits\FieldTrait;
use common\validators\JsonValidator;

/**
 * This is the model class for table "{{%team}}".
 *
 * @property int $id
 * @property int $sport_id
 * @property string $r_name
 * @property string $r_icon
 * @property string $team_jersey
 * @property array $visual
 * @property array $ext_info
 *
 * @property SportLeagueAR[] $leagues
 * @property SmanAR[] $smen
 * @property array $title
 * @property array $colors
 * @property SportAR $sport
 * @property XitBufferAR $xitBuffer
 * @property SouTeamAR $souTeam
 */
class SportTeamAR extends ExtInfoAR
{
    use FindOrFailTrait, FieldTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%team}}';
    }

    /**
     * @return int
     */
    public static function kind()
    {
        return S3Kind::KIND_TEAM;
    }

    /**
     * @return array
     */
    protected function jsonFields(): array
    {
        return [
            'title',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sport_id'], 'integer'],
            [['r_name', 'sport_id'], 'required'],
            [['ext_info'], JsonValidator::class],
            [['colors'], JsonValidator::class],
            [['r_name'], 'string', 'max' => 250],
            [['r_icon', 'team_jersey'], 'string', 'max' => 90],
        ];
    }

    /**
     * @return array
     */
    protected function visualAttribute(): array
    {
        return [
            'colors',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sport_id' => 'Спорт',
            'r_name' => 'Название',
            'r_icon' => 'Лого',
            'team_jersey' => 'Изображение футболки',
            'ext_info' => 'Ext Info',
            'title.rus' => 'Название(rus)',
            'title.rus_short' => 'Короткое название(rus)',
            'title.eng' => 'Название(eng)',
            'title.eng_short' => 'Короткое название(eng)',
            'colors' => 'Цвета команды',
            'color' => 'Цвет команд',
        ];
    }

    /**
     * @param $name
     * @return array|mixed|null
     */
    public function __get($name)
    {
        if (in_array($name, $this->visualAttribute(), true)) {
            $attr = $this->visual;

            return $attr[$name] ?? [];
        }

        return parent::__get($name);
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if (in_array($name, $this->visualAttribute(), true)) {
            $attr = $this->visual;
            if (empty($value)) {
                unset($attr[$name]);
            } else {
                $attr[$name] = $value;
            }

            $attr = !empty($attr) ? $attr : null;
            parent::__set('visual', $attr);

            return;
        }

        parent::__set($name, $value);
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        if (S3Core::isAllowedDelete($this) && parent::beforeDelete()) {
            foreach ($this->smen as $sman) {
                $sman->delete();
            }
            SportMemberAR::deleteAll(['team_id' => $this->id]);

            $match = SportMatchAR::tableName();
            $touMatch = GameTouMatchAR::tableName();

            Yii::$app->db->createCommand("
                DELETE `m`, `mt` FROM $match `m`
                LEFT JOIN $touMatch `mt` ON `mt`.`match_id`= `m`.`id`
                WHERE `m`.`team_owner` = $this->id OR `m`.`team_guest` = $this->id;
            ")->execute();

            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     *
     * @return SportTeamAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SportTeamAQ(static::class);
    }

    /**
     * @return S3DictionaryAQ|\yii\db\ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(SportAR::class, ['id' => 'sport_id']);
    }

    /**
     * @return SportLeagueAQ|\yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getLeagues()
    {
        return $this->hasMany(SportLeagueAR::class, ['id' => 'id_right'])
            ->viaTable(
                Many2Many::tableName(),
                ['id_left' => 'id'],
                static function ($query) {
                    /** @var ActiveQuery $query */
                    $query->andOnCondition(['rel_kind' => Many2Many::TYPE_TEAM_LEAGUE]);
                }
            );
    }

    /**
     * @param SportLeagueAR $model
     * @throws \Exception
     */
    public function unlinkLeague(SportLeagueAR $model)
    {
        Many2Many::deleteAll([
            'rel_kind' => Many2Many::TYPE_TEAM_LEAGUE,
            'id_right' => $model->id,
            'id_left' => $this->id,

        ]);

    }

    /**
     * @param SportLeagueAR $model
     * @return bool
     */
    public function linkLeague(SportLeagueAR $model)
    {
        $attr = [
            'rel_kind' => Many2Many::TYPE_TEAM_LEAGUE,
            'id_right' => $model->id,
            'id_left' => $this->id,
        ];

        $many2many = Many2Many::find()
            ->where($attr)
            ->exists();

        if ($many2many) {
            return true;
        }

        $many2many = new Many2Many($attr);

        return $many2many->save();
    }

    /**
     * @return SmanAQ|ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getSmen($date = null)
    {
        // FIXME FAN-720 Множественность команд спортсмена и рефактор lastTeamMember
        // return Smen::find()->whereTeam($this)->actual($date)
        return $this->hasMany(SmanAR::class, ['id' => 'sman_id'])
            ->viaTable(SportMemberAR::tableName(), ['team_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getXitBuffer()
    {
        return $this->hasOne(XitBufferAR::class, ['our_id' => 'id'])
            ->andWhere(['ent_kind' => S3Kind::KIND_TEAM]);
    }

    /**
     * @return ActiveQuery
     */
    public function getSouTeam()
    {
        return $this->hasOne(SouTeamAR::class, ['team_our_id' => 'id']);
    }
}
