<?php

namespace common\modules\sport\models;

use Yii;

/**
 * Description of SportProvider
 *
 * @author Skynin <sohay_ua@yahoo.com>
 */
class SportProvider extends yii\base\Component
{
    /**
     *
     * @param array $opts
     * @return SmanAR
     */
    function createSman($opts = null) : SmanAR
    {
        return Yii::createObject(SmanAR::class);
    }

    /**
     *
     * @param callable $funcQuery
     * @param array $opts
     * @return SmanAR[]|SmanDTO[]
     *
     */
    function listSman(callable $funcQuery = null, $opts = null) : ?array
    {
        $aq = SmanAR::find();
        if (!empty($funcQuery)) $aq = $funcQuery($aq);

        return $aq->all();
    }
}

/*
Example:

    $smanQuery = function(SmanAQ $aq) : SmanAQ {
        $aq->where ... ...

        return $aq;
    }

    $listSmen = S3Core::mSport()->provider->listSman($smanQuery, ['mode' => 'fooMode']);
 */
