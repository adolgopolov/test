<?php

namespace common\modules\sport\models;

/**
 * @author Skynin <sohay_ua@yahoo.com>
 * created: 27-Mar-2019
 */
class SoccerFactAR extends SportFactAR
{
    public const FACT_SCORED_GOAL = 'soccer.scored_goal';
    public const FACT_MISSED_GOAL = 'soccer.missed_goal';
    public const FACT_GOALPASS = 'soccer.goalpass';
    public const FACT_AUTOGOAL = 'soccer.autogoal';
    public const FACT_SCORED_PENALTY = 'soccer.scored_penalty';
    public const FACT_SAVE_PENALTY = 'soccer.save_penalty';
    public const FACT_YELLOW_CARD = 'soccer.yellow_card';
    public const FACT_RED_CARD = 'soccer.red_card';
    public const FACT_SHOT_ON_TARGET = 'soccer.shot_on_target';
    public const FACT_FOUL_COMMITTED = 'soccer.foul_committed';
    public const FACT_TACKLE_WON = 'soccer.tackle_won';
    public const FACT_BLOCKED_SHOT = 'soccer.blocked_shot';
    public const FACT_KEEPER_SAVE = 'soccer.keeper_save';

    public const FORM_GOAL = 'form-goal';
    public const FORM_SHOT_ON_TARGET = 'form-shot-on-target';
    public const FORM_FOUL = 'form-foul';
    public const FORM_TACKLE_WON = 'form-tackle-won';
    public const FORM_OTHER = 'form-other';

    /**
     * @return array
     */
    public static function getLinks(): array
    {
        return [
            [
                'name' => 'Гол',
                'form' => self::FORM_GOAL,
                'factName' => self::FORM_GOAL
            ],
            [
                'name' => 'Удар в створ',
                'form' => self::FORM_SHOT_ON_TARGET,
                'factName' => self::FACT_SHOT_ON_TARGET
            ],
            [
                'name' => 'Фол',
                'form' => self::FORM_FOUL,
                'factName' => self::FACT_FOUL_COMMITTED
            ],
            [
                'name' => 'Красная карточка',
                'form' => self::FORM_OTHER,
                'factName' => self::FACT_RED_CARD
            ],
            [
                'name' => 'Жёлтая карточка',
                'form' => self::FORM_OTHER,
                'factName' => self::FACT_YELLOW_CARD
            ],
            [
                'name' => 'Успешные отборы',
                'form' => self::FORM_TACKLE_WON,
                'factName' => self::FACT_TACKLE_WON
            ],
            [
                'name' => 'Отражённый пенальти',
                'form' => self::FORM_OTHER,
                'factName' => self::FACT_SAVE_PENALTY,
                'urlArgs' => '&goalkeepers=true'],
            [
                'name' => 'Сэйв',
                'form' => self::FORM_OTHER,
                'factName' => self::FACT_KEEPER_SAVE,
                'urlArgs' => '&goalkeepers=true'
            ],
        ];
    }
}
