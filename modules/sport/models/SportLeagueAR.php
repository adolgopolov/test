<?php

namespace common\modules\sport\models;

use common\components\FindOrFailTrait;
use common\components\S3DictionaryAQ;
use common\components\S3Kind;
use common\extensions\ExtInfoAR;
use common\modules\game\models\SportMatchAR;
use common\traits\FieldTrait;
use common\traits\StatusARTrait;
use common\validators\JsonValidator;
use Exception;

/**
 * This is the model class for table "ss_league".
 *
 * @property int $id
 * @property int $sport_id
 * @property string $r_name
 * @property string $r_icon
 * @property string $country
 * @property array $ext_info
 *
 * @property SportAR $sport
 * @property SportTeamAR[] $teams
 */
class SportLeagueAR extends ExtInfoAR
{
    use FindOrFailTrait, FieldTrait, StatusARTrait;

    public const STATUS_MAIN = 64;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%league}}';
    }

    /**
     * @return int
     */
    public static function kind()
    {
        return S3Kind::KIND_LEAGUE;
    }


    /**
     * @return array
     */
    protected function jsonFields(): array
    {
        return array_merge(parent::jsonFields(), [
            'title',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sport_id', 'r_status'], 'integer'],
            [['r_name', 'sport_id'], 'required'],
            [['r_name'], 'string', 'max' => 250],
            [['r_icon'], 'string', 'max' => 90],
            [['country'], 'string', 'max' => 30],
            [['ext_info'], JsonValidator::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sport_id' => 'Спорт',
            'r_status' => 'Статус',
            'r_name' => 'Название',
            'r_icon' => 'Лого',
            'country' => 'Код страны',
            'ext_info' => 'Ext Info',
        ];
    }

    public static function find(): SportLeagueAQ
    {
        return (new SportLeagueAQ(static::class))->cache(1);
    }

    /**
     * @return array
     */
    public static function getStatusList(): array
    {
        return [
            self::STATUS_MAIN => 'Основная',
        ];
    }

    /**
     * @return string
     */
    public function getStatusName(): string
    {
        return self::getStatusList()[$this->r_status] ?? 'Неосновная';
    }

    /**
     * @return S3DictionaryAQ|\yii\db\ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(SportAR::class, ['id' => 'sport_id']);
    }

    /**
     * @param SportTeamAR $teams
     * @throws Exception
     */
    public function attachTeam(SportTeamAR $teams)
    {
        // $model->link('teams', $team, ['rel_kind' => Many2Many::TYPE_TEAM_LEAGUE]);
        throw new Exception('Not implemented');
    }

    /**
     * @param SportTeamAR $teams
     * @throws Exception
     */
    public function detachTeam(SportTeamAR $teams)
    {
        /*
            $link = Many2Many::findOne(['id_right' => $id, 'id_left' => $child_id, 'rel_kind' => Many2Many::TYPE_TEAM_LEAGUE]);
            if ($link) {
                $link->delete();
                return true
            }
            return false
         */
        throw new Exception('Not implemented');
    }

    /**
     * @param string $indexBy
     * @return array
     */
    public static function getLeaList($indexBy = 'id'): array
    {
        return self::find()
            ->asArray()
            ->select(['r_name'])
            ->indexBy($indexBy)
            ->orderBy('r_name')
            ->column();
    }

    /**
     * @param $sportIds
     * @return array|\common\modules\goods\models\CardAR[]|SportLeagueAR[]|SportMatchAR[]|\yii\db\ActiveRecord[]
     */
    public static function getListBySport($sportIds)
    {
        return self::find()
            ->select('id, r_name as name')
            ->where(['sport_id' => $sportIds])
            ->orderBy('r_name ASC')
            ->asArray()
            ->all();
    }
}
