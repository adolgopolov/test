<?php

namespace common\modules\sport\models;

use common\components\S3Core;
use backend\modules\xitimport\models\XitBufferAR;
use common\components\FindOrFailTrait;
use common\components\S3Kind;
use common\extensions\ExtInfoAR;
use common\modules\game\models\GameLineupCellAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\game\models\SportMatchAR;
use common\traits\FieldTrait;
use common\validators\JsonValidator;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%people}}".
 *
 * @property int $id
 * @property string $r_name
 * @property string $r_icon
 * @property-read string $sman_status виртуальное поле
 * @property string $status
 * @property array $ext_info
 * @property string $updated_at
 * @property string $power
 * @property string $nickname
 * @property string $date_of_birth
 * @property SportMemberAR $lastTeamMember
 * @property SportMatchAR $lastMatch
 * @property XitBufferAR $xitBuffer
 * @property SportAR $sport
 */
class SmanAR extends ExtInfoAR
{
    use FindOrFailTrait, FieldTrait;

    public const STATUS_OUTALL = 'outall';
    public const STATUS_TRAUMA = 'trauma';
    public const STATUS_ANY = 'any';
    public const STATUS_NOT_OUTALL = 'not_outall';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%people}}';
    }

    /**
     * @return int
     */
    public static function kind()
    {
        return S3Kind::KIND_SMAN;
    }

    /**
     * {@inheritdoc}
     */
    protected function jsonFields(): array
    {
        return array_merge(parent::jsonFields(), [
            'nickname',
            'date_of_birth',
            'power',
            'title',
            'price',
            'status',
            'date_of_birth',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_name'], 'required'],
            [['id'], 'integer'],
            [['r_name'], 'string', 'max' => 250],
            [['r_icon'], 'string', 'max' => 90],
            [['ext_info'], JsonValidator::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_name' => 'Имя',
            'r_icon' => 'Фото',
            'team' => 'Команда',
            'sman_status' => 'Статус',
            'status' => 'Статус',
            'ext_info' => 'Ext Info',
            'updated_at' => 'Последнее изменение',
            'power' => 'Сила',
            'nickname' => 'Nickname',
            'date_of_birth' => 'День рождения',
            'title.rus' => 'Имя (rus)',
            'title.eng' => 'Имя (eng)',
            'price' => 'Стоимость',
            'position' => 'Позиция',
            'x_code' => 'Буферный номер',
            'i_title' => 'Буферное имя',
            'sport_id' => 'Вид спорта',
            'lea_list' => 'Лиги команды',
            'pos_id' => 'Амплуа',
        ];
    }

    public function beforeDelete()
    {
        if (S3Core::isAllowedDelete($this) && parent::beforeDelete()) {
            GameLineupCellAR::deleteAll(['sman_id' => $this->id]);
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     * @return SmanAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SmanAQ(static::class);
    }

    /**
     * TODO логическая ошибка - спортсмен может одновременно состоять в 2 командах - нельзя использовать для определения команды в расчетах
     * @param bool $lockRead
     * @param bool $lockUpdated
     * @return array|SportMemberAR|ActiveRecord
     */
    public function getLastTeamMember($lockRead = false, $lockUpdated = false)
    {
        $query = SportMemberAR::find()
            ->andWhere(['sman_id' => $this->id])
            ->orderBy(['id' => SORT_DESC]);

        if ($lockRead) {
            if ($lockUpdated) {
                $query->forUpdate();
            } else {
                $query->forRead();
            }
        }

        return $query->one();
    }

    /**
     *  выборка по дате турнира и номерам доступных команд в турнире
     * @param GameTourneyAR $tourney
     * @param bool $lockRead
     * @param bool $lockUpdated
     * @return SportMemberAR|null
     * @deprecated 1.4
     * FIXME удалить
     * и заменить на
     * SportMemberAR::find()->whereTourney()->whereSmen()
     */
    public function getTouTeamMember(GameTourneyAR $tourney ,$lockRead = false, $lockUpdated = false)
    {

        $query = SportMemberAR::find()
            ->byDate($tourney->start_date)
            ->andWhere(['sman_id' => $this->id]);

        if(!empty($tourney->teamIds)){
            $query->byTeam($tourney->teamIds);
        }

        if ($lockRead) {
            if ($lockUpdated) {
                $query->forUpdate();
            } else {
                $query->forRead();
            }
        }

        return $query->one();
    }

    /**
     * @return array
     */
    public static function getSmen(): array
    {
        $smen = self::find()
            ->asArray()
            ->all();

        $list = [];
        foreach ($smen as $sman) {
            $list[$sman['id']] = $sman['r_name'];
        }

        return $list;
    }

    /**
     * @return ActiveQuery
     */
    public function getTeamMembers()
    {
        return $this->hasMany(SportMemberAR::class, ['sman_id' => 'id']);
    }

    /**
     * @param bool $lockRead
     * @param bool $lockUpdated
     * @return array|SportMatchAR|null|ActiveRecord
     */
    public function getLastMatch($lockRead = false, $lockUpdated = false)
    {
        $lastTeamMember = $this->getLastTeamMember($lockRead, $lockUpdated);

        if ($lastTeamMember) {
            $query = SportMatchAR::find()
                ->where(['team_owner' => $lastTeamMember->team_id])
                ->orWhere(['team_guest' => $lastTeamMember->team_id])
                ->orderBy('DATE(start_date) DESC');

            if ($lockRead) {
                if ($lockUpdated) {
                    $query->forUpdate();
                } else {
                    $query->forRead();
                }
            }

            return $query->one();
        }

        return null;
    }

    /**
     * @return array
     */
    public static function getSmenList()
    {
        return self::find()
            ->asArray()
            ->select(['r_name'])
            ->indexBy('id')
            ->column();
    }

    /**
     * @return ActiveQuery
     */
    public function getXitBuffer()
    {
        return $this->hasOne(XitBufferAR::class, ['our_id' => 'id'])
            ->andWhere(['ent_kind' => S3Kind::KIND_SMAN]);
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            'outall' => 'outall',
            'trauma' => 'trauma',
        ];
    }

    /**
     * @return SportAR|null
     */
    public function getSport()
    {
        return $this->lastTeamMember->team->sport ?? null;
    }

    /**
     *
     * FIXME должен быть удален и везде заменен на
     *
     * SmanAR::find()->whereTeam($teams)->actualDate($time)
     *
     * actualDate взять из UserMoneyAccAQ или actualDate вынести в EaxAQ
     *
     * запрос должен возвращать всех спортсменов, которые играют в данной команде на текущий момент либо на момент указанной даты
     * @param array|int $teams
     * @param bool $returnQuery
     * @param null $time
     * @return array|SmanAQ
     */
    public static function getSmenListByTeam($teams, $returnQuery = false,$time = null)
    {
        if($time === null) {
            $time = date('Y-m-d H:i:s');
        }
        $queryMember = SportMemberAR::find()
            ->select(['team_id'])
            ->andWhere('sman_id = sman.id')
            ->andWhere(['team_id'=>$teams])
            ->byDate($time)
            ->orderBy(['id' => SORT_DESC])
            ->limit(1);

        $query = self::find()
            ->alias('sman')
            ->select(['sman.*', '(' . $queryMember->createCommand()->rawSql . ') as team_id'])
            ->andHaving(['team_id' => $teams]);

        return $returnQuery ? $query : $query->all();
    }
}
