<?php

namespace common\modules\sport\models;

use common\modules\goods\models\CardAR;
use common\modules\score\models\CalcItemAR;
use common\components\{S3DictionaryAQ, S3DictionaryAR, S3Kind};
use common\validators\JsonValidator;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 *  @property string $xcode
 *
 * @author Skynin <sohay_ua@yahoo.com>
 * created: 27-Mar-2019
 */
class SportPositionAR extends S3DictionaryAR
{
    public const POSITION_SOCCER_GOALKEEPER_ID = 10;
    public const POSITION_SOCCER_FORWARD_ID = 11;
    public const POSITIOIN_SOCCER_BACK_ID = 12;
    public const POSITION_SOCCER_HALF_BACK_ID = 13;

    public const POSITION_HOCKEY_GOALKEEPER = 20;
    public const POSITION_HOCKEY_FORWARD = 21;
    public const POSITION_HOCKEY_BACK = 22;

    public const POSITION_BASKET_SMALL_FORWARD = 30;
    public const POSITION_BASKET_POWER_FORWARD = 31;
    public const POSITION_BASKET_SHOOTING_GUARD = 32;
    public const POSITION_BASKET_CENTER = 33;
    public const POSITION_BASKET_POINT_GUARD = 34;

    /**
     * @return int|null
     */
    public static function kind()
    {
        return S3Kind::KIND_POSITION;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'ent_code'  => 'Код',
            'ent_value' => 'Значение'
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'ent_kind', 'ent_value'], 'integer'],
            [['r_name', 'ent_code'], 'string'],
            ['id', 'unique', 'targetAttribute' => ['id', 'ent_kind']],
            [['ext_info'], JsonValidator::class],
        ];
    }

    protected function jsonFields(): array
    {
        return ['xcode'];
    }

    /**
     * @return S3DictionaryAQ
     * @deprecated 1.4
     * FIXME вынести в SportPositionAQ::actual
     */
    public static function queryPosition()
    {
        return self::find()
            ->asArray()
            ->andWhere(['>','id', 0]);
    }

    /**
     * @param $team_id
     * @return array|SportPositionAR[]|ActiveRecord[]
     * @deprecated 1.4
     * FIXME вынести в SportPositionAR::find()->select(['id', 'r_name as name'])->whereTeam
     */
    public static function getPositionsByTeam($team_id)
    {
        /** @var SportTeamAR $team */
        $team = SportTeamAR::findOne($team_id);

        return self::queryPosition()
            ->select(['id', 'r_name as name'])
            ->andWhere([
                'OR',
                ['ent_value' => $team->sport_id],
                ['is', 'ent_value', new Expression('null')],
            ])
            ->all();
    }

    /**
     * @return array
     * @deprecated 1.4
     * FIXME переделать в SportPositionAR::find(), а $pos['r_name'] . '(' . $sport . ')' вынести в array_map
     */
    public static function getPositions()
    {
        $positions = self::queryPosition()->all();

        $sports = SportAR::getAsOptsList();

        $list = [];
        foreach ($positions as $pos) {
            $sport = 'Любой вид спорта';
            if ($pos['ent_value']) {
                $sport = $sports[$pos['ent_value']];
            }

            $list[$pos['id']] = $pos['r_name'] . '(' . $sport . ')';
        }

        return $list;
    }

    /**
     * @param SmanAR $smanModel
     * @param int $sport_id
     * @return array|CardAR[]|CalcItemAR[]|SportPositionAR[]|ActiveRecord[]
     * @deprecated 1.4
     * FIXME переделать
     * в SportPositionAR::find()->whereSman()
     * в SportPositionAR::find()->whereSport()
     */
    public static function findBySmanOrSport($smanModel, $sport_id)
    {
        $query = self::find();

        if ($smanModel) {
            $query->andWhere(['id' => $smanModel->lastTeamMember->position->id]);
        } else {
            $query->andWhere(['ent_value' => $sport_id]);
        }

        return $query->indexBy('ent_code')
            ->all();
    }

    /**
     * @param $sport_id
     * @return array
     * @deprecated 1.4
     * FIXME убрать.
     * Cases - непонятное название
     * Вынести в SportPositionAQ::whereОсмысленноеНазвание()
     */
    public static function getPositionCases($sport_id)
    {
        switch ($sport_id) {
            case SportAR::SOCCER:
                return [
                    self::POSITION_SOCCER_GOALKEEPER_ID,
                    self::POSITIOIN_SOCCER_BACK_ID,
                    self::POSITION_SOCCER_HALF_BACK_ID,
                    self::POSITION_SOCCER_FORWARD_ID,
                ];
            case SportAR::HOCKEY:
                return [
                    self::POSITION_HOCKEY_GOALKEEPER,
                    self::POSITION_HOCKEY_BACK,
                    self::POSITION_HOCKEY_FORWARD,
                ];
            case SportAR::BASKET:
                return [
                    self::POSITION_BASKET_POINT_GUARD,
                    self::POSITION_BASKET_SHOOTING_GUARD,
                    self::POSITION_BASKET_SMALL_FORWARD,
                    self::POSITION_BASKET_POWER_FORWARD,
                    self::POSITION_BASKET_CENTER,
                ];
            default:
                return [];
        }
    }
}
