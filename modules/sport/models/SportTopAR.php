<?php

namespace common\modules\sport\models;

use common\components\{S3DictionaryAR, S3Kind};
use common\validators\JsonValidator;

/**
 * @author Skynin <sohay_ua@yahoo.com>
 * created: 27-Mar-2019
 */
class SportTopAR extends SportAR
{

    public function attributeLabels()
    {
        return [
            'id' => 'Cпорт',
            'r_name'    => 'Название',
            'sman_id' => 'Номер спортсмена',
            'sman_name' => 'Имя спортсмена',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SportTopAQ the active query used by this AR class.
     */
    public static function find()
    {
        return (new SportTopAQ(static::class))
            ->from(['s' => self::tableName()])
            ->select('s.id, s.r_name, sm.id sman_id, sm.r_name sman_name')
            ->where(['s.ent_kind' => self::kind()]);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        throw new \Exception('SportTopAR read only');
    }
}
