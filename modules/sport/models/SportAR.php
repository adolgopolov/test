<?php

namespace common\modules\sport\models;

use common\components\{S3DictionaryAR, S3Kind};
use common\validators\JsonValidator;

/**
 * Class SportAR
 * @package common\modules\sport\models
 */
class SportAR extends S3DictionaryAR
{
    /*
     * !ВНИМАНИЕ должны совпадать с значениями в dictionary
     */
    public const SOCCER = 1;
    public const HOCKEY = 2;
    public const BASKET = 3;
    public const CYBER_SPORT = 99;
    public const DOTA2 = 100;
    public const CSGO = 101;

    public const ANY_SPORT = null;//0;

    public static function kind()
    {
        return S3Kind::KIND_SPORT;
    }

    public static function getSportIds()
    {
        return self::find()->andWhere(['>','id', 0])->column();
    }

    /**
     * инкрементальные виды спорта, отметить в ext_info.increment, хоккей дота кс баскетбол
     * @return array
     * @deprecated 1.4
     * FIXME вынести в SportAQ с осмысленным названием
     */
    public static function getIncSportIds()
    {
        return self::find()->whereJO(['increment' => 1])->column();
    }

    public static function getAsOptsList($not_in = [self::CYBER_SPORT])
    {
        return self::find()
            ->asArray()
            ->select(['r_name'])
            ->andWhere(['NOT IN', 'id', array_merge([0], $not_in)])
            ->indexBy('id')
            ->column();
    }

    public function attributeLabels()
    {
        return [
            'ent_code'  => 'Код',
            'r_nzme'    => 'Название',
            'ent_kind'  => 'Kind',
            'ent_value' => 'Значение',
            'r_name'    => 'Название'
        ];
    }

    public function rules()
    {
        return [
            [['id', 'ent_value'], 'integer'],
            ['r_name', 'string'],
            ['id', 'unique', 'targetAttribute' => ['id', 'ent_kind']],
            [['ext_info'], JsonValidator::class],
        ];
    }

    /**
     * @param $id
     * @return bool|string
     */
    public static function getCodeById($id)
    {
        $sport = self::findOne($id);

        if (!$sport) {
            return false;
        }

        return $sport->ent_code;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(SportPositionAR::class, ['ent_value' => 'id']);
    }
}
