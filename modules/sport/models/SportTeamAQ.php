<?php

namespace common\modules\sport\models;

use common\modules\game\models\GameTouMatchAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\game\models\SportMatchAR;

/**
 * This is the ActiveQuery class for [[SportTeamAR]].
 *
 * @see SportTeamAR
 */
class SportTeamAQ extends \common\extensions\ExtAQ
{
    public function filterTourneyStatus($tou_status)
    {
        $tblTeam = $this->modelClass::tableName();
        $tblTourney = GameTourneyAR::tableName();

        $touQuery = GameTourneyAR::find()->select('id')->andWhere("$tblTourney.sport_id = $tblTeam.sport_id");
        $touQuery->filterStatus($tou_status);

        $result = $this
            ->innerJoin(SportMatchAR::tableName(). " m", "$tblTeam.id=m.team_owner OR $tblTeam.id=m.team_guest")
            ->innerJoin(GameTouMatchAR::tableName()." gtm", 'gtm.match_id=m.id')
            // ->innerJoin(GameTourneyAR::tableName()." gtou", 'gtou.tou_id=m.id')
            ->andWhere(
                ['in', 'gtm.tou_id', $touQuery]
            );

        return $result;
    }

}
