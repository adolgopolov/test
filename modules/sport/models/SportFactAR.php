<?php

namespace common\modules\sport\models;

use common\components\FindOrFailTrait;
use common\components\{
    S3DictionaryAR, S3Kind
};
use common\validators\JsonValidator;

/**
 * @author Skynin <sohay_ua@yahoo.com>
 * created: 27-Mar-2019
 *
 * @property string $type статичтические и реальные типы фактов
 */
class SportFactAR extends S3DictionaryAR
{
    use FindOrFailTrait;

    public const FACT_MATCH_START = 1;
    public const FACT_MATCH_FINISH = 9;
    public const FACT_MATCH_PRESENCE = 12;

    public const TYPE_REAL = 'real';
    public const TYPE_STATS = 'stats';

    public static function kind()
    {
        return S3Kind::KIND_FACT;
    }

    /*public function getScoringCalcs()
    {
        return $this->hasMany(MarkerCalcAR::class,['id'=>'id_left'])
            ->viaTable(Many2Many::tableName(),['id_right'=>'id'])
            ->onCondition(['rel_kind'=>Many2Many::TYPE_OCCA_SCORING]);
    }

    public function addSportScoring($score_code)
    {
        $ids = MarkerCalcAR::findByCode($score_code,'eq')->column();
        foreach ($ids as $id) {
            $rel = new Many2Many([
                'id_left' => $this->id,
                'id_right' => $id,
                'rel_kind'=>Many2Many::TYPE_OCCA_SCORING
            ]);
            $rel->save();
        }
    }

    public function clearSportScoring($score_code)
    {
        $ids = MarkerCalcAR::findByCode($score_code,'eq')->column();
        Many2Many::deleteAll([
            'id_right' => $ids,
            'rel_kind'=>Many2Many::TYPE_OCCA_SCORING
        ]);
    }*/

    public function attributeLabels()
    {
        return [
            'ent_code' => 'Код',
            'ent_value' => 'Значение',
            'type' => 'Тип',
        ];

    }

    public function rules()
    {
        return [
            [['id', 'ent_kind', 'ent_value'], 'integer'],
            [['r_name', 'ent_code'], 'string'],
            ['id', 'unique', 'targetAttribute' => ['id', 'ent_kind']],
            ['type','in', 'range' => [
                self::TYPE_REAL,
                self::TYPE_STATS,
            ]],
            [['ext_info'], JsonValidator::class],
        ];
    }

    /**
     * @param mixed|null $sport_id
     * @return array
     */
    public static function getFacts($sport_id = null): array
    {
        $query = self::find()
            ->select(['r_name'])
            ->andWhere(['>', 'id', 0])
            ->indexBy('id');

        if ($sport_id !== null) {
            $query->andWhere([
                'OR',
                ['ent_value' => $sport_id],
                ['ent_value' => null],
            ]);
        }

        return $query->column();
    }

    /**
     * @param int $sport_id
     * @return self[]
     */
    public static function getSportFacts($sport_id): array
    {
        $query = self::find();
        switch ($sport_id) {
            case SportAR::HOCKEY:
                $query->andWhere(['ent_value' => SportAR::HOCKEY]);
                $query->andWhere(['!=', 'id', 214]);

                break;
            case SportAR::BASKET:
                $andWhere = [
                    'ent_code' => [
                        'basket.scored_points',
                        'basket.goal_attempted',
                        'basket.assist',
                        'basket.rebound',
                        'basket.blocked_shot',
                        'basket.steal',
                        'basket.turnover',
                        'basket.personal_foul',
                    ],
                ];

                break;
            case SportAR::DOTA2:
                $query->andWhere(['ent_value' => [SportAR::DOTA2,SportAR::CYBER_SPORT]]);
                $query->andWhere(['!=', 'id', 9000]);


                break;
            case SportAR::CSGO:
                $andWhere = [
                    'ent_code' => [
                        'cyber.murder',
                        'cyber.death',
                        'cyber.нelp',
                        'csgo.headshot',
                    ],
                ];

                break;
            default:
                $andWhere = [
                    'OR',
                    ['ent_value' => $sport_id],
                ];

                if ($sport_id >= SportAR::CYBER_SPORT) {
                    $andWhere[] = ['ent_value' => SportAR::CYBER_SPORT];
                } else {
                    $andWhere[] = ['ent_value' => null];
                }
        }


        $query->andWhere(['NOT IN', 'id', [0, self::FACT_MATCH_START, self::FACT_MATCH_FINISH]]);
        if (!empty($andWhere)) $query->andWhere($andWhere);

        return $query->all();
    }
    /**
     * @return string
     */
    public function getType()
    {
        return $this->joGet('type',self::TYPE_REAL);
    }

    public function setType($value)
    {
        return $this->joSet('type',$value);
    }

    public function isReal()
    {
        return $this->type == self::TYPE_REAL;
    }
}
