<?php

namespace common\modules\sport\models;

use yii\base\Exception;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[SportMemberAR]].
 *
 * @see SportTeamMemberAR
 */
class SportMemberAQ extends \common\extensions\ExtAQ
{
    public function active($date = null)
    {
        $date = $date ?? date('Y-m-d');
        return $this->andWhere(['<=', 'first_day', new Expression("DATE(:date)", [':date' => $date])])
            ->andWhere(['>=', 'last_day', new Expression("DATE(:date)", [':date' => $date])]);
    }

    /**
     * @param $date
     * @return SportMemberAQ
     * @deprecated 1.4 use active
     */
    public function byDate($date)
    {
        return $this->andWhere(['<=', 'first_day', new Expression("DATE(:date)", [':date' => $date])])
            ->andWhere(['>=', 'last_day', new Expression("DATE(:date)", [':date' => $date])]);
    }

    /**
     * @param bool $desc
     * @return SportMemberAQ
     */
    public function sortByPosition($desc = false)
    {
        return $this->orderBy(['pos_id' => $desc ? SORT_DESC : SORT_ASC]);
    }

    public function onlySmenPosition()
    {
        return $this->andWhere(['>=','pos_id',10]); // FIXME magic number 10 
    }

    // FIXME rename whereTeam
    public function byTeam($teamIds)
    {
        return $this->andWhere(['team_id'=>$teamIds]);
    }

    // FIXME rename whereSman
    public function byMan($manId)
    {
        return $this->andWhere(['sman_id' => $manId]);
    }

}
