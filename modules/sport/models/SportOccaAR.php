<?php

namespace common\modules\sport\models;

use common\components\S3DictionaryAQ;
use common\extensions\ExtInfoAR;
use common\modules\game\models\SportMatchAQ;
use common\modules\game\models\SportMatchAR;
use common\validators\JsonValidator;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%occasion}}".
 *
 * @property int $id
 * @property int $match_id
 * @property int $after_start minutes after start match
 * @property bool $is_deleted
 * @property int $fact_id
 * @property int $sman_id
 * @property int $occa_amount
 * @property array $ext_info
 *
 * @property SportFactAR $fact
 * @property SmanAR $sman
 * @property SportMatchAR $match
 */
class SportOccaAR extends ExtInfoAR
{
    public const DEFAULT_OCCA_AMOUNT = 1;
    public $team;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%occasion}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_id'], 'required'],
            [['after_start'], 'required', 'when' => static function (self $model) {
                return !in_array($model->fact_id, [SportFactAR::FACT_MATCH_START, SportFactAR::FACT_MATCH_FINISH], true);
            },'on'=> ['default']],
            [['match_id', 'fact_id', 'sman_id', 'occa_amount'], 'integer'],
            [['occa_amount'],'integer','min'=>0,'when' =>  function (self $model) {
                return $model->fact->isReal();
            }],
            [['after_start'], 'integer', 'min' => 0,'on'=> ['default']],
            [['sman_id'], 'default', 'value' => 0],
            [['ext_info'], JsonValidator::class],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['delete'] = ['fact_id','sman_id','after_start','occa_amount','ext_info','match_id','id'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'match_id' => 'Матч',
            'after_start' => 'Прошло от начала',
            'is_deleted' => 'Удалено',
            'fact_id' => 'Факт',
            'sman_id' => 'Спортсмен',
            'occa_amount' => 'Количество фактов',
            'ext_info' => 'Ext Info',
            'team' => 'Команда',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SportOccaAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SportOccaAQ(static::class);
    }

    /**
     * @return void
     */
    public function markDeleted()
    {
        $this->scenario = 'delete';
        $this->fact_id = 0;
        $this->sman_id = 0;
        $this->after_start = null;
        $this->occa_amount = 0;
        $this->ext_info = null;
    }

    /**
     * @param $match
     * @return int
     */
    public static function markDeletedAll($match)
    {
        if (is_object($match)) {
            $match = $match->id;
        }

        return self::updateAll(['fact_id' => 0, 'sman_id' => 0, 'after_start' => null, 'occa_amount' => null, 'ext_info' => null], "match_id = $match");
    }

    /**
     * @return S3DictionaryAQ|ActiveQuery
     */
    public function getFact()
    {
        return $this->hasOne(SportFactAR::class, ['id' => 'fact_id']);
    }

    /**
     * @return SmanAQ|ActiveQuery
     */
    public function getSman()
    {
        return $this->hasOne(SmanAR::class, ['id' => 'sman_id']);
    }

    /**
     * @return SportMatchAQ|ActiveQuery
     */
    public function getMatch()
    {
        return $this->hasOne(SportMatchAR::class, ['id' => 'match_id']);
    }
}
