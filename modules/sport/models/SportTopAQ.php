<?php

namespace common\modules\sport\models;

use common\extensions\ExtAQ;
use common\modules\game\models\GameTouMatchAR;
use yii\db\ActiveQuery;
use common\components\S3Core;

/**
 * This is the ActiveQuery class for [[SmanAR]].
 *
 * @see SmanAR
 */
class SportTopAQ extends \common\extensions\ExtAQ
{
    public function filterSmen($sportId)
    {
        $maxDate = S3Core::MAX_DATE;

        $result = $this
            ->innerJoin(SportTeamAR::tableName(). " t", 't.sport_id=s.id')
            ->innerJoin(SportMemberAR::tableName()." tm","tm.team_id=t.id AND tm.last_day='$maxDate'")
            ->innerJoin(SmanAR::tableName()." sm","tm.sman_id=sm.id")

            ->andWhere([
                's.id' => $sportId,
            ]);

        return $result;
    }
}
