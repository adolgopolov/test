<?php

namespace common\modules\sport\models;

/**
 * This is the ActiveQuery class for [[SportOccaAR]].
 *
 * @see SportOccaAR
 */
class SportOccaAQ extends \common\extensions\ExtAQ
{
    public function isDeleted()
    {
        return $this->andWhere(['is_deleted' => 0]);
    }

    public function whereMatch($match_id)
    {
        return $this->andWhere(['match_id' => $match_id]);
    }

    public function onlyCalcOcca()
    {
        return $this->andWhere(['not', ['fact_id' => [SportFactAR::FACT_MATCH_FINISH, SportFactAR::FACT_MATCH_START]]]);
    }

    /**
     * выбрать все удаленные и  реальные события по матчу
     * если указан спортсмен то выбрать все удаленные + расчетные события спортмена по матчу
     * @param null $smanId
     * @return SportOccaAQ
     */
    public function forSummaryMode($smanId = null)
    {
        if ($smanId) {
            $query = [
                'or',
                ['is_deleted' => 1],
                ['and',
                    ['>=', 'fact_id', 50],
                    ['sman_id' => $smanId],
                ],
            ];
        } else {
            $query = [
                'or',
                ['is_deleted' => 1],
                ['>=', 'fact_id', 50],
            ];
        }

        return $this->andWhere($query);
    }

    public function existFinisOcca($match_id)
    {
        return $this->whereMatch($match_id)
            ->andWhere(['fact_id' => SportFactAR::FACT_MATCH_FINISH])
            ->exists();
    }
}
