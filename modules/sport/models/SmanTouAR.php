<?php

namespace common\modules\sport\models;

/**
 * Расширение для "турнирного спортсмена"
 *
 * @property int $tou_id
 * @property int $team_id
 * @property int $position_id
 * @property string $status
 * @property int $points
 * @property int $price
 *
 * @property SportMemberAR $lastTeamMember
 */
class SmanTouAR extends SmanAR
{
    /**
     * {@inheritdoc}
     * @return SmanAQ the active query used by this AR class.
     */
    public static function find()
    {
        // FIXME вынести в SmanTouAQ, и отдельно price - он может браться как с специальной таблицы так и из `s`.ext_info
        return (new SmanTouAQ(static::class))->select([
            's.*',
            'price' => 'coalesce(json_unquote(json_extract(`s`.ext_info, "$.price")), 0) ',
            'tou_id' => 'gtm.tou_id',
            'team_id' => 'tm.team_id',
            'pos_id' => 'tm.pos_id',
            'position' => 'coalesce(json_unquote(json_extract(`pos`.ext_info, "$.title.rus_short")), pos.r_name)',
        ])->andWhere(['tm.last_day' => '9999-12-30']); // DELETE after 14.01.2020 lastTeamMember
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        throw new \Exception('SmanTouAR read only');
    }

}
