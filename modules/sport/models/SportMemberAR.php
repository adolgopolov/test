<?php

namespace common\modules\sport\models;

use common\extensions\ExtInfoAR;
use common\validators\JsonValidator;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%team_member}}".
 *
 * @property int $id
 * @property int $team_id
 * @property int $sman_id
 * @property string $first_day
 * @property string $last_day
 * @property int $pos_id position
 * @property array $ext_info
 *
 * @property int $r_status FAN-720 Множественность команд спортсмена и рефактор lastTeamMember
 *
 * @property SportPositionAR|null $position
 * @property SmanAR $sman
 * @property SportTeamAR $team
 */
class SportMemberAR extends ExtInfoAR
{
    const MAIN_TEAM = 128; // основная команда, заполняется при изменении в ChangeTeamMemberDoing,
    // или по бизнес логике - сменили признак основной лиги, надо пройтись и у активных SportMemberAR убрать-поставить

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%team_member}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'sman_id'], 'required'],
            [['team_id', 'sman_id', 'pos_id'], 'integer'],
            [['first_day', 'last_day', 'ext_info'], 'safe'],
            [['ext_info'], JsonValidator::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Команда',
            'sman_id' => 'Спортсмен',
            'first_day' => 'С',
            'last_day' => 'По',
            'pos_id' => 'Позиция',
            'ext_info' => 'Ext Info',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SportMemberAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SportMemberAQ(static::class);
    }

    /**
     * @return SportPositionAR|null
     */
    public function getPosition()
    {
        return SportPositionAR::findOne($this->pos_id);
    }

    /**
     * @return SmanAQ|ActiveQuery
     */
    public function getSman()
    {
        return $this->hasOne(SmanAR::class, ['id' => 'sman_id']);
    }

    /**
     * @return SportTeamAQ|ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(SportTeamAR::class, ['id' => 'team_id']);
    }

    /**
     * @param null $date
     */
    public function close($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d');
        }

        $this->last_day = $date;
        $this->save();
    }
}
