<?php

namespace common\modules\sport\models;

use common\components\S3DictionaryAR;
use common\extensions\ExtAQ;
use common\modules\game\models\GameTouMatchAR;
use common\modules\game\models\SportMatchAR;
use common\traits\StatusAQTrait;

/**
 * This is the ActiveQuery class for [[SmanAR]].
 *
 * @see SmanAR
 */
class SmanAQ extends ExtAQ
{
    use StatusAQTrait;

    function active()
    {
        list($tableName, $alias) = $this->getTableNameAndAlias();
        $this->andWhere(['!=',"$alias.sman_status",'outall']);
        return $this->andWhere(['>=',"$alias.id",100]); // AUTO_INCREMENT=100
    }

    /**
     * FIXME FAN-720 Множественность команд спортсмена и рефактор lastTeamMember
     * Основная работа должна делаться в whereTeam
     * filterTeam для ss3api
     *
     * @param array $teamIDs
     * @param type $actualDate
     * @return \common\modules\sport\models\SmanAQ
     */
    function filterTeamIDs( array $teamIDs, $actualDate = null) : SmanAQ
    {
        $tblMember = SportMemberAR::tableName();
		$tblMan = SmanAR::tableName();
		$this->innerJoin($tblMember, "$tblMember.sman_id = $tblMan.id");

        $this->andWhere(["$tblMember.team_id" => $teamIDs]);

        if (!empty($actualDate)) {
			if ( is_bool($actualDate)) $actualDate = time();

			if ( is_numeric($actualDate) ) $actualDate = date ( 'Y-m-d', $actualDate );

			$this->andWhere("$tblMember.first_day <= $actualDate and $tblMember.last_day >= $actualDate");
		}

        return $this;
    }

    public function filterTourney($touId)
    {
        // ATTENTION alisases @see SmanTouAR::find()
        // select('$tblMan.*, gtm.tou_id as tou_id, tm.team_id as team_id, tm.pos_id as pos_id');

        // FIXME FAN-720 Множественность команд спортсмена и рефактор lastTeamMember
        // переделать полностью
        // filterTourney и whereTourney вероятно станут синонимами
        // наверное отличаться будут только
        // filterTourney() это
        // whereTourney()->andWhere(['!=','pos.ent_code','any.сoach'])->active();

        $result = $this->alias('s')
            ->innerJoin(SportMemberAR::tableName()." tm","tm.sman_id=s.id")
            ->innerJoin(SportTeamAR::tableName(). " t", 't.id=tm.team_id')
            // TODO добавить в условие привязки матчей tm.first_day < m.start_date < tm.last_day
            ->innerJoin(SportMatchAR::tableName(). " m", 't.id=m.team_owner OR t.id=m.team_guest')
            ->innerJoin(GameTouMatchAR::tableName()." gtm", 'gtm.match_id=m.id')
            ->innerJoin(S3DictionaryAR::tableName()." pos", 'pos.id=tm.pos_id AND pos.ent_kind=2') // pos.ent_kind=2 кто такое 2?
            ->andWhere([
                'gtm.tou_id' => $touId,
            ]);
        $result->andWhere(['!=','pos.ent_code','any.сoach']);
        // $result->andWhere('sman_status <> "outall"'); // заменить на active()

        return $result;
    }
}
