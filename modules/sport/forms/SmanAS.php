<?php

namespace common\modules\sport\forms;

use backend\modules\xitimport\models\XitBufferAR;
use common\components\S3Kind;
use common\models\Many2Many;
use common\modules\sount\widgets\buffer\interfaces\ColumnHiderModelInterface;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportMemberAR;
use common\modules\sport\models\SportTeamAR;
use common\providers\SmanDataProvider;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class SmanAS
 * @package common\modules\sport\forms
 * @property null|int $sport_id
 */
class SmanAS extends SmanAR implements ColumnHiderModelInterface
{
    public $lea_list;
    public $team_id;
    public $team_id_copy;
    public $sport_id;
    public $sport_id_copy;
    public $updated_at_ru;
    public $x_code;
    public $i_title;
    public $photo;
    public $pos_id;
    public $power;
    public $price;
    public $nickname;
    public $columns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sport_id', 'sport_id_copy', 'pos_id', 'power', 'price', 'team_id_copy', 'team_id'], 'integer'],
            [['id', 'r_name', 'sman_status', 'r_icon', 'r_name', 'updated_at', 'x_code', 'i_title', 'lea_list', 'nickname'], 'string'],
            [['ext_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'team_id' => 'Команда',
            'team_id_copy' => 'ID Команды',
            'photo' => 'Фото',
            'r_icon' => 'Фото (название)',
            'sport_id_copy' => 'ID Спорта',
        ]);
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $whereSql = Many2Many::find()
            ->alias('m2m')
            ->select(['m2m.id_right'])
            ->andWhere('m2m.id_left = team.id')
            ->andWhere(['m2m.rel_kind' => Many2Many::TYPE_TEAM_LEAGUE]);

        $leagueSql = SportLeagueAR::find()
            ->alias('lea')
            ->select(["GROUP_CONCAT(' ', lea.r_name)"])
            ->andWhere('lea.id IN (' . $whereSql->createCommand()->rawSql . ')');

        $joinMember = SportMemberAR::find()
            ->select(['MAX(id)'])
            ->where('sman_id = sman.id')
            ->orderBy(['id' => SORT_DESC])
            ->limit(1);

        $query = self::find()
            ->alias('sman')
            ->select([
                'sman.*',
                "DATE_FORMAT(sman.updated_at,'%Y %b %d, %H:%i:%s') AS updated_at_ru",
                '(' . $leagueSql->createCommand()->rawSql . ') as lea_list',
                'team_member.pos_id as pos_id',
                "JSON_EXTRACT(sman.ext_info, '$.power') as power",
                "JSON_EXTRACT(sman.ext_info, '$.price') as price",
                "JSON_EXTRACT(sman.ext_info, '$.nickname') as nickname",
            ])
            ->leftJoin(SportMemberAR::tableName() . ' team_member', 'team_member.id = (' . $joinMember->createCommand()->rawSql . ')')
            ->leftJoin(SportTeamAR::tableName() . ' team', 'team_member.team_id = team.id')
            ->leftJoin(XitBufferAR::tableName() . ' xit_buffer', [
                'AND',
                'xit_buffer.our_id = sman.id',
                ['xit_buffer.ent_kind' => S3Kind::KIND_SMAN],
            ])
            ->with(['teamMembers.team', 'xitBuffer']);

        $dataProvider = new SmanDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
                'attributes' => [
                    'id',
                    'r_name',
                    'sport_id',
                    'team_id',
                    'r_icon',
                    'sman_status',
                    'updated_at',
                    'x_code',
                    'i_title',
                    'lea_list',
                    'pos_id',
                    'power',
                    'price',
                    'nickname',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AND',
            ['sman.id' => $id],
            ['like', 'sman.r_name', $this->r_name],
            ['like', 'sman.r_icon', $this->r_icon],
            ['like', "DATE_FORMAT(sman.updated_at,'%Y %b %d, %H:%i:%s')", $this->updated_at_ru],
            ['team_member.team_id' => $this->team_id_copy ?: $this->team_id],
            ['team.sport_id' => $this->sport_id_copy ?: $this->sport_id],
            ['like', 'xit_buffer.x_code', $this->x_code],
            ['like', 'xit_buffer.i_title', $this->i_title],
        ]);

        if ($this->sman_status) {
            switch ($this->sman_status) {
                case self::STATUS_ANY:
                    break;
                case self::STATUS_NOT_OUTALL:
                    $query->andFilterWhere(['<>', 'sman.sman_status', 'outall']);
                    break;
                default:
                    $query->andFilterWhere(['sman.sman_status' => $this->sman_status]);
                    break;
            }
        }

        $query->andFilterWhere(['like', 'ext_info', $this->ext_info]);
        $query->andFilterHaving([
            'AND',
            ['like', 'lea_list', $this->lea_list],
            ['pos_id' => $this->pos_id],
            ['like', 'power', $this->power],
            ['like', 'price', $this->price],
            ['like', 'nickname', $this->nickname],
        ]);

        return $dataProvider;
    }

    /**
     * @param $teamsArray
     * @param $params
     * @return ActiveDataProvider
     */
    public function searchByTeam($teamsArray, $params)
    {
        $query = self::getSmenListByTeam($teamsArray, true)
            ->alias('sman')
            ->andWhere(['!=', 'sman.sman_status', 'outall']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        $query->andFilterWhere(['sman.id' => $id]);
        $query->andFilterWhere(['like', 'sman.r_name', $this->r_name]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public static function getStatusesFilterList()
    {
        return [
            self::STATUS_ANY => 'Все',
            self::STATUS_OUTALL => 'Только outall',
            self::STATUS_NOT_OUTALL => 'Без outall',
            self::STATUS_TRAUMA => 'trauma',
        ];
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'x_code',
            'id',
            'sport_id_copy',
            'sman_status',
            'r_icon',
            'r_icon',
            'photo',
            'power',
            'updated_at',
            'team_id',
            'team_id_copy',
        ];
    }
}
