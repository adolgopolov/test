<?php

namespace common\modules\sport\forms;

use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use common\modules\sport\models\SportLeagueAQ;
use common\behaviors\ImageUploadBehavior;
use common\models\Many2Many;
use common\modules\sport\models\SportLeagueAR;

/**
 * Class SportLeagueFM
 * @package common\modules\sport\forms
 */
class SportLeagueFM extends SportLeagueAR
{
    public $image_file;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => ImageUploadBehavior::class,
                'kind' => self::kind(),
                'model' => $this,
                'attribute' => 'image_file',
                'image_name' => 'r_icon',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['image_file'], 'file', 'extensions' => Yii::$app->images->leagueImage->extensions],
        ]);
    }

    /**
     * @return SportLeagueAQ|\yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getTeams()
    {
        return $this->hasMany(SportTeamFM::class, ['id' => 'id_left'])
                    ->viaTable(
                        Many2Many::tableName(),
                        ['id_right' => 'id'],
                        static function ($query) {
                            /** @var ActiveQuery $query */
                            $query->andOnCondition(['rel_kind' => Many2Many::TYPE_TEAM_LEAGUE]);
                        }
                    );
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getListTeams()
    {
        return $this->getTeams()
                    ->select('id, r_name as name')
                    ->orderBy('r_name ASC')
                    ->asArray()
                    ->all();
    }
}
