<?php

namespace common\modules\sport\forms;

use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\components\S3Kind;
use common\models\Many2Many;
use common\modules\sount\models\SouTeamAR;
use common\modules\sport\models\SportLeagueAR;
use backend\modules\xitimport\models\XitBufferAR;

/**
 * Class SportTeamAS
 * @package common\modules\sport\forms
 */
class SportTeamAS extends SportTeamFM
{
    public $x_code;
    public $i_title;
    public $lea_list;
    public $sou_id;
    public $sou_name;
    public $sport_id_copy;

    public $columns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sport_id', 'sou_id', 'sport_id_copy'], 'integer'],
            [['id', 'r_name', 'r_icon', 'team_jersey', 'x_code', 'i_title', 'lea_list', 'sou_name'], 'string'],
            [['ext_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'x_code' => 'Xit код',
            'sou_id' => 'Буферный номер',
            'sou_name' => 'Буферное название',
            'lea_list' => 'Лиги',
            'sport_id_copy' => 'ID Спорта',
        ]);
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $whereSql = Many2Many::find()
            ->alias('m2m')
            ->select(['m2m.id_right'])
            ->andWhere('m2m.id_left = team.id')
            ->andWhere(['m2m.rel_kind' => Many2Many::TYPE_TEAM_LEAGUE]);

        $selectSql = SportLeagueAR::find()
            ->alias('lea')
            ->select(["GROUP_CONCAT(' ', lea.r_name)"])
            ->andWhere('lea.id IN (' . $whereSql->createCommand()->rawSql . ')');

        $query = static::find()
            ->alias('team')
            ->select(['team.*', 'sou_team.id as sou_id', 'sou_team.name as sou_name', '(' . $selectSql->createCommand()->rawSql . ') AS lea_list'])
            ->leftJoin(XitBufferAR::tableName() . ' xit_buffer', [
                'AND',
                'xit_buffer.our_id = team.id',
                ['xit_buffer.ent_kind' => S3Kind::KIND_TEAM],
            ])
            ->leftJoin(SouTeamAR::tableName() . ' sou_team', 'sou_team.team_our_id = team.id')
            ->with('sport');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
                'attributes' => [
                    'id',
                    'sport_id',
                    'x_code',
                    'sou_id',
                    'sou_name',
                    'r_name',
                    'r_icon',
                    'team_jersey',
                    'lea_list',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AND',
            ['team.id' => $id],
            ['like', 'team.r_name', $this->r_name],
            ['like', 'team.r_icon', $this->r_icon],
            ['like', 'team.team_jersey', $this->team_jersey],
            ['team.sport_id' => $this->sport_id_copy ?: $this->sport_id],
            ['like', 'xit_buffer.x_code', $this->x_code],
            ['like', 'xit_buffer.i_title', $this->i_title],
        ]);

        $query->andFilterHaving(['like', 'lea_list', $this->lea_list])
            ->andFilterHaving(['like', 'sou_id', $this->sou_id])
            ->andFilterHaving(['like', 'sou_name', $this->sou_name])
            ->andFilterWhere(['like', 'ext_info', $this->ext_info]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'r_name',
            'sport_id',
            'sport_id_copy',
            'lea_list',
            'x_code',
            'sou_id',
            'sou_name',
            'r_icon',
            'team_jersey',
            'colors',
        ];
    }
}
