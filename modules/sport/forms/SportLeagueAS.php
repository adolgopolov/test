<?php

namespace common\modules\sport\forms;

use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\modules\sount\widgets\buffer\interfaces\ColumnHiderModelInterface;
use common\modules\sport\models\SportLeagueAR;

/**
 * Class SportLeagueAS
 * @package common\modules\sport\forms
 */
class SportLeagueAS extends SportLeagueAR implements ColumnHiderModelInterface
{
    public $columns;
    public $sport_id_copy;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sport_id', 'sport_id_copy'], 'integer'],
            [['id', 'r_name', 'r_icon', 'country'], 'string'],
            [['ext_info'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'sport_id_copy' => 'ID Спорта',
        ]);
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find()
            ->alias('lea')
            ->with('sport');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AND',
            ['lea.id' => $id],
            ['like', 'lea.r_name', $this->r_name],
            ['like', 'lea.r_icon', $this->r_icon],
            ['like', 'lea.country', $this->country],
            ['lea.sport_id' => $this->sport_id_copy ?: $this->sport_id],
        ]);

        $query->andFilterWhere(['like', 'ext_info', $this->ext_info]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'sport_id_copy',
            'sport_id',
            'r_icon',
            'country',
        ];
    }
}
