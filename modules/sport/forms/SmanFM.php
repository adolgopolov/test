<?php

namespace common\modules\sport\forms;

use common\behaviors\ImageUploadBehavior;
use common\modules\sport\models\SmanAR;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class SmanFM
 * @package common\modules\sport\forms
 */
class SmanFM extends SmanAR
{
    public $image_file;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => ImageUploadBehavior::class,
                'kind' => self::kind(),
                'model' => $this,
                'attribute' => 'image_file',
                'image_name' => 'r_icon',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['image_file'], 'file', 'extensions' => Yii::$app->images->smanImage->extensions],
        ]);
    }
}
