<?php

namespace common\modules\sport\forms;

use common\modules\sount\widgets\buffer\interfaces\ColumnHiderModelInterface;
use common\modules\sport\models\SportMemberAR;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class SportMemberAS extends SportMemberAR implements ColumnHiderModelInterface
{
    public $columns;
    public $sman_name;
    public $sman_status;

    /**
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'team_id_copy' => 'ID команды',
            'sman_id_copy' => 'ID Спортсмена',
            'pos_id_copy' => 'ID Позиции',
            'sman_name' => 'Спортсмен',
            'sman_status' => 'Статус',
        ]);
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'team_id_copy',
            'team_id',
            'sman_id_copy',
            'first_day',
            'last_day',
            'pos_id_copy',
        ];
    }
}
