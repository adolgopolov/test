<?php

namespace common\modules\sport\forms;

use common\components\S3Kind;
use common\modules\game\models\SportMatchAR;
use common\modules\sount\widgets\buffer\interfaces\ColumnHiderModelInterface;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportFactAR;
use common\modules\sport\models\SportMemberAR;
use common\modules\sport\models\SportOccaAR;
use common\modules\sport\models\SportTeamAR;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class SportOccaAS extends SportOccaAR implements ColumnHiderModelInterface
{
    public $columns;
    public $sman_name;
    public $team_id;
    public $fact_name;
    public $match_name;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['sman_name', 'fact_name'], 'string'],
            [['team_id'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'match_id' => 'ID Матча',
            'match_name' => 'Матч',
            'sman_name' => 'Спортсмен',
            'sman_id' => 'ID Спортсмена',
            'team_id' => 'ID Команды',
            'fact_name' => 'Факт',
            'fact_id' => 'ID Факта',
        ]);
    }

    /**
     * @param array $params
     * @param int|null $match_id
     * @param array $providerOptions
     * @return ActiveDataProvider
     */
    public function search($params, $match_id = null, $providerOptions = [])
    {
        $joinMember = SportMemberAR::find()
            ->select(['MAX(id)'])
            ->where('sman_id = sman.id')
            ->orderBy(['id' => SORT_DESC])
            ->limit(1);

        $query = self::find()
            ->alias('occasion')
            ->select([
                'occasion.*',
                'team.r_name as team',
                'sman.r_name as sman_name',
                'team.id as team_id',
                'fact.r_name as fact_name',
                'match.r_name as match_name',
            ])
            ->andWhere(['!=', 'occasion.fact_id', SportFactAR::FACT_MATCH_PRESENCE])
            ->andWhere([ 'occasion.is_deleted'=> 0])
            ->leftJoin(SportMatchAR::tableName() . ' match', 'occasion.match_id = match.id')
            ->leftJoin(SmanAR::tableName() . ' sman', 'occasion.sman_id = sman.id')
            ->leftJoin(SportMemberAR::tableName() . ' team_member', 'team_member.id = (' . $joinMember->createCommand()->rawSql . ')')
            ->leftJoin(SportTeamAR::tableName() . ' team', 'team.id = team_member.team_id')
            ->leftJoin(SportFactAR::tableName() . ' fact', 'fact.id = occasion.fact_id and fact.ent_kind = :ent_kind', [
                ':ent_kind' => S3Kind::KIND_FACT,
            ]);

        if ($match_id) {
            $query->andWhere(['occasion.match_id' => $match_id]);
        }

        $dataProvider = new ActiveDataProvider(ArrayHelper::merge([
            'query' => $query,
            'pagination' => false,
        ], $providerOptions));

        $dataProvider->setSort([
            'defaultOrder' => [
                'id' => SORT_DESC,
            ],
            'attributes' => ArrayHelper::merge($dataProvider->sort->attributes, [
                'team',
                'sman_name',
                'team_id',
                'fact_name',
                'match_name',
            ]),
        ]);

        $this->load($params);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'match_id',
            'sman_id',
            'team',
            'team_id',
            'after_start',
            'fact_name',
            'fact_id',
            'occa_amount',
            'is_deleted',
        ];
    }
}
