<?php

namespace common\modules\sport\forms;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\modules\sport\models\SportAR;
use common\behaviors\ImageUploadBehavior;
use common\components\S3Kind;
use common\modules\sport\models\SportTeamAR;

/**
 * Class SportTeamFM
 * @package common\modules\sport\forms
 */
class SportTeamFM extends SportTeamAR
{
    public $image_file;
    public $jersey_name;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => ImageUploadBehavior::class,
                'kind' => S3Kind::KIND_TEAM,
                'model' => $this,
                'attribute' => 'image_file',
                'image_name' => 'r_icon',
            ],
            [
                'class' => ImageUploadBehavior::class,
                'kind' => S3Kind::KIND_TEAM,
                'model' => $this,
                'attribute' => 'jersey_name',
                'image_name' => 'team_jersey',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['image_file', 'jersey_name'], 'file', 'extensions' => Yii::$app->images->teamImage->extensions],
            [['image_file', 'jersey_name'], 'checkLeague'],
        ]);
    }

    /**
     * @param $attribute
     * @param $params
     * @param $validator
     */
    public function checkLeague($attribute, $params, $validator): void
    {
        if (!$this->leagues) {
            $this->addError($attribute, 'Нельзя загружать изображения в команду без лиги');
        }
    }

    /**
     * @return null
     */
    public function getColorList()
    {
        $colorList = '';

        foreach ((array)$this->colors as $color) {
            $colorList .= Html::tag('div', '', [
                'style' => 'background-color: ' . $color . '; width: 100%; height: 30px; max-width: 200px; min-width: 30px;',
            ]);
        }

        return $colorList ?: null;
    }

    /**
     * @return array
     */
    public static function getTeams(): array
    {
        $teams = self::find()->orderBy('r_name')->all();
        $sports = SportAR::getAsOptsList();
        $list = [];

        foreach ($teams as $team) {
            /** @var $team SportTeamAR */
            $list[$team->id] = $team->getTitle() . '(' . $sports[$team->sport_id] . ')';
        }

        return $list;
    }
}
