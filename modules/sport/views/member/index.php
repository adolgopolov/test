<?php

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SportMemberAS;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportPositionAR;
use common\modules\sport\models\SportTeamAR;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $searchModel SportMemberAS
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = 'Позиции спортсменов';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    [
        'attribute' => 'team_id_copy',
        'value' => static function (SportMemberAS $model) {
            return $model->team_id;
        },
    ],
    [
        'attribute' => 'team_id',
        'value' => static function (SportMemberAS $model) {
            return SportTeamAR::findOne(['id' => $model->team_id])->r_name;
        }
    ],
    [
        'attribute' => 'sman_id_copy',
        'value' => static function (SportMemberAS $model) {
            return $model->sman_id;
        }
    ],
    [
        'attribute' => 'sman_id',
        'value' => static function (SportMemberAS $model) {
            return SmanAR::findOne(['id' => $model->sman_id])->r_name;
        }
    ],
    'first_day',
    'last_day',
    [
        'attribute' => 'pos_id_copy',
        'value' => static function (SportMemberAS $model) {
            return $model->pos_id;
        }
    ],
    [
        'attribute' => 'pos_id',
        'value' => static function (SportMemberAS $model) {
            $positions = SportPositionAR::getPositions();

            return $positions[$model->pos_id];
        }
    ],
    [
        'class' => ActionColumn::class,
        'template' => '<div class="btn-group action-column">{view}</div>',
    ],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sport-member-ar-index">
    <div class="d-flex flex-row justify-content-between mT-10 mB-30 form-inline">
        <h4 class="c-grey-900"><?= Html::encode($this->title) ?></h4>
        <?= ColumnHiderWidget::widget([
            'pjaxSelector' => '#sport-member-pjax',
            'pjaxUrl' => '/sport/member/index',
            'model' => $searchModel,
        ]) ?>
    </div>
    <?php Pjax::begin(['id' => 'sport-member-pjax']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
    ]) ?>
    <?php Pjax::end() ?>
</div>
