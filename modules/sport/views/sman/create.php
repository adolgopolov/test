<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\sport\models\SmanAR */

$this->title = 'Добавить спортсмена';
$this->params['breadcrumbs'][] = ['label' => 'Спортсмены', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sman-ar-create">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
