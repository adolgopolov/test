<?php

use common\modules\sport\forms\SmanFM;
use common\modules\sport\models\SmanAR;
use common\widgets\DetailForm\DetailFormWidget;
use common\widgets\image\ImageWidget;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model SmanFM
 * @var $form yii\widgets\ActiveForm
 */
?>
<div class="sman-ar-form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]) ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'r_name',
            'sport_id' => [
                'inputHidden' => $model->isNewRecord,
                'inputValue' => Html::textInput('sport_id', $model->sport->r_name ?? null, [
                    'class' => 'form-control',
                    'disabled' => true,
                ]),
            ],
            'team' => [
                'inputHidden' => $model->isNewRecord,
                'inputValue' => Html::textInput('team', $model->lastTeamMember->team->r_name ?? null, [
                    'class' => 'form-control',
                    'disabled' => true,
                ]),
            ],
            'pos_id' => [
                'inputHidden' => $model->isNewRecord,
                'inputValue' => Html::textInput('pos_id', $model->lastTeamMember->position->r_name ?? null, [
                    'class' => 'form-control',
                    'disabled' => true,
                ]),
            ],
            'title[rus]' => [
                'label' => $model->getAttributeLabel('title.rus'),
            ],
            'title[eng]' => [
                'label' => $model->getAttributeLabel('title.eng'),
            ],
            'status' => Select2::widget([
                'model' => $model,
                'attribute' => 'status',
                'data' => SmanAR::getStatuses(),
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => 'Выберите статус',
                ],
            ]),
            'power' => [
                'inputType' => 'number',
            ],
            'price' => [
                'inputType' => 'number',
            ],
            'r_icon' => ImageWidget::widget([
                'model' => $model,
                'attribute' => 'r_icon',
                'kind' => SmanAR::kind(),
            ]),
            'x_code' => [
                'inputHidden' => $model->isNewRecord,
                'inputValue' => Html::textInput('x_code', $model->xitBuffer->x_code ?? null, [
                    'class' => 'form-control',
                    'disabled' => true,
                ]),
            ],
            'i_title' => [
                'inputHidden' => $model->isNewRecord,
                'inputValue' => Html::textInput('i_title', $model->xitBuffer->i_title ?? null, [
                    'class' => 'form-control',
                    'disabled' => true,
                ]),
            ],
            'updated_at' => [
                'inputHidden' => $model->isNewRecord,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'nickname',
            'date_of_birth' => DatePicker::widget([
                'model' => $model,
                'attribute' => 'date_of_birth',
                'options' => [
                    'placeholder' => 'Выберите дату рождения спортсмена',
                    'autocompplete' => 'off',
                ],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ],
            ]),
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
