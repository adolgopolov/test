<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SmanAS;
use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel \common\modules\sport\forms\SmanAS
 * @var $teams array
 * @var $sports array
 * @var $positions array
 */

$this->title = 'Спортсмены';
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.col .summary {
    display: none;
}
CSS;

$this->registerCss($css);

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    'r_name',
    [
        'attribute' => 'sport_id_copy',
        'value' => static function (SmanAS $model) {
            return $model->sport->id ?? null;
        }
    ],
    [
        'attribute' => 'sport_id',
        'options' => [
            'class' => 'sport-id-grid-column',
        ],
        'content' => static function (SmanAS $model) {
            return $model->sport->r_name ?? null;
        },
        'filter' => $sports,
    ],
    [
        'attribute' => 'lea_list',
        'value' => static function (SmanAS $model) {
            $links = [];
            $leagues = $model->lastTeamMember->team->leagues ?? [];

            foreach ($leagues as $league) {
                $links[] = Html::a($league->r_name, ['/sport/league/view', 'id' => $league->id]);
            }

            return implode(', ', $links);
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'team_id_copy',
        'value' => static function (SmanAS $model) {
            return $model->lastTeamMember->team_id ?? null;
        }
    ],
    [
        'attribute' => 'team_id',
        'options' => [
            'class' => 'team-id-grid-column',
        ],
        'content' => static function (SmanAS $model) {
            $team = $model->lastTeamMember->team ?? null;

            if ($team) {
                return Html::a($team->getTitle(), ['team/update', 'id' => $team->id]);
            }

            return null;
        },
        'filter' => $teams,
    ],
    [
        'attribute' => 'pos_id',
        'value' => static function (SmanAS $model) use ($positions) {
            return $positions[$model->pos_id] ?? null;
        },
        'filter' => $positions,
    ],
    [
        'attribute' => 'sman_status',
        'options' => [
            'class' => 'sman-status-grid-column',
        ],
        'filter' => SmanAS::getStatusesFilterList(),
    ],
    [
        'attribute' => 'power',
        'value' => static function (SmanAS $model) {
            return $model->power ?? null;
        },
    ],
    'price',
    'r_icon',
    [
        'attribute' => 'photo',
        'value' => static function (SmanAS $model) {
            return $model->htmlImage();
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'x_code',
        'value' => static function (SmanAS $model) {
            return $model->xitBuffer->x_code ?? null;
        },
    ],
    [
        'attribute' => 'i_title',
        'value' => static function (SmanAS $model) {
            return $model->xitBuffer->i_title ?? null;
        },
    ],
    'nickname',
    [
        'attribute' => 'updated_at',
        'value' => static function (SmanAS $model) {
            return $model->updated_at_ru;
        },
    ],
//            [
//                'attribute' => 'i_price',
//                'value' => static function (SmanAS $model) {
//                    return $model->xitBuffer->i_price ?? null;
//                }
//            ],
    [
        'class' => ActionColumn::class,
        'template' => '<div class="btn-group">{view}{update}</div>',
    ],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sman-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#sport-sman-pjax',
        'pjaxUrl' => '/sport/sman/index',
        'model' => $searchModel,
    ]) ?>

    <?= Html::a('<span class="ti-plus"></span> Добавить', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    <?php Pjax::begin(['id' => 'sport-sman-pjax']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-responsive',
        ],
        'columns' => $columns,
        'rowClickAction' => GridView::CLICK_ACTION_UPDATE,
    ]) ?>
    <?php Pjax::end(); ?>
</div>
