<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SmanAR
 * @var $modelMember
 * @var $teamMemberProvider
 * @var $teams
 */

$this->title = 'Редактирование спортсмена: ' . $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Спортсмены', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';

$tabs = [
    [
        'label' => '<span class="ti ti-pencil"></span> Основная информация',
        'active' => true,
        'content' => $this->render('_form', [
            'model' => $model,
        ]),
    ],
    [
        'label' => '<span class="ti ti-layout-grid3"></span> История трансферов',
        'content' => $this->render('_member', [
            'model' => $model,
            'modelMember' => $modelMember,
            'teamMemberProvider' => $teamMemberProvider,
            'teams' => $teams,
        ]),
    ],
];
?>
<div class="sman-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= TabsX::widget(['items' => $tabs, 'encodeLabels' => false]) ?>
</div>
