<?php

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $teamMemberProvider
 * @var \common\modules\sport\models\SportMemberAR $modelMember
 * @var \common\modules\sport\models\SmanAR $model
 * @var array $teams
 */

use backend\components\aboard\ActionColumn;
use common\modules\sport\models\SportMemberAR;
use common\widgets\JsonEditor;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\bootstrap4\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

Url::remember();
?>
<?php //Pjax::begin(['id' => 'members-create-pjax']) ?>
<!--<div class="text-right">-->
<!--    --><?php //Modal::begin([
//        'title' => "<h5 class=\"c-grey-900\">Добавить запись <strong>{$model->r_name}</strong></h5>",
//        'size' => Modal::SIZE_LARGE,
//        'toggleButton' => [
//            'label' => '<span class="fa fa-plus"></span> Добавить запись',
//            'class' => 'btn btn-outline-success',
//        ],
//    ]) ?>
<!--    --><?php //$form = ActiveForm::begin([
//        'action' => '/sport/sman/create-member?sman_id=' . $model->id,
//        'method' => 'POST',
//        'options' => [
//            'class' => 'js-form-attach-entity text-left',
//            'data-pjax-selector' => '#members-pjax',
//        ],
//    ]) ?>
<!---->
<!--    --><?//= $form->field($modelMember, 'team_id')->widget(Select2::class, [
//        'data' => $teams,
//        'options' => [
//            'placeholder' => 'Выберите команду',
//            'id' => 'team-id',
//        ],
//    ])->label('Команда') ?>
<!---->
<!--    --><?//= $form->field($modelMember, 'pos_id')->widget(DepDrop::class, [
//        'options' => [
//            'id' => 'position-dd',
//            'placeholder' => 'Выберите позицию',
//        ],
//        'pluginOptions' => [
//            'depends' => ['team-id'],
//            'url' => Url::to(['/sport/sman/ajax-position', 'id' => $model->id]),
//            'initialize' => true,
//            'initDepends' => ['team-id'],
//        ],
//        'type' => DepDrop::TYPE_SELECT2,
//    ]) ?>
<!---->
<!--    --><?//= $form->field($modelMember, 'ext_info')->widget(JsonEditor::class) ?>
<!---->
<!--    <hr>-->
<!--    <div class="text-right">-->
<!--        --><?//= Html::submitButton('Добавить запись', ['class' => 'btn btn-success js-submit-attach-entity']) ?>
<!--    </div>-->
<!---->
<!--    --><?php //ActiveForm::end() ?>
<!--    --><?php //Modal::end() ?>
<!--</div>-->
<?php //Pjax::end() ?>

<?php Pjax::begin(['id' => 'members-pjax']) ?>

<?= GridView::widget([
    'dataProvider' => $teamMemberProvider,
    'columns' => [
        [
            'attribute' => 'id',
            'options' => [
                'class' => 'id-grid-column',
            ],
        ],
        [
            'attribute' => 'team_id',
            'value' => static function (SportMemberAR $model) {
                $team = $model->team ?? null;

                if ($team) {
                    return Html::a($team->getTitle(), Url::to(['/sport/team/update', 'id' => $model->team_id]));
                }

                return null;
            },
            'format' => 'raw',
        ],
        'first_day',
        'last_day',
        [
            'attribute' => 'pos_id',
            'value' => static function (SportMemberAR $model) {
                return $model->position->r_name ?? null;
            }
        ],
        [
            'class' => ActionColumn::class,
            'template' => Html::tag('div', '{update}', ['class' => 'btn-group action-column']),
            'buttons' => [
                'update' => static function ($url, SportMemberAR $model, $key) {
                    $title = Yii::t('yii', 'Update');

                    $icon = Html::tag('em', '', ['class' => 'fas fa-pencil-alt']);

                    return Html::a($icon, Url::to(['/sport/member/update', 'id' => $model->id]), [
                        'title' => $title,
                        'aria-label' => $title,
                        'data-pjax' => '1',
                        'class' => 'btn btn-outline-primary btn-update',
                        'data-title' => $title,
                    ]);
                },
            ],
        ],
    ]]) ?>
<?php Pjax::end() ?>
