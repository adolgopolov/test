<?php

use common\modules\sport\models\SmanAR;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SmanAR
 */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Спортсмены', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];

?>
<div class="sman-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-arrow-left"></span> К списку', ['index'], [
            'class' => 'btn btn-outline-primary pull-left',
        ]) ?>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'r_name',
            [
                'attribute' => 'sport_id',
                'value' => static function (SmanAR $model) {
                    return $model->sport->r_name ?? null;
                },
            ],
            [
                'attribute' => 'lea_list',
                'value' => static function (SmanAR $model) {
                    $links = [];
                    $leagues = $model->lastTeamMember->team->leagues ?? [];

                    foreach ($leagues as $league) {
                        $links[] = Html::a($league->r_name, ['/sport/league/view', 'id' => $league->id]);
                    }

                    return implode(', ', $links);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'team',
                'format' => 'raw',
                'value' => static function (SmanAR $model) {
                    $team = $model->lastTeamMember->team;

                    if (!$team) {
                        return null;
                    }

                    return Html::a($team->getTitle(), ['team/view', 'id' => $team->id], [
                        'target' => '_blank',
                    ]);
                },
            ],
            [
                'attribute' => 'pos_id',
                'value' => static function (SmanAR $model) {
                    return $model->lastTeamMember->position->r_name ?? null;
                },
            ],
            'title.rus',
            'title.eng',
            'sman_status',
            'power',
            'price',
            [
                'attribute' => 'r_icon',
                'value' => static function (SmanAR $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'x_code',
                'value' => static function (SmanAR $model) {
                    return $model->xitBuffer->x_code ?? null;
                },
            ],
            [
                'attribute' => 'i_title',
                'value' => static function (SmanAR $model) {
                    return $model->xitBuffer->i_title ?? null;
                },
            ],
            'updated_at',
            'nickname',
            'date_of_birth',
        ],
    ]) ?>
</div>
