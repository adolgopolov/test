<?php

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = 'Виды спорта';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sport-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'ent_code',
                'options' => [
                    'class' => 'code-grid-column',
                ],
            ],
            'r_name',
            [
                'class' => ActionColumn::class,
                'template' => '<div class="btn-group action-column">{view}</div>',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
