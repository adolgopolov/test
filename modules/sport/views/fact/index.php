<?php

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = 'Спортивные факты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sport-fact-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            // 'ent_kind',
            [
                'attribute' => 'ent_code',
                'options' => [
                    'class' => 'code-grid-column',
                ],
            ],
            'ent_value',
            [
                'attribute' => 'r_name',
                'label' => 'Название'
            ],
            [
                'class' => ActionColumn::class,
                'template' => '<div class="btn-group action-column">{view}</div>',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
