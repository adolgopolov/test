<?php

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $smenDataProvider
 * @var \common\modules\sport\models\SportMemberAR $modelMember
 * @var \common\modules\sport\models\SportTeamAR $model
 * @var array $smen
 * @var array $positions
 */

use yii\widgets\ActiveForm;
use yii\bootstrap4\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\modules\sport\models\SmanAR;
use common\widgets\JsonEditor;
use kartik\select2\Select2;

?>
<?php Pjax::begin(['id' => 'sman-pjax']) ?>
<div class="text-right">
    <?php Modal::begin([
        'title' => '<h5 class="c-grey-900">Добавить спортсмена в команду</h5>',
        'size' => Modal::SIZE_LARGE,
        'toggleButton' => [
            'label' => '<span class="fa fa-plus"></span> Добавить спортсмена в команду',
            'class' => 'btn btn-outline-success',
        ]
    ]) ?>
    <?php $form = ActiveForm::begin([
        'action' => '/sport/team/create-member',
        'method' => 'POST',
        'options' => [
            'class' => 'js-form-attach-entity text-left',
            'data-pjax-selector' => '#sman-pjax',
        ]
    ]) ?>

    <?= $form->field($modelMember, 'sman_id')->widget(Select2::class, [
        'data' => $smen,
        'options' => [
            'placeholder' => 'Выберите спортсмена',
        ],
    ]) ?>

    <?= $form->field($modelMember, 'team_id')->hiddenInput(['value' => $model->id])->label(false) ?>

    <?= $form->field($modelMember, 'pos_id')->widget(Select2::class, [
        'data' => $positions,
        'pluginOptions' => [
            'placeholder' => 'Выберите позицию',
        ],
    ]) ?>

    <?= $form->field($modelMember, 'ext_info')->widget(JsonEditor::class) ?>

    <hr>
    <div class="text-right">
        <?= Html::submitButton('Добавить спортсмена в команду', ['class' => 'btn btn-success js-submit-attach-entity']) ?>
    </div>

    <?php ActiveForm::end() ?>
    <?php Modal::end() ?>
</div>

<br/>

<?= GridView::widget([
    'dataProvider' => $smenDataProvider,
    'columns' => [
        [
            'attribute' => 'id',
            'options' => [
                'class' => 'id-grid-column',
            ],
        ],
        [
            'attribute' => 'r_name',
            'value' => static function (SmanAR $model) {
                return Html::a($model->getTitle(), Url::to(['/sport/sman/view', 'id' => $model->id]));
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'position',
            'value' => static function (SmanAR $model) {
                return $model->lastTeamMember->position->r_name ?? null;
            }
        ],
        [
            'attribute' => 'r_icon',
            'value' => static function (SmanAR $model) {
                return $model->htmlImage();
            },
            'format' => 'raw',
        ],
        'sman_status',
        'power',
        'price',
        [
            'value' => static function (SmanAR $model) {
                return Html::button(Html::tag('span', null, ['class' => 'fa fa-trash']), [
                    'class' => 'btn btn-outline-danger js-btn-detach-entity',
                    'data' => [
                        'parent' => $model->id,
                        'url' => '/sport/team/drop-member',
                        'pjax-selector' => '#sman-pjax',
                    ],
                ]);
            },
            'format' => 'raw'
        ],
    ],
]) ?>
<?php Pjax::end() ?>
