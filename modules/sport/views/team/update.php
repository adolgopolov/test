<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\sport\models\SportTeamAR $model
 * @var array $sports
 * @var \yii\data\ActiveDataProvider $leaDataProvider
 * @var array $availableLeagues
 * @var \yii\data\ActiveDataProvider $smenDataProvider
 * @var \common\modules\sport\models\SportMemberAR $modelMember
 * @var array $smen
 * @var array $positions
 */

$this->title = 'Редактирование команды: ' . $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';

$tabs = [
    [
        'label' => '<span class="ti ti-pencil"></span> Основная информация',
        'active' => true,
        'content' => $this->render('_form', [
            'model' => $model,
            'sports' => $sports,
            'availableLeagues' => $availableLeagues,
            'leaDataProvider' => $leaDataProvider,
        ]),
    ],
    [
        'label' => '<span class="ti ti-layout-grid3"></span> Спортсмены',
        'content' => $this->render('_smen', [
            'smenDataProvider' => $smenDataProvider,
            'model' => $model,
            'modelMember' => $modelMember,
            'smen' => $smen,
            'positions' => $positions,
        ]),
    ],
];
?>
<div class="sport-team-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= TabsX::widget(['items' => $tabs, 'encodeLabels' => false]) ?>
</div>
