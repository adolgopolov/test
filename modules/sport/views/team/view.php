<?php

use common\modules\sport\forms\SportTeamFM;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportTeamAR;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SportTeamAR
 * @var $leaDataProvider \yii\data\ActiveDataProvider
 * @var $smanDataProvider \yii\data\ActiveDataProvider
 */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];

?>
<div class="sport-team-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-arrow-left"></span> К списку', ['index'], [
            'class' => 'btn btn-outline-primary pull-left',
        ]) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
            'attribute' => 'sport_id',
                'value' => static function (SportTeamFM $model) {
                    return $model->sport->r_name ?? null;
                }
            ],
            [
                'attribute' => 'r_name',
                'value' => static function (SportTeamFM $model) {
                    return $model->getTitle();
                },
            ],
            'title.rus_short',
            'title.eng',
            'title.eng_short',
            [
                'attribute' => 'r_icon',
                'value' => static function (SportTeamFM $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'team_jersey',
                'value' => static function (SportTeamFM $model) {
                    return $model->htmlImage('team_jersey');
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'colors',
                'value' => static function (SportTeamFM $model) {
                    return $model->getColorList();
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

    <h4 class="c-grey-900 mT-10 mB-30">Лиги</h4>
    <?= GridView::widget([
        'dataProvider' => $leaDataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'r_name',
                'value' => static function (SportLeagueAR $model) {
                    return Html::a($model->getTitle(), Url::to(['/sport/league/view', 'id' => $model->id]));
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'sport_id',
                'value' => static function (SportLeagueAR $model) {
                    return $model->sport->r_name ?? null;
                },
            ],
            [
                'attribute' => 'r_icon',
                'value' => static function (SportLeagueAR $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
            'country',
        ],
    ]) ?>

    <h4 class="c-grey-900 mT-10 mB-30">Спортсмены</h4>
    <?= GridView::widget([
        'dataProvider' => $smanDataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'r_name',
                'value' => static function (SmanAR $model) {
                    return Html::a($model->getTitle(), Url::to(['/sport/sman/view', 'id' => $model->id]));
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'r_icon',
                'value' => static function (SmanAR $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>
</div>
