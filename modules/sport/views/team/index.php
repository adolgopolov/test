<?php

use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SportTeamAS;
use common\widgets\GridActions\GridActionsWidget;
use kartik\select2\Select2;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\modules\sport\forms\SportTeamAS $searchModel
 * @var array $sports
 * @var array $leagues
 */

$this->title = 'Команды';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    [
        'attribute' => 'r_name',
        'value' => static function (SportTeamAS $model) {
            return $model->getTitle();
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'sport_id_copy',
        'options' => [
            'class' => 'sport-id-grid-column',
        ],
        'value' => static function (SportTeamAS $model) {
            return $model->sport_id;
        },
    ],
    [
        'attribute' => 'sport_id',
        'options' => [
            'class' => 'sport-id-grid-column',
        ],
        'value' => static function (SportTeamAS $model) {
            return $model->sport->r_name ?? null;
        },
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'sport_id',
            'data' => $sports,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
            'pjaxContainerId' => 'sport-team-pjax',
        ]),
    ],
    [
        'attribute' => 'lea_list',
        'options' => [
            'class' => 'lea-list-grid-column',
        ],
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'lea_list',
            'data' => $leagues,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
            'pjaxContainerId' => 'sport-team-pjax',
        ]),
    ],
    [
        'attribute' => 'x_code',
        'value' => static function (SportTeamAS $model) {
            return $model->xitBuffer->x_code ?? null;
        }
    ],
    'sou_id',
    'sou_name',
    [
        'attribute' => 'r_icon',
        'value' => static function (SportTeamAS $model) {
            return $model->htmlImage();
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'team_jersey',
        'value' => static function (SportTeamAS $model) {
            return $model->htmlImage('team_jersey');
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'colors',
        'value' => static function (SportTeamAS $model) {
            return $model->getColorList();
        },
        'format' => 'raw',
    ],
    ['class' => ActionColumn::class],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sport-team-ar-index">
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#sport-team-pjax',
        'pjaxUrl' => '/sport/team/index',
        'model' => $searchModel,
        'additionalContainerOptions' => [
            'class' => [
                'columns-select2-margined',
            ],
        ],
    ]) ?>
    <?= GridActionsWidget::widget([
        'title' => $this->title,
    ]) ?>
    <?php Pjax::begin(['id' => 'sport-team-pjax']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => ArrayHelper::merge([GridActionsWidget::$checkboxColumn], $columns),
    ]) ?>
    <?php Pjax::end(); ?>
</div>
