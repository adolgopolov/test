<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SportTeamAR
 * @var $sports array
 */

$this->title = 'Добавление спортивной команды';
$this->params['breadcrumbs'][] = ['label' => 'Добавление спортивной команды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$queryParams = Yii::$app->request->queryParams;
$list = [];
if($queryParams) {
    foreach($queryParams as $key=>$name){
        $list[] = $key. " -- " .$name;
    }
}
?>
<div class="sport-team-ar-create">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <div>
        <?=!empty($list)?Html::ul($list) :''?>
    </div>
    <?= $this->render('_form', [
        'model'     => $model,
        'sports'    => $sports,
        'availableLeagues' => null,
        'leaDataProvider' => null,
    ]) ?>

</div>
