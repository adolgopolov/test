<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportTeamAR;
use common\widgets\ColorPicker\assets\ColorPickerAssets;
use common\widgets\DetailForm\DetailFormWidget;
use common\widgets\image\ImageWidget;
use kartik\select2\Select2;

/**
 * @var yii\web\View $this
 * @var common\modules\sport\models\SportTeamAR $model
 * @var yii\widgets\ActiveForm $form
 * @var array $sports
 * @var array $availableLeagues
 * @var \yii\data\ActiveDataProvider $leaDataProvider
 */

ColorPickerAssets::register($this);
?>
<div class="sport-team-ar-form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]) ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'sport_id' => Select2::widget([
                'model' => $model,
                'attribute' => 'sport_id',
                'data' => $sports,
                'options' => [
                    'placeholder' => 'Выберите спорт'
                ],
            ]),
            'r_name',
            'r_icon' => [
                'inputHidden' => $model->isNewRecord,
                'inputValue' => ImageWidget::widget([
                    'model' => $model,
                    'attribute' => 'r_icon',
                    'kind' => SportTeamAR::kind(),
                    'alwaysShow' => true,
                ]),
            ],
            'team_jersey' => [
                'inputHidden' => $model->isNewRecord,
                'inputValue' => ImageWidget::widget([
                    'model' => $model,
                    'attribute' => 'team_jersey',
                    'kind' => SportTeamAR::kind(),
                    'alwaysShow' => true,
                ]),
            ],
            'colors[c1]' => [
                'label' => $model->getAttributeLabel('color') . ' 1',
                'inputOptions' => [
                    'maxlength' => true,
                    'role-color-picker' => true,
                    'autocomplete' => 'off',
                ],
                'inputContainer' => [
                    'class' => 'color-picker-detail-form',
                ],
            ],
            'colors[c2]' => [
                'label' => $model->getAttributeLabel('color') . ' 2',
                'inputOptions' => [
                    'maxlength' => true,
                    'role-color-picker' => true,
                    'autocomplete' => 'off',
                ],
                'inputContainer' => [
                    'class' => 'color-picker-detail-form',
                ],
            ],
            'colors[c3]' => [
                'label' => $model->getAttributeLabel('color') . ' 3',
                'inputOptions' => [
                    'maxlength' => true,
                    'role-color-picker' => true,
                    'autocomplete' => 'off',
                ],
                'inputContainer' => [
                    'class' => 'color-picker-detail-form',
                ],
            ],
            'title[rus]' => [
                'label' => $model->getAttributeLabel('title.rus'),
            ],
            'title[rus_short]' => [
                'label' => $model->getAttributeLabel('title.rus_short'),
            ],
            'title[eng]' => [
                'label' => $model->getAttributeLabel('title.eng'),
            ],
            'title[eng_short]' => [
                'label' => $model->getAttributeLabel('title.eng_short'),
            ],
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <hr/>
    <?php if (!$model->isNewRecord) { ?>
        <?php Pjax::begin(['id' => 'leagues-pjax']) ?>
        <div class="d-flex flex-row justify-content-between">
            <h4 class="c-grey-900">Лиги</h4>
            <?php Modal::begin([
                'title' => '<h5 class="c-grey-900">Добавить лигу в команду</h5>',
                'size' => Modal::SIZE_LARGE,
                'toggleButton' => [
                    'label' => '<span class="fa fa-plus"></span> Добавить лигу в команду',
                    'class' => 'btn btn-outline-success',
                ]
            ]) ?>
            <?php $form = ActiveForm::begin([
                'action' => '/sport/team/attach-league',
                'method' => 'POST',
                'options' => [
                    'class' => 'js-form-attach-entity',
                    'data-pjax-selector' => '#leagues-pjax',
                ]
            ]) ?>

            <?= Html::hiddenInput('id', $model->id) ?>

            <?= Select2::widget([
                'name' => 'child_id',
                'data' => $availableLeagues ?? [],
            ]) ?>

            <br/>

            <?= Html::submitButton('Добавить лигу в команду', ['class' => 'btn btn-success js-submit-attach-entity']) ?>

            <?php ActiveForm::end() ?>
            <?php Modal::end() ?>
        </div>

        <br/>

        <?= GridView::widget([
            'dataProvider' => $leaDataProvider,
            'columns' => [
                [
                    'attribute' => 'r_name',
                    'value' => static function (SportLeagueAR $model) {
                        return Html::a($model->getTitle(), Url::to(['/sport/league/view', 'id' => $model->id]));
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'r_icon',
                    'value' => static function (SportLeagueAR $model) {
                        return $model->htmlImage();
                    },
                    'format' => 'raw',
                ],
                [
                    'value' => static function ($item) use ($model) {
                        return Html::button(Html::tag('span', null, ['class' => 'fa fa-trash']), [
                            'class' => 'btn btn-outline-danger js-btn-detach-entity',
                            'data' => [
                                'parent' => $model->id,
                                'child' => $item->id,
                                'url' => '/sport/team/attach-league?action=detach',
                                'pjax-selector' => '#leagues-pjax',
                            ],
                        ]);
                    },
                    'format' => 'raw',
                ],
            ],
        ]) ?>

        <?php Pjax::end() ?>
    <?php } ?>
</div>
