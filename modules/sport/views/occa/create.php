<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SportOccaAR
 * @var $matches array
 * @var $facts array
 * @var $smen array
 */

$this->title = 'Добавление спортивного события';
$this->params['breadcrumbs'][] = ['label' => 'Спортивные события', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sport-occa-ar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'matches' => $matches,
        'facts' => $facts,
        'smen' => $smen,
    ]) ?>

</div>
