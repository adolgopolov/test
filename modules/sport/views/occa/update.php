<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SportOccaAR
 * @var $matches array
 * @var $facts array
 * @var $smen array
 */

$this->title = 'Редактирование спортивного события: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Спортивные события', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
?>
<div class="sport-occa-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
        'matches' => $matches,
        'facts' => $facts,
        'smen' => $smen,
    ]) ?>
</div>
