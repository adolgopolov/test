<?php

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SportOccaAS;
use common\widgets\GridActions\GridActionsWidget;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var $this View
 * @var $searchModel SportOccaAS
 * @var $dataProvider ActiveDataProvider
 */

$this->title = 'Спортивные события';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    'id',
    'match_name',
    'match_id',
    'sman_name',
    'sman_id',
    'team',
    'team_id',
    'after_start',
    'fact_name',
    'fact_id',
    'occa_amount',
    'is_deleted:boolean',
    [
        'class' => ActionColumn::class,
        'template' => '<div class="btn-group">{view}{update}{soft-delete}{delete}</div>',
        'buttons' => [
            'soft-delete' => static function ($url) {
                $options = [
                    'title' => 'soft-delete',
                    'aria-label' => 'soft-delete',
                    'data-pjax' => '0',
                    'class' => 'btn btn-outline-default btn-info',
                    'data-confirm' => false,
                    'data-toggle' => 'tooltip',
                    'data-title' => 'soft-delete'
                ];
                $icon = Html::tag('em', '', ['class' => 'fas fa-trash']);

                return Html::a($icon, $url, $options);
            },
        ],
    ],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sport-occa-ar-index">
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#sport-occa-pjax',
        'pjaxUrl' => '/sport/occa/index',
        'model' => $searchModel,
        'additionalContainerOptions' => [
            'class' => [
                'columns-select2-margined',
            ],
        ],
    ]) ?>
    <?= GridActionsWidget::widget([
        'title' => $this->title,
    ]) ?>
    <?php Pjax::begin(['id' => 'sport-occa-pjax']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => ArrayHelper::merge([GridActionsWidget::$checkboxColumn], $columns),
    ]) ?>
    <?php Pjax::end() ?>
</div>
