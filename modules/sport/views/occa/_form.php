<?php

use common\widgets\DetailForm\DetailFormWidget;
use common\widgets\JsonEditor;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SportOccaAR
 * @var $form yii\widgets\ActiveForm
 * @var $matches array
 * @var $facts array
 * @var $smen array
 */
?>
<div class="sport-occa-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'match_id' => Select2::widget([
                'model' => $model,
                'attribute' => 'match_id',
                'data' => $matches,
                'options' => [
                    'placeholder' => 'Выберите матч',
                ],
            ]),
            'after_start',
            'is_deleted' => [
                'inputHidden' => $model->isNewRecord,
                'inputValue' => Select2::widget([
                    'model' => $model,
                    'attribute' => 'is_deleted',
                    'data' => [
                        'Нет',
                        'Да',
                    ],
                ]),
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'fact_id' => Select2::widget([
                'model' => $model,
                'attribute' => 'fact_id',
                'data' => $facts,
                'options' => [
                    'placeholder' => 'Выберите спортивный факт',
                ],
            ]),
            'sman_id' => Select2::widget([
                'model' => $model,
                'attribute' => 'sman_id',
                'data' => $smen,
                'options' => [
                    'placeholder' => 'Выберите спортсмена',
                ],
            ]),
            'occa_amount',
            'ext_info' => JsonEditor::widget([
                'model' => $model,
                'attribute' => 'ext_info',
            ]),
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
