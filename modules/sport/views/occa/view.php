<?php

use common\modules\sport\models\SportOccaAR;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SportOccaAR
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Спортивные события', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];

YiiAsset::register($this);
?>
<div class="sport-occa-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-arrow-left"></span> К списку', ['index'], [
            'class' => 'btn btn-outline-primary pull-left',
        ]) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'match_id',
                'value' => static function (SportOccaAR $model) {
                    return $model->match->r_name ?? null;
                }
            ],
            'after_start',
            'is_deleted',
            [
                'attribute' => 'fact_id',
                'value' => static function (SportOccaAR $model) {
                    return $model->fact->r_name ?? null;
                }
            ],
            [
                'attribute' => 'sman_id',
                'value' => static function (SportOccaAR $model) {
                    return $model->sman->r_name ?? null;
                }
            ],
            'occa_amount',
            'ext_info',
        ],
    ]) ?>
</div>
