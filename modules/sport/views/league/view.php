<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportTeamAR;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SportLeagueAR
 * @var $teamDataProvider \yii\data\ActiveDataProvider
 */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Лиги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];
?>
<div class="sport-league-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-arrow-left"></span> К списку', ['index'], [
            'class' => 'btn btn-outline-primary pull-left',
        ]) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Спорт',
                'value' => static function ($model) {
                    return $model->sport->r_name;
                },
            ],
            'r_name',
            [
                'attribute' => 'r_icon',
                'value' => static function (SportLeagueAR $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
            'country',
            [
                'attribute' => 'r_status',
                'value' => static function (SportLeagueAR $model) {
                    return $model->getStatusName();
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

    <h4 class="c-grey-900 mT-10 mB-30">Команды</h4>
    <?= GridView::widget([
        'dataProvider' => $teamDataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'r_name',
                'value' => static function (SportTeamAR $model) {
                    return Html::a($model->getTitle(), Url::to(['team/update', 'id' => $model->id]));
                },
                'format' => 'raw',
            ],
            'title.rus_short',
            'title.eng',
            'title.eng_short',
            [
                'attribute' => 'r_icon',
                'value' => static function (SportTeamAR $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'team_jersey',
                'value' => static function (SportTeamAR $model) {
                    return $model->htmlImage('team_jersey');
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'colors',
                'value' => static function (SportTeamAR $model) {
                    return $model->getColorList();
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>
</div>
