<?php

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SportLeagueAS;
use common\widgets\GridActions\GridActionsWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel SportLeagueAS
 * @var $sports array
 */

$this->title = 'Лиги';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    [
        'attribute' => 'sport_id_copy',
        'options' => [
            'class' => 'sport-id-grid-column',
        ],
        'value' => static function (SportLeagueAS $model) {
            return $model->sport_id;
        },
    ],
    [
        'attribute' => 'sport_id',
        'value' => static function (SportLeagueAS $model) {
            return $model->sport->r_name ?? null;
        },
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'sport_id',
            'data' => $sports,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
            'pjaxContainerId' => 'sport-league-pjax',
        ]),
    ],
    [
        'attribute' => 'r_name',
        'value' => static function (SportLeagueAS $model) {
            return $model->getTitle();
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'r_icon',
        'value' => static function (SportLeagueAS $model) {
            return $model->htmlImage();
        },
        'format' => 'raw',
    ],
    'country',
    [
        'class' => ActionColumn::class,
        'template' => '<div class="btn-group">{view}{update}</div>',
    ],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sport-league-ar-index">
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#sport-league-pjax',
        'pjaxUrl' => '/sport/league/index',
        'model' => $searchModel,
        'additionalContainerOptions' => [
            'class' => [
                'columns-select2-margined',
            ],
        ],
    ]) ?>
    <?= GridActionsWidget::widget([
        'title' => $this->title,
    ]) ?>
    <?php Pjax::begin(['id' => 'sport-league-pjax']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => ArrayHelper::merge([GridActionsWidget::$checkboxColumn], $columns),
    ]) ?>
    <?php Pjax::end() ?>
</div>
