<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\modules\sport\models\SportLeagueAR
 * @var $teamsProvider \yii\data\ActiveDataProvider
 * @var $availableTeams array
 */

use yii\bootstrap4\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\modules\sport\models\SportTeamAR;
use kartik\select2\Select2;

?>
<?php Pjax::begin(['id' => 'teams-pjax']) ?>
    <div class="text-right">
        <?php Modal::begin([
            'title' => "<h5 class=\"c-grey-900\">Добавить команду в лигу <strong>{$model->r_name}</strong></h5>",
            'size' => Modal::SIZE_LARGE,
            'toggleButton' => [
                'label' => '<span class="fa fa-plus"></span> Добавить команду в лигу',
                'class' => 'btn btn-outline-success',
            ]
        ]) ?>
        <?php $form = ActiveForm::begin([
            'action' => '/sport/league/attach-team',
            'method' => 'POST',
            'options' => [
                'class' => 'js-form-attach-entity text-left',
                'data-pjax-selector' => '#teams-pjax',
            ],
        ]) ?>

        <?= Html::hiddenInput('id', $model->id) ?>

        <div>Начните вводить название команды:</div>
        <br/>
        <?= Select2::widget(['name' => 'child_id', 'data' => $availableTeams]) ?>
        <hr>
        <div class="text-right">
            <?= Html::submitButton('<span class="fa fa-save"></span> Добавить', ['class' => 'btn btn-success js-submit-attach-entity']) ?>
        </div>

        <?php ActiveForm::end() ?>
        <?php Modal::end() ?>
    </div>

<?= GridView::widget([
    'dataProvider' => $teamsProvider,
    'columns' => [
        [
            'attribute' => 'id',
            'options' => [
                'class' => 'id-grid-column',
            ],
        ],
        [
            'attribute' => 'r_name',
            'value' => static function (SportTeamAR $model) {
                return Html::a($model->getTitle(), Url::to(['team/update', 'id' => $model->id]));
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'r_icon',
            'value' => static function (SportTeamAR $model) {
                return $model->htmlImage();
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'team_jersey',
            'value' => static function (SportTeamAR $model) {
                return $model->htmlImage('team_jersey');
            },
            'format' => 'raw',
        ],
        [
            'value' => static function (SportTeamAR $item) use ($model) {
                return Html::button(Html::tag('span', null, ['class' => 'fa fa-trash']), [
                    'class' => 'btn btn-outline-danger js-btn-detach-entity',
                    'data' => [
                        'parent' => $model->id,
                        'child' => $item->id,
                        'url' => '/sport/league/attach-team?action=detach',
                        'pjax-selector' => '#teams-pjax',
                    ],
                ]);
            },
            'format' => 'raw'
        ],
    ],
]) ?>
<?php Pjax::end() ?>