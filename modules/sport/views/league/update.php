<?php

use yii\helpers\Html;
use kartik\tabs\TabsX;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SportLeagueAR
 * @var $teamsProvider
 * @var $availableTeams
 * @var $model common\modules\sport\models\SportLeagueAR
 */

$this->title = 'Редактирование лиги : ' . $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Лиги', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->r_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';

$tabs = [
    [
        'label' => '<span class="ti ti-pencil"></span> Основная информация',
        'active' => true,
        'content' => $this->render('_form', compact('model', 'sports', 'statusList'))
    ],
    [
        'label' => '<span class="ti ti-layout-grid3"></span> Команды',
        'content' => $this->render('_teams', [
            'teamsProvider' => $teamsProvider,
            'model' => $model,
            'availableTeams' => $availableTeams,
        ])
    ],
];
?>
<div class="sport-league-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= TabsX::widget(['items' => $tabs, 'encodeLabels' => false]) ?>
</div>
