<?php

use common\modules\sport\models\SportLeagueAR;
use common\widgets\DetailForm\DetailFormWidget;
use common\widgets\image\ImageWidget;
use common\widgets\JsonEditor;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SportLeagueAR
 * @var $form yii\widgets\ActiveForm
 * @var $sports array
 * @var $statusList array
 */
?>
<div class="sport-league-ar-form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]) ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'sport_id' => Select2::widget([
                'model' => $model,
                'attribute' => 'sport_id',
                'data' => $sports,
                'options' => [
                    'placeholder' => 'Выберите спорт',
                ],
            ]),
            'r_name',
            'r_icon' => [
                'inputHidden' => $model->isNewRecord,
                'value' => ImageWidget::widget([
                    'model' => $model,
                    'attribute' => 'r_icon',
                    'kind' => SportLeagueAR::kind(),
                ]),
            ],
            'country',
            'r_status' => Select2::widget([
                'model' => $model,
                'attribute' => 'r_status',
                'data' => $statusList,
                'options' => [
                    'placeholder' => 'Статус',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]),
            'ext_info' => JsonEditor::widget([
                'model' => $model,
                'attribute' => 'ext_info',
            ]),
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
