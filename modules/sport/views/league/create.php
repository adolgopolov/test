<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sport\models\SportLeagueAR
 * @var $sports array
 * @var $statusList array
 */

$this->title = 'Добавление лиги';
$this->params['breadcrumbs'][] = ['label' => 'Лиги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sport-league-ar-create">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model'     => $model,
        'sports'    => $sports,
        'statusList' => $statusList,
    ]) ?>

</div>
