<?php

namespace common\modules\sport;

use backend\components\aboard\MenuEntryInterface;
use common\modules\game\models\SportMatchAR;
use common\modules\game\senders\SportMatchSender;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportProvider;
use Yii;

/**
 * Description of SportModule
 *
 * @property SportProvider $provider
 *
 * @author Skynin <sohay_ua@yahoo.com>
 */
class SportModule extends \yii\base\Module implements MenuEntryInterface
{
    public $provider;

    public function init()
    {
        if (!empty($this->provider)) {
            $this->provider = Yii::createObject($this->provider);
        }

        parent::init();
    }

    public function createSman(array $souSman)
    {
        //todo создать спортсмена вернуть его айди
        $smanAR = new SmanAR([

        ]);

        if ($smanAR->save()) {
            return $smanAR->id;
        }

        return false;
    }

    public function createMatch(array $souMatch)
    {
        //todo создать матч вернуть его айди
        $model = new SportMatchAR([
            'r_name',
            'team_owner',
            'team_guest',
            'r_status',
            'sport_id',
            'lea_id',
        ]);

        if ($model->save()) {
            $model->sendModel(SportMatchSender::SCENARIO_CREATE);

            return $model->id;
        }

        return false;
    }

    public function createTeam(array $souTeam)
    {
        //todo создать команду вернуть его айди
        return true;
    }

    public static function getMenuItems(): ?array
    {
        return [
            [
                'label' => 'Спорт',
                'icon' => 'basketball',
                'color' => 'yellow-900',
                'url' => '#',
                'items' => [
                    [
                        'label' => 'Спортсмены',
                        'url' => ['/sport/sman/index']
                    ],
                    [
                        'label' => 'Лиги',
                        'url' => ['/sport/league/index']
                    ],
                    [
                        'label' => 'Команды',
                        'url' => ['/sport/team/index']
                    ],
                    [
                        'label' => 'Спортивные факты',
                        'url' => ['/sport/fact/index']
                    ],
                    [
                        'label' => 'Амплуа, звания',
                        'url' => ['/sport/position/index']
                    ],
                    [
                        'label' => 'Виды спорта',
                        'url' => ['/sport/sport/index']
                    ],
                    [
                        'label' => 'Позиции спортсменов',
                        'url' => ['/sport/member/index']
                    ],
                    [
                        'label' => 'Спортивные события',
                        'url' => ['/sport/occa/index']
                    ],
                ]
            ]
        ];
    }

}
