<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 13.04.19 11:55
 */

namespace common\modules\sport\doings;

use common\components\S3Component;
use common\modules\sount\dto\MatchOccaDTO;
use common\modules\sport\models\SportOccaAR;

/*
 * todo!!! DRAFT  перенести в common/doings/sport/ChangeMatchOccasionDoing.php и удалить
 *
 *  @brief класс для реализации действий с событиями
 *  создание , перевод в разные состояния и т.д.
 */
class OccaDoing extends S3Component
{

    public $match;
    /** @var SportOccaAR[] | null */
    protected $records;

    public static function loadOccaForMatch(MatchOccaDTO $matchOcca)
    {
        $doing = new self(['match' => $matchOcca]);
        foreach ($doing->match as $eachOcca) {
            $occaAR = $doing->getArFor($eachOcca);
            if($matchOcca->is_aggregate) {
                $occaAR->occa_amount =  (int) ($occaAR->occa_amount/$matchOcca->getCountGame());
            }
            $occaAR->save();
            $occaAR->errors;
        }
    }

    /**
     * @param $arrayOcca
     * @return SportOccaAR
     */
    protected function getArFor($arrayOcca): SportOccaAR
    {
        if($this->records === null) {
            $this->records = self::getOccaRecs();
        }
        if($rec = array_pop($this->records)) {
            $rec->setAttributes($arrayOcca);
            return $rec;
        }
        return new SportOccaAR($arrayOcca);
    }

    /**
     * @return SportOccaAR[]
     */
    protected function getOccaRecs()
    {
        SportOccaAR::markDeletedAll($this->match->id);
        //todo заблокировать записи
        return SportOccaAR::find()->whereMatch($this->match->id)->all();
    }


}