<?php

namespace common\modules\sandbox;

use backend\components\aboard\MenuEntryInterface;
use backend\components\aboard\DashboardWidgetInterface;

/**
 * sandbox module definition class
 */
class Module extends \yii\base\Module implements MenuEntryInterface, DashboardWidgetInterface
{
    public $defaultRoute = 'demo';

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\sandbox\controllers';

    /**
     * {@inheritDoc}
     */
    public static function getMenuItems(): ?array
    {
        return [
            [
                'label' => 'Демо-модуль',
                'icon'  => 'ruler-pencil',
                'color' => 'red-500',
                'items' => [
                    [
                        'label' => 'Dashboard',
                        'icon'  => 'dashboard',
                        'color' => 'orange-500',
                        'url'   => ['/site/index']
                    ],
                    [
                        'label' => 'Календарь',
                        'icon'  => 'calendar',
                        'color' => 'purple-500',
                        'url'   => ['/sandbox/demo/calendar']
                    ],
                    [
                        'label' => '3-level menu',
                        'icon'  => 'menu-alt',
                        'color' => 'green-800',
                        'items' => [
                            [
                                'label' => 'Second level link',
                                'icon'  => 'menu',
                                'color' => 'yellow-800'
                            ],
                            [
                                'label' => 'Second - submenu',
                                'icon'  => 'book',
                                'color' => 'blue-800',
                                'items' => [
                                    [
                                        'label' => '3rd level with icon',
                                        'icon'  => 'cloud',
                                        'color' => 'blue-300'
                                    ],
                                    [
                                        'label' => 'Without icon',
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    public static function getDashboardCounters(): ?array
    {
        return [
            '<div class="col">
              <div class="layers bd bgc-white p-20">
                <div class="layer w-100 mB-10">
                  <h6 class="lh-1">Тестовый счетчик</h6>
                </div>
                <div class="layer w-100">
                  <div class="peers ai-sb fxw-nw">
                    <div class="peer peer-greed">
                      <span id="sparklinedash4"></span>
                    </div>
                    <div class="peer">
                      <span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">38</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>',
            '<div class="col">
              <div class="layers bd bgc-white p-20">
                <div class="layer w-100 mB-10">
                  <h6 class="lh-1">Тестовый счетчик #2</h6>
                </div>
                <div class="layer w-100">
                  <div class="peers ai-sb fxw-nw">
                    <div class="peer peer-greed">
                      <span id="sparklinedash4"></span>
                    </div>
                    <div class="peer">
                      <span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">162</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>'
        ];
    }

    public static function getDashboardWidgets(): ?array
    {
        return [
          '  <div class="masonry-item col-12">
          <div class="bd bgc-white">
            <div class="peers fxw-nw@lg+ ai-s">
              <div class="peer bdL p-20 w-100p@lg+ w-100p@lg-">
                <div class="layers">
                  <div class="layer w-100">
                    <!-- Progress Bars -->
                    <div class="layers">
                      <div class="layer w-100">
                        <h5 class="mB-5">Тестовый виджет для дашбоарда</h5>
                        <small class="fw-600 c-grey-700">Visitors From USA</small>
                        <span class="pull-right c-grey-600 fsz-sm">50%</span>
                        <div class="progress mT-10">
                          <div class="progress-bar bgc-deep-purple-500" role="progressbar" aria-valuenow="50"
                               aria-valuemin="0" aria-valuemax="100" style="width:50%;"><span
                              class="sr-only">50% Complete</span></div>
                        </div>
                      </div>
                      <div class="layer w-100 mT-15">
                        <h5 class="mB-5">1M</h5>
                        <small class="fw-600 c-grey-700">Visitors From Europe</small>
                        <span class="pull-right c-grey-600 fsz-sm">80%</span>
                        <div class="progress mT-10">
                          <div class="progress-bar bgc-green-500" role="progressbar" aria-valuenow="50" aria-valuemin="0"
                               aria-valuemax="100" style="width:80%;"><span class="sr-only">80% Complete</span></div>
                        </div>
                      </div>
                      <div class="layer w-100 mT-15">
                        <h5 class="mB-5">450k</h5>
                        <small class="fw-600 c-grey-700">Visitors From Australia</small>
                        <span class="pull-right c-grey-600 fsz-sm">40%</span>
                        <div class="progress mT-10">
                          <div class="progress-bar bgc-light-blue-500" role="progressbar" aria-valuenow="50" aria-valuemin="0"
                               aria-valuemax="100" style="width:40%;"><span class="sr-only">40% Complete</span></div>
                        </div>
                      </div>
                      <div class="layer w-100 mT-15">
                        <h5 class="mB-5">1B</h5>
                        <small class="fw-600 c-grey-700">Visitors From India</small>
                        <span class="pull-right c-grey-600 fsz-sm">90%</span>
                        <div class="progress mT-10">
                          <div class="progress-bar bgc-blue-grey-500" role="progressbar" aria-valuenow="50" aria-valuemin="0"
                               aria-valuemax="100" style="width:90%;"><span class="sr-only">90% Complete</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>',
        '<div class="masonry-item col-md-12">
        <!-- #Todo ==================== -->
        <div class="bd bgc-white p-20">
          <div class="layers">
            <div class="layer w-100 mB-10">
              <h6 class="lh-1">Todo List - тестовый виджет для дашбоарда #2</h6>
            </div>
            <div class="layer w-100">
              <ul class="list-task list-group" data-role="tasklist">
                <li class="list-group-item bdw-0" data-role="task">
                  <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                    <input type="checkbox" id="inputCall1" name="inputCheckboxesCall" class="peer">
                    <label for="inputCall1" class=" peers peer-greed js-sb ai-c">
                      <span class="peer peer-greed">Call John for Dinner</span>
                    </label>
                  </div>
                </li>
                <li class="list-group-item bdw-0" data-role="task">
                  <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                    <input type="checkbox" id="inputCall2" name="inputCheckboxesCall" class="peer">
                    <label for="inputCall2" class=" peers peer-greed js-sb ai-c">
                      <span class="peer peer-greed">Book Boss Flight</span>
                      <span class="peer">
                                    <span class="badge badge-pill fl-r badge-success lh-0 p-10">2 Days</span>
                                  </span>
                    </label>
                  </div>
                </li>
                <li class="list-group-item bdw-0" data-role="task">
                  <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                    <input type="checkbox" id="inputCall3" name="inputCheckboxesCall" class="peer">
                    <label for="inputCall3" class=" peers peer-greed js-sb ai-c">
                      <span class="peer peer-greed">Hit the Gym</span>
                      <span class="peer">
                                    <span class="badge badge-pill fl-r badge-danger lh-0 p-10">3 Minutes</span>
                                  </span>
                    </label>
                  </div>
                </li>
                <li class="list-group-item bdw-0" data-role="task">
                  <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                    <input type="checkbox" id="inputCall4" name="inputCheckboxesCall" class="peer">
                    <label for="inputCall4" class=" peers peer-greed js-sb ai-c">
                      <span class="peer peer-greed">Give Purchase Report</span>
                      <span class="peer">
                                    <span class="badge badge-pill fl-r badge-warning lh-0 p-10">not important</span>
                                  </span>
                    </label>
                  </div>
                </li>
                <li class="list-group-item bdw-0" data-role="task">
                  <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                    <input type="checkbox" id="inputCall5" name="inputCheckboxesCall" class="peer">
                    <label for="inputCall5" class=" peers peer-greed js-sb ai-c">
                      <span class="peer peer-greed">Watch Game of Thrones Episode</span>
                      <span class="peer">
                                    <span class="badge badge-pill fl-r badge-info lh-0 p-10">Tomorrow</span>
                                  </span>
                    </label>
                  </div>
                </li>
                <li class="list-group-item bdw-0" data-role="task">
                  <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                    <input type="checkbox" id="inputCall6" name="inputCheckboxesCall" class="peer">
                    <label for="inputCall6" class=" peers peer-greed js-sb ai-c">
                      <span class="peer peer-greed">Give Purchase report</span>
                      <span class="peer">
                                    <span class="badge badge-pill fl-r badge-success lh-0 p-10">Done</span>
                                  </span>
                    </label>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>'
        ];
    }
}
