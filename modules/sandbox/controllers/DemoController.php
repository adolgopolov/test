<?php

namespace common\modules\sandbox\controllers;

use backend\controllers\AdminController;

/**
 * Default controller for the `sandbox` module
 */
class DemoController extends AdminController
{
    /**
     * Renders the index view for the module
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCalendar()
    {
        return $this->render('calendar');
    }
}
