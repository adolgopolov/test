<?php

use backend\components\aboard\widgets\GridView;
use common\modules\feedback\models\FeedbackAR;
use common\modules\user\models\UserAR;
use yii\helpers\StringHelper;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Обратная связь';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            'subject',
            [
                'attribute' => 'content',
                'value' => static function (FeedbackAR $model) {
                    return StringHelper::truncate($model->content, 30);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'user_id',
                'value' => static function (FeedbackAR $model) {
                    if ($model->user_id === UserAR::GUEST_USER) {
                        return 'Гость';
                    }

                    return Html::a($model->user->username, Url::to(['/user/manage/view', 'id' => $model->user_id]));
                },
                'format' => 'raw',
            ],
            'created_at:date',
            ['class' => 'backend\components\aboard\ActionColumn', 'template' => '<div class="btn-group">{view}</div>'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
