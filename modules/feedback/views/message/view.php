<?php

use common\modules\user\models\UserAR;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\YiiAsset;

$this->title = $model->subject;
$this->params['breadcrumbs'][] = ['label' => 'Сообщения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';

YiiAsset::register($this);
?>
<div class="post-ar-view">
    <p class="text-right">
        <a href="<?= Url::to(['index']) ?>" class="btn btn-outline-primary pull-left"><span class="fa fa-arrow-left"></span> Вернуться к списку</a>
    </p>
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <br>
    <h4>Тема:</h4>
    <article><?= $model->subject ?></article>
    <br>
    <h4>Полный текст:</h4>
    <article><?= $model->content ?></article>

    <h4>Пользователь:</h4>
    <article><?= $model->user_id === UserAR::GUEST_USER ? 'Гость' : Html::a($model->user->username, Url::to(['/user/manage/view', 'id' => $model->user_id])) ?></article>

    <?php if (!empty($model->email)): ?>
        <h4>Email:</h4>
        <article><?= $model->email ?></article>
    <?php endif; ?>
</div>
