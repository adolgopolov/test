<?php

namespace common\modules\feedback\controllers;

use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\modules\feedback\models\FeedbackAR;
use yii\data\ActiveDataProvider;

/**
 * PostController implements the CRUD actions for PostAR model.
 */
class MessageController extends AdminController
{
    use CRUDTrait;

    protected $modelClass = FeedbackAR::class;

    /**
     * Lists all PostAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => FeedbackAR::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = FeedbackAR::findOrFail($id);

        return $this->render('update', compact('model'));
    }
}
