<?php

namespace common\modules\feedback\models;

use common\components\S3Kind;
use common\extensions\ExtInfoAR;
use common\modules\user\models\UserAR;

/**
 * Class FeedbackAR
 * @package common\modules\cms\models
 */
class FeedbackAR extends ExtInfoAR
{
    /**
     * @return int|null
     */
    public static function kind()
    {
        return S3Kind::KIND_FEEDBACK;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%feedback}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject', 'content', 'user_id', 'sys_info'], 'required'],
            [['subject', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['content', 'sys_info'], 'string'],
            ['created_at', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Тема',
            'email' => 'Email',
            'content' => 'Сообщение пользователя',
            'sys_info' => 'Системная информация',
            'user_id' => 'Пользователь',
            'created_at' => 'Дата',
        ];
    }

    /**
     * @return \common\extensions\ExtAQ|\common\modules\feedback\models\FeedbackAQ|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return (new FeedbackAQ(static::class));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserAR::class, ['id' => 'user_id']);
    }
}
