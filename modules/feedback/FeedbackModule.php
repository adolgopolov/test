<?php

namespace common\modules\feedback;

use backend\components\aboard\MenuEntryInterface;

/**
 * Class FeedbackModule
 * @package common\modules\game
 */
class FeedbackModule extends \yii\base\Module implements MenuEntryInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getMenuItems(): ?array
    {
        return [
            [
                'label' => 'Обратная связь',
                'icon'  => 'game',
                'color' => 'green-800',
                'items' => [
                    [
                        'label' => 'Сообщения от пользователей',
                        'url'   => ['/feedback/message/index']
                    ],
                ]
            ]
        ];
    }
}
