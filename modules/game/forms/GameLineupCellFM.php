<?php

namespace common\modules\game\forms;

use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameLineupCellAR;
use common\modules\game\senders\GameLineupSender;

/**
 * Class GameLineupCellFM
 * @package common\modules\game\forms
 * @property array $sman_id
 * @property array $sman_points
 */
class GameLineupCellFM extends GameLineupCellAR
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['sman_id'], 'required'],
            [['sman_id', 'sman_points'], 'safe'],
        ];
    }

    /**
     * @param $lineupId
     * @return bool
     */
    public function saveCells($lineupId)
    {
        $cellIds = array_keys($this->sman_id);
        $models = $this->getModels($cellIds, $lineupId);

        foreach ($models as $model) {
            $model->sman_id = $this->sman_id[$model->cell_id];
            $model->sman_points = $this->sman_points[$model->cell_id];

            if (!$model->save()) {
                $this->addErrors($model->errors);
            }
        }

        $lineup = GameLineupAR::findOne($lineupId);
        GameLineupSender::sendModel($lineup);

        return !$this->hasErrors();
    }

    /**
     * @param $cellIds
     * @param $lineupId
     * @return array|GameLineupCellFM[]|GameLineupCellAR[]
     */
    public function getModels($cellIds, $lineupId)
    {
        return self::find()
            ->andWhere([
                'cell_id' => $cellIds,
                'lineup_id' => $lineupId,
            ])
            ->all();
    }
}
