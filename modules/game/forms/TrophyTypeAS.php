<?php

namespace common\modules\game\forms;

use common\modules\game\models\TrophyTypeAR;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class TrophyTypeAS extends TrophyTypeAR
{
    public $is_free;
    public $is_auto_pfund;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'r_name'], 'string'],
            [['is_free', 'is_auto_pfund'], 'boolean'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->alias('trophy_type')
            ->select([
                'trophy_type.*',
                "JSON_EXTRACT(trophy_type.ext_info, '$.is_free') as is_free",
                "JSON_EXTRACT(trophy_type.ext_info, '$.is_auto_pfund') as is_auto_pfund",
                "JSON_EXTRACT(trophy_type.ext_info, '$.buyin_currency') as buyin_currency_ids",
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                'id' => SORT_DESC,
            ],
            'attributes' => ArrayHelper::merge($dataProvider->sort->attributes, [
                'is_free',
                'is_auto_pfund',
            ]),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        $query->andFilterWhere([
            'AND',
            ['id' => $id],
            ['like', 'r_name', $this->r_name],
        ]);

        $query->andFilterHaving([
            'AND',
            ['is_free' => $this->is_free],
            ['is_auto_pfund' => $this->is_auto_pfund],
        ]);

        return $dataProvider;
    }
}
