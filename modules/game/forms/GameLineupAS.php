<?php

namespace common\modules\game\forms;

use common\modules\game\models\GameLineupAR;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class GameLineupAS extends GameLineupAR
{
    public $columns;
    public $tou_id_copy;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'user_name'], 'string'],
            [['user_id', 'tou_id', 'tou_id_copy', 'l_points', 'l_award', 'l_place'], 'integer'],
            [['is_draft', 'isnt_reg'], 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'tou_id_copy' => 'ID Турнира',
        ]);
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $alias = 'lineup';

        $markDeleteAttributes = self::markDeleteAttributes(true, $alias);

        $query = self::find()
            ->alias($alias)
            ->andWhere(['not', $markDeleteAttributes]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                'id' => SORT_DESC,
            ],
            'attributes' => ArrayHelper::merge($dataProvider->sort->attributes, [
            ]),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        $query->andFilterWhere([
            'AND',
            ['lineup.id' => $id],
            ['lineup.user_id' => $this->user_id],
            ['like', 'lineup.user_name', $this->user_name],
            ['lineup.tou_id' => $this->tou_id_copy ?: $this->tou_id],
            ['lineup.l_points' => $this->l_points],
            ['lineup.l_award' => $this->l_award],
            ['lineup.l_place' => $this->l_place],
            ['lineup.is_draft' => $this->is_draft],
            ['lineup.isnt_reg' => $this->isnt_reg],
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'user_id',
            'user_name',
            'tou_id',
            'l_points',
            'l_place',
            'l_award',
            'is_draft',
            'isnt_reg',
        ];
    }
}
