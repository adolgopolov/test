<?php

namespace common\modules\game\forms;

use common\modules\game\models\SportMatchAR;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

class SportMatchAS extends SportMatchAR
{
    public $start_date_ru;
    public $sport_id_copy;
    public $lea_id_copy;
    public $columns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_status', 'sport_id', 'sport_id_copy', 'lea_id', 'lea_id_copy', 'team_owner', 'team_guest', 'total_owner', 'total_guest'], 'integer'],
            [['id', 'r_name', 'start_date'], 'string'],
            [['ext_info'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'sport_id_copy' => 'ID Спорта',
            'lea_id_copy' => 'ID Лиги',
        ]);
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws Exception
     */
    public function search($params)
    {
        $query = static::find()
            ->alias('match')
            ->setLcTimeNames()
            ->select(['*', "DATE_FORMAT(match.start_date,'%Y %b %d, %H:%i') AS 'start_date_ru'"])
            ->with('sport');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AND',
            ['match.id' => $id],
            ['like', 'match.r_name', $this->r_name],
            ['like', "DATE_FORMAT(match.start_date,'%Y %b %d, %H:%i')", $this->start_date],
            ['like', 'match.lea_id', $this->lea_id_copy ?: $this->lea_id],
            ['like', 'match.r_status', $this->r_status],
            ['match.sport_id' => $this->sport_id_copy ?: $this->sport_id],
        ]);

        $query->andFilterWhere(['like', 'ext_info', $this->ext_info]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'sport_id',
            'sport_id_copy',
            'lea_id',
            'lea_id_copy',
            'r_status',
            'start_date',
        ];
    }
}
