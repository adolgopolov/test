<?php

namespace common\modules\game\forms;

use common\modules\game\models\SportMatchAR;
use common\modules\sport\models\SportFactAR;
use common\modules\sport\models\SportOccaAR;
use Throwable;
use yii\base\Model;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;

/**
 * Class GameLineupCellFM
 * @package common\modules\game\forms
 */
class SportLineupFM extends Model
{
    /** @var SportOccaAR[] */
    protected $occa;
    /** @var SportMatchAR */
    public $match;
    /** @var array */
    public $smen;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['smen', 'match'], 'required'],
        ];
    }

    /**
     * @return bool
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function update()
    {
        if (!empty($this->smen)) {
            foreach ($this->smen as $sman_id => $isPresence) {
                if ($isPresence) {
                    if (!$this->saveOccaPresence($sman_id)) {
                        return false;
                    }
                } else {
                    $this->deleteOccaPresence($sman_id);
                }

            }
        }

        return !$this->hasErrors();
    }

    /**
     * Добавить событие был на поле
     * @param $sman_id
     * @return bool
     */
    public function saveOccaPresence($sman_id)
    {
        if ($this->hasOccaPresence($sman_id)) {
            return true;
        }

        $occa = new SportOccaAR([
            'sman_id' => $sman_id,
            'match_id' => $this->match->id,
            'fact_id' => SportFactAR::FACT_MATCH_PRESENCE,
            'after_start' => 0,
            'occa_amount' => 0,
        ]);

        if (!$occa->save()) {
            $this->addErrors($occa->errors);

            return false;
        }

        $this->occa[$occa->sman_id] = $occa;

        return true;
    }

    /**
     * @param int $sman_id
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function deleteOccaPresence($sman_id)
    {
        if ($this->hasOccaPresence($sman_id)) {
            $this->occa[$sman_id]->delete();
        }
    }

    /**
     * проверочная функция есть ли событие для спортсмена
     * @param $sman_id
     * @return bool
     */
    public function hasOccaPresence($sman_id)
    {
        if (empty($this->occa)) {
            $this->occa = SportOccaAR::findAll([
                'match_id' => $this->match->id,
                'fact_id' => SportFactAR::FACT_MATCH_PRESENCE
            ]);

            $this->occa = ArrayHelper::index($this->occa, 'sman_id');
        }

        return array_key_exists($sman_id, $this->occa);
    }
}
