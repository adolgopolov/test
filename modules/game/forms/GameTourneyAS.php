<?php

namespace common\modules\game\forms;

use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\user\models\UserAR;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

class GameTourneyAS extends GameTourneyAR
{
    public $start_date_ru;
    public $gamers;
    public $sport_id_copy;
    public $lea_id_copy;
    public $columns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'r_name', 'r_icon', 'start_date', 'gamers'], 'string'],
            [['ext_info', 'buyin'], 'safe'],
            [['sport_id', 'lea_id', 'r_status',
                'prizefund', 'lup_budget', 'max_smen', 'max_gamers',
                'reg_lineups', 'trophy_kind', 'score_kind', 'sport_id_copy', 'lea_id_copy'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'gamers' => 'Игроки',
            'sport_id_copy' => 'ID Спорта',
            'lea_id_copy' => 'ID Лиги',
        ]);
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws Exception
     */
    public function search($params)
    {
        // TODO ужас какой-то. Не понакручено ли?

        $lineupsQuery = GameLineupAR::find()
            ->alias('lup')
            ->select(['user_id'])
            ->andWhere('lup.tou_id = tou.id');

        $usersQuery = UserAR::find()
            ->alias('user')
            ->select(["GROUP_CONCAT(' ', user.username)"])
            ->andWhere('user.id IN (' . $lineupsQuery->createCommand()->rawSql . ')');

        $query = static::find()
            ->alias('tou')
            ->setLcTimeNames()
            ->select([
                'tou.*',
                "DATE_FORMAT(tou.start_date,'%Y %b %d, %H:%i:%s') AS start_date_ru",
                '(' . $usersQuery->createCommand()->rawSql . ') as gamers',
            ])
            ->with('sport');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
                'attributes' => [
                    'id',
                    'sport_id',
                    'lea_id',
                    'r_status',
                    'r_name',
                    'r_icon',
                    'buyin',
                    'prizefund',
                    'max_gamers',
                    'gamers',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        $query->andFilterWhere([
            'AND',
            ['tou.id' => $id],
            ['like', 'tou.r_name', $this->r_name],
            ['like', 'tou.r_icon', $this->r_icon],
            ['like', "DATE_FORMAT(tou.start_date,'%Y %b %d, %H:%i:%s')", $this->start_date_ru],
            ['tou.sport_id' => $this->sport_id_copy ?: $this->sport_id],
            ['tou.lea_id' => $this->lea_id_copy ?: $this->lea_id],
            ['tou.r_status' => $this->r_status],
            ['tou.prizefund' => $this->prizefund],
            ['tou.lup_budget' => $this->lup_budget],
            ['tou.max_smen' => $this->max_smen],
            ['tou.max_gamers' => $this->max_gamers],
            ['tou.reg_lineups' => $this->reg_lineups],
            ['tou.trophy_kind' => $this->trophy_kind],
            ['tou.score_kind' => $this->score_kind],
            ['tou.reg_lineups' => $this->reg_lineups],
            ['like', 'ext_info', $this->ext_info],
            ['like', 'buyin', $this->buyin],
        ]);

        $query->andFilterHaving(['like', 'gamers', $this->gamers]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'sport_id_copy',
            'sport_id',
            'lea_id_copy',
            'lea_id',
            'r_status',
            'r_icon',
            'buyin',
            'prizefund',
            'max_gamers',
            'gamers',
        ];
    }
}
