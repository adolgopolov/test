<?php

namespace common\modules\game\senders;

use common\components\pusher\senders\AbstractSender;
use common\components\pusher\traits\BeforeSendTrait;
use common\components\S3Core;
use common\components\S3Kind;
use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameTourneyAR;
use Yii;

/**
 * Class GameLineupSender
 * @package common\components\pusher\senders
 */
class GameLineupSender extends AbstractSender
{
    use BeforeSendTrait;

    /** @var GameLineupAR */
    public $model;

    /**
     * @return void
     */
    public function defaultScenario()
    {
        /** @var GameLineupAR|null $clonedModel */
        $clonedModel = $this->getData('clonedModel');
        $tou_id = $clonedModel ? $clonedModel->getOldAttribute('tou_id') : $this->model->tou_id;
        $tourney = GameTourneyAR::findOne($tou_id);

        if (!$tourney) {
            return;
        }

        if ($tourney->r_status & S3Core::STATUS_LIVE || $tourney->r_status & S3Core::STATUS_PROCESSING) {
            self::sendUpdatePush($tou_id);
        }
    }

    /**
     * @param int $tou_id
     */
    public static function sendUpdatePush($tou_id) // TODO название не соответствует действию: GameLineupSender отправлаяет... турнир?
    {
        Yii::$app->pushSender->privateTou->update
            ->setSubjectId($tou_id)
            ->setKind(S3Kind::KIND_UPDATE_DATA)
            ->addMeta('kind_data', S3Kind::combine(S3Kind::KIND_USER, S3Kind::KIND_LINEUP))
            ->addMeta('params', [
                'tou_id' => $tou_id,
            ])
            ->pushJob();
    }
}
