<?php

namespace common\modules\game\senders;

use common\components\pusher\senders\AbstractSender;
use common\components\pusher\traits\BeforeSendTrait;
use common\components\S3Core;
use common\components\S3Kind;
use common\modules\game\models\SportMatchAR;
use Yii;
use yii\base\InvalidConfigException;

class SportMatchSender extends AbstractSender
{
    use BeforeSendTrait;
    
    public const SCENARIO_CREATE = 'create';
    public const SCENARIO_UPDATE = 'update';

    /** @var SportMatchAR */
    public $model;

    /**
     * @throws InvalidConfigException
     */
    public function updateScenario()
    {
        /** @var SportMatchAR|null $clonedModel */
        $clonedModel = $this->getData('clonedModel');

        if (!$clonedModel) {
            return;
        }

        $changedStartDate = $clonedModel->isAttributeChanged('start_date');
        $changedStatus = $clonedModel->isAttributeChanged('r_status');
        $changedTotalOwner = $clonedModel->isAttributeChanged('total_owner');
        $changedTotalGuest = $clonedModel->isAttributeChanged('total_guest');
        $oldStatus = $clonedModel->getOldAttribute('r_status');

        if ($oldStatus & S3Core::STATUS_WAITING && ($changedStartDate || $changedStatus)) {
            Yii::$app->pushSender->global->update
                ->addOneRecord('matches', $this->model)
                ->pushJob();

            $this->sendMatch();

            return;
        }

        if (($oldStatus & S3Core::STATUS_LIVE || $oldStatus & S3Core::STATUS_PROCESSING) &&
            ($changedStartDate || $changedStatus || $changedTotalOwner || $changedTotalGuest)) {
            $this->sendMatch();
        }
    }

    /**
     * @return void
     */
    public function createScenario()
    {
    }

    /**
     * @throws InvalidConfigException
     */
    public function sendMatch()
    {
        $tourneys = $this->model->getTourneys()
            ->all();

        foreach ($tourneys as $tourney) {
            Yii::$app->pushSender->privateTou->update
                ->setSubjectId($tourney->id)
                ->setKind(S3Kind::KIND_USER)
                ->addOneRecord('matches', $this->model)
                ->pushJob();
        }
    }
}
