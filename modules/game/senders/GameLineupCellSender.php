<?php

namespace common\modules\game\senders;

use common\components\pusher\senders\AbstractSender;
use common\components\pusher\traits\BeforeSendTrait;
use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameLineupCellAR;
use yii\base\InvalidConfigException;

class GameLineupCellSender extends AbstractSender
{
    use BeforeSendTrait;

    public const SCENARIO_UPDATE = 'update';

    /** @var GameLineupCellAR */
    public $model;

    /**
     * @throws InvalidConfigException
     */
    public function updateScenario()
    {
        /** @var GameLineupAR $lineup */
        $lineup = $this->model->getLineup()
            ->one();

        GameLineupSender::sendModel($lineup);
    }
}
