<?php

namespace common\modules\game\senders;

use common\components\pusher\senders\AbstractSender;
use common\components\pusher\traits\BeforeSendTrait;
use common\components\S3Core;
use common\components\S3Kind;
use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\sport\models\SportTeamAR;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Class GameTourneySender
 * @package common\components\pusher\senders
 */
class GameTourneySender extends AbstractSender
{
    use BeforeSendTrait;

    public const SCENARIO_UPDATE = 'update';
    public const SCENARIO_CREATE = 'create';

    /** @var GameTourneyAR */
    public $model;

    /**
     * @throws InvalidConfigException
     */
    public function updateScenario()
    {
        /** @var GameTourneyAR|null $clonedModel */
        $clonedModel = $this->getData('clonedModel');

        if (!$clonedModel) {
            return;
        }

        $oldStatus = $clonedModel->getOldAttribute('r_status');

        if (!($oldStatus & S3Core::STATUS_WAITING) && $this->model->r_status & S3Core::STATUS_WAITING) {
            $teams = SportTeamAR::findAll($this->model->teamIds);

            Yii::$app->pushSender->global->update
                ->addManyRecords('matches', $this->model->matches)
                ->addManyRecords('teams', $teams)
                ->addOneRecord('tourneys', $this->model)
                ->pushManyJob();

            return;
        }

        if ($oldStatus & S3Core::STATUS_WAITING) {
            Yii::$app->pushSender->global->update
                ->addOneRecord('tourneys', $this->model)
                ->pushJob();

            $this->sendTourney();

            if ($this->model->r_status & S3Core::STATUS_LIVE) {
                $this->sendUpdateKind();
            }

            return;
        }

        if ($oldStatus & S3Core::STATUS_LIVE || $oldStatus & S3Core::STATUS_PROCESSING) {
            $this->sendTourney();
        }
    }

    /**
     * @return void
     */
    public function createScenario()
    {
    }

    /**
     * @throws InvalidConfigException
     */
    protected function sendTourney()
    {
        Yii::$app->pushSender->privateTou->update
            ->setKind(S3Kind::KIND_USER)
            ->setSubjectId($this->model->id)
            ->addOneRecord('tourneys', $this->model)
            ->pushJob();
    }

    /**
     * @return void
     */
    public function sendUpdateKind()
    {
        /** @var GameLineupAR[] $lineups */
        $lineups = $this->getData('clonedLineups') ?: $this->model->getLineups()
            ->all();

        foreach ($lineups as $lineup) {
            if (!$lineup->isnt_reg) {
                continue;
            }

            Yii::$app->pushSender->private->update
                ->setSubjectId($lineup->user_id)
                ->setKind(S3Kind::KIND_UPDATE_DATA)
                ->addMeta('kind_data', S3Kind::combine(S3Kind::KIND_USER, S3Kind::KIND_TOURNEY))
                ->pushJob();
        }
    }
}
