<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 22.01.2020 09:21
 */

namespace common\modules\game;


use common\components\S3Component;
use common\doings\game\ChangeStatusMatchDoing;
use common\doings\game\ChangeStatusTourneyDoing;
use common\modules\game\models\GameTourneyAR;
use common\modules\game\models\SportMatchAR;
use common\modules\sport\models\SportOccaAR;

class GameManager extends S3Component
{
    protected $match;
    protected $tourney;
    /**
     * @param GameTourneyAR $tourney
     * @return GameTourneyAR
     */
    public function createTourney(GameTourneyAR $tourney)
    {
        $this->tourney = $tourney;
        //todo создание турнира на основе заполненной модели
        return $this->tourney->save();
    }

    /**
     * @param SportMatchAR $match
     * @return SportMatchAR
     */
    public function createMatch(SportMatchAR $match)
    {
        //todo создание матча на основе заполненной модели

        return $match->save();
    }

    /**
     * @param GameTourneyAR $tourney
     * @return GameTourneyAR
     */
    public function changeTourney(GameTourneyAR $tourney)
    {
        //todo изменение турнира
        if($tourney->isScenarioChangeStatus()){
            $doing = ChangeStatusTourneyDoing::new();
            //todo изменение статуса матча и сопутствующих турниров
        }

        return $tourney->save();
    }

    /**
     * @param SportMatchAR $match
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function changeMatch(SportMatchAR $match)
    {
        //todo изменить запись матча
        if($match->isScenarioChangeStatus()){
            $doing = ChangeStatusMatchDoing::new();
            //todo изменение статуса матча и сопутствующих турниров
        }

        return $match->save();
    }

    /**
     * @param GameTourneyAR $tourney
     * @return GameTourneyAR
     */
    public function deleteTourney(GameTourneyAR $tourney)
    {
        //todo удаление турнира
        return $tourney;
    }

    /**
     * @param SportMatchAR $match
     * @return SportMatchAR
     */
    public function deleteMatch(SportMatchAR $match)
    {
        //todo удаление матча
        return $match;
    }

    /**
     * @param GameTourneyAR $tourney
     * @return GameTourneyAR
     */
    public function cancelTourney(GameTourneyAR $tourney)
    {
        //todo отмена турнира
        return $tourney;
    }

    /**
     * @param SportMatchAR $match
     * @return SportMatchAR
     */
    public function cancelMatch(SportMatchAR $match)
    {
        //todo отмена матча
        return $match;
    }

    /**
     * @param SportMatchAR $match
     * @param SportOccaAR[] $occa
     * @return SportMatchAR
     */
    public function holdingMatch($match, $occa)
    {
        //todo заполнить событиями матч (проведение лайва)

        return $match;
    }

    public function getTourney()
    {
        return $this->tourney;
    }

    /**
     * @return SportMatchAR
     */
    public function getMatch()
    {
        return $this->match;
    }
}