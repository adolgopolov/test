<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 11.04.19 17:18
 */

namespace common\modules\game;

use Yii;
use backend\components\aboard\MenuEntryInterface;

class GameModule extends \yii\base\Module implements MenuEntryInterface
{
    public $provider;// FIXME FAN-720 GameProvider будет удален
    protected $gameManager;

    /**
     * {@inheritDoc}
     */
    public static function getMenuItems(): ?array
    {
        return [
            [
                'label' => 'Игра',
                'icon'  => 'game',
                'color' => 'green-800',
                'items' => [
                    [
                        'label' => 'Матчи',
                        'url'   => ['/game/match/index']
                    ],
                    [
                        'label' => 'Турниры',
                        'url'   => ['/game/tourney/index']
                    ],
                    [
                        'label' => 'Составы',
                        'url'   => ['/game/lineup/index']
                    ],
                    // [
                    //     'label' => 'Линейки турниров',
                    //     'url'   => ['/game/tour-trunk/index'],
                    // ],
                    // [
                    //     'label' => 'Игроки',
                    //     'url'   => ['/game/player/index']
                    // ],
                    [
                        'label' => 'Раскладки поля',
                        'url'   => ['/game/layout/index']
                    ],
                ]
            ]
        ];
    }
}