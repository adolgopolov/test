<?php

namespace common\modules\game\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\components\FindOrFailTrait;
use common\components\pusher\interfaces\SenderModelInterface;
use common\components\pusher\senders\AbstractSender;
use common\components\S3Core;
use common\extensions\ExtInfoAR;
use common\modules\game\senders\SportMatchSender;
use common\modules\score\models\ScoreKindAQ;
use common\modules\score\models\ScoreKindAR;
use common\modules\sport\forms\SportMemberAS;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportMemberAQ;
use common\modules\sport\models\SportMemberAR;
use common\modules\sport\models\SportOccaAQ;
use common\modules\sport\models\SportOccaAR;
use common\modules\sport\models\SportTeamAQ;
use common\modules\sport\models\SportTeamAR;
use common\traits\StatusARTrait;
use common\validators\JsonValidator;
use common\traits\NotChangeStatusTrait;

/**
 * This is the model class for table "{{%match}}".
 *
 * @property int $id
 * @property int $r_status
 * @property int $sport_id
 * @property int $lea_id league
 * @property string $r_name
 * @property string $start_date
 * @property int $team_owner
 * @property int $team_guest
 * @property int $total_owner
 * @property int $total_guest
 * @property int $autostart
 * @property array $ext_info
 *
 * @property SportOccaAR[] $occasions
 * @property SportMemberAR[] $teamGuestMembers
 * @property SportMemberAR[] $teamOwnerMembers
 * @property GameTourneyAR[] $tourneys
 * @property ScoreKindAR[] $scoreKinds
 *
 * @property SportTeamAR $teamGuest
 * @property SportTeamAR $teamOwner
 * @property SportLeagueAR $league
 * @property SportAR $sport
 * @property SmanAR $smen
 * @property string $matchTitle
 * @property array $teamTitles
 */
class SportMatchAR extends ExtInfoAR implements SenderModelInterface
{
    use FindOrFailTrait, StatusARTrait, NotChangeStatusTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%match}}';
    }

    /**
     * @return array
     */
    protected function jsonFields(): array
    {
        return [
            'autostart',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_status', 'sport_id', 'lea_id', 'team_owner', 'team_guest', 'total_owner', 'total_guest'], 'integer'],
            [['r_status'], 'in', 'range' => array_keys(S3Core::code2status()), 'skipOnEmpty' => true],
            [['team_owner', 'team_guest', 'lea_id', 'start_date'], 'required'],
            [['ext_info'], JsonValidator::class],
            [['r_name'], 'string', 'max' => 250, 'skipOnEmpty' => true],
            [
                ['team_guest'],
                'compare',
                'compareAttribute' => 'team_owner',
                'operator' => '!=',
                'message' => 'Команда не может играть сама с собой.'
            ],
            [['start_date'], 'date', 'format' => 'php:Y-m-d H:i:s', 'message' => 'Неверный формат даты'],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_status' => 'Статус',
            'sport_id' => 'Спорт',
            'lea_id' => 'Лига',
            'r_name' => 'Название',
            'start_date' => 'Время начала',
            'team_owner' => 'Команда-хозяин',
            'team_guest' => 'Команда-гость',
            'total_owner' => 'Счет команды-хозяина',
            'total_guest' => 'Счет команды-гостя',
            'autostart' => 'Автостарт',
            'ext_info' => 'Ext Info',
        ];
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (S3Core::isAllowedDelete($this) && parent::beforeDelete()) {
            GameTouMatchAR::deleteAll(['match_id' => $this->id]);
            SportOccaAR::deleteAll(['match_id' => $this->id]);

            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     * @return SportMatchAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SportMatchAQ(static::class);
    }

    /**
     * @return string|null
     */
    public function generateMatchName()
    {
        $teamOwner = $this->teamOwner;
        $teamGuest = $this->teamGuest;

        if (!$teamOwner || !$teamGuest) {
            return null;
        }

        $startDateUTC = strtotime($this->start_date);
        $startDate = date('d.m.Y', $startDateUTC);

        return $teamOwner->getTitle() . ' - ' . $teamGuest->getTitle() . ', ' . $startDate;
    }

    /**
     * @param string $scenario
     * @param array $data
     * @throws InvalidConfigException
     */
    public function sendModel($scenario = AbstractSender::SCENARIO_DEFAULT, $data = [])
    {
        SportMatchSender::sendModel($this, $scenario, $data);
    }

    /**
     * {@inheritdoc}
     * @return GameTourneyAQ|ActiveQuery.
     * @throws InvalidConfigException
     */
    public function getTourneys()
    {
        return $this->hasMany(GameTourneyAR::class, ['id' => 'tou_id'])
            ->viaTable(GameTouMatchAR::tableName(), ['match_id' => 'id']);
    }

    /**
     *  все типы расчетов по турнирам для матча
     * @return ScoreKindAQ|ActiveQuery.
     */
    public function getScoreKinds()
    {
        return ScoreKindAR::find()
            ->groupBy('ss_score_kind.id')
            ->innerJoin(GameTourneyAR::tableName(), 'ss_tourney.score_kind=ss_score_kind.id')
            ->innerJoin(GameTouMatchAR::tableName(), 'ss_tou_match.tou_id=ss_tourney.id')
            ->andWhere(['ss_tou_match.match_id' => $this->id]);

    }

    /**
     * @return ScoreKindAQ|ActiveQuery
     */
    public function getScoreKind()
    {
        return $this->hasOne(ScoreKindAR::class, ['id' => 'score_kind']);
    }

    /**
     * {@inheritdoc}
     * @return SportMemberAQ|ActiveQuery
     */
    public function getTeamGuestMembers($searchModel = false)
    {
        return $this->hasMany($searchModel ? SportMemberAS::class : SportMemberAR::class, ['team_id' => 'team_guest'])->byDate($this->start_date);
    }

    /**
     * {@inheritdoc}
     * @return SportMemberAQ|ActiveQuery
     */
    public function getTeamOwnerMembers($searchModel = false)
    {
        return $this->hasMany($searchModel ? SportMemberAS::class : SportMemberAR::class, ['team_id' => 'team_owner'])->byDate($this->start_date);
    }

    /**
     * {@inheritdoc}
     * @return SportOccaAQ|ActiveQuery
     */
    public function getOccasions()
    {
        return $this->hasMany(SportOccaAR::class, ['match_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return SportTeamAQ|ActiveQuery
     */
    public function getTeamGuest()
    {
        return $this->hasOne(SportTeamAR::class, ['id' => 'team_guest']);
    }

    /**
     * @return SportTeamAQ|ActiveQuery
     */
    public function getTeamOwner()
    {
        return $this->hasOne(SportTeamAR::class, ['id' => 'team_owner']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(SportAR::class, ['id' => 'sport_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getLeague()
    {
        return $this->hasOne(SportLeagueAR::class, ['id' => 'lea_id']);
    }

    /**
     * @return SportTeamAR[]
     */
    public function teams(): array
    {
        return [$this->teamOwner, $this->teamGuest];
    }

    /**
     * @return SmanAR[]
     */
    public function getSmen(): array
    {
        $teams = $this->teams();

        $smen = [];

        foreach ($teams as $team) {
            if (!$team) {
                continue;
            }

            $smen[] = $team->smen;
        }

        return ArrayHelper::merge(...$smen);
    }

    /**
     * @return array
     * @throws InvalidConfigException
     * @deprecated
     */
    public function getSmenList(): array
    {
        // DELETE
        // метод еще и назван неправильно, возвращает не спортсменов
        $teams = $this->teams();

        $smen = [];

        foreach ($teams as $team) {
            // FIXME НЕ надо в цикле, если можно сразу
            // Sman::find->whereTeam($teams)->byDate($this->start_date)
            $smen[] = $team->getSmen($this->start_date)
                ->select(['r_name'])
                ->indexBy('id')
                ->column();
        }

        return ArrayHelper::merge(...$smen);
    }

    /**
     * @param $smanId
     * @return array|ActiveRecord
     */
    public function findPositionOfMan($smanId)
    {
        if (($member = $this->getTeamOwnerMembers()->byMan($smanId)->one()) === null) {
            $member = $this->getTeamGuestMembers()->byMan($smanId)->one();
        }

        return $member;
    }

    /**
     *  проверка в какой команде играет спортсмен (хозяева/гости)
     * @param $smanId
     * @return bool
     */
    public function isOwnerTeamSman($smanId)
    {
        return $this->getTeamOwnerMembers()->byMan($smanId)->exists();
    }

    /**
     * @return array
     */
    public static function getMatches(): array
    {
        $matches = self::find()
            ->asArray()
            ->all();

        $sports = SportAR::getAsOptsList();

        $list = [];
        foreach ($matches as $match) {
            $list[$match['id']] = $match['r_name'] . '(' . $sports[$match['sport_id']] . ')';
        }

        return $list;
    }

    /**
     * @return string
     */
    public function getMatchTitle(): string
    {
        $title = $this->r_name;

        if (!$title) {
            $teamTitles = $this->getTeamTitles();

            $title = $teamTitles['owner'] . ' - ' . $teamTitles['guest'];

            $this->updateAttributes(['r_name' => $title]);
        }

        return $title;
    }

    /**
     * @return array
     */
    public function getTeamTitles(): array
    {
        return [
            'guest' => $this->teamGuest->r_name ?? $this->getAttributeLabel('team_guest'),
            'owner' => $this->teamOwner->r_name ?? $this->getAttributeLabel('team_owner'),
        ];
    }

    /**
     * @param SportMemberAQ $query
     * @param int $pos_id
     * @return mixed
     * @deprecated 1.4
     * FIXME достаточно правильного getTeams
     * а у SportTeamAQ добавить whereSman wherePosition
     * или для удобства упаковать в SportTeamAQ::forДляЧегоВсеЭтиsortByPositionИandFilterHaving
     */
    public function getTeamsQuery($query, $pos_id)
    {
        $sman_name = (string)Yii::$app->request->post('sman_name'); // FIXME протечка веб окружения в ядро!

        return $query
            ->onlySmenPosition()
            ->sortByPosition()
            ->alias('member')
            ->select([
                'member.*',
                'sman.r_name as sman_name',
                'sman.sman_status as sman_status',
            ])
            ->leftJoin(SmanAR::tableName() . ' sman', 'sman.id = member.sman_id')
            ->andWhere(['member.pos_id' => $pos_id])
            ->andFilterHaving(['like', 'sman_name', $sman_name]);
    }

    /**
     * @param int $pos_id
     * @return SportMemberAQ
     * @deprecated 1.4
     * FIXME удалить
     * если надо SportMember то и запрашивать SportMemberAR::find()->whereMatch()
     */
    public function getOwnersQuery($pos_id)
    {
        $query = $this->getTeamOwnerMembers(true);

        return $this->getTeamsQuery($query, $pos_id);
    }

    /**
     * @param int $pos_id
     * @return SportMemberAQ
     */
    public function getGuestsQuery($pos_id)
    {
        $query = $this->getTeamGuestMembers(true);

        return $this->getTeamsQuery($query, $pos_id);
    }

    /**
     * @param SportMemberAQ $query
     * @return ActiveDataProvider
     * @deprecated 1.4
     * FIXME это для админки - и должно быть в админке
     */
    public function getTeamsProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                'sman_name' => SORT_ASC,
            ],
            'attributes' => ArrayHelper::merge($dataProvider->sort->attributes, [
                'sman_name',
                'sman_status',
            ])
        ]);

        return $dataProvider;
    }

    /**
     * @param int $pos_id
     * @return ActiveDataProvider
     * @deprecated 1.4
     */
    public function getOwnersProvider($pos_id)
    {
        $query = $this->getOwnersQuery($pos_id);

        return $this->getTeamsProvider($query);
    }

    /**
     * @param int $pos_id
     * @return ActiveDataProvider
     * @deprecated 1.4
     */
    public function getGuestsProvider($pos_id)
    {
        $query = $this->getGuestsQuery($pos_id);

        return $this->getTeamsProvider($query);
    }

}
