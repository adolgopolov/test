<?php

namespace common\modules\game\models;

use common\components\S3DictionaryAR;
use common\components\S3Kind;
use common\components\FindOrFailTrait;
use yii\helpers\ArrayHelper;

/**
 * Class GameLayoutAR
 * @package common\modules\game\models
 * @property array $cells
 */
class GameLayoutAR extends S3DictionaryAR
{
    use FindOrFailTrait;

    public static function kind()
    {
        return S3Kind::KIND_LAYOUT;
    }

    public function jsonFields(): array
    {
        return ['cells'];
    }

    public function cellById($cell_id)
    {
        foreach ($this->cells as $eachCell) {
            if ($eachCell['id'] == $cell_id) return $eachCell;
        }

        return null;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ent_kind' => 'Kind',
            'ent_code' => 'Код',
            'r_name' => 'Название',
            'ent_value' => 'Значение',
            'cells' => 'Ячейки'
        ];
    }

    public function rules()
    {
        return [
            ['id', 'integer'],
            [['ent_code', 'r_name', 'ent_value'], 'string'],
            ['id', 'unique', 'targetAttribute' => ['id', 'ent_kind']]
        ];
    }

    public function getRuleCell($cellId)
    {
        $rules = ArrayHelper::index($this->joGet('cells'), 'id');
        return $rules[$cellId] ?? null;
    }
}
