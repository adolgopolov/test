<?php

namespace common\modules\game\models;

use common\extensions\ExtAQ;
use common\components\S3Core;
use common\traits\StatusAQTrait;
use common\modules\game\models\GameTouMatchAR;

/**
 * Description of SportMatchAQ
 *
 * @author Skynin <sohay_ua@yahoo.com>
 * created: 28-Mar-2019
 */
class SportMatchAQ extends ExtAQ
{
    use StatusAQTrait;

    const DATE_COLUMN_NAME = 'start_date';
    
    /**
     * return SportMatchAQ;
     * @deprecated
     */
    public function live() // TODO заменить использование на isStatus и удалить,
    {
        return $this->andWhere(['r_status' => S3Core::STATUS_LIVE]);
    }

    public function filterTourney($tourneys)
    {
        $tblMain = $this->getTableNameAndAlias()[1];
        $tblTouMatch = GameTouMatchAR::tableName();

        $this->innerJoin($tblTouMatch, "$tblMain.id = $tblTouMatch.match_id")
                ->andWhere(["$tblTouMatch.tou_id" => $tourneys]);

        return $this;
    }
}
