<?php

namespace common\modules\game\models;

use common\extensions\ExtAQ;
use common\components\S3Core;
use common\traits\StatusAQTrait;

/**
 * This is the ActiveQuery class for [[GameTourneyAR]].
 *
 * @see GameTourneyAR
 */
class GameTourneyAQ extends ExtAQ
{
    use StatusAQTrait;

    const DATE_COLUMN_NAME = 'start_date';

    public function filterUser($user_id)
    {
        $this->alias('t');
        $this->innerJoin(GameLineupAR::tableName()." l",'t.id=l.tou_id');
        $this->andWhere(['l.user_id'=> $user_id]);
        return $this;
    }

    public function contextWhere($args)
    {
        if (!empty($args['ss3api'])) {
            $this->andWhere(['>', 'r_status', S3Core::STATUS_DRAFT]);
        }

        return $this;
    }
}
