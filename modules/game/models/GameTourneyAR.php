<?php

namespace common\modules\game\models;

use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\StaleObjectException;
use common\components\FindOrFailTrait;
use common\modules\user\models\UserAR;
use common\components\pusher\interfaces\SenderModelInterface;
use common\components\pusher\senders\AbstractSender;
use common\components\S3Core;
use common\components\S3DictionaryAQ;
use common\components\S3Kind;
use common\extensions\ExtAQ;
use common\extensions\ExtInfoAR;
use common\modules\game\senders\GameLineupSender;
use common\modules\game\senders\GameTourneySender;
use common\modules\money\models\UserMoneyAccAR;
use common\modules\money\MoneyModule;
use common\modules\score\models\{CalcSportAR, CalcTourneyAR, ScoreKindAQ, ScoreKindAR};
use common\modules\sport\models\{SmanAQ, SmanAR, SportAR, SportLeagueAQ, SportLeagueAR, SportMemberAR, SportOccaAR, SportTeamAR};
use common\traits\FieldTrait;
use common\traits\StatusARTrait;
use common\traits\NotChangeStatusTrait;
use common\validators\JsonValidator;
use Throwable;

/**
 * This is the model class for table "ss_tourney".
 *
 * @property int $id
 * @property int $sport_id
 * @property int $lea_id league
 * @property int $r_status
 * @property string $r_name
 * @property string $r_icon
 * @property string $start_date
 * @property array $buyin
 * @property array $buyInGold
 * @property array $buyInGreen
 *
 * @property int $prizefund
 * @property int $prize // TODO rename to $this->prizelist
 * @property int $lup_budget max sport team budget
 * @property int $max_smen max sportsmen from same team
 * @property int $max_gamers
 * @property int $reg_lineups
 * @property int $trophy_kind
 * @property int $score_kind
 * @property array $ext_info
 *
 * @property UserAR[] $users
 * @property SportMatchAR[] $matches
 * @property ScoreKindAR $scoreKind
 * @property GameLineupAR[] $lineups
 * @property TrophyTypeAR $trophyType
 * @property SportAR $sport
 * @property SportLeagueAR $league
 * @property-read array $teamIds
 *
 * @property-not-read  SmanAR[] $smen !!! возвращает запрос one()
 *
 */
class GameTourneyAR extends ExtInfoAR implements SenderModelInterface
{
    use FindOrFailTrait, FieldTrait, StatusARTrait, NotChangeStatusTrait;

    private $teamIds;

    public function init()
    {
        $this->on(GameTourneyAR::EVENT_AFTER_UPDATE, static function ($event) {
            $tourney = $event->sender;

            // TODO можно ли оставить это тут
            if ($tourney->isStatus(S3Core::STATUS_COMPLETED)) {
                foreach ($tourney->getLineups()->registered()->all() as $lineup) {
                    $lineup->award_day = date('Y-m-d H:i:s');
                    $lineup->save();
                }
            }
        });

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tourney}}';
    }

    /**
     * @return int
     */
    public static function kind()
    {
        return S3Kind::KIND_TOURNEY;
    }

    protected function jsonFields(): array
    {
        return ['prize'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sport_id', 'lea_id', 'r_status',
                'prizefund', 'lup_budget', 'max_smen', 'max_gamers',
                'reg_lineups', 'trophy_kind', 'score_kind', 'buyInGreen', 'buyInGold'], 'integer'],
            [['r_name', 'sport_id', 'lea_id', 'lup_budget', 'max_smen'], 'required'],
            [['r_status'], 'in', 'range' => array_keys(S3Core::code2status())],
            [['sport_id'], 'in', 'range' => SportAR::getSportIds()],
            [['sport_id'], 'notChangeSport',],
            [['lea_id'], 'exist', 'targetClass' => SportLeagueAR::class, 'targetAttribute' => 'id'],
            // TODO выбор тип спорта, существующие виды расчета
            //[['score_kind'], 'exist', 'targetClass' => ScoreKindAR::class, 'targetAttribute' => 'id'],
            [['score_kind'], 'default', 'value' => static function (self $model, $attribute) {
                return ScoreKindAR::getDefaultKindId($model->sport_id);
            }],
            [['start_date'], 'date', 'format' => 'php:Y-m-d H:i:s', 'message' => 'Неверный формат даты'],
            [['r_name'], 'string', 'max' => 250],
            [['r_icon'], 'string', 'max' => 90],
            [['ext_info', 'buyin'], JsonValidator::class],
            [['r_status'], 'changeStatus'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sport_id' => 'Спорт',
            'lea_id' => 'Лига',
            'r_status' => 'Статус',
            'r_name' => 'Название',
            'r_icon' => 'Лого турнира',
            'start_date' => 'Дата начала',
            'buyInGold' => 'Байн-ин золотые',
            'buyInGreen' => 'Байн-ин зеленые',
            'buyin' => 'Бай-ин',
            'prizefund' => 'Призовой фонд',
            'lup_budget' => 'Лайнап-бюджет',
            'max_smen' => 'Макс. число спорт-нов одной команды',
            'max_gamers' => 'Максимальное число игроков в турнире',
            'reg_lineups' => 'Количество зарегистрированных составов',
            'trophy_kind' => 'Trophy Kind',
            'score_kind' => 'Score Kind',
            'ext_info' => 'Ext Info',
        ];
    }

    /**
     * @param string $scenario
     * @param array $data
     * @throws InvalidConfigException
     */
    public function sendModel($scenario = AbstractSender::SCENARIO_DEFAULT, $data = [])
    {
        GameTourneySender::sendModel($this, $scenario, $data);

        self::EVENT_AFTER_UPDATE;
    }

    /**
     * @return void
     */
    public function sendUpdateLineupsPush()
    {
        GameLineupSender::sendUpdatePush($this->id);
    }

    /**
     * @return bool
     * @throws Throwable
     * @throws Exception
     * @throws StaleObjectException
     */
    public function beforeDelete()
    {
        if (S3Core::isAllowedDelete($this) && parent::beforeDelete()) {
            foreach ($this->lineups as $lineup) {
                $lineup->delete();
            }

            $match = SportMatchAR::tableName();
            $touMatch = GameTouMatchAR::tableName();
            $occasion = SportOccaAR::tableName();
            $calsMatch = CalcSportAR::tableName();

            Yii::$app->db->createCommand("
                DELETE `m`, `mt` FROM $match `m`
                LEFT JOIN $touMatch `mt` ON `mt`.`match_id`= `m`.`id`
                LEFT JOIN $occasion `oc` ON `oc`.`match_id`= `m`.`id`
                LEFT JOIN $calsMatch `cm` ON `cm`.`match_id`= `m`.`id`
                WHERE `mt`.`tou_id` = $this->id;
            ")->execute();

            CalcTourneyAR::deleteAll(['tou_id' => $this->id]);

            UserMoneyAccAR::deleteAll(['tou_id' => $this->id]);

            return true;
        }

        return false;
    }

    /**
     * @param null $accId
     * @return array|int
     */
    public function getBuyIn($accId = null)
    {
        if ($accId !== null) {
            return $this->buyin[$accId] ?? 0;
        }

        return $this->buyin;
    }

    /**
     * @param int|array $value
     * @param int $accId
     */
    public function setBuyIn($value, $accId = MoneyModule::ACCOUNT_GOLD)
    {
        if (is_numeric($value)) {
            $buyin = $this->buyin ?? [
                    MoneyModule::ACCOUNT_GOLD => 0,
                    MoneyModule::ACCOUNT_GREEN => 0,
                ];
            $buyin[$accId] = $value;
            $this->buyin = $buyin;
        } else if (is_array($value)) {
            $this->buyin = $value;
        }
    }

    /**
     * @return int
     */
    public function getBuyInGold()
    {
        return $this->getBuyIn(MoneyModule::ACCOUNT_GOLD);
    }

    public function setBuyInGold($value): void
    {
        $this->setBuyIn($value, MoneyModule::ACCOUNT_GOLD);
    }

    /**
     * @return int
     */
    public function getBuyInGreen(): int
    {
        return $this->getBuyIn(MoneyModule::ACCOUNT_GREEN);
    }

    public function setBuyInGreen($value): void
    {
        $this->setBuyIn($value, MoneyModule::ACCOUNT_GREEN);
    }

    /**
     * {@inheritdoc}
     * @return GameTourneyAQ the active query used by this AR class.
     */
    public static function find(): GameTourneyAQ
    {
        return new GameTourneyAQ(static::class);
    }

    /**
     * @return S3DictionaryAQ|ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(SportAR::class, ['id' => 'sport_id']);
    }

    /**
     * {@inheritdoc}
     * @return SportMatchAQ|ActiveQuery
     * @throws InvalidConfigException
     */
    public function getMatches()
    {
        return $this->hasMany(SportMatchAR::class, ['id' => 'match_id'])
            ->viaTable(GameTouMatchAR::tableName(), ['tou_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return GameLineupAQ|ActiveQuery
     */
    public function getLineups()
    {
        return $this->hasMany(GameLineupAR::class, ['tou_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ExtAQ|ActiveQuery
     */
    public function getTrophyType()
    {
        return $this->hasOne(TrophyTypeAR::class, ['id' => 'trophy_kind']);
    }

    /**
     * {@inheritdoc}
     * @return ScoreKindAQ|ActiveQuery
     */
    public function getScoreKind()
    {
        return $this->hasOne(ScoreKindAR::class, ['id' => 'score_kind']);
    }

    /**
     * {@inheritdoc}
     *
     * @return SportLeagueAQ|ActiveQuery
     */
    public function getLeague()
    {
        return $this->hasOne(SportLeagueAR::class, ['id' => 'lea_id']);
    }

    /**
     * @param $attribute
     * @param $params
     * @param $validator
     */
    public function notChangeSport($attribute, $params, $validator): void
    {
        if (!$this->isNewRecord && $this->$attribute != $this->getOldAttribute($attribute)) {
            $this->addError($attribute, 'Нельзя менять вид спорта у турнира');
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @param $validator
     */
    public function changeStatus($attribute, $params, $validator): void
    {
        if (!$this->matches && $this->isntStatuses([S3Core::STATUS_DRAFT, S3Core::STATUS_CANCELED])) {
            $this->addError($attribute, 'Нельзя изменить статус у турнира без матчей');
        }
    }

    /**
     * список учасников турнира
     * @return SmanAQ
     */
    public function getSmen()
    {
    /*
     * FIXME убрать этот ужасъ
     * заменить на SmanAR::find()->whereTourney($this)
    */
        return SmanAR::find()
            ->alias('s')
            ->innerJoin(SportMemberAR::tableName() . " tm", 's.id=tm.sman_id')
            ->innerJoin(SportTeamAR::tableName() . " t", 'tm.team_id=t.id')
            ->innerJoin(SportMatchAR::tableName() . " m", 'm.team_guest=t.id OR m.team_owner=t.id')
            ->innerJoin(GameTouMatchAR::tableName() . " tou_m", 'm.id=tou_m.match_id')
            ->innerJoin(self::tableName() . " tou", 'tou.id=tou_m.tou_id')
            ->andWhere(['tou.id' => $this->id]);
    }

    /**
     * В запросе можно сослаться на GameLineupAR::tableName()
     *
     * @return \common\modules\user\models\UserAQ
     */
    public function getUsers()
    {
        $lineupTable = GameLineupAR::tableName();
        $userTable = UserAR::tableName();

        return UserAR::find()
            ->innerJoin($lineupTable, "$lineupTable.user_id = $userTable.id")
            ->andWhere(["$lineupTable.tou_id" => $this->id]);
    }

    /**
     *
     * @return type
     * @deprecated 1.4
     * FIXME getTeams возвращает SportTeamAQ
     */
    public function getTeamIds()
    {
        if (empty($this->teamIds)) {
            $this->teamIds = [];
            foreach ($this->matches as $eachMatch) {
                $this->teamIds[$eachMatch->team_owner] = $eachMatch->team_owner;
                $this->teamIds[$eachMatch->team_guest] = $eachMatch->team_guest;
            }
        }

        return (!empty($this->teamIds)) ? array_keys($this->teamIds) : [];
    }

    /**
     * @param $smanId
     * @return SportMemberAR|null
     * FIXME findSome должны возвращать AQ
     * в названии должна звучать возвращаемая сущность
     * findMember
     *
     * в данном случае лучше вообще заменить на
     * getMembers без параметров
     * и $smanId передавать в вызывающем коде
     */
    public function findPositionOfMan($smanId)//: ?SportMemberAR
    {
        return SportMemberAR::find()->where(['and',
            ['sman_id' => $smanId],
            ['<=', 'first_day', $this->start_date],
            ['>=', 'last_day', $this->start_date],
        ])->one(); // FIXME нельзя сразу
    }

    /**
     * @return SportMatchAQ
     */
    public function getAvailableMatchesQuery()
    {
        // FIXME добавить ограничение по дате, например минус месяц от меньшей из текущей даты start_date турнира
        // start_date проиндексировано, а поля в запросе ниже - нет
        return SportMatchAR::find()
            ->andWhere(['sport_id' => $this->sport_id])
            ->andWhere(['lea_id' => $this->lea_id])
            ->andWhere(['&', 'r_status', S3Core::STATUS_WAITING]);
    }

    /**
     * @return array
     */
    public static function getMaxGamersItems(): array
    {
        return [
            10 => '10',
            30 => '30',
            100 => '100',
            1000 => '1000',
        ];
    }

    /**
     *  подсчет зарегистрированных составов , расчет призового фонда, массив призовых мест
     * @param bool $save
     * @return bool
     * @throws InvalidConfigException
     */
    public function calcGameData($save = false)
    {
        $regLineups = count($this->getLineups()->registered()->all());
        $data = new PrizeFundDTO([
            "gamer_count" => $regLineups,
            "buyin" => $this->getBuyInGold(),
            "prize_fund" => $this->prizefund,
        ]);

        $this->reg_lineups = $regLineups;
        //todo выполнить расчет , данные сохранить в таблицу турнира
        if ($this->reg_lineups > 2) {
            try {
                $result = $this->trophyType->calcAwards($data);
                $this->prize = $result['prize_list']; // TODO rename to $this->prizelist
                if ($this->trophyType->isKit()) {
                    $this->prizefund = $result['prize_fund'];
                }
            } catch (Exception $exception) {
                $this->addError('prizefund', $exception->getMessage());
                return false;
            }
        } else {
            if ($this->trophyType->isKit()) {
                $this->prizefund = 0;
            }
            $this->prize = null;
        }
        if ($save) {
            $clonedModel = clone $this;

            if ($this->save()) {
                $this->sendModel(GameTourneySender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);

                return true;
            }

            return false;
        }

        return true;
    }

    /**
     * только для турниров сос татусом WAITING
     * @return bool
     */
    public function canChangeLineup()
    {
        return (strtotime($this->start_date) > time()) &&
            ($this->r_status & S3Core::STATUS_WAITING);
    }

    /**
     * TODO возращает ВСЕ турниры???
     *
     * @return array
     */
    public static function getList()
    {
        return self::find()
            ->select(['r_name'])
            ->indexBy('id')
            ->column();
    }
}
