<?php

namespace common\modules\game\models;

use common\components\S3DictionaryAR;
use common\components\S3Kind;
use common\components\FindOrFailTrait;
use common\validators\JsonValidator;
use yii\helpers\ArrayHelper;

/**
 * типы турниров, виды распределения выигрыша
 * @property mixed|null joInfo
 * @property string $info
 * @property int $is_free
 * @property int $is_auto_pfund
 * @property array $buyin_currency
 */
class TrophyTypeAR extends S3DictionaryAR
{
    use FindOrFailTrait;

    private $tblAwards;

    /**
     * @return int|null
     */
    public static function kind()
    {
        return S3Kind::TROPHY_TYPE;
    }

    /**
     * @return array
     */
    public function jsonFields(): array
    {
        return [
            'info',
            'is_free',
            'is_auto_pfund',
            'buyin_currency',
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['r_name', 'info'], 'string'],
            [['is_free', 'is_auto_pfund'], 'boolean'],
            [['buyin_currency'], 'safe'],
            [['ext_info'], JsonValidator::class, 'rules' => [
                'is_free' => 'int',
                'is_auto_pfund' => 'int',
            ]],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_name' => 'Название',
            'info' => 'Информация',
            'is_free' => 'Бесплатный',
            'is_auto_pfund' => 'Авто-расчёт призового фонда',
            'buyin_currency' => 'Список валют',
        ];
    }

    /**
     * расчитать награды по значению призового фонда, и процентам в таблице наград ext_info->info
     * @param PrizeFundDTO $data
     * @return array
     */
    public function getAwards(PrizeFundDTO $data)
    {
        // parse {"info": "35% 1 место, 15% 2 место, 10% 3 место, 3% 4-10 места, 1.5% 11-20 места, 0.2%, 21-40 места", "table": "wf_dictionary"}
        $tStr = $this->joInfo;
        if (empty($tStr)) {
            return [];
        }

        $result_percent = [];

        foreach (explode(',', $tStr) as $eachP) {
            /*
             * 3% 4-5 места
             * 0=>3% 4-5 1=>3 2=>4 3=>5
             */

            if (preg_match('/(\d*[.]?\d*)%\s*(\d+)-*(\d*)/', $eachP, $mathes)) {
                $startRate = $mathes[2];
                $finisRate = empty($mathes[3]) ? $startRate : $mathes[3];

                for (; $startRate <= $finisRate; ++$startRate) {
                    $result_percent[$startRate] = (float)$mathes[1] / 100;
                }
            }
        }

        $result = [];

        foreach ($result_percent as $key => $value) {

            if ($data->prize_fund) {
                $summAward = self::asRound($data->prize_fund * $value, 5);
            } else {
                $summAward = self::asRound(($data->buyin * 0.9) * $value, 5);
            }
            $result[$key] = $summAward;
        }

        return [
            'prize_list' => $result,
            'prize_percent' => $result_percent,
            'prize_fund' => array_sum($result),
            'xp_list' => [], // DELETE xp_list больше не нужен, это для старого вида опыта
        ];
    }

    /**
     * @param PrizeFundDTO $data
     * @return array
     */
    public function getAwardsByBuyin(PrizeFundDTO $data)
    {
        $gamer_count = $data->gamer_count;
        $buyin = $data->buyin;
        $prize = $gamer_count * $buyin;

        return $this->calcByFunc($gamer_count, $prize);
    }

    /**
     * @param PrizeFundDTO $data
     * @return array
     */
    public function getAwardsByPrize(PrizeFundDTO $data)
    {
        $gamer_count = $data->gamer_count;
        $prize = $data->prize_fund;

        return $this->calcByFunc($gamer_count, $prize, 1);
    }

    /**
     * служебная формула
     * @param $x
     * @param $base_st_k
     * @return float
     */
    private function base($x, $base_st_k)
    {
        return exp(-0.2 * ($x ** $base_st_k));
    }

    /**
     * служебная формула
     * @param $mesto
     * @param $zoom
     * @param $base
     * @return float|int
     */
    private function award_ratio($mesto, $zoom, $base)
    {
        $firstEl = $zoom * ($mesto - 1) + 1;
        $lastEl = $zoom * $mesto;
        $list = array_slice($base, $firstEl, $lastEl - $firstEl);

        return array_sum($list) / $zoom;
    }

    /**
     * @param PrizeFundDTO $data
     * @return void|array
     */
    public function calcAwards(PrizeFundDTO $data)
    {
        if (!$data->validate()) {
            $this->addError('award', 'Не заданы параметры для расчета байин или призовой фонд');

            return;
        }

        if (empty($this->tblAwards)) {
            if ($this->isKit()) {
                if (!$data->validateKit()) {
                    $this->addError('award', 'Ошибка для турнира типа КИТ необходимо задать байин');
                    return;
                }
                $this->tblAwards = $this->getAwardsByBuyin($data);
            } elseif ($this->isFreeroll()) {
                if (!$data->validateFreeroll()) {
                    $this->addError('award', 'Ошибка для турнира типа Freeroll необходимо задать призовой фонд');
                    return;

                }
                $this->tblAwards = $this->getAwardsByPrize($data);
            } else {
                $this->tblAwards = $this->getAwards($data);
            }
        }

        return [
            'prize_list' => $this->tblAwards['prize_list'],
            'prize_fund' => $this->tblAwards['prize_fund'],
            'xp_list' => $this->tblAwards['xp_list'] ?? [], // DELETE xp_list больше не нужен, это для старого вида опыта
        ];
    }

    /**
     * @return bool
     */
    public function isKit()
    {
        return $this->ent_code === 'trophy.kit_fan';
    }

    /**
     * @return bool
     */
    public function isFreeroll()
    {
        return $this->ent_code === 'trophy.freeroll';
    }

    /**
     * оккругление до ближайшего целого значения 5,10 и так далее
     * @param $number
     * @param int $roundTo
     * @return float|int
     */
    public static function asRound($number, $roundTo = 10)
    {
        $roundTo = $roundTo >= 1 ? $roundTo : 10;
        //todo округлять по пятерке
        $number = ceil($number / 0.5) * 0.5;
        $number = floor($number);
        $mod = $number % $roundTo;

        return $number + ($mod < ($roundTo / 2) ? -$mod : $roundTo - $mod);
    }

    /**
     * @return array
     */
    public static function getTrophyList()
    {
        $kinds = self::find()
            ->notDeleted()
            ->all();

        return ArrayHelper::map($kinds, 'id', 'r_name');
    }

    /**
     * @param $gamer_count
     * @param $prize
     * @return array
     */
    protected function calcByFunc($gamer_count, $prize, $rake = 0.90): array
    {

        $win_percent = round(24 + (13 * exp(-0.005 * ($gamer_count ** 0.9))), 6);
        $base_st_k = 0.21 + 0.1 * (log($gamer_count) / 10);
        $zoom = ceil(100000 / $gamer_count);
        $winners = floor($gamer_count * $win_percent / 100);

        $max = floor($win_percent * 1000);

        for ($i = 1; $i < $max; $i++) {
            $base[] = $this->base($i, $base_st_k);
        }

        $result['award_ratio'] = $result['prize_percent'] = $result['prize_fund'] = $result['xp'] = []; // DELETE xp больше не нужен, это для старого вида опыта
        for ($i1 = 1; $i1 <= $winners; $i1++) {
            $result['award_ratio'][$i1] = $this->award_ratio($i1, $zoom, $base);
        }

        for ($i1 = $winners + 1; $i1 <= $gamer_count; $i1++) {
            $result['award_ratio'][$i1] = 0;
        }

        $award_ratio_sum = array_sum($result['award_ratio']);
        foreach ($result['award_ratio'] as $key => $value) {
            $result['prize_percent'][$key] = round($value / $award_ratio_sum * 100, 2);
            $result['prize_fund'][$key] = self::asRound(($prize * $rake) * $value / $award_ratio_sum, 5);
        }

        return [
            'prize_list' => $result['prize_fund'],
            'prize_percent' => $result['prize_percent'],
            'prize_fund' => array_sum($result['prize_fund']),
            'xp_list' => $result['xp'],  // DELETE xp_list больше не нужен, это для старого вида опыта
        ];
    }

    /**
     * @return array
     */
    public static function getFreeTrophyTypes()
    {
        return self::find()
            ->whereJO(['is_free' => 1])
            ->select(['id'])
            ->indexBy('id')
            ->column();
    }

    /**
     * @return array
     */
    public static function getAutoPrizefundTrophyTypes()
    {
        return self::find()
            ->whereJO(['is_auto_pfund' => 1])
            ->select(['id'])
            ->indexBy('id')
            ->column();
    }

    /**
     * @return array
     */
    public static function getBooleanStatuses()
    {
        return [
            'Нет',
            'Да',
        ];
    }
}
