<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 19.08.2019 10:59
 */

namespace common\modules\game\models;


use yii\helpers\Json;

class PrizeFundDTO
{
    public $prize_fund;
    public $format;
    public $gamer_count;
    public $buyin;

    public function __construct($array)
    {
        $this->prize_fund = $array["prize_fund"]??null;
        $this->format = $array["format"]??"full";
        $this->buyin = $array["buyin"]??null;
        $this->gamer_count = $array["gamer_count"]??null;
    }

    public function validate():bool
    {
        if(empty($this->buyin) && empty($this->prize_fund)){
            return false;
        }

        if($this->gamer_count <= 2 ) {
            return false;
        }

        if(!in_array($this->format,["full","short"])) {
            return false;
        }
        return true;
    }

    public function validateKit():bool
    {
        if($this->buyin < 0){
            return false;
        }

        return true;
    }

    public function validateFreeroll():bool
    {
        if($this->prize_fund < 0){
            return false;
        }
        return true;
    }

    public function asString()
    {
        return Json::encode($this);
    }

}