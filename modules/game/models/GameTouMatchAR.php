<?php

namespace common\modules\game\models;

use common\models\BaseAR;
use yii\helpers\ArrayHelper;
use common\modules\game\models\SportMatchAR;
use common\modules\game\models\GameTourneyAR;

/**
 * This is the model class for table "ss_tou_match".
 *
 * @property int $tou_id
 * @property int $match_id
 */
class GameTouMatchAR extends BaseAR
{
    public const SCENARIO_TOURNEY = 'tourney';

    public $match_ids;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tou_match}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return $this->strongRules([
            [['tou_id', 'match_id'], 'required'],
            [['tou_id', 'match_id'], 'integer'],
            [['match_ids'], 'safe'],
            [['tou_id'], 'exist','targetClass' => GameTourneyAR::class,'targetAttribute' => 'id'],
            [['match_id'], 'exist','targetClass' => SportMatchAR::class,'targetAttribute' => 'id']
            ],[
            [['tou_id', 'match_id'], 'unique', 'targetAttribute' => ['tou_id', 'match_id']],
        ]);
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_TOURNEY => ['tou_id', 'match_ids'],
            self::SCENARIO_DEFAULT => ['tou_id', 'match_id'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tou_id' => 'Турнир',
            'match_id' => 'Матч',
            'match_ids' => 'Матчи',
        ];
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return false|int|void
     * @throws \Exception
     */
    public function update($runValidation = true, $attributeNames = null)
    {
        throw new  \Exception('Нельзя редактировать записи');
    }
}
