<?php

namespace common\modules\game\models;

use common\extensions\ExtAQ;

/**
 * This is the ActiveQuery class for [[GameLineupCellAR]].
 *
 * @see GameLineupCellAR
 */
class GameLineupCellAQ extends ExtAQ
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return GameLineupCellAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return GameLineupCellAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
