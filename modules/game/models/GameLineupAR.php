<?php

namespace common\modules\game\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use common\validators\JsonValidator;
use common\components\FindOrFailTrait;
use common\components\pusher\interfaces\SenderModelInterface;
use common\components\pusher\senders\AbstractSender;
use common\components\S3DictionaryAQ;
use common\doings\game\ChangeLineUpDoing;
use common\extensions\ExtInfoAR;
use common\modules\game\senders\GameLineupSender;
use common\modules\money\MoneyModule;
use common\modules\user\models\UserAR;
use Exception;
use ss3api\core\AuthEvent;
use Throwable;

/**
 * This is the model class for table "ss_lineup".
 *
 * @property int $id
 * @property int $user_id gamer
 * @property int $layout_id
 * @property string $user_name
 * @property int $tou_id
 * @property int $l_points points * 100
 * @property int $exp_points points * sport_coef
 * @property int $award_day
 * @property int $l_place
 * @property int $l_award
 * @property-read  int $is_draft
 * @property int $isdraft
 * @property-read int $isnt_reg
 * @property int $isntreg
 * @property int $acc_id номер счета для списания денег
 * @property string $created_at
 * @property string $updated_at
 * @property array $ext_info
 *
 * @property GameLineupCellAR[] $lineUpCells
 * @property GameTourneyAR $tourney
 * @property GameLayoutAR $layout
 * @property UserAR $user
 */
class GameLineupAR extends ExtInfoAR implements SenderModelInterface
{
    use FindOrFailTrait;

    /**
     *  !!! в extinfo пишутся поля 'isdraft', 'isntreg' без подчеркивания
     *  а виртуальные колонки для проверки с подчеркиваниями $isnt_reg $is_draft
     * {@inheritDoc}
     */
    public function jsonFields(): array
    {
        return array_merge(
            parent::jsonFields(),
            ['isdraft', 'isntreg', 'acc_id']
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lineup}}';
    }

    /** todo переделать на saveThroughDoing()
     * @return bool
     * @throws InvalidConfigException
     * @deprecated
     */
    public function _save(): bool
    {
        $doing = ChangeLineUpDoing::new();
        if (!$doing->doIt([
            'model' => $this,
        ])) {
            $this->addErrors($doing->getErrors());
            return false;
        }
        return true;
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['scoring'] = ['l_points', 'l_place',];
        return $scenarios;
    }

    /**
     * @param string $opts
     * @param array $cells
     * @return bool
     */
    public function createCells($opts = 'save', &$cells = []): bool
    {
        $layout = $this->layout;
        $result = true;

        foreach ($layout->cells as $key => $cell) {
            $cellAR = new GameLineupCellAR([
                'lineup_id' => $this->id,
                'cell_id' => $cell['id'] ?? $key,
            ]);

            $cells[] = $cellAR;

            if ($opts === 'save') {
                $result = $result && $cellAR->save();
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function dropCells(): bool
    {
        GameLineupCellAR::deleteAll(['lineup_id' => $this->id]);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge([
            [['user_id', 'tou_id', 'layout_id', 'l_points', 'l_place', 'l_award', 'isdraft', 'isntreg', 'acc_id', 'exp_points'], 'integer'],
            [['acc_id'], 'in', 'range' => array_keys(MoneyModule::accountLabel())],
            ['layout_id', 'default', 'value' => static function (GameLineupAR $model) {
                $layout = empty($model->tou_id) ? false : GameLayoutAR::find()->whereSport($model->tourney->sport_id, 'strict')->one();
                return empty($layout) ? 0 : $layout->id;
            }],
            [['user_id', 'tou_id', 'layout_id'], 'required'],
            [['user_id', 'tou_id'], function ($attribute) {
                static $attrResult = []; // чтобы не проверять дважды
                $cacheKey = 'tou: ' . $this->tou_id . ' - user: ' . $this->user_id;
                if (isset($attrResult[$cacheKey])) {
                    return $attrResult[$cacheKey];
                }

                $prevLineUp = GameLineupAR::find()->andWhere(['tou_id' => $this->tou_id, 'user_id' => $this->user_id]);
                if ($this->user_id == UserAR::GUEST_USER) {
                    $prevLineUp->andWhere(['user_name' => GameLineupAR::sessionGuestName()]);
                }
                $prevLineUp->andFilterWhere(['<>', 'id', $this->id]);

                $prevLineUp = $prevLineUp->limit(1)->one();

                $attrResult[$cacheKey] = true;
                if (!empty($prevLineUp)) {
                    $attrResult[$cacheKey] = false;
                    $this->addError($attribute, "Double lineup $cacheKey");
                }
                return $attrResult[$cacheKey];
            }, 'when' => function () {
                return isset(Yii::$app->response->cookies)
                    && (Yii::$app->response->cookies->hasProperty('guest_session') || $this->user_id != UserAR::GUEST_USER);
            }],
            [['award_day',], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_name'], 'string', 'max' => 250],
        ], $this->extInfoRules());
    }

    /**
     * @return array
     */
    protected function extInfoRules()
    {
        return [
            ['ext_info', JsonValidator::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID пользователя',
            'user_name' => 'Имя пользователя',
            'tou_id' => 'Турнир',
            'l_points' => 'Очки (L Points)',
            'exp_points' => 'Очки опыта(Experience points)',
            'award_day' => 'День начисления опыта',
            'l_place' => 'Место (L Place)',
            'l_award' => 'Награда (L Award)',
            'is_draft' => 'Драфт',
            'isdraft' => 'Драфт',
            'isnt_reg' => 'НЕзарегестрированный',
            'isntreg' => 'НЕзарегестрированный',
            'ext_info' => 'Ext Info',
            'acc_id' => 'Номер счета',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * {@inheritdoc}
     * @return GameLineupAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new GameLineupAQ(static::class);
    }

    /**
     * @param string $scenario
     * @param array $data
     * @throws InvalidConfigException
     */
    public function sendModel($scenario = AbstractSender::SCENARIO_DEFAULT, $data = [])
    {
        GameLineupSender::sendModel($this, $scenario, $data);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserAR::class, ['id' => 'user_id']);
    }

    /**
     * @return GameLineupCellAQ|ActiveQuery
     */
    public function getLineUpCells() // TODO rename getCells
    {
        return $this->hasMany(GameLineupCellAR::class, ['lineup_id' => 'id']);
    }

    /**
     * @return GameTourneyAQ|ActiveQuery
     */
    public function getTourney()
    {
        return $this->hasOne(GameTourneyAR::class, ['id' => 'tou_id']);
    }

    /**
     * @return S3DictionaryAQ|ActiveQuery
     */
    public function getLayout()
    {
        if (empty($this->layout_id)) {
            return GameLayoutAR::find()->whereSport($this->tourney->sport_id, 'strict');
        }

        return GameLayoutAR::find()->andWhere(['id' => $this->layout_id]);
    }

    /**
     * @return bool|false|int
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function delete()
    {
        if (empty($this->isntreg)) {
            $tError = "Attempt delete registered lineup isnt_reg $this->isnt_reg";
            Yii::error($tError, 'ss3');
            $this->addError('isnt_reg', $tError);

            return false;
        }

        return $this->dropCells() && parent::delete();
    }

    /**
     * @return void
     */
    public function clearScoring(): void
    {
        $this->l_points = 0;
        $this->exp_points = 0;
        $this->l_place = 0;
        $this->l_award = 0;
    }

    /**
     * @return void
     */
    public function markDeleted(): void
    {
        $markDeleteAttributes = self::markDeleteAttributes();

        foreach ($markDeleteAttributes as $attribute => $value) {
            $this->$attribute = $value;
        }
    }

    /**
     * @return bool
     */
    public function isMarkDeleted()
    {
        $markDeleteAttributes = self::markDeleteAttributes();

        foreach ($markDeleteAttributes as $attribute => $value) {
            if (!$this->hasAttribute($attribute)) {
                continue;
            }

            if ($this->$attribute !== $value) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param bool $forQuery
     * @param null|string $alias
     * @return array
     */
    public static function markDeleteAttributes($forQuery = false, $alias = null)
    {
        $alias = $alias ? $alias . '.' : null;

        $deleteMarks = [
            "{$alias}user_id" => UserAR::GUEST_USER,
            "{$alias}user_name" => null,
            "{$alias}tou_id" => 0,
            "{$alias}layout_id" => 0,
            "{$alias}l_points" => 0,
            "{$alias}exp_points" => 0,
            "{$alias}l_place" => 0,
        ];

        if ($forQuery) {
            $deleteMarks["{$alias}is_draft"] = 1;
            $deleteMarks['isnt_reg'] = 1;
        } else {
            $deleteMarks["{$alias}ext_info"] = null;
            $deleteMarks["{$alias}isdraft"] = 1;
            $deleteMarks["{$alias}isntreg"] = 1;
        }

        return $deleteMarks;
    }

    /**
     * @param null $guest_session
     * @return string
     */
    public static function sessionGuestName($guest_session = null)
    {
        if (empty($guest_session)) {
            $guest_session = Yii::$app->response->cookies->getValue('guest_session');
        }

        return hash('md4', $guest_session);
    }

    /**
     * @param $post_data
     * @return bool
     * @throws InvalidConfigException
     */
    public function saveThroughDoing($post_data)
    {
        $user = UserAR::authorizedUser();

        $user_id = $user ? $user->id : UserAR::GUEST_USER;
        $user_name = $user ? $user->username : self::sessionGuestName();
        $cells = $post_data['data']['cells'] ?? [];

        unset($post_data['data']['cells']);

        $this->load(ArrayHelper::merge($post_data['data'], [
            'user_id' => $user_id,
            'user_name' => $user_name,
        ]), '');

        $doing = ChangeLineUpDoing::new();

        $result = $doing->doIt([
            'model' => $this,
            'params' => [
                'cells' => $cells,
            ],
        ]);

        if (!$result) {
            $this->addErrors($doing->errors);

            return false;
        }

        return true;
    }

    /**
     * @param array $post_data
     * @return bool
     * @throws InvalidConfigException
     */
    public function dataCreate($post_data): bool
    {
        return $this->saveThroughDoing($post_data);
    }

    /**
     * @param array $post_data
     * @return bool
     * @throws InvalidConfigException
     * @throws Exception
     */
    public function dataUpdate($post_data): bool
    {
        if (!$this->isSelfLineUp()) {
            throw new Exception('Пользователь может редактировать только свои составы');
        }

        return $this->saveThroughDoing($post_data);
    }

    /**
     * @return bool
     */
    public function isSelfLineUp()
    {
        if ($user = UserAR::authorizedUser()) {
            return $this->user_id == $user->id;
        }

        return ($this->user_id == UserAR::GUEST_USER) && ($this->user_name === self::sessionGuestName());
    }

    /**
     * @param AuthEvent $event
     * @return AuthEvent
     * @throws InvalidConfigException
     */
    public static function attachLinups(AuthEvent $event)
    {
        $lineups = self::find()
            ->filterUser(UserAR::GUEST_USER)
            ->filterUserName(self::sessionGuestName($event->session_name))->all();

        foreach ($lineups as $eachLineup) {
            /** @var GameLineupAR $eachLineup */
            $doing = ChangeLineUpDoing::new();
            $eachLineup->user_id = $event->user_id;
            $eachLineup->user_name = UserAR::findOne($event->user_id)->username ?? '';
            $cells = [];

            foreach ($eachLineup->lineUpCells as $cell) {
                /** @var GameLineupCellAR $cell */
                $cells[] = [
                    'id' => $cell->cell_id,
                    'sman_id' => $cell->sman_id,
                    'goods' => $cell->joGet('goods', []),
                ];
            }

            $result = $doing->doIt([
                'model' => $eachLineup,
                'params' => [
                    'cells' => $cells,
                ],
            ]);

            if (!$result) {
                $eachLineup->markDeleted();
                $eachLineup->dropCells();
                $eachLineup->save();
            }
        }

        return $event;
    }
}
