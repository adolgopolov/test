<?php

namespace common\modules\game\models;

use common\components\S3Core;
use common\extensions\ExtAQ;
use common\modules\user\models\UserAR;

/**
 * This is the ActiveQuery class for [[GameLineupAR]].
 *
 * @see GameLineupAR
 */
class GameLineupAQ extends ExtAQ
{
    /**
     * @return GameLineupAQ
     */
    public function registered()
    {
        return $this->andWhere(['isnt_reg' => 0]);
    }

    /**
     * @return GameLineupAQ
     */
    public function actualAward()
    {
        return $this->andWhere(['is_actual_award' => 1]);
    }

    public function whereTourney($tou_id)
    {
        return $this->andWhere(['tou_id' => $tou_id]);
    }

    public function apiLineups($user_id, $user_name)
    {
        $this->alias('l')
            ->innerJoin(GameTourneyAR::tableName() . " t", 'l.tou_id=t.id');
        if(!$user_id){
            $user_id = UserAR::GUEST_USER;
        }
        $userNameQuery = ($user_id == UserAR::GUEST_USER)?[
            'and',
            ['&', 't.r_status', S3Core::STATUS_WAITING],
            ['l.user_id' => $user_id],
            ['l.user_name' => $user_name]
        ]:[
            'and',
            ['&', 't.r_status', S3Core::STATUS_WAITING],
            ['l.user_id' => $user_id],
        ];

        $this->andWhere(['OR',
            //статус вейтинг только свои
            $userNameQuery,
            // статус больше вейтинг свои и чужие зарегистрированные
            [
                'and',
                ['&', 't.r_status', S3Core::STATUS_LIVE + S3Core::STATUS_PROCESSING + S3Core::STATUS_COMPLETED + S3Core::STATUS_CLOSED],
                ['isnt_reg' => 0],
            ],

        ]);
        return $this;
    }

    public function filterUser($user_id)
    {
        return $this->andWhere(['user_id' => $user_id]);
    }

    public function filterUserName($user_name)
    {
        return $this->andWhere(['user_name' => $user_name]);
    }

    public function filterTourney($tou_id)
    {
        return $this->andWhere(['tou_id' => $tou_id]);
    }

    /**
     * @param string $args
     * @return GameLineupAQ
     */
    public function actualPeriod($args = '-6 week')
    {
        return $this->andWhere(['>=', 'created_at', date('Y-m-d', strtotime($args))]);
    }

}
