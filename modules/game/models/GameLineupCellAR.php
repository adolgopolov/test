<?php

namespace common\modules\game\models;

use common\modules\goods\components\S3GoodKindAR;
use common\modules\goods\models\CardAR;
use common\modules\goods\models\UserCardAR;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use common\modules\sport\models\SmanAQ;
use common\components\FindOrFailTrait;
use common\components\pusher\interfaces\SenderModelInterface;
use common\components\pusher\senders\AbstractSender;
use common\extensions\ExtAQ;
use common\extensions\ExtInfoAR;
use common\modules\game\GameProvider;
use common\modules\game\senders\GameLineupCellSender;
use common\modules\goods\components\S3UserGoodAR;
use common\modules\sport\models\{SmanAR, SportPositionAR, SportMemberAR};
use common\validators\JsonValidator;

/**
 * This is the model class for table "ss_lineup_cell".
 *
 * @property int $lineup_id
 * @property int $cell_id
 * @property int $sman_id
 * @property int $sman_points points * 100
 * @property array $ext_info
 *
 * @property array $reasons
 *
 * @property SmanAR $sman
 * @property SportMemberAR $teamMember
 * @property GameLineupAR $lineup
 */
class GameLineupCellAR extends ExtInfoAR implements SenderModelInterface
{
    use FindOrFailTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lineup_cell}}';
    }

    /**
     * @param $lineupCell GameLineupCellAR
     * @param bool $exclude
     * @return array|null
     */
    public static function getSmenTourney($lineupCell, $exclude = true)
    {
        $provider = new GameProvider(); // FIXME FAN-720 GameProvider будет удален
        $ids = ArrayHelper::getColumn($lineupCell->lineup->lineUpCells, 'sman_id');
        $ids = array_filter($ids);
        $pos = $lineupCell->allowedPositions();

        return $provider->smenTourney($lineupCell->lineup->tou_id, static function (ExtAQ $query) use ($ids, $pos, $exclude) {
            if ($exclude) {
                $query->andWhere(['not', ['s.id' => $ids]]);
            }

            return $query->andWhere(['tm.pos_id' => $pos]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return $this->strongRules([
            [['lineup_id', 'cell_id'], 'required'],
            [['lineup_id', 'cell_id', 'sman_id', 'sman_points'], 'integer'],
            [['ext_info'], JsonValidator::class, 'rules' => [
                'positions' => 'checkPositions',
                'goods' => ['array', 'checkGoods'],
            ]],
            [['sman_id'], 'checkDuplicates'],
            [['sman_id'], 'required', 'on' => 'registration'],
            [['sman_id'], 'validatePositions', 'on' => 'registration'],
        ], [
            [['lineup_id', 'cell_id'], 'unique', 'targetAttribute' => ['lineup_id', 'cell_id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lineup_id' => 'ID состава',
            'cell_id' => 'ID',
            'sman_id' => 'ID спортсмена',
            'sman_points' => 'Очки',
            'ext_info' => 'Ext Info',
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['registration'] = ['lineup_id', 'cell_id', 'sman_id'];
        $scenarios['scoring'] = ['lineup_id', 'cell_id', 'sman_points'];

        return $scenarios;
    }

    /**
     * @param string $scenario
     * @param array $data
     * @throws InvalidConfigException
     */
    public function sendModel($scenario = AbstractSender::SCENARIO_DEFAULT, $data = [])
    {
        GameLineupCellSender::sendModel($this, $scenario, $data);
    }

    /**
     * @return void
     */
    public function forRegistration()
    {
        $this->scenario = 'registration';
    }

    /**
     * {@inheritdoc}
     * @return GameLineupCellAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new GameLineupCellAQ(static::class);
    }

    /**
     * получить массив всех модификаторов ячейки состава
     * ['имя модификатора' => 'количество начисленных очков']
     * @return mixed|null
     */
    public function getReasons()
    {
        return $this->joGet('reason', []);
    }

    /**
     * проверка на специальные модификаторы
     * @return bool
     */
    public function haveSpecialReason(): bool
    {
        return !empty($this->reasons);
    }

    /**
     * добавить специальный модификатор ячейки состава
     * @param array | string $reason
     */
    public function addReason($reason)
    {
        if (!is_array($reason)) {
            $reason = [$reason => 0];
        }
        $reasons = $this->reasons;
        $reasons = array_merge($reasons, $reason);
        return $this->joSet('reason', array_unique($reasons));
    }

    /**
     * удалить специальный модификатор ячейки состава
     * @param array | string $reason
     */
    public function removeReason($reason)
    {
        if (!is_array($reason)) {
            $reason = [$reason => 0];
        }
        $reasons = $this->reasons;
        $reasons = array_diff_key($reasons, $reason);
        return $this->joSet('reason', $reasons);
    }

    /**
     * установить модификатору очки при расчете
     * @param $reason
     * @param $points
     */
    public function updateReasonPoints($reason, $points)
    {
        $array = $this->reasons;
        $array[$reason] = $points;
        return $this->joSet('reason', $array);
    }

    /**
     * @return SmanAQ|ActiveQuery
     */
    public function getSman()
    {
        return $this->hasOne(SmanAR::class, ['id' => 'sman_id']);
    }

    /**
     * @return GameLineupAQ|ActiveQuery
     */
    public function getLineup()
    {
        return $this->hasOne(GameLineupAR::class, ['id' => 'lineup_id']);
    }

    /**
     * @return \common\modules\sport\models\SportMemberAQ|ActiveQuery
     */
    public function getTeamMember()
    {
        throw new Exception("TODO FAN-720 SportMemberAR::find()->whereLineupCell($this)");
    }

    /**
     * все доступные позиции для ячейки
     *
     * @return array|mixed|null
     */
    public function allowedPositions()
    {
        $layout = $this->lineup->layout;

        $positions = $layout->cellById($this->cell_id)['rules']['positions'] ?? [1];
        $first = reset($positions);
        if ($first === 1) {
            $allPositions = SportPositionAR::find()->andWhere([
                'ent_value' => $this->lineup->tourney->sport_id,
            ])->asArray()->all();
            return ArrayHelper::getColumn($allPositions, 'id');
        }
        return $positions;
    }

    /**
     * @param $attribute
     * @return mixed
     */
    public function checkDuplicates($attribute)
    {
        $lineup = $this->lineup;

        // FIXME FAN-416 Ошибка валидации состава при смене ячейки для спортсмена
        $exists = $lineup->getLineUpCells()
            ->andWhere([
                'AND',
                ['!=', 'cell_id', $this->cell_id],
                ['sman_id' => $this->sman_id],
            ])
            ->exists();

        if ($exists) {
            $this->addError($attribute, 'Duplicate sman');
        }

        return $this->sman_id;
    }

    /**
     * @param $attribute
     * @return bool
     */
    public function validatePositions($attribute)
    {
        $rules = $this->lineup->layout->getRuleCell($this->cell_id);
        // FIXME FAN-720 Множественность команд спортсмена и рефактор lastTeamMember
        // $smanPosition = $this->teamMember->pos_id ?? null;
        $smanPosition = $this->sman->getTouTeamMember($this->lineup->tourney)->pos_id ?? null;

        if ($rules && $smanPosition) {
            $positions = ArrayHelper::getValue($rules, 'rules.positions');
            if ($positions === null) {
                // позиции не валидируются
                return true;
            }
            if (is_array($positions) && in_array($smanPosition, $positions)) {
                return true;
            }
        }

        $this->addError($attribute, 'Wrong position');

        return false;
    }

    public function checkGoods(array $cellGoods, $attribute)
    {
        if (empty($cellGoods)) {
            return null;
        }
        if (!is_array($cellGoods)) {
            $this->addError($attribute, 'Bad structure array of goodIDS: must be array of integers');
            return null;
        }
        // могут храниться только масив id вещей
        foreach ($cellGoods as $eachGood) {
            if (!is_numeric($eachGood)) {
                $this->addError($attribute, 'Bad structure array of goodIDS: must be array of integers');
                return null;
            }
        }

        $countGoods = S3UserGoodAR::find()
            ->andWhere(['user_id' => $this->lineup->user_id])
            ->andWhere(['id' => $cellGoods])
            ->count();

        if ($countGoods != count($cellGoods)) {
            $this->addError($attribute, 'User does not have some cards: ' . implode(',', $cellGoods));
        }

        return $cellGoods;
    }

    // TODO какие позиции мы проверяем в ячейке ???
    public function checkPositions($value, $attribute)
    {
        $positions = (array)$this->allowedPositions();

        $sman = $this->sman;

        if ($sman) {
            $smanMember = $sman->getTouTeamMember($this->lineup->tourney);

            if (!$smanMember || !in_array($smanMember->pos_id, $positions, true)) {
                $this->addError($attribute, 'Invalid position specified for sman.');
            }
        }

        return $value;
    }

    /**
     * @param $nCell
     */
    public function fillCell($nCell)
    {
        $this->sman_id = $nCell['sman_id'];

        if (!empty($nCell['goods'])) {
            $this->joSet('goods', $this->prepareGoods($nCell['goods']));
        } else {
            $this->joSet('goods', null);
        }
    }

    /**
     * замена массива объектов на массив ID
     *  "23" => [23]
     *  ["23","12"] => [23,12]
     *  [['id' => 23,'good_kind' => 45],['id'=>12,'good_kind' => 45]] => [23,12]
     *  иначе  => null
     *
     * @param mixed $goods массив елементов или строковое значение или номер
     * @return array
     */
    protected function prepareGoods($goods): array
    {
        if (is_numeric($goods)) {
            return [(int)$goods];
        }

        if (is_array($goods)) {
            if (!empty($goods['id'])) {
                return [(int)$goods['id']];
            }

            $goodIds = [];
            foreach ($goods as $eachGood) {
                if (is_numeric($eachGood)) {
                    $goodIds[] = (int)$eachGood;
                } elseif (!empty($eachGood['id'])) {
                    $goodIds[] = (int)$eachGood['id'];
                }
            }

            return $goodIds;
        }
        return [];
    }

    /**
     * @param $nCell
     */
    public function addGoods($nCell)
    {
        /*
         *  1. ячейка пустая -> записываем
         *  2. ячейка не пустая
         *      2.1 проверить есть ли данная вещь (по id) -> ничего не делаем
         *      2.2 можно ли этой вещью заменить существующую -> заменяем
         *      2.3 если нельзя, то совместима ли она с сущ. -> exeption
         */

        if (!empty($nCell['goods'])) {
            $goods = $this->joGet('goods');
            if (!empty($goods)) {
                foreach ($this->prepareGoods($goods) as $good) {
                    $userCard = UserCardAR::findOne($good);
                    if ($userCard->card->ent_type == S3GoodKindAR::KIND_CLASSIC_CARD) {
                        $this->joSet('goods', $this->prepareGoods($nCell['goods']));
                    }
                }
            } else {
                $this->joSet('goods', $this->prepareGoods($nCell['goods']));
            }
        } else {
            $this->joSet('goods', null);
        }
    }
}
