<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/** @var yii\web\View $this */
/** @var common\modules\game\models\TrophyTypeAR $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="trophy-type-ar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'ent_kind')->textInput() ?>

    <?= $form->field($model, 'ent_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ent_value')->textInput() ?>

    <?= $form->field($model, 'r_name')->textInput(['maxlength' => true]) ?>

    <hr>

    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
