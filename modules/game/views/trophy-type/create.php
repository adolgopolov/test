<?php

use yii\helpers\Html;

/** @var yii\web\View $this*/
/** @var common\modules\game\models\TrophyTypeAR $model */

$this->title = 'Добавление Trophy Type Ar';
$this->params['breadcrumbs'][] = ['label' => 'Trophy Type Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trophy-type-ar-create">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

