<?php

use yii\helpers\Html;
use backend\components\aboard\widgets\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this*/
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Типы трофеев';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trophy-type-ar-index">

    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>
    <?php Pjax::begin(); ?>

    <!-- <?= Html::a('<span class="ti-plus"></span> Добавить', ['create'], ['class' => 'btn btn-success pull-right mT-10']) ?> -->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'ent_code',
                'options' => [
                    'class' => 'code-grid-column',
                ],
            ],
            // 'ent_value',
            'r_name',
            //'ext_info',
            [
                'class'     => 'backend\components\aboard\ActionColumn',
                'template'  => '<div class="btn-group">{view}{update}</div>'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
