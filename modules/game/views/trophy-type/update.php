<?php

use yii\helpers\Html;

/** @var yii\web\View $this*/
/** @var common\modules\game\models\TrophyTypeAR $model */

$this->title = 'Update Trophy Type Ar: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Trophy Type Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'ent_kind' => $model->ent_kind]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trophy-type-ar-update">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
