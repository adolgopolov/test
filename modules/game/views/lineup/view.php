<?php

use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameLineupCellAR;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model common\modules\game\models\GameLineupAR
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Составы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];

YiiAsset::register($this);
?>
<div class="game-lineup-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-arrow-left"></span> К списку', ['index'], [
            'class' => 'btn btn-outline-primary pull-left',
        ]) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'user_name',
            [
                'attribute' => 'tou_id',
                'label' => 'ID Турнира',
            ],
            [
                'attribute' => 'tou_name',
                'label' => 'Турнир',
                'value' => static function (GameLineupAR $model) {
                    return $model->tourney ? $model->tourney->getTitle() : null;
                },
            ],
            'l_points',
            'l_place',
            'l_award',
            'is_draft',
            'isnt_reg',
        ],
    ]) ?>

    <h4 class="c-grey-900 mT-10 mB-30">Ячейки состава</h4>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'cell_id',
                'label' => 'Ячейка',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'sman_id',
                'label' => 'ID Спортсмена',
            ],
            [
                'attribute' => 'sman_name',
                'label' => 'Спортсмен',
                'value' => static function (GameLineupCellAR $model) {
                    // FIXME FAN-720 добавить в GameLineupCellAR getTeamMember
                    // $teamMember = $model->teamMember
                    return $model->sman ? $model->sman->getTitle() : null;
                },
            ],
            [
                'attribute' => 'pos_id',
                'label' => 'ID Позиции',
                'value' => static function (GameLineupCellAR $model) {
                    return $model->sman->lastTeamMember->pos_id ?? null;
                }
            ],
            [
                'attribute' => 'pos_name',
                'label' => 'Позиция',
                'value' => static function (GameLineupCellAR $model) {
                    return $model->sman->lastTeamMember->position->r_name ?? null;
                },
            ],
            [
                'attribute' => 'team_id',
                'label' => 'ID Команды',
                'value' => static function (GameLineupCellAR $model) {
                    return $model->sman->lastTeamMember->team_id ?? null;
                },
            ],
            [
                'attribute' => 'team_name',
                'label' => 'Команда',
                'value' => static function (GameLineupCellAR $model) {
                    return isset($model->sman->lastTeamMember->team) ? $model->sman->lastTeamMember->team->getTitle() : null;
                }
            ],
            'sman_points',
        ],
    ]) ?>
</div>
