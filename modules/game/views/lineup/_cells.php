<?php

use common\modules\game\models\GameLineupCellAR;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var $modelCells GameLineupCellAR
 */

?>
<?php Pjax::begin() ?>

<?php ActiveForm::begin() ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'cell_id',
        [
            'attribute' => 'sman_id',
            'value' => static function (GameLineupCellAR $model) use ($modelCells) {
                $smen = GameLineupCellAR::getSmenTourney($model, false);
                $smenList = ArrayHelper::map($smen, 'id', 'r_name');
                $attribute = "sman_id[$model->cell_id]";
                $sman_id = $modelCells->sman_id;
                $sman_id[$model->cell_id] = $model->sman_id;
                $modelCells->sman_id = $sman_id;

                return Select2::widget([
                    'model' => $modelCells,
                    'attribute' => $attribute,
                    'data' => $smenList,
                    'pluginOptions' => [
                        'placeholder' => 'Выберите спортсмена' . $model->sman_id,
                    ],
                ]);
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'sman_points',
            'value' => static function (GameLineupCellAR $model) use ($modelCells) {
                $attribute = "sman_points[$model->cell_id]";

                return Html::activeInput('number', $modelCells, $attribute, [
                    'value' => $model->sman_points,
                    'class' => 'form-control',
                ]);
            },
            'format' => 'raw',
        ],
    ],
]) ?>
<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success pull-right mB-5']) ?>

<?php ActiveForm::end() ?>

<?php Pjax::end() ?>
