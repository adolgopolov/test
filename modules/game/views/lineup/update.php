<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\game\models\GameLineupAR $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var $modelCells \common\modules\game\models\GameLineupCellAR
 * @var $users array
 * @var $tourneys array
 */

$this->title = 'Редактирование состава: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Составы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';

$tabs = [
    [
        'label' => '<span class="ti ti-pencil"></span> Основная информация',
        'content' => $this->render('_form', [
            'model' => $model,
            'users' => $users,
            'tourneys' => $tourneys,
        ]),
    ],
    [
        'label' => '<span class="ti ti-layout-grid3"></span> Ячейки',
        'content' => $this->render('_cells', [
            'dataProvider' => $dataProvider,
            'modelCells' => $modelCells,
        ]),
    ]
];
?>
<div class="game-lineup-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= TabsX::widget(['items' => $tabs, 'encodeLabels' => false]) ?>
</div>
