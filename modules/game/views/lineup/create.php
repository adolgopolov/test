<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\game\models\GameLineupAR $model
 * @var $users array
 * @var $tourneys array
 */

$this->title = 'Добавление состава';
$this->params['breadcrumbs'][] = ['label' => 'Составы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-lineup-ar-create">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
        'users' => $users,
        'tourneys' => $tourneys,
    ]) ?>
</div>
