<?php

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use common\modules\game\forms\GameLineupAS;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\widgets\GridActions\GridActionsWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var GameLineupAS $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var array $users
 * @var array $tourneys
 */

$this->title = 'Составы';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    [
        'attribute' => 'user_id',
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'user_id',
            'data' => $users,
            'theme' => Select2::THEME_BOOTSTRAP,
            'pluginOptions' => [
                'placeholder' => '',
                'allowClear' => true,
            ],
            'pjaxContainerId' => 'game-lineup-pjax',
        ]),
    ],
    'user_name',
    [
        'attribute' => 'tou_id_copy',
        'options' => [
            'class' => 'medium-grid-column',
        ],
        'value' => static function (GameLineupAS $model) {
            return $model->tou_id;
        },
    ],
    [
        'attribute' => 'tou_id',
        'options' => [
            'class' => 'tou-id-grid-column',
        ],
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'tou_id',
            'data' => $tourneys,
            'theme' => Select2::THEME_BOOTSTRAP,
            'pluginOptions' => [
                'placeholder' => '',
                'allowClear' => true,
            ],
            'pjaxContainerId' => 'game-lineup-pjax',
        ]),
        'value' => static function (GameLineupAS $model) {
            if (!$model->tourney) {
                return null;
            }

            return html::a($model->tourney->getTitle(), ['tourney/view', 'id' => $model->tou_id], [
                'target' => '_blank',
            ]);
        },
        'format' => 'raw',
    ],
    'l_points',
    'l_place',
    'l_award',
    'is_draft:boolean',
    'isnt_reg:boolean',
    //'ext_info',
    [
        'class' => ActionColumn::class,
        'template' => '<div class="btn-group">{view}{update}{delete}{lineup-sign-up}</div>',
        'buttons' => [
            'lineup-sign-up' => static function ($url, GameLineupAS $model, $key) {
                if ($model->isnt_reg) {
                    $options = [
                        'title' => 'lineup-sign-up',
                        'aria-label' => 'lineup-sign-up',
                        'data-pjax' => '0',
                        'class' => 'btn btn-outline-primary',
                        'data-confirm' => false,
                        'data-toggle' => 'tooltip',
                        'data-title' => 'lineup-sign-up'
                    ];
                    $icon = Html::tag('em', '', ['class' => 'fas fa-registered']);

                    return Html::a($icon, $url, $options);
                }

                return '';
            },
        ],
    ],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="game-lineup-ar-index">
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#game-lineup-pjax',
        'pjaxUrl' => '/game/lineup/index',
        'model' => $searchModel,
        'additionalContainerOptions' => [
            'class' => [
                'columns-select2-margined',
            ],
        ],
    ]) ?>
    <?= GridActionsWidget::widget([
        'title' => $this->title,
    ]) ?>
    <?php Pjax::begin(['id' => 'game-lineup-pjax']); ?>
    <?= GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => ArrayHelper::merge([GridActionsWidget::$checkboxColumn], $columns),
    ]); ?>
    <?php Pjax::end(); ?>
</div>
