<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;
use common\widgets\DetailForm\DetailFormWidget;
use common\modules\user\models\UserAR;

/**
 * @var yii\web\View $this
 * @var common\modules\game\models\GameLineupAR $model
 * @var yii\widgets\ActiveForm $form
 * @var $users array
 * @var $tourneys array
 */

$disabled = !$model->isNewRecord;
?>
<div class="game-lineup-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => !$disabled,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'user_id' => Select2::widget([
                'model' => $model,
                'attribute' => 'user_id',
                'data' => $users,
                'options' => [
                    'disabled' => $disabled,
                ],
                'pluginOptions' => [
                    'placeholder' => 'Выберите игрока',
                ],
            ]),
            'user_name' => [
                'inputOptions' => [
                    'disbaled' => $disabled,
                ],
            ],
            'tou_id' => Select2::widget([
                'model' => $model,
                'attribute' => 'tou_id',
                'data' => $tourneys,
                'options' => [
                    'disabled' => $disabled,
                ],
                'pluginOptions' => [
                    'placeholder' => 'Выберите турнир',
                ],
            ]),
            'l_points' => [
                'inputType' => 'number',
                'inputOptions' => [
                    'disabled' => $disabled,
                ],
            ],
            'l_place' => [
                'inputType' => 'number',
                'inputOptions' => [
                    'disabled' => $disabled,
                ],
            ],
            'l_award' => [
                'inputType' => 'number',
                'inputOptions' => [
                    'disabled' => $disabled,
                ],
            ],
            'isdraft' => [
                'inputType' => 'checkbox',
            ],
            'isntreg' => [
                'inputType' => 'checkbox',
            ],
        ],
    ]) ?>
    <?php if ($model->user_id == UserAR::GUEST_USER): ?>
        <div class="alert col-sm-10 offset-sm-2 alert-info">
            <span class="fa fa-info-circle"></span>
            Это гостевой состав (user_id = <?= UserAR::GUEST_USER ?>), в поле 'Имя пользователя' хранится ID сессии пользователя.
        </div>
    <?php endif; ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
