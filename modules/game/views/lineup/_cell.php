<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var \common\modules\game\models\GameLineupCellAR $model
 * @var array $smen
 */

?>

<h1><?= $model->sman->r_name ?? null ?></h1>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'sman_id',
        [
            'attribute' => 'sman.r_name',
            'label' => 'Имя спортсмена'
        ],

        'sman_points'
    ],
]) ?>

<div class="game-lineup-ar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sman_id')->dropDownList(
        ArrayHelper::map($smen, 'id', 'r_name'),
        ['prompt' => 'Выберите спортсмена']
    ) ?>

    <hr>

    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
