<?php

/**
 * @var $this View
 * @var $modelOcca SportOccaAR
 * @var $model SportMatchAR
 * @var $teamList array
 */

use common\modules\game\models\SportMatchAR;
use common\modules\sport\models\SoccerFactAR;
use common\modules\sport\models\SportOccaAR;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$factName = SoccerFactAR::FACT_TACKLE_WON;
$factAction = 'match-occa/form-tackle-won';
$smanAction = '/game/match-occa/ajax-scored-goal';
?>
<?php $form = ActiveForm::begin([
    'action' => Url::to([$factAction, 'id' => $model->id, 'factName' => $factName]),
    'method' => 'POST',
    'options' => [
        'class' => 'js-form-attach-entity',
        'data-pjax-selector' => '#match-occa-container',
    ]
]) ?>

<?= $form->field($modelOcca, 'match_id')
    ->hiddenInput(['value' => $model->id])
    ->label(false) ?>

<?= $form->field($modelOcca, 'fact_id')
    ->hiddenInput([
        'id' => 'occa-fact-id',
    ])
    ->label(false) ?>

<?= $form->field($modelOcca, 'after_start')->input('number', [
    'value' => 0,
]) ?>

<?= Html::hiddenInput('team_id', array_key_first($teamList), [
    'id' => 'team-id',
]) ?>

<?= Html::radioList('team_radio', null, $teamList, [
    'class' => 'btn-group d-flex radio-list',
    'item' => static function ($index, $label, $name, $checked, $value) {
        if ($index === 0) {
            $checked = true;
        }

        return Html::radio($name, $checked, [
            'label' => $label,
            'value' => $value,
            'labelOptions' => [
                'class' => 'btn btn-light' . ($checked ? ' active' : ''),
            ],
            'class' => 'op-0',
        ]);

    }
]) ?>

<?= $form->field($modelOcca, 'sman_id')->widget(DepDrop::class, [
    'options' => [
        'id' => 'sman-dd',
        'placeholder' => 'Выберите спортсмена',
        'name' => 'fact[' . $factName . ']',
    ],
    'pluginOptions' => [
        'depends' => ['team-id'],
        'url' => Url::to([$smanAction, 'match_id' => $model->id]),
        'initialize' => true,
    ],
    'type' => DepDrop::TYPE_SELECT2,
]) ?>

<?= $form->field($modelOcca, 'occa_amount')->input('number', [
    'name' => 'amount[' . $factName . ']',
    'value' => SportOccaAR::DEFAULT_OCCA_AMOUNT,
    'min' => 1,
    'max' => 90,
]) ?>

<?= Html::submitButton('Добавить', ['class' => 'btn btn-success js-submit-attach-entity']) ?>

<?php ActiveForm::end() ?>
