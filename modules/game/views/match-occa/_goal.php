<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\modules\game\models\SportMatchAR
 * @var $modelOcca \common\modules\sport\models\SportOccaAR
 * @var $teams array
 */

use common\modules\sport\models\SoccerFactAR;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['match-occa/form-goal', 'id' => $model->id]),
    'method' => 'POST',
    'options' => [
        'class' => 'js-form-attach-entity',
        'data-pjax-selector' => '#match-occa-container',
    ]
]) ?>

<?= $form->field($modelOcca, 'after_start')->input('number', [
    'value' => 0,
]) ?>

<?= Html::hiddenInput('team_id', array_key_first($teams), [
    'id' => 'team-id',
]) ?>

<?= Html::radioList('team_radio', null, $teams, [
    'class' => 'btn-group d-flex radio-list',
    'item' => static function ($index, $label, $name, $checked, $value) {
        if ($index === 0) {
            $checked = true;
        }

        return Html::radio($name, $checked, [
            'label' => $label,
            'value' => $value,
            'labelOptions' => [
                'class' => 'btn btn-light' . ($checked ? ' active' : ''),
            ],
            'class' => 'op-0',
        ]);
    }
]) ?>

<?= $form->field($modelOcca, 'sman_id')->widget(DepDrop::class, [
    'options' => [
        'id' => 'goal-sman-id',
        'placeholder' => 'Выберите спортсмена',
        'name' => 'fact[' . SoccerFactAR::FACT_SCORED_GOAL . ']',
    ],
    'pluginOptions' => [
        'depends' => ['team-id'],
        'url' => Url::to(['/game/match-occa/ajax-scored-goal', 'match_id' => $model->id]),
        'initialize' => true,
    ],
    'select2Options' => [
        'theme' => Select2::THEME_BOOTSTRAP,
    ],
    'type' => DepDrop::TYPE_SELECT2,
])->label('Кто забил') ?>

<?= $form->field($modelOcca, 'sman_id')->widget(DepDrop::class, [
    'options' => [
        'id' => 'goalpass-sman-id',
        'placeholder' => 'Выберите спортсмена',
        'name' => 'fact[' . SoccerFactAR::FACT_GOALPASS . ']',
    ],
    'pluginOptions' => [
        'depends' => [
            'team-id',
            'goal-sman-id',
        ],
        'url' => Url::to(['/game/match-occa/ajax-goalpass', 'match_id' => $model->id]),
        'initialize' => true,
    ],
    'select2Options' => [
        'theme' => Select2::THEME_BOOTSTRAP,
    ],
    'type' => DepDrop::TYPE_SELECT2,
])->label('С чьей передачи забил') ?>

<?= $form->field($modelOcca, 'sman_id')->widget(DepDrop::class, [
    'options' => [
        'id' => 'goalkeeper-sman-id',
        'placeholder' => 'Выберите спортсмена',
        'name' => 'fact[' . SoccerFactAR::FACT_MISSED_GOAL . ']',
    ],
    'pluginOptions' => [
        'depends' => ['team-id'],
        'url' => Url::to(['/game/match-occa/ajax-goalkeeper', 'match_id' => $model->id]),
        'initialize' => true,
        'params' => [
            'match_id' => $model->id,
        ]
    ],
    'select2Options' => [
        'theme' => Select2::THEME_BOOTSTRAP,
    ],
    'type' => DepDrop::TYPE_SELECT2,
])->label('Кому забили') ?>

<?= Html::submitButton('Добавить', ['class' => 'btn btn-success js-submit-attach-entity']) ?>

<?php ActiveForm::end() ?>
