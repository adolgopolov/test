<?php

/**
 * @var $this View
 * @var $modelMatch SportMatchAR
 * @var $model SportOccaAR
 * @var $teamList array
 */

use common\modules\game\models\SportMatchAR;
use common\modules\sport\models\SoccerFactAR;
use common\modules\sport\models\SportOccaAR;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'action' => Url::to(['match-occa/form-shot-on-target', 'id' => $modelMatch->id]),
    'method' => 'POST',
    'options' => [
        'class' => 'js-form-attach-entity',
        'data-pjax-selector' => '#match-occa-container',
    ]
]) ?>

<?= $form->field($model, 'after_start')->input('number', [
    'value' => 0,
]) ?>

<?= Html::hiddenInput('team_id', array_key_first($teamList), [
    'id' => 'team-id',
]) ?>

<?= Html::radioList('team_radio', null, $teamList, [
    'class' => 'btn-group d-flex radio-list',
    'item' => static function ($index, $label, $name, $checked, $value) {
        if ($index === 0) {
            $checked = true;
        }

        return Html::radio($name, $checked, [
            'label' => $label,
            'value' => $value,
            'labelOptions' => [
                'class' => 'btn btn-light' . ($checked ? ' active' : ''),
            ],
            'class' => 'op-0',
        ]);
    }
]) ?>

<?= $form->field($model, 'sman_id')->widget(DepDrop::class, [
    'options' => [
        'id' => 'sman-dd',
        'placeholder' => 'Выберите спортсмена',
        'name' => 'fact[' . SoccerFactAR::FACT_SHOT_ON_TARGET . ']',
    ],
    'pluginOptions' => [
        'depends' => ['team-id'],
        'url' => Url::to(['/game/match-occa/ajax-scored-goal', 'match_id' => $modelMatch->id]),
        'initialize' => true,
    ],
    'type' => DepDrop::TYPE_SELECT2,
]) ?>

<?= $form->field($model, 'occa_amount')->input('number', [
    'name' => 'amount[' . SoccerFactAR::FACT_SHOT_ON_TARGET . ']',
    'value' => SportOccaAR::DEFAULT_OCCA_AMOUNT,
    'min' => 1,
    'max' => 90,
]) ?>

<div class="accordion form-group" id="accordionExample">
    <div class="card">
        <div class="card-header" id="headingOne">
            <button class="btn" id="block-btn" type="button" data-toggle="collapse" data-target="#collapseOne"
                    aria-expanded="true" aria-controls="collapseOne">
                Блокированный удар
            </button>
        </div>
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <div class="form-group" id="block-input">
                    <?= $form->field($model, 'sman_id')->widget(DepDrop::class, [
                        'options' => [
                            'id' => 'blocked-shot',
                            'placeholder' => 'Выберите спортсмена',
                            'name' => 'fact[' . SoccerFactAR::FACT_BLOCKED_SHOT . ']',
                        ],
                        'pluginOptions' => [
                            'depends' => ['team-id'],
                            'url' => Url::to(['/game/match-occa/ajax-not-goalkeeper', 'match_id' => $modelMatch->id, 'blocked_shot' => true]),
                            'initialize' => true,
                        ],
                        'type' => DepDrop::TYPE_SELECT2,
                    ])->label('Кто блокировал') ?>
                    <?= $form->field($model, 'occa_amount')->input('number', [
                        'name' => 'amount[' . SoccerFactAR::FACT_BLOCKED_SHOT . ']',
                        'value' => SportOccaAR::DEFAULT_OCCA_AMOUNT,
                        'min' => 1,
                        'max' => 90,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingTwo">
            <button class="btn" id="save-btn" type="button" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="false" aria-controls="collapseTwo">
                Сэйв
            </button>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body">
                <div class="form-group" id="save-input">
                    <?= $form->field($model, 'sman_id')->widget(DepDrop::class, [
                        'options' => [
                            'id' => 'keeper-save',
                            'placeholder' => 'Выберите спортсмена',
                            'name' => 'fact[' . SoccerFactAR::FACT_KEEPER_SAVE . ']',
                        ],
                        'pluginOptions' => [
                            'depends' => ['team-id'],
                            'url' => Url::to(['/game/match-occa/ajax-goalkeeper', 'match_id' => $modelMatch->id]),
                            'initialize' => true,
                        ],
                        'type' => DepDrop::TYPE_SELECT2,
                    ])->label('Кто засэйвил') ?>
                    <?= $form->field($model, 'occa_amount')->input('number', [
                        'name' => 'amount[' . SoccerFactAR::FACT_KEEPER_SAVE . ']',
                        'value' => SportOccaAR::DEFAULT_OCCA_AMOUNT,
                        'min' => 1,
                        'max' => 90,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <?= Html::submitButton('Добавить', ['class' => 'btn btn-success js-submit-attach-entity']) ?>
</div>

<?php ActiveForm::end() ?>
