<?php

use common\modules\game\models\GameTourneyAR;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var GameTourneyAR $model
 * @var array $sports
 * @var array $statuses
 * @var array $trophyKinds
 * @var array $maxGamerItems
 * @var array $freeTrophyTypes
 * @var array $autoPrizefundTrophyTypes
 */

$this->title = 'Добавление турнира';
$this->params['breadcrumbs'][] = ['label' => 'Турниры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-tourney-ar-create">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'sports' => $sports,
        'statuses' => $statuses,
        'maxGamerItems' => $maxGamerItems,
        'trophyKinds' => $trophyKinds,
        'freeTrophyTypes' => $freeTrophyTypes,
        'autoPrizefundTrophyTypes' => $autoPrizefundTrophyTypes,
    ]) ?>

</div>
