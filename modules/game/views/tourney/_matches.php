<?php

/**
 * @var View $this
 * @var GameTourneyAR|ActiveRecord $model
 * @var ActiveDataProvider $matchesProvider
 * @var array $sports
 * @var array $statuses
 * @var SportMatchAR[] $availableMatches
 * @var array $touMatchIds
 */

use common\modules\game\models\GameTourneyAR;
use common\modules\game\models\SportMatchAR;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

?>
<?php Pjax::begin(['id' => 'matches-pjax']) ?>
<?= GridView::widget([
    'dataProvider' => $matchesProvider,
    'columns' => [
        [
            'value' => static function (SportMatchAR $model) use ($touMatchIds) {
                return Html::checkbox('match_ids[]', in_array($model->id, $touMatchIds), [
                    'value' => $model->id,
                    'class' => 'match-tourney-selection',
                    'onclick' => 'event.stopPropagation()',
                ]);
            },
            'format' => 'raw',
            'contentOptions' => [
                'onClick' => 'this.firstChild.click()',
            ],
        ],
        [
            'attribute' => 'id',
            'options' => [
                'class' => 'id-grid-column',
            ],
        ],
        [
            'attribute' => 'sport_id',
            'value' => static function (SportMatchAR $model) {
                return $model->sport->r_name ?? null;
            }
        ],
        'start_date',
        [
            'attribute' => 'r_name',
            'value' => static function (SportMatchAR $model) {
                return Html::a($model->r_name ?? '', Url::to(['/game/match/view', 'id' => $model->id]));
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'lea_id',
            'value' => static function (SportMatchAR $model) {
                $league = $model->league ?? null;

                $title = '';
                if ($league) {
                    $title = $league->getTitle();
                }

                return Html::a($title, Url::to(['/sport/league/view', 'id' => $model->lea_id]));
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'team_owner',
            'value' => static function (SportMatchAR $model) {
                $team = $model->teamOwner ?? null;

                $title = '';
                if ($team) {
                    $title = $team->getTitle();
                }

                return Html::a($title, Url::to(['/sport/team/update', 'id' => $model->team_owner]));
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'team_guest',
            'value' => static function (SportMatchAR $model) {
                $team = $model->teamGuest ?? null;

                $title = '';
                if ($team) {
                    $title = $team->getTitle();
                }

                return Html::a($title, Url::to(['/sport/team/update', 'id' => $model->team_guest]));
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'r_status',
            'value' => static function (SportMatchAR $model) use ($statuses) {
                return $statuses[$model->r_status] ?? null;
            }
        ],
    ]
]) ?>
<?php Pjax::end() ?>
