<?php

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use common\modules\game\forms\GameTourneyAS;
use common\modules\money\models\MoneyCurrencyAR;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\user\models\UserAR;
use common\widgets\GridActions\GridActionsWidget;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var GameTourneyAS $searchModel
 * @var array $sports
 * @var array $leaList
 * @var array $statuses
 */

$this->title = 'Турниры';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    [
        'attribute' => 'r_name',
        'value' => static function (GameTourneyAS $model) {
            return $model->getTitle();
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'sport_id_copy',
        'options' => [
            'class' => 'medium-grid-column',
        ],
        'value' => static function (GameTourneyAS $model) {
            return $model->sport_id;
        },
    ],
    [
        'attribute' => 'sport_id',
        'value' => static function (GameTourneyAS $model) {
            return $model->sport->r_name ?? null;
        },
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'sport_id',
            'data' => $sports,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
            'pjaxContainerId' => 'game-tourney-pjax',
        ]),
    ],
    [
        'attribute' => 'lea_id_copy',
        'options' => [
            'class' => 'medium-grid-column',
        ],
        'value' => static function (GameTourneyAS $model) {
            return $model->lea_id;
        },
    ],
    [
        'attribute' => 'lea_id',
        'value' => static function (GameTourneyAS $model) {
            $league = $model->league ?? null;

            if ($league) {
                return Html::a($league->getTitle(), Url::to(['/sport/league/view', 'id' => $model->lea_id]));
            }

            return null;
        },
        'format' => 'raw',
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'lea_id',
            'data' => $leaList,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
            'pjaxContainerId' => 'game-tourney-pjax',
        ]),
    ],
    [
        'attribute' => 'r_status',
        'value' => static function (GameTourneyAS $model) use ($statuses) {
            return $statuses[$model->r_status] ?? null;
        },
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'r_status',
            'data' => $statuses,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
            'pjaxContainerId' => 'game-tourney-pjax',
        ]),
    ],
    [
        'attribute' => 'r_icon',
        'value' => static function (GameTourneyAS $model) {
            return $model->htmlImage();
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'buyin',
        'format' => 'raw',
        'value' => static function (GameTourneyAS $model) {
            $html = null;
            if (empty($model->buyin)) return null;
            foreach ($model->buyin as $currencyId => $buyin) {
                $currency = MoneyCurrencyAR::findOne($currencyId);

                if (!$currency) {
                    continue;
                }

                $html .= Html::tag('div', $currency->r_name . ': ' . $buyin);
            }

            return trim($html, ', ');
        },
    ],
    'prizefund',
    'max_gamers',
    [
        'attribute' => 'gamers',
        'format' => 'raw',
        'value' => static function (GameTourneyAS $model) {
            $html = null;

            $userNames = explode(',', $model->gamers);

            foreach ($userNames as $userName) {
                $user = UserAR::findOne(['username' => trim($userName)]);

                if (!$user) {
                    continue;
                }

                $html .= Html::a($user->username, ['/user/manage/view', 'id' => $user->id], [
                        'target' => '_blank',
                    ]) . ', ';
            }

            return trim($html, ', ');
        }
    ],
    ['class' => ActionColumn::class],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="game-tourney-ar-index">
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#game-tourney-pjax',
        'pjaxUrl' => '/game/tourney/index',
        'model' => $searchModel,
        'additionalContainerOptions' => [
            'class' => [
                'columns-select2-margined',
            ],
        ],
    ]) ?>
    <?= GridActionsWidget::widget([
        'title' => $this->title,
        'enableChangeStatus' => true,
    ]) ?>
    <?php Pjax::begin(['id' => 'game-tourney-pjax']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => ArrayHelper::merge([GridActionsWidget::$checkboxColumn], $columns),
    ]); ?>
    <?php Pjax::end(); ?>
</div>
