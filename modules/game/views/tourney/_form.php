<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\web\View;
use common\modules\game\models\GameTourneyAR;
use common\modules\money\MoneyModule;
use common\widgets\DetailForm\DetailFormWidget;
use common\widgets\image\ImageWidget;
use common\widgets\JsonEditor;
use kartik\datetime\DateTimePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;

/**
 * @var View $this
 * @var GameTourneyAR $model
 * @var ActiveForm $form
 * @var array $sports
 * @var array $statuses
 * @var array $trophyKinds
 * @var array $maxGamerItems
 * @var array $freeTrophyTypes
 * @var array $autoPrizefundTrophyTypes
 */

$this->registerJsVar('freeTrophyTypes', $freeTrophyTypes);
$this->registerJsVar('autoPrizefundTrophyTypes', $autoPrizefundTrophyTypes);
$this->registerJsFile('js/game/game.js', ['depends' => JqueryAsset::class]);

$inputs = [
    'id' => [
        'inputHidden' => $model->isNewRecord,
        'inputOptions' => [
            'disabled' => true,
        ],
    ],
    'r_name',
    'sport_id' => Select2::widget([
        'model' => $model,
        'attribute' => 'sport_id',
        'data' => $sports,
        'options' => ArrayHelper::merge([
            'id' => 'sport-id',
            'placeholder' => 'Выберите спорт',
        ], $model->isNewRecord ? [] : [
            'disabled' => true,
        ]),
    ]),
    'lea_id' => DepDrop::widget([
        'model' => $model,
        'attribute' => 'lea_id',
        'options' => [
            'id' => 'league-id',
            'placeholder' => 'Выберите лигу',
        ],
        'pluginOptions' => ArrayHelper::merge([
            'depends' => ['sport-id'],
            'url' => Url::to(['/game/tourney/ajax-leagues', 'id' => $model->id]),
        ], $model->isNewRecord ? [] : [
            'initialize' => true,
            'initDepends' => ['sport-id'],
        ]),
        'type' => DepDrop::TYPE_SELECT2,
    ]),
    'r_status' => Select2::widget([
        'model' => $model,
        'attribute' => 'r_status',
        'data' => $statuses,
        'options' => [
            'id' => 'status-id',
            'placeholder' => 'Выберите статус',
        ],
    ]),
    'r_icon' => [
        'inputHidden' => $model->isNewRecord,
        'inputValue' => ImageWidget::widget([
            'model' => $model,
            'attribute' => 'r_icon',
            'kind' => GameTourneyAR::kind(),
        ]),
    ],
];

if (!$model->isNewRecord) {
    $inputs['start_date'] = DateTimePicker::widget([
        'model' => $model,
        'attribute' => 'start_date',
        'options' => [
            'autocomplete' => 'off',
        ],
    ]);
}

$inputs = array_merge($inputs, [ 'buyInGold' => [
    'inputType' => 'number',
    'step' => 1,
    'id' => 'buyin-gold',
    'data' => [
        'currency' => MoneyModule::ACCOUNT_GOLD,
    ],
],
    'buyInGreen' => [
        'inputType' => 'number',
        'step' => 1,
        'id' => 'buyin-green',
        'data' => [
            'currency' => MoneyModule::ACCOUNT_GREEN,
        ],
    ],
    'prizefund' => [
        'inputType' => 'number',
        'step' => 1,
        'id' => 'prizefund',
    ],
    'lup_budget' => [
        'inputType' => 'number',
        'step' => 1,
    ],
    'max_smen' => [
        'inputType' => 'number',
        'step' => 1,
    ],
    'max_gamers' => Html::activeRadioList($model, 'max_gamers', $maxGamerItems, [
        'class' => 'btn-group d-flex radio-list',
        'item' => static function ($index, $label, $name, $checked, $value) {
            return Html::radio($name, $checked, [
                'label' => $label,
                'value' => $value,
                'labelOptions' => [
                    'class' => 'btn btn-light bdc-grey-500' . ($checked ? ' active' : ''),
                ],
                'class' => 'op-0',
            ]);
        },
    ]),
    'reg_lineups' => [
        'inputOptions' => [
            'disabled' => true,
        ],
    ],
    'trophy_kind' => Select2::widget([
        'model' => $model,
        'attribute' => 'trophy_kind',
        'data' => $trophyKinds,
        'options' => [
            'id' => 'trophy-kind',
        ],
        'pluginOptions' => [
            'placeholder' => 'Выберите Trophy Kind',
        ],
    ]),
    'score_kind' => DepDrop::widget([
        'model' => $model,
        'attribute' => 'score_kind',
        'options' => [
            'id' => 'score-kind',
            'placeholder' => 'Выберите Score Kind',
        ],
        'pluginOptions' => ArrayHelper::merge([
            'depends' => ['sport-id'],
            'url' => Url::to(['ajax-score-kind', 'id' => $model->id]),
        ], $model->isNewRecord ? [] : [
            'initialize' => true,
            'initDepends' => ['sport-id'],
        ]),
        'type' => DepDrop::TYPE_SELECT2,
    ]),
    'ext_info' => JsonEditor::widget([
        'model' => $model,
        'attribute' => 'ext_info',
    ]),])
?>

<div class="game-tourney-ar-form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]) ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => $inputs,
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
