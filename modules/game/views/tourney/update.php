<?php

/**
 * @var View $this
 * @var GameTourneyAR $model
 * @var $matchesProvider ActiveDataProvider
 * @var array $sports
 * @var array $statuses
 * @var array $maxGamerItems
 * @var array $trophyKinds
 * @var array $freeTrophyTypes
 * @var array $autoPrizefundTrophyTypes
 * @var array $touMatchIds
 */

use common\components\S3Core;
use common\modules\game\models\GameTourneyAR;
use kartik\tabs\TabsX;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Редактирование турнира: ' . $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Турниры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';

$this->registerJsVar('touId', $model->id);
$this->registerJsFile('/js/game/tourney.js');

$items = [
    [
        'label' => '<span class="ti ti-pencil"></span> Основная информация',
        'active' => true,
        'linkOptions' => ['id' => 'tourney-basic-information-tab'],
        'content' => $this->render('_form', [
            'model' => $model,
            'sports' => $sports,
            'statuses' => $statuses,
            'maxGamerItems' => $maxGamerItems,
            'trophyKinds' => $trophyKinds,
            'freeTrophyTypes' => $freeTrophyTypes,
            'autoPrizefundTrophyTypes' => $autoPrizefundTrophyTypes,
        ]),
    ],
];

if ($model->r_status & S3Core::STATUS_DRAFT) {
    $items[] = [
        'label' => '<span class="ti ti-layout-grid3"></span> Матчи',
        'content' => $this->render('_matches', [
            'matchesProvider' => $matchesProvider,
            'statuses' => $statuses,
            'touMatchIds' => $touMatchIds,
        ]),
    ];
}
?>
<div class="game-tourney-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= TabsX::widget([
        'items' => $items,
        'encodeLabels' => false,
    ]) ?>
</div>
