<?php

use common\components\S3Core;
use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\game\models\SportMatchAR;
use common\modules\game\models\TrophyTypeAR;
use common\modules\score\models\ScoreKindAR;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model common\modules\game\models\GameTourneyAR
 * @var $statuses array
 * @var $matchesProvider ActiveDataProvider
 * @var $lineupsProvider ActiveDataProvider
 */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Турниры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];
?>
<div class="game-tourney-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <div class="mB-10 text-right">
        <?= Html::a('<span class="fa fa-arrow-left"></span> К списку', ['index'], [
            'class' => 'btn btn-outline-primary pull-left',
        ]) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно желаете уделить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Tou скоринг', ['calc-tou', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Award скоринг', ['calc-award', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Dynamic скоринг', ['calc-dynamic', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Обновить призовой фонд', ['calc-game-data', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Обновить рейтинг юзеров', ['calc-rating', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'r_name',
            [
                'attribute' => 'sport_id',
                'value' => static function (GameTourneyAR $model) {
                    return $model->sport->r_name ?? null;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'lea_id',
                'value' => static function (GameTourneyAR $model) {
                    if (!$model->league) {
                        return null;
                    }

                    return Html::a($model->league->getTitle(), ['/sport/league/view', 'id' => $model->lea_id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'r_status',
                'value' => static function (GameTourneyAR $model) use ($statuses) {
                    return $statuses[$model->r_status] ?? null;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'r_icon',
                'value' => static function (GameTourneyAR $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
            'start_date',
            [
                'label' => 'buyin',
                'value' => static function (GameTourneyAR $model) {
                    $list = [];
                    if (is_array($model->buyIn)) {
                        foreach ($model->buyIn as $code => $value) {
                            $name = S3Core::mMoney()::accountLabel($code);
                            $list[] = $name . " - " . $value;
                        }
                    }
                    return Html::ul($list);
                },
                'format' => 'raw',
            ],
            'prizefund',
            'lup_budget',
            'max_smen',
            'max_gamers',
            'reg_lineups',
            [
                'label' => 'trophy_kind',
                'value' => static function (GameTourneyAR $model) {
                    return TrophyTypeAR::getTrophyList()[$model->trophy_kind] ?? 'Не задано';
                },
                'format' => 'raw',
            ],
            [
                'label' => 'score_kind',
                'value' => static function (GameTourneyAR $model) {
                    return ScoreKindAR::getScoresList($model->sport_id)[$model->score_kind] ?? 'Не задано';
                },
                'format' => 'raw',
            ],
            [
                'label' => 'prize',
                'value' => static function (GameTourneyAR $model) {
                    $list = [];
                    if (is_array($model->prize)) {
                        foreach ($model->prize as $code => $value) {
                            $list[] = $code . "м - " . $value;
                        }
                    }
                    return Html::ul($list);
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

    <h3 class="c-grey-900 mT-10 mB-20">Матчи</h3>
    <?= GridView::widget([
        'dataProvider' => $matchesProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'sport_id',
                'value' => static function (SportMatchAR $model) {
                    return $model->sport->r_name ?? null;
                },
            ],
            [
                'attribute' => 'lea_id',
                'value' => static function (SportMatchAR $model) {
                    $league = $model->league ?? null;

                    if ($league) {
                        return Html::a($league->getTitle(), Url::to(['/sport/league/view', 'id' => $model->lea_id]), [
                            'target' => '_blank',
                        ]);
                    }

                    return null;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'team_owner',
                'value' => static function (SportMatchAR $model) {
                    $teamOwner = $model->teamOwner;

                    if (!$teamOwner) {
                        return null;
                    }

                    return Html::a($teamOwner->getTitle(), ['/sport/team/view', 'id' => $teamOwner->id], [
                        'target' => '_blank',
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'team_guest',
                'value' => static function (SportMatchAR $model) {
                    $teamGuest = $model->teamGuest;

                    if (!$teamGuest) {
                        return null;
                    }

                    return Html::a($teamGuest->getTitle(), ['/sport/team/view', 'id' => $teamGuest->id], [
                        'target' => '_blank',
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'r_name',
                'value' => static function (SportMatchAR $model) {
                    return Html::a($model->r_name, ['/game/match/view', 'id' => $model->id], [
                        'target' => '_blank',
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'r_status',
                'value' => static function (SportMatchAR $model) use ($statuses) {
                    return $statuses[$model->r_status] ?? null;
                },
            ],
            'start_date',
        ],
    ]) ?>

    <h3 class="c-grey-900 mT-10 mB-20">Составы</h3>
    <?= GridView::widget([
        'dataProvider' => $lineupsProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'user_id',
                'value' => static function (GameLineupAR $model) {
                    if (!$model->user) {
                        return null;
                    }

                    return Html::a($model->user_id, ['/user/manage/view', 'id' => $model->user_id], [
                        'target' => '_blank',
                    ]);
                },
                'format' => 'raw',
            ],
            'user_name',
            [
                'attribute' => 'tou_id',
                'value' => static function (GameLineupAR $model) {
                    if (!$model->tourney) {
                        return null;
                    }

                    return Html::a($model->tourney->getTitle(), ['tourney/view', 'id' => $model->tou_id], [
                        'target' => '_blank',
                    ]);
                },
                'format' => 'raw',
            ],
            'l_points',
            'l_place',
            'l_award',
            'is_draft:boolean',
            'isnt_reg:boolean',
            //'ext_info',
        ],
    ]) ?>
</div>
