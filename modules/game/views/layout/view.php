<?php

use backend\components\aboard\widgets\GridView;
use common\modules\sport\models\SportPositionAR;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model common\modules\game\models\GameLayoutAR
 * @var $cellsProvider \yii\data\ArrayDataProvider
 */

$this->title = $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Раскладки состава', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id, 'ent_kind' => $model->ent_kind]];

YiiAsset::register($this);

$model->cells = json_encode($model->cells);
?>
<div class="game-layout-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-arrow-left"></span> К списку', ['index'], [
            'class' => 'btn btn-outline-primary pull-left',
        ]) ?>
        <?= Html::a('<span class="fa fa-trash"></span> Удалить', ['delete', 'id' => $model->id, 'ent_kind' => $model->ent_kind], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно желаете уделить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ent_kind',
            'ent_code',
            'ent_value',
            'r_name',
        ],
    ]) ?>

    <h4 class="c-grey-900 mT-10 mB-30">Ячейки</h4>
    <?= GridView::widget([
        'dataProvider' => $cellsProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'pos_ids',
                'label' => 'Амплуа (IDs)',
                'value' => static function ($model) {
                    $positions = $model['rules']['positions'] ?? [];

                    return implode(', ', $positions);
                },
            ],
            [
                'attribute' => 'pos_names',
                'label' => 'Амплуа',
                'value' => static function ($model) {
                    $positions = $model['rules']['positions'] ?? [];
                    $positionNames = SportPositionAR::find()
                        ->select(['r_name'])
                        ->andWhere(['id' => $positions])
                        ->column();

                    return implode(', ', $positionNames);
                }
            ],
        ],
    ]) ?>
</div>
