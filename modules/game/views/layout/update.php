<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\game\models\GameLayoutAR $model
 */

$this->title = 'Редактирование раскладки состава: ' . $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Раскладки состава', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->r_name, 'url' => ['view', 'id' => $model->id, 'ent_kind' => $model->ent_kind]];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id, 'ent_kind' => $model->ent_kind]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
?>
<div class="game-layout-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
