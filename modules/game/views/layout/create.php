<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\game\models\GameLayoutAR $model
 */

$this->title = 'Добавление Game Layout Ar';
$this->params['breadcrumbs'][] = ['label' => 'Game Layout Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-layout-ar-create">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
