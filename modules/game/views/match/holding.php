<?php

/**
 * @var View $this
 * @var SportMatchAR $model
 * @var ActiveDataProvider $occaDataProvider
 * @var SportFactAR[] $sportFacts
 * @var SmanAR[] $smen
 * @var SportOccaAR $modelOcca
 * @var SportLineupFM $lineupsForm
 * @var SportPositionAR[] $positions
 */

use common\components\S3Core;
use common\modules\game\forms\SportLineupFM;
use common\modules\game\models\SportMatchAR;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportFactAR;
use common\modules\sport\models\SportOccaAR;
use common\modules\sport\models\SportPositionAR;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Проведение матча: ' . $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Матчи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Проведение матча';

$title = 'ID: ' . $model->id . ', ' . $model->r_name;

$actions = [
    Html::a('Скоринг', ['calc-sport', 'id' => $model->id], [
        'class' => 'btn btn-primary',
        'title' => 'Спортивный скоринг',
        'data' => [
            'toggle' => 'tooltip',
            'placement' => 'top',
        ],
    ]),
    Html::a('Импорт', ['occa-import', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'title' => 'Импорт событий',
        'data' => [
            'toggle' => 'tooltip',
            'placement' => 'top',
        ],
    ]),
    Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Вы действительно хотите удалить матч?',
            'method' => 'post',
        ],
    ]),
];

if ($model->isntStatus(S3Core::STATUS_CLOSED)) {
    $actions[] = Html::a('Закрыть', ['close-match', 'id' => $model->id], [
        'class' => 'btn btn-danger js-form-attach-entity ',
        'title' => 'Закрыть матч',
        'data' => [
            'confirm' => 'Вы действительно хотите закрыть матч?',
            'method' => 'post',
            'toggle' => 'tooltip',
            'placement' => 'top',
        ],
    ]);
}

if ($model->isntStatus(S3Core::STATUS_CLOSED | S3Core::STATUS_DELETED)) {
    $actions[] = Html::a('Отменить', ['cancel-match', 'id' => $model->id], [
        'class' => 'btn btn-danger js-form-attach-entity ',
        'title' => 'Отменить матч',
        'data' => [
            'confirm' => 'Вы действительно хотите отменить матч?',
            'method' => 'post',
            'toggle' => 'tooltip',
            'placement' => 'top',
        ],
    ]);
}

if ($model->r_status < S3Core::STATUS_PROCESSING) {
    if ($model->r_status & S3Core::STATUS_WAITING) {
        $actions['specialAction'] = Html::a('Старт', ['match/start-match', 'id' => $model->id], [
            'class' => 'btn btn-info',
            'title' => 'Старт матча',
            'data' => [
                'confirm' => 'Вы действительно начать матч?',
                'toggle' => 'tooltip',
                'placement' => 'top',
            ],
        ]);
    } elseif ($model->r_status & S3Core::STATUS_LIVE && !$model->getOccasions()->existFinisOcca($model->id)) {
        $actions['specialAction'] = Html::a('Завершение', ['match/finis-match', 'id' => $model->id], [
            'class' => 'btn btn-info',
            'title' => 'Завершение матча',
            'data' => [
                'confirm' => 'Вы действительно хотите завершить матч?',
                'toggle' => 'tooltip',
                'placement' => 'top',
            ],
        ]);
    } else {
        $actions['specialAction'] = Html::a('Статус', ['match/next-status', 'id' => $model->id], [
            'class' => 'btn btn-primary',
            'title' => 'Следующий статус',
            'data' => [
                'confirm' => 'Вы действительно хотите перевести статус?',
                'toggle' => 'tooltip',
                'placement' => 'top',
            ],
        ]);
    }
}

$occaView = '_occa';
if ($model->sport_id === SportAR::SOCCER) {
    $occaView = '_soccer-occa';
}

$this->registerJsFile('/js/game/matchHolding.js');
?>
<div class="sport-match-ar-holding">
    <h4 class="c-grey-900 mT-10 mB-10"><?= Html::encode($title) ?></h4>
    <?= $this->render('_change-status', [
        'model' => $model,
    ]) ?>
    <?= $this->render($occaView, [
        'occaDataProvider' => $occaDataProvider,
        'model' => $model,
        'sportFacts' => $sportFacts,
        'smen' => $smen,
        'modelOcca' => $modelOcca,
        'action' => $actions,
    ]) ?>
    <?= $this->render('_form-lineups', [
        'model' => $model,
        'lineupsForm' => $lineupsForm,
        'positions' => $positions,
    ]) ?>
</div>
