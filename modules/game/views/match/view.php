<?php

use common\components\S3Core;
use common\modules\game\models\GameTourneyAR;
use common\modules\game\models\SportMatchAR;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var SportMatchAR $model
 * @var \yii\data\ActiveDataProvider $tourneyDataProvider
 */

$this->title = $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Матчи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = ['label' => 'Проведение матча', 'url' => ['holding', 'id' => $model->id]];

$title = 'ID: ' . $model->id . ', ' . $model->r_name;
?>
<div class="sport-team-ar-update">
    <h4 class="c-grey-900"><?= Html::encode($title) ?></h4>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'r_status',
                'value' => static function (SportMatchAR $model) {
                    return S3Core::code2status($model->r_status);
                }
            ],
            [
                'attribute' => 'sport.r_name',
                'label' => 'Спорт'
            ],
            [
                'attribute' => 'lea_id',
                'format' => 'raw',
                'value' => static function (SportMatchAR $model) {
                    $league = $model->league ?? null;

                    if ($league) {
                        return Html::a($league->r_name, Url::to(['/sport/league/view', 'id' => $league->id]), [
                            'target' => '_blank',
                        ]);
                    }

                    return null;
                }
            ],
            'r_name',
            [
                'attribute' => 'start_date',
                'format' => 'raw',
                'value' => static function (SportMatchAR $model) {
                    $startDate = $model->start_date;

                    $autoStart = $model->joGet('autostart') ?? 'разрешен';
                    $startDate .= ' <b>автостарт:</b> ' . $autoStart;

                    return $startDate;
                }
            ],
            [
                'attribute' => 'team_owner',
                'format' => 'raw',
                'value' => static function (SportMatchAR $model) {
                    $teamOwner = $model->teamOwner ?? null;

                    if ($teamOwner) {
                        return Html::a($teamOwner->r_name, Url::to(['team/view', 'id' => $teamOwner->id]), [
                            'target' => '_blank',
                        ]);
                    }

                    return null;
                }
            ],
            [
                'attribute' => 'team_guest',
                'format' => 'raw',
                'value' => static function (SportMatchAR $model) {
                    $teamGuest = $model->teamGuest ?? null;

                    if ($teamGuest) {
                        return Html::a($teamGuest->r_name, Url::to(['team/view', 'id' => $teamGuest->id]), [
                            'target' => '_blank',
                        ]);
                    }

                    return null;
                }
            ],
            'total_owner',
            'total_guest',
        ],
    ]) ?>

    <h4 class="c-grey-900 mT-10 mB-30 pull-left">Турниры</h4>
    <?= GridView::widget([
        'dataProvider' => $tourneyDataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'r_status',
                'value' => static function (GameTourneyAR $model) {
                    return S3Core::code2status($model->r_status);
                }
            ],
            [
                'attribute' => 'r_name',
                'format' => 'raw',
                'value' => static function (GameTourneyAR $model) {
                    return Html::a($model->r_name, Url::to(['/game/tourney/update', 'id' => $model->id]), [
                        'target' => '_blank',
                    ]);
                }
            ],
        ]
    ]) ?>
</div>
