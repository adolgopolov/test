<?php

use common\components\S3Core;
use common\modules\game\models\SportMatchAR;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportFactAR;
use common\modules\sport\models\SportOccaAR;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var ActiveDataProvider $occaDataProvider
 * @var SportMatchAR $model
 * @var SportFactAR[] $sportFacts
 * @var SmanAR[] $smen
 * @var SportOccaAR $modelOcca
 * @var array $action
 */

$smenList = ArrayHelper::map($smen, 'id', 'r_name');

$columns = [
    [
        'label' => 'Спортсмен',
        'attribute' => 'sman.r_name',
        'value' => static function ($model) {
            /** @var SmanAR $sman */
            $sman = $model['sman'];

            $position = $sman->lastTeamMember->position->r_name ?? null;

            $title = $sman->getTitle();

            if ($position) {
                $title .= '(' . $position . ')';
            }

            return $title;
        },
        'contentOptions' => static function () {
            $posData = [];

            return [
                'data' => array_merge([
                    'sman' => true,
                ], $posData),
            ];
        },
    ],
    [
        'label' => 'Статус',
        'attribute' => 'sman.status',
        'format' => 'text',
    ],
    [
        'label' => 'Команда',
        'attribute' => 'team',
    ],
];

foreach ($sportFacts as $fact) {
    $columns[] = [
        'label' => $fact->r_name,
        'attribute' => 'fact' . $fact->id,
        'value' => static function ($model) use ($fact) {
            /**
             * @var SportOccaAR $occa
             * @var SmanAR $sman
             */

            $occa = $model['fact' . $fact->id];
            $sman = $model['sman'];
            $position = $sman->lastTeamMember->position->ent_code ?? null;

            if ($fact->ent_code === 'hockey.plus_minus' && $position === 'hockey.goalkeeper') {
                return '-';
            }

            if ($position !== 'hockey.goalkeeper' &&
                in_array($fact->ent_code, ['hockey.missed_goal', 'any.saves', 'hockey.dry_match'])) {

                return '-';
            }

            return $occa->occa_amount ?? '-';
        },
        'contentOptions' => static function ($model) use ($fact) {
            /** @var SmanAR $sman */
            $sman = $model['sman'];
            $position = $sman->lastTeamMember->position->ent_code ?? null;

            $data = [
                'data' => [
                    'fact-id' => $fact->id,
                ],
            ];

            if ($fact->ent_code === 'hockey.plus_minus' && $position === 'hockey.goalkeeper') {
                $data['data']['disabled'] = 1;
            }
            elseif ($position !== 'hockey.goalkeeper' &&
                in_array($fact->ent_code, ['hockey.missed_goal', 'hockey.keeper_save'])) {

                $data['data']['disabled'] = 1;
            }

            return $data;
        },
    ];
}

$this->registerJsFile('js/sport/occa.js', ['depends' => JqueryAsset::class]);
?>
<?php Pjax::begin(['id' => 'match-occa-container']) ?>
<div class="fixed-container">
    <div class="fixed-content">
        <div class="d-flex flex-row flex-wrap justify-content-between mB-10">
            <div>
                <?= $this->render('_match-total', [
                    'model' => $model,
                ]) ?>
            </div>
            <div>
                <?= implode(' ', $action ?? []) ?>
            </div>
        </div>
    </div>
</div>

<?php if ($model->r_status & S3Core::STATUS_LIVE) { ?>
    <?php Modal::begin([
        'title' => Html::tag('h5', 'Обновление спортивных событий - <span></span>', ['class' => 'c-grey-900']),
        'size' => Modal::SIZE_LARGE,
        'id' => 'occa-modal'
    ]) ?>
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['match/create-occa-many']),
        'method' => 'POST',
        'options' => [
            'class' => 'js-form-attach-entity',
            'data-pjax-selector' => '#match-occa-container',
        ]
    ]) ?>

    <?= $form->field($modelOcca, 'match_id')
        ->hiddenInput(['value' => $model->id])
        ->label(false) ?>

    <?= $form->field($modelOcca, 'sman_id')
        ->hiddenInput(['id' => 'sman-id'])
        ->label(false) ?>

    <?php foreach ($sportFacts as $fact) { ?>
        <?= $form->field($modelOcca, 'fact_id')
            ->input('number', [
                'name' => 'SportOccaAR[fact_id][' . $fact->id . ']',
            ])
            ->label($fact->r_name) ?>
    <?php } ?>

    <?= Html::submitButton('Обновить', ['class' => 'btn btn-success js-submit-attach-entity']) ?>

    <?php ActiveForm::end() ?>
    <?php Modal::end() ?>
<?php } ?>

<?= GridView::widget([
    'dataProvider' => $occaDataProvider,
    'options' => [
        'class' => 'grid-view-occa',
    ],
    'tableOptions' => [
        'class' => 'table table-bordered table-hover',
    ],
    'columns' => $columns,
    'rowOptions' => static function ($model) {
        /** @var SmanAR $sman */
        $sman = $model['sman'];
        $class = ($sman->status)?['class' => 'alert-danger']:[];
        return array_merge([
            'data' => [
                'update-occa' => $sman->id,
            ],
        ],$class);
    },
]) ?>

<?php Pjax::end() ?>
