<?php

/**
 * @var \yii\web\View $this
 * @var \common\modules\game\models\SportMatchAR $model
 */

use common\components\S3Core;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$statuses = S3Core::code2status([
    S3Core::STATUS_WAITING,
    S3Core::STATUS_LIVE,
    S3Core::STATUS_PROCESSING,
    S3Core::STATUS_COMPLETED,
]);

?>
<?php $form = ActiveForm::begin([
    'action' => ['/game/match/change-status', 'id' => $model->id],
    'method' => 'POST',
    'options' => [
        'class' => 'js-form-attach-entity form-inline',
        'data-pjax-selector' => '#match-occa-container',
    ],
]) ?>
<div class="d-flex flex-row-reverse">
    <?= $form->field($model, 'r_status')->widget(Select2::class, [
        'data' => $statuses,
        'options' => [
            'id' => 'status-input',
            'placeholder' => 'Выберите статус',
        ],
        'theme' => Select2::THEME_BOOTSTRAP,
    ])->label(false) ?>

    <?= Html::submitButton('<i class="ti-save"></i> В этап', [
        'class' => 'btn btn-success mR-5',
        'title' => 'Перевести в выбранный этап',
        'data' => [
            'toggle' => 'tooltip',
            'placement' => 'top',
        ],
    ]) ?>
</div>
<?php ActiveForm::end() ?>
