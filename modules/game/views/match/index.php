<?php

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use common\modules\game\forms\SportMatchAS;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\widgets\GridActions\GridActionsWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var SportMatchAS $searchModel
 * @var array $leaList
 * @var array $statuses
 * @var array $sports
 */

Yii::$app->user->setReturnUrl(Url::to());

$this->title = 'Матчи';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    'r_name',
    [
        'attribute' => 'sport_id_copy',
        'options' => [
            'class' => 'sport-id-grid-column',
        ],
        'value' => static function (SportMatchAS $model) {
            return $model->sport_id;
        },
    ],
    [
        'attribute' => 'sport_id',
        'value' => static function (SportMatchAS $model) {
            return $model->sport->r_name ?? null;
        },
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'sport_id',
            'data' => $sports,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
            'pjaxContainerId' => 'game-match-pjax',
        ]),
    ],
    [
        'attribute' => 'lea_id_copy',
        'options' => [
            'class' => 'medium-grid-column',
        ],
        'value' => static function (SportMatchAS $model) {
            return $model->lea_id;
        },
    ],
    [
        'attribute' => 'lea_id',
        'value' => static function (SportMatchAS $model) {
            $league = $model->league ?? null;

            if ($league) {
                return Html::a($league->getTitle(), Url::to(['/sport/league/view', 'id' => $model->lea_id]));
            }

            return null;
        },
        'format' => 'raw',
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'lea_id',
            'data' => $leaList,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
            'pjaxContainerId' => 'game-match-pjax',
        ]),
    ],
    [
        'attribute' => 'r_status',
        'value' => static function (SportMatchAS $model) use ($statuses) {
            return $statuses[$model->r_status] ?? null;
        },
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'r_status',
            'data' => $statuses,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
            'pjaxContainerId' => 'game-match-pjax',
        ]),
    ],
    [
        'attribute' => 'start_date',
        'value' => static function (SportMatchAS $model) {
            return $model->start_date_ru;
        },
    ],
    [
        'class' => ActionColumn::class,
        'template' => '<div class="btn-group">{view}{holding}{update}{occa-import}</div>',
        'buttons' => [
            'holding' => static function ($url) {
                $icon = Html::tag('em', null, [
                    'class' => 'fas fa-gamepad',
                ]);

                return Html::a($icon, $url, [
                    'class' => 'btn btn-outline-primary',
                    'data' => [
                        'toggle' => 'tooltip',
                        'title' => 'Проведение матча',
                    ],
                ]);
            },
            'occa-import' => static function ($url) {
                $icon = Html::tag('em', null, [
                    'class' => 'fas fa-download',
                ]);

                return Html::a($icon, $url, [
                    'data-pjax' => '0',
                    'class' => 'btn btn-outline-success',
                    'data' => [
                        'toggle' => 'tooltip',
                        'title' => 'Импорт',
                    ],
                ]);
            },
        ],
    ],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sport-match-ar-index">
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#game-match-pjax',
        'pjaxUrl' => '/game/match/index',
        'model' => $searchModel,
        'additionalContainerOptions' => [
            'class' => [
                'columns-select2-margined',
            ],
        ],
    ]) ?>
    <?php GridActionsWidget::begin([
        'title' => $this->title,
        'contentInsideForm' => true,
    ]) ?>
    <?php $icon = Html::tag('em', '', ['class' => 'fas fa-caret-square-o-right mR-5']) ?>
    <?= Html::submitButton($icon . 'Следующий статус', [
        'class' => 'btn btn-primary mL-5',
        'name' => 'next-status',
        'data-confirm' => 'Вы действительно хотите перевести статус у всех выбранных записей?',
    ]) ?>
    <?php GridActionsWidget::end() ?>

    <?php Pjax::begin(['id' => 'game-match-pjax']) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => ArrayHelper::merge([GridActionsWidget::$checkboxColumn], $columns),
    ]) ?>
    <?php Pjax::end(); ?>
</div>
