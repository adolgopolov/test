<?php

use common\components\S3Core;
use common\modules\game\models\SportMatchAR;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SoccerFactAR;
use common\modules\sport\models\SportFactAR;
use common\modules\sport\models\SportOccaAR;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var ActiveDataProvider $occaDataProvider
 * @var SportMatchAR $model
 * @var SportFactAR[] $sportFacts
 * @var SmanAR[] $smen
 * @var SportOccaAR $modelOcca
 * @var array $action
 */

$smenList = ArrayHelper::map($smen, 'id', 'r_name');

$this->registerJsFile('js/sport/occa.js', ['depends' => JqueryAsset::class]);

$factLinks = SoccerFactAR::getLinks();
?>
<?php Pjax::begin(['id' => 'match-occa-container']) ?>
<div class="fixed-container">
    <div class="fixed-content">
        <div class="d-flex flex-row flex-wrap justify-content-between mB-10">
            <div>
                <?= $this->render('_match-total', [
                    'model' => $model,
                ]) ?>
            </div>
            <div>
                <?= implode(' ', $action ?? []) ?>
            </div>
        </div>
        <?php if ($model->r_status & S3Core::STATUS_LIVE) { ?>
            <div class="d-flex flex-row flex-wrap">
                <?php foreach ($factLinks as $fact) { ?>
                    <?= Html::a($fact['name'], '', [
                        'class' => 'btn btn-outline-primary flex-grow-1 m-1',
                        'data' => [
                            'soccer-occa' => true,
                            'form-name' => $fact['form'],
                            'match-id' => $model->id,
                            'fact-name' => $fact['factName'],
                            'fact-title' => $fact['name'],
                            'url-args' => $fact['urlArgs'] ?? null,
                        ],
                    ]) ?>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>

<?php if ($model->r_status & S3Core::STATUS_LIVE) { ?>

    <?php Modal::begin([
        'title' => Html::tag('h5', 'Добавление спортивного события: <span id="modal-fact-title"></span>', ['class' => 'c-grey-900']),
        'size' => Modal::SIZE_LARGE,
        'id' => 'occa-modal'
    ]) ?>

    <div id="occa-modal-content"></div>

    <?php Modal::end() ?>
<?php } ?>

<?= GridView::widget([
    'dataProvider' => $occaDataProvider,
    'options' => [
        'class' => 'grid-view-occa soccer',
    ],
    'columns' => [
//        ['class' => SerialColumn::class],
        'id',
        'sman_name',
        'sman_id',
        'team',
        'team_id',
        'after_start',
        'fact_name',
        'fact_id',
        [
            'attribute' => 'occa_amount',
            'value' => static function (SportOccaAR $model) {
                return $model->occa_amount;
            }
        ],
        [
            'class' => ActionColumn::class,
            'template' => '<div class="btn-group">{update}{delete}</div>',
            'buttons' => [
                'update' => static function ($url, SportOccaAR $model) {
                    if (!in_array($model->fact_id, [SportFactAR::FACT_MATCH_START, SportFactAR::FACT_MATCH_FINISH], true)) {
                        $title = Yii::t('yii', 'Update');

                        $icon = Html::tag('em', '', ['class' => 'fas fa-pencil-alt']);

                        return Html::a($icon, Url::to(['/sport/occa/update', 'id' => $model->id]), [
                            'title' => $title,
                            'aria-label' => $title,
                            'data-pjax' => '1',
                            'class' => 'btn btn-outline-primary btn-update',
                            'data-title' => $title,
                        ]);
                    }

                    return '';
                },
                'delete' => static function ($url, SportOccaAR $model) {
                    if (!in_array($model->fact_id, [SportFactAR::FACT_MATCH_START, SportFactAR::FACT_MATCH_FINISH], true)) {
                        $title = Yii::t('yii', 'Delete');

                        $icon = Html::tag('em', '', ['class' => 'fas fa-trash']);

                        return Html::a($icon, Url::to(['/game/match-occa/delete', 'id' => $model->id]), [
                            'title' => $title,
                            'aria-label' => $title,
                            'data-pjax' => '1',
                            'data-confirm' => 'Вы действительно хотите удалить эту запись?',
                            'data-method' => 'post',
                            'class' => 'btn btn-outline-danger',
                            'data-title' => $title,
                        ]);
                    }

                    return '';
                },
            ],
        ]
    ]
]) ?>

<?php Pjax::end() ?>
