<?php

use common\modules\game\forms\SportLineupFM;
use common\modules\game\models\SportMatchAR;
use common\modules\sport\forms\SportMemberAS;
use common\modules\sport\models\SportPositionAR;
use yii\bootstrap4\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/**
 *
 * @var View $this
 * @var ActiveForm $form
 * @var SportMatchAR $model
 * @var SportLineupFM $lineupsForm
 * @var ArrayDataProvider $ownersProvider
 * @var ArrayDataProvider $guestsProvider
 * @var SportPositionAR[] $positions
 */

$smanName = (string)Yii::$app->request->post('sman_name');
?>
<h4 class="c-grey-900 mT-10 mB-10">Составы команд</h4>
<?php Pjax::begin(['id' => 'form-lineups-pjax']) ?>
<?= Html::beginForm('', 'POST', [
    'id' => 'sman-filter-form',
    'data' => [
        'pjax' => true,
    ],
]) ?>
<?= Html::textInput('sman_name', $smanName, [
    'class' => 'form-control',
    'placeholder' => 'Поиск спортсменов',
    'id' => 'sman-filter-input',
]) ?>
<?= Html::endForm() ?>
<?php $form = ActiveForm::begin([
    'id' => 'presence',
    'action' => [
        'match-occa/presence',
        'id' => $model->id,
    ],
]) ?>
<?= Html::submitButton('Сохранить составы команд', ['class' => 'btn btn-primary mT-10']) ?>
<div class="row">
    <div class="col-md-6">
        <h4 class="c-grey-700"><?= $model->teamOwner->title['rus'] ?> - хозяева</h4>
        <?php foreach ($positions as $position) { ?>
            <?php Pjax::begin() ?>
            <h5 class="c-grey-600"><?= $position->r_name ?></h5>
            <?= GridView::widget([
                'dataProvider' => $model->getOwnersProvider($position->id),
                'summary' => false,
                'rowOptions' => [
                    'class' => ArrayHelper::merge(['sman-selection-row'], $smanName ? ['table-secondary'] : []),
                    'data' => [
                        'owner-selection-row' => $position->id,
                    ],
                ],
                'columns' => [
                    [
                        'label' => Html::checkbox('selectOwners', false, [
                            'data' => [
                                'owner-select-all' => $position->id,
                            ],
                        ]),
                        'encodeLabel' => false,
                        'value' => static function (SportMemberAS $model) use ($lineupsForm, $position) {
                            return Html::activeCheckbox($lineupsForm, "smen[$model->sman_id]", [
                                'checked' => $lineupsForm->hasOccaPresence($model->sman_id),
                                'label' => false,
                                'id' => "sportmemberar-sman_$model->sman_id",
                                'data' => [
                                    'owner-checkbox' => $position->id,
                                    'checkbox' => $model->id,
                                ],
                            ]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'sman_id',
                        'label' => 'ID Спортсмена',
                    ],
                    'sman_name',
                    [
                        'attribute' => 'pos_id',
                        'value' => static function (SportMemberAS $model) {
                            return $model->position->r_name ?? null;
                        },
                    ],
                    'sman_status',
                ],
            ]) ?>
            <?php Pjax::end() ?>
        <?php } ?>
    </div>
    <div class="col-md-6">
        <h4 class="c-grey-700"><?= $model->teamGuest->title['rus'] ?> - гости</h4>
        <?php foreach ($positions as $position) { ?>
            <?php Pjax::begin() ?>
            <h5 class="c-grey-600"><?= $position->r_name ?></h5>
            <?= GridView::widget([
                'dataProvider' => $model->getGuestsProvider($position->id),
                'summary' => false,
                'rowOptions' => [
                    'class' => ArrayHelper::merge(['sman-selection-row'], $smanName ? ['table-secondary'] : []),
                    'data' => [
                        'guest-selection-row' => $position->id,
                    ],
                ],
                'columns' => [
                    [
                        'label' => Html::checkbox('selectGuests', false, [
                            'data' => [
                                'guest-select-all' => $position->id,
                            ],
                        ]),
                        'encodeLabel' => false,
                        'value' => static function (SportMemberAS $model) use ($lineupsForm, $position) {
                            return Html::activeCheckbox($lineupsForm, "smen[$model->sman_id]", [
                                'checked' => $lineupsForm->hasOccaPresence($model->sman_id),
                                'label' => false,
                                'id' => "sportmemberar-sman_$model->sman_id",
                                'data' => [
                                    'guest-checkbox' => $position->id,
                                    'checkbox' => $model->id,
                                ],
                            ]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'sman_id',
                        'label' => 'ID Спортсмена',
                    ],
                    'sman_name',
                    [
                        'attribute' => 'pos_id',
                        'value' => static function (SportMemberAS $model) {
                            return $model->position->r_name ?? null;
                        },
                    ],
                    'sman_status',
                ],
            ]) ?>
            <?php Pjax::end() ?>
        <?php } ?>
    </div>
</div>
<?= Html::submitButton('Сохранить составы команд', ['class' => 'btn btn-primary mT-10']) ?>
<?php ActiveForm::end() ?>
<?php Pjax::end() ?>
