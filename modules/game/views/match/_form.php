<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\S3Core;
use common\widgets\DetailForm\DetailFormWidget;
use kartik\datetime\DateTimePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;

/**
 * @var common\modules\game\models\SportMatchAR $model
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var array $sports
 */

$nameTooltip = $model->isNewRecord ? 'Если не заполнять это поле, то название будет сгенерировано автоматически.' : '';
?>
<div class="sport-match-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
                'inputOptions' => [
                    'disaabled' => true,
                ],
            ],
            'r_status' => [
                'inputHidden' => $model->isNewRecord,
                'isActiveInput' => false,
                'inputValue' => S3Core::code2status($model->r_status),
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'sport.name' => [
                'inputHidden' => $model->isNewRecord,
                'isActiveInput' => false,
                'label' => 'Спорт',
                'inputValue' => $model->sport->r_name ?? null,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'r_name' => [
                'inputOptions' => [
                    'placeholder' => $nameTooltip,
                ],
            ],
            'sport_id' => Select2::widget([
                'model' => $model,
                'attribute' => 'sport_id',
                'data' => $sports,
                'options' => [
                    'id' => 'sport-id',
                    'placeholder' => 'Выберите спорт',
                ],
            ]),
            'lea_id' => DepDrop::widget([
                'model' => $model,
                'attribute' => 'lea_id',
                'options' => [
                    'id' => 'league-id',
                    'placeholder' => 'Выберите лигу',
                ],
                'pluginOptions' => array_merge([
                    'depends' => ['sport-id'],
                    'url' => Url::to(['/game/match/ajax-leagues', 'id' => $model->id]),
                ], (!$model->isNewRecord ? [
                    'initialize' => true,
                    'initDepends' => ['sport-id'],
                ] : [])),
                'type' => DepDrop::TYPE_SELECT2,
            ]),
            'team_owner' => DepDrop::widget([
                'model' => $model,
                'attribute' => 'team_owner',
                'options' => [
                    'id' => 'team-owner-id',
                    'placeholder' => 'Выберите команду',
                ],
                'pluginOptions' => array_merge([
                    'depends' => ['league-id'],
                    'url' => Url::to(['/game/match/ajax-teams', 'id' => $model->id, 'attr' => 'team_owner']),
                ], (!$model->isNewRecord ? [
                    'initialize' => true,
                    'initDepends' => ['sport-id'],
                ] : [])),
                'type' => DepDrop::TYPE_SELECT2,
            ]),
            'team_guest' => DepDrop::widget([
                'model' => $model,
                'attribute' => 'team_guest',
                'options' => [
                    'id' => 'team-guest-id',
                    'placeholder' => 'Выберите команду',
                ],
                'pluginOptions' => array_merge([
                    'depends' => ['league-id'],
                    'url' => Url::to(['/game/match/ajax-teams', 'id' => $model->id, 'attr' => 'team_guest']),
                ], (!$model->isNewRecord ? [
                    'initialize' => true,
                    'initDepends' => ['sport-id'],
                ] : [])),
                'type' => DepDrop::TYPE_SELECT2,
            ]),
            'start_date' => DateTimePicker::widget([
                'model' => $model,
                'attribute' => 'start_date',
                'options' => [
                    'autocomplete' => 'off',
                ],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd hh:ii:ss',
                    'autoclose' => true,
                ],
            ]),
            'autostart' => [
                'inputType' => 'checkbox',
                'inputHidden' => $model->r_status > S3Core::STATUS_WAITING ? true : false,
            ],
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
