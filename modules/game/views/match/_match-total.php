<?php
/**
 * @var \yii\web\View $this
 * @var \common\modules\game\models\SportMatchAR $model
 */

use common\components\S3Core;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$teamTitles = $model->getTeamTitles();
$enable = ($model->r_status & S3Core::STATUS_LIVE);

$fieldOptions = [
    'class' => 'form-control mX-10',
    'style' => 'width: 70px;',
];
if (!$enable) {
    $fieldOptions['readonly'] = true;
}

?>
<?php $form = ActiveForm::begin([
    'action' => ['/game/match/update-total', 'id' => $model->id],
    'method' => 'POST',
    'options' => [
        'class' => 'js-form-attach-entity form-inline',
        'data-pjax-selector' => '#match-occa-container',
    ],
]) ?>

<?= $form->field($model, 'total_owner', [
    'template' => '{label} {input}{error}{hint}',
])->input('number', $fieldOptions)->label($teamTitles['owner']) ?>
:
<?= $form->field($model, 'total_guest', [
    'template' => '{input} {label}{error}{hint}',
])->input('number', $fieldOptions)->label($teamTitles['guest']) ?>

<?php if ($enable) { ?>
    <?= Html::submitButton('<span class="ti-save"></span> Счет', [
        'class' => 'btn btn-success mX-20',
        'title' => 'Обновить счет',
        'data' => [
            'toggle' => 'tooltip',
            'placement' => 'top',
        ],
    ]) ?>
<?php } ?>

<?php ActiveForm::end() ?>
