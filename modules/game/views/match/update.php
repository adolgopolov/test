<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model common\modules\game\models\SportMatchAR
 * @var $sports array
 */

$this->title = 'Редактирование матча : ' . $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Матчи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->r_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
$this->params['crud-breadcrumbs'][] = ['label' => 'Проведение матча', 'url' => ['holding', 'id' => $model->id]];

$title = 'ID: ' . $model->id . ', ' . $model->r_name;
?>
<div class="sport-match-ar-update">
    <div class="d-flex flex-row justify-content-between">
        <h4 class="c-grey-900 mT-10 mB-10 mR-5"><?= Html::encode($title) ?></h4>
        <div class="d-flex flex-row">
            <div class="mR-5">
                <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Вы действительно хотите удалить матч?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
            <div class="mR-5">
                <?= $this->render('_change-status', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
    <?= $this->render('_form', [
        'model'     => $model,
        'sports'    => $sports
    ]) ?>
</div>
