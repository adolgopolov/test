<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model common\modules\game\models\SportMatchAR
 * @var $sports array
 */

$this->title = 'Добавление матча';
$this->params['breadcrumbs'][] = ['label' => 'Матчи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sport-match-ar-create">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'sports' => $sports
    ]) ?>

</div>
