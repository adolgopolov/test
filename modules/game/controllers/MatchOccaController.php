<?php

namespace common\modules\game\controllers;

use backend\controllers\AdminController;
use common\components\S3Kind;
use common\doings\sport\ChangeMatchOccasionDoing;
use common\modules\game\forms\SportLineupFM;
use common\modules\game\models\SportMatchAR;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SoccerFactAR;
use common\modules\sport\models\SportOccaAR;
use common\modules\sport\models\SportPositionAR;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class MatchOccaController extends AdminController
{
    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionFormGoal($id)
    {
        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($id);

        if ($this->saveOcca($id)) {
            return $this->redirect(['match/holding', 'id' => $id]);
        }

        $modelOcca = new SportOccaAR();

        $teams = $model->teams();
        $teamList = [
            $teams[0]->id => $teams[0]->getTitle(),
            $teams[1]->id => $teams[1]->getTitle(),
        ];

        return $this->renderAjax('_goal', [
            'model' => $model,
            'modelOcca' => $modelOcca,
            'teams' => $teamList,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionFormShotOnTarget($id)
    {
        /** @var SportMatchAR $modelMatch */
        $modelMatch = SportMatchAR::findOrFail($id);

        if ($this->saveOcca($id)) {
            return $this->redirect(['match/holding', 'id' => $id]);
        }

        $model = new SportOccaAR();

        $teams = $modelMatch->teams();
        $teamList = [
            $teams[0]->id => $teams[0]->getTitle(),
            $teams[1]->id => $teams[1]->getTitle(),
        ];

        return $this->renderAjax('_shot-on-target', [
            'model' => $model,
            'modelMatch' => $modelMatch,
            'teamList' => $teamList,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionFormFoul($id)
    {
        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($id);

        if ($this->saveOcca($id)) {
            return $this->redirect(['match/holding', 'id' => $id]);
        }

        $modelOcca = new SportOccaAR();

        $teams = $model->teams();
        $teamList = [
            $teams[0]->id => $teams[0]->getTitle(),
            $teams[1]->id => $teams[1]->getTitle(),
        ];

        return $this->renderAjax('_foul', [
            'model' => $model,
            'modelOcca' => $modelOcca,
            'teamList' => $teamList,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionFormTackleWon($id)
    {
        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($id);

        if ($this->saveOcca($id)) {
            return $this->redirect(['match/holding', 'id' => $id]);
        }

        $modelOcca = new SportOccaAR();

        $teams = $model->teams();
        $teamList = [
            $teams[0]->id => $teams[0]->getTitle(),
            $teams[1]->id => $teams[1]->getTitle(),
        ];

        return $this->renderAjax('_tackle-won', [
            'model' => $model,
            'modelOcca' => $modelOcca,
            'teamList' => $teamList,
        ]);
    }

    /**
     * @param $id
     * @param $factName
     * @param bool $goalkeepers
     * @return string
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionFormOther($id, $factName, $goalkeepers = false)
    {
        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($id);

        if ($this->saveOcca($id)) {
            return $this->redirect(['match/holding', 'id' => $id]);
        }

        $modelOcca = new SportOccaAR();

        $teams = $model->teams();
        $teamList = [
            $teams[0]->id => $teams[0]->getTitle(),
            $teams[1]->id => $teams[1]->getTitle(),
        ];

        return $this->renderAjax('_other', [
            'model' => $model,
            'modelOcca' => $modelOcca,
            'teamList' => $teamList,
            'factName' => $factName,
            'goalkeepers' => $goalkeepers,
        ]);
    }

    public function actionPresence($id)
    {
        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($id);
        $form = new SportLineupFM(['match' => $model]);

        if ($form->load(Yii::$app->request->post()) && $form->update()) {
            Yii::$app->session->addFlash('success', 'Спортсмены на поле успешно обновлены.');
        } else {
            Yii::$app->session->setFlash('error', $form->errors);
        }

        return $this->redirect(['match/holding', 'id' => $id]);
    }

    /**
     * @param int $match_id
     * @param int $id
     * @return array
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionAjaxScoredGoal($match_id, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($match_id);

        $out = [];
        $selected = '';
        $params = Yii::$app->request->post('depdrop_all_params', []);
        $teamId = $params['team-id'] ?? null;

        if ($teamId) {
            $team = $model->teamOwner;
            if ($model->team_guest === (int)$teamId) {
                $team = $model->teamGuest;
            }

            $out = $team->getSmen()
                ->asArray()
                ->select(['id', 'r_name AS name'])
                ->andWhere(['<>', 'sman_status', 'outall'])
                ->all();
        }

        if ($id) {
            $model = SmanAR::findOne($id);
            if ($model) {
                $selected = $model->id;
            }
        }

        if (!$selected && count($out) === 1) {
            $selected = $out[0]['id'];
        }

        return ['output' => $out, 'selected' => $selected];
    }

    /**
     * @param $match_id
     * @param null $id
     * @return array
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionAjaxGoalkeeper($match_id, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($match_id);

        $out = [];
        $selected = '';
        $params = Yii::$app->request->post('depdrop_all_params', []);
        $teamId = $params['team-id'] ?? null;

        if ($teamId) {
            $team = $model->teamGuest;
            if ($model->team_guest === (int)$teamId) {
                $team = $model->teamOwner;
            }

            // FIXME FAN-720 Множественность команд спортсмена и рефактор lastTeamMember
            /*
             $query = SmanAR::find()->select(['id', 'r_name AS name'])
                ->andWhere(['not', ['sman_status' => 'outall']]);

            $query->wherePosition(['<>' => SportPositionAR::POSITION_SOCCER_GOALKEEPER_ID]);

            if ($blocked_shot) {
                $query->wherePosition([
                    SportPositionAR::POSITION_SOCCER_FORWARD_ID,
                    SportPositionAR::POSITIOIN_SOCCER_BACK_ID,
                    SportPositionAR::POSITION_SOCCER_HALF_BACK_ID
                ]);
            }

            или заменить на
            $query->byFact($special_fact)

            префикс by означает что для запроса будет использована информация осутствующая в связанных с запросом таблицами, что-то хитрое
             */

            $lastTeamMembers = $team->getSmen()
                ->alias('sman')
                ->select(['team_member.sman_id', 'MAX(team_member.last_day) AS last_day'])
                ->joinWith(['teamMembers AS team_member'])
                ->andWhere(['not', ['sman.sman_status' => 'outall']])
                ->andWhere(['team_member.pos_id' => SportPositionAR::POSITION_SOCCER_GOALKEEPER_ID])
                ->groupBy(['team_member.sman_id'])
                ->column();

            $out = $team->getSmen()
                ->asArray()
                ->select(['id', 'r_name AS name'])
                ->andWhere(['not', ['sman_status' => 'outall']])
                ->andWhere(['id' => $lastTeamMembers])
                ->all();
        }

        if ($id) {
            $model = SmanAR::find()
                ->where(['id' => $id])
                ->andWhere(['not', ['sman_status' => 'outall']])
                ->one();

            if ($model) {
                $selected = $model->id;
            }
        }

//        if (!$selected && count($out) === 1) {
//            $selected = $out[0]['id'];
//        }

        return ['output' => $out, 'selected' => $selected];
    }

    /**
     * @param int $match_id
     * @param int $id
     * @return array
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionAjaxGoalpass($match_id, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($match_id);

        $out = [];
        $selected = '';
        $params = Yii::$app->request->post('depdrop_all_params', []);
        $teamId = $params['team-id'] ?? null;
        $smanId = $params['goal-sman-id'] ?? null;

        if ($teamId) {
            $team = $model->teamOwner;
            if ($model->team_guest === (int)$teamId) {
                $team = $model->teamGuest;
            }

            $query = $team->getSmen()
                ->asArray()
                ->select(['id', 'r_name AS name'])
                ->andWhere(['not', ['sman_status' => 'outall']]);

            if ($smanId) {
                $query->andWhere(['!=', 'id', $smanId]);
            }

            $out = $query->all();
        }

        if ($id) {
            $model = SmanAR::findOne($id);
            if ($model) {
                $selected = $model->id;
            }
        }

        if (!$selected && count($out) === 1) {
            $selected = $out[0]['id'];
        }

        return ['output' => $out, 'selected' => $selected];
    }

    /**
     * @param int $match_id
     * @param int $id
     * @param bool $blocked_shot FIXME магический спортивный факт? передать код или id! переименовать на $special_fact
     * @return array
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionAjaxNotGoalkeeper($match_id, $id = null, $blocked_shot = false)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($match_id);

        $out = [];
        $selected = '';
        $teamIds = Yii::$app->request->post('depdrop_parents');

        if (is_array($teamIds) && isset($teamIds[0])) {

            $team = $model->teamOwner;
            if ($model->team_owner === (int)$teamIds[0]) {
                $team = $model->teamGuest;
            }

            // FIXME FAN-720 Множественность команд спортсмена и рефактор lastTeamMember
            /*
             $query = SmanAR::find()->select(['id', 'r_name AS name'])
                ->andWhere(['not', ['sman_status' => 'outall']]);

            $query->wherePosition(['<>' => SportPositionAR::POSITION_SOCCER_GOALKEEPER_ID]);

            if ($blocked_shot) {
                $query->wherePosition([
                    SportPositionAR::POSITION_SOCCER_FORWARD_ID,
                    SportPositionAR::POSITIOIN_SOCCER_BACK_ID,
                    SportPositionAR::POSITION_SOCCER_HALF_BACK_ID
                ]);
            }

            или заменить на
            $query->byFact($special_fact)

            префикс by означает что для запроса будет использована информация осутствующая в связанных с запросом таблицами, что-то хитрое
             */

            $query = $team->getSmen()
                ->alias('sman')
                ->select(['team_member.sman_id', 'MAX(team_member.last_day) AS last_day'])
                ->joinWith(['teamMembers AS team_member'])
                ->andWhere(['not', ['sman.sman_status' => 'outall']])
                ->andWhere([
                    '!=', 'team_member.pos_id', SportPositionAR::POSITION_SOCCER_GOALKEEPER_ID,
                ]);

            if ($blocked_shot) {
                $query->andWhere(['team_member.pos_id' => [
                    SportPositionAR::POSITION_SOCCER_FORWARD_ID,
                    SportPositionAR::POSITIOIN_SOCCER_BACK_ID,
                    SportPositionAR::POSITION_SOCCER_HALF_BACK_ID
                ]]);
            }

            $lastTeamMembers = $query->groupBy(['team_member.sman_id'])
                ->column();

            $out = $team->getSmen()
                ->asArray()
                ->select(['id', 'r_name AS name'])
                ->andWhere(['not', ['sman_status' => 'outall']])
                ->andWhere(['id' => $lastTeamMembers])
                ->all();
        }

        if ($id) {
            $model = SmanAR::findOne($id);
            if ($model) {
                $selected = $model->id;
            }
        }

        if (!$selected && count($out) === 1) {
            $selected = $out[0]['id'];
        }

        return ['output' => $out, 'selected' => $selected];
    }


    /**
     * @param $id
     * @return bool
     * @throws InvalidConfigException
     */
    protected function saveOcca($id)
    {
        if (!Yii::$app->request->isPost) {
            return false;
        }

        $facts = Yii::$app->request->post('fact', []);
        $amounts = Yii::$app->request->post('amount', []);

        $factModels = SoccerFactAR::find()
            ->asArray()
            ->whereCode(array_keys($facts))
            ->andWhere(['ent_kind' => S3Kind::KIND_FACT])
            ->indexBy('ent_code')
            ->all();

        $models = [];

        foreach ($facts as $factCode => $smanId) {
            if (!$smanId) {
                continue;
            }

            $modelOcca = new SportOccaAR();

            if (isset($factModels[$factCode]) && $modelOcca->load(Yii::$app->request->post())) {
                $modelOcca->fact_id = $factModels[$factCode]['id'];
                $modelOcca->sman_id = $smanId;
                $modelOcca->match_id = $id;
                $modelOcca->occa_amount = $amounts[$factCode] ?? SportOccaAR::DEFAULT_OCCA_AMOUNT;

                $models[] = $modelOcca;
            }
        }

        if ($models) {
            $doing = ChangeMatchOccasionDoing::new();

            $result = $doing->doIt([
                'models' => $models,
            ]);

            if (!$result) {
                Yii::$app->session->setFlash('error', $doing->errors);
            }

            return true;
        }

        return false;
    }

    /**
     * Deletes an existing SportOccaAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws InvalidConfigException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $model->markDeleted();

        $doing = ChangeMatchOccasionDoing::new();

        $doing->doIt([
            'models' => [
                $model
            ],
        ]);

        return $this->goBack();
    }

    /**
     * Finds the SportOccaAR model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SportOccaAR the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SportOccaAR::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
