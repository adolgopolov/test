<?php

namespace common\modules\game\controllers;

use Yii;
use yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\components\S3Core;
use common\modules\game\forms\GameTourneyAS;
use common\modules\game\forms\GameTourneyFM;
use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameTouMatchAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\game\models\SportMatchAR;
use common\modules\game\models\TrophyTypeAR;
use common\modules\game\senders\GameTourneySender;
use common\modules\score\models\ScoreKindAR;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportLeagueAQ;
use common\modules\sport\models\SportLeagueAR;
use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use Throwable;

/**
 * TourneyController implements the CRUD actions for GameTourneyFM model.
 */
class TourneyController extends AdminController
{
    use CRUDTrait;

    protected $modelClass = GameTourneyFM::class;

    /**
     * Lists all GameTourneyFM models.
     *
     * @return mixed
     * @throws Exception
     */
    public function actionIndex()
    {
        $searchModel = new GameTourneyAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $leaList = SportLeagueAR::getLeaList();
        $statuses = S3Core::code2status();
        $sports = SportAR::getAsOptsList();

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'sports' => $sports,
            'leaList' => $leaList,
            'statuses' => $statuses,
        ]);
    }

    /**
     * Creates a new GameTourneyFM model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws ExitException
     * @throws InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new GameTourneyFM([
            'r_status' => S3Core::STATUS_DRAFT,
        ]);

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно добавлена');
            $model->sendModel(GameTourneySender::SCENARIO_CREATE);

            return $this->redirect(['update', 'id' => $model->id]);
        }

        $sports = SportAR::getAsOptsList();
        $statuses = S3Core::code2status();
        $maxGamerItems = GameTourneyFM::getMaxGamersItems();
        $trophyKinds = TrophyTypeAR::getTrophyList();
        $freeTrophyTypes = TrophyTypeAR::getFreeTrophyTypes();
        $autoPrizefundTrophyTypes = TrophyTypeAR::getAutoPrizefundTrophyTypes();

        $model->max_gamers = $maxGamerItems[array_key_first($maxGamerItems)];

        return $this->render('create', [
            'model' => $model,
            'sports' => $sports,
            'statuses' => $statuses,
            'maxGamerItems' => $maxGamerItems,
            'trophyKinds' => $trophyKinds,
            'freeTrophyTypes' => $freeTrophyTypes,
            'autoPrizefundTrophyTypes' => $autoPrizefundTrophyTypes,
        ]);
    }

    /**
     * @param $tourney
     * @param $match
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionHookMatch($tourney, $match)
    {
        /**
         * @var GameLineupAR $tourney
         * @var SportMatchAR $match
         */

        $tourney = GameTourneyAR::findOrFail($tourney);
        $match = SportMatchAR::findOrFail($match);

        $model = new GameTouMatchAR([
            'tou_id' => $tourney->id,
            'match_id' => $match->id,
        ]);


        if ($model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно добавлена');

            return $this->redirect(['view', 'id' => $tourney->id]);
        }

        return $this->redirect(['view', 'id' => $tourney->id]);
    }

    /**
     * @param $id
     * @return string
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        /** @var GameTourneyAR $model */
        $model = GameTourneyFM::findOrFail($id);
        $statuses = S3Core::code2status();

        $matchesProvider = new ActiveDataProvider([
            'query' => $model->getMatches(),
            'sort' => false,
        ]);

        $lineupsProvider = new ActiveDataProvider([
            'query' => $model->getLineups(),
            'sort' => false,
        ]);

        return $this->render('view', [
            'model' => $model,
            'statuses' => $statuses,
            'matchesProvider' => $matchesProvider,
            'lineupsProvider' => $lineupsProvider,
        ]);
    }

    /**
     * Updates an existing GameTourneyFM model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws ExitException
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var GameTourneyFM $model */
        $model = GameTourneyFM::findOrFail($id);
        $matchesProvider = new ActiveDataProvider([
            'query' => $model->getAvailableMatchesQuery(),
        ]);

        $this->performAjaxValidation($model);
        $clonedModel = clone $model;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
            $model->sendModel(GameTourneySender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);

            return $this->redirect(['update', 'id' => $id]);
        }

        $sports = SportAR::getAsOptsList();
        $statuses = S3Core::code2status();
        $maxGamerItems = GameTourneyFM::getMaxGamersItems();
        $trophyKinds = TrophyTypeAR::getTrophyList();
        $freeTrophyTypes = TrophyTypeAR::getFreeTrophyTypes();
        $autoPrizefundTrophyTypes = TrophyTypeAR::getAutoPrizefundTrophyTypes();

        $touMatchIds = $model->getMatches()
            ->select(['id'])
            ->column();

        return $this->render('update', [
            'model' => $model,
            'matchesProvider' => $matchesProvider,
            'sports' => $sports,
            'statuses' => $statuses,
            'maxGamerItems' => $maxGamerItems,
            'trophyKinds' => $trophyKinds,
            'freeTrophyTypes' => $freeTrophyTypes,
            'autoPrizefundTrophyTypes' => $autoPrizefundTrophyTypes,
            'touMatchIds' => $touMatchIds,
        ]);
    }

    /**
     * @return array
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionAjaxTouMatch()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $error_message = ['success' => false, 'message' => 'Произошла ошибка'];

        if (!Yii::$app->request->isAjax) {
            return $error_message;
        }

        $tou_id = Yii::$app->request->post('tou_id');
        $tourney = GameTourneyAR::findOne($tou_id);

        if (!$tourney) {
            return $error_message;
        }

        if (!($tourney->r_status & S3Core::STATUS_DRAFT)) {
            return ['success' => false, 'message' => 'Нельзя добавлять матчи в этот турнир'];
        }

        $match_id = Yii::$app->request->post('match_id');
        $checked = Yii::$app->request->post('checked');

        $touMatch = GameTouMatchAR::findOne([
            'tou_id' => $tou_id,
            'match_id' => $match_id,
        ]);

        if ($checked) {
            if ($touMatch) {
                return ['success' => false, 'message' => 'Этот матч уже добавлен в турнир'];
            }

            $model = new GameTouMatchAR();
            $model->tou_id = $tou_id;
            $model->match_id = $match_id;

            if ($model->save()) {
                return ['success' => true, 'message' => 'Матч успешно добавлен в турнир'];
            }

            return ['success' => false, 'message' => 'Произошла ошибка при добавлении матча в турнир'];
        }

        if (!$touMatch) {
            return ['success' => false, 'message' => 'Этот матч ещё не добавлялся в турнир'];
        }

        if ($touMatch->delete()) {
            return ['success' => true, 'message' => 'Матч успешно удалён из турнира'];
        }

        return ['success' => false, 'message' => 'Произошла ошибка при удалении матча из турнира'];
    }

    /**
     * @param int|null $id
     * @return array
     */
    public function actionAjaxLeagues(int $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        $selected = '';
        $sportIds = Yii::$app->request->post('depdrop_parents');

        if (is_array($sportIds) && isset($sportIds[0])) {
            $out = (new SportLeagueAQ(SportLeagueAR::class))
                ->whereSport($sportIds[0])
                ->select('id, r_name as name')
                ->asArray()
                ->orderBy('r_name ASC')
                ->all();
        }

        if ($id) {
            $model = GameTourneyFM::findOne($id);
            if ($model) {
                $selected = $model->lea_id;
            }
        }

        if (!$selected && count($out) === 1) {
            $selected = $out[0]['id'];
        }

        return ['output' => $out, 'selected' => $selected];
    }

    /**
     * @param null $id
     * @return array
     */
    public function actionAjaxScoreKind($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $output = [];
        $selected = '';
        $sportIds = Yii::$app->request->post('depdrop_parents');

        if ($sportIds) {
            $output = ScoreKindAR::find()
                ->andWhere(['sport_id' => $sportIds])
                ->andWhere(['&', 'r_status', S3Core::STATUS_LIVE])
                ->select('id, r_name as name')
                ->asArray()
                ->all();
        }

        if ($id) {
            $model = GameTourneyFM::findOne($id);

            if ($model) {
                $selected = $model->score_kind;
            }
        }

        return ['output' => $output, 'selected' => $selected];
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCalcTou($id)
    {
        /** @var GameTourneyFM $model */
        $model = GameTourneyFM::findOrFail($id);

        $opts = [];
        if (!S3Core::mScore()->calcTourney($model, $opts)) {
            foreach ($opts as $name => $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        } else {
            Yii::$app->session->addFlash('success', "Проведен турнирный скоринг турнира №$id");
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCalcRating($id)
    {
        /** @var GameTourneyFM $model */
        $model = GameTourneyFM::findOrFail($id);

        $opts = [];
        if (!S3Core::mScore()->calcExperience($model, $opts)) {
            Yii::$app->session->addFlash('error', $opts);
        } else {
            Yii::$app->session->addFlash('success', 'Успешно обновлены очки юзеров');
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCalcAward($id)
    {
        /** @var GameTourneyFM $model */
        $model = GameTourneyFM::findOrFail($id);

        $opts = [];

        if (S3Core::mScore()->calcTrophy($model, $opts)) {
            Yii::$app->session->addFlash('success', 'Расчитан призовой скоринг');
        } else {
            foreach ($opts as $name => $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCalcDynamic($id)
    {
        /** @var GameTourneyFM $model */
        $model = GameTourneyFM::findOrFail($id);

        if (!S3Core::mScore()->calcDynamic($model)) {
            Yii::$app->session->addFlash('error', 'Произошла ошибка');
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * @param $id
     * @return Response
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionCalcGameData($id)
    {
        /** @var GameTourneyFM $model */
        $model = GameTourneyFM::findOrFail($id);

        if ($model->calcGameData('save')) {
            Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
        } else {
            Yii::$app->session->addFlash('error', $model->errors);
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * @return array
     */
    public function actionAjaxBuyinCurrency()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var TrophyTypeAR $model */
        $model = TrophyTypeAR::findOne(Yii::$app->request->post('id'));

        if (!$model) {
            return [];
        }

        return (array)$model->buyin_currency;
    }
}
