<?php

namespace common\modules\game\controllers;

use Yii;
use common\modules\game\models\GameLayoutAR;
use yii\data\ActiveDataProvider;
use backend\controllers\AdminController;
use backend\controllers\DictCRUDTrait;
use common\components\S3Kind;
use yii\data\ArrayDataProvider;

/**
 * LayoutController implements the CRUD actions for GameLayoutAR model.
 */
class LayoutController extends AdminController
{
    use DictCRUDTrait;

    protected $modelClass = GameLayoutAR::class;

    /**
     * Lists all GameLayoutAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => GameLayoutAR::find()
                ->andWhere(['>', 'id', 0]),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * Creates a new GameLayoutAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function actionCreate()
    {
        $model = new GameLayoutAR();
        $model->ent_kind = S3Kind::KIND_LAYOUT;
        $model->cells = [
            ['id' => 1, 'rules' => ['positions' => []]],
            ['id' => 2, 'rules' => ['positions' => []]],
            ['id' => 3, 'rules' => ['positions' => []]],
            ['id' => 4, 'rules' => ['positions' => []]],
            ['id' => 5, 'rules' => ['positions' => []]],
            ['id' => 6, 'rules' => ['positions' => []]],
        ];

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно добавлена');

            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        /** @var GameLayoutAR $model */
        $model = GameLayoutAR::findOrFail($id);
        $cellsProvider = new ArrayDataProvider([
            'allModels' => $model->cells,
        ]);

        return $this->render('view', [
            'model' => $model,
            'cellsProvider' => $cellsProvider,
        ]);
    }

    /**
     * Updates an existing GameLayoutAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var GameLayoutAR $model */
        $model = GameLayoutAR::findOrFail($id);

        $this->performAjaxValidation($model);

        $model->load(Yii::$app->request->post());
        if ($model->cells && is_string($model->cells)) {
            $model->cells = json_decode($model['cells'], true);
        }

        if (Yii::$app->request->isPost && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно обновлена');

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionDelete()
    {
        // $model = GameLayoutAR::findOrFail($id);
        // if ($model) {
        //     Yii::$app->session->addFlash('success', 'Запись успешно удалена');
        //     $model->delete();
        // }

        return $this->redirect(['index']);
    }
}
