<?php

namespace common\modules\game\controllers;

use Yii;
use common\modules\game\models\TrophyTypeAR;
use yii\data\ActiveDataProvider;
use backend\controllers\AdminController;
use common\modules\sport\models\SportAR;
use common\modules\game\forms\TrophyTypeAS;
use backend\controllers\CRUDTrait;
use yii\web\BadRequestHttpException;

/**
 * TrophyTypeController implements the CRUD actions for TrophyTypeAR model.
 */
class TrophyTypeController extends AdminController
{
    use CRUDTrait;

    /**
     * Lists all TrophyTypeAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider(['query' => TrophyTypeAS::find()]);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * Displays a single TrophyTypeAR model.
     *
     * @param integer $id
     * @param integer $ent_kind
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $ent_kind)
    {
        $model = TrophyTypeAR::findOrFail($id);

        return $this->render('view', compact('model'));
    }

    /**
     * Creates a new TrophyTypeAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        throw new BadRequestHttpException('Cannot create new Trophy Type');

        // $model = new TrophyTypeAR();
        // $sports = SportAR::getAsOptsList();

        // $this->performAjaxValidation($model);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     \Yii::$app->session->addFlash('success', 'Запись успешно добавлена');
        //     return $this->redirect(['index']);
        // }

        // return $this->render('create', compact('model', 'sports'));
    }

    /**
     * Updates an existing TrophyTypeAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @param integer $ent_kind
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = TrophyTypeAR::findOrFail($id);
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('model', 'sports'));
    }

    /**
     * Deletes an existing TrophyTypeAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @param integer $ent_kind
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = TrophyTypeAR::findOrFail($id);
        if ($model) {
            if (!S3Core::isAllowedDelete($model)) {
                Yii::$app->session->addFlash('info', 'Удаление запрещено');
                return $this->redirect(['index']);
            }

            Yii::$app->session->addFlash('success', 'Запись успешно удалена');
            $model->delete();
        }

        return $this->redirect(['index']);
    }
}
