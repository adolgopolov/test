<?php

namespace common\modules\game\controllers;

use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\doings\game\ChangeLineUpDoing;
use common\doings\game\RegisterLineUpDoing;
use common\modules\game\forms\GameLineupAS;
use common\modules\game\forms\GameLineupCellFM;
use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameLineupCellAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\game\senders\GameLineupCellSender;
use common\modules\game\senders\GameLineupSender;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\user\models\UserAR;
use Yii;
use yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * LineupController implements the CRUD actions for GameLineupAR model.
 */
class LineupController extends AdminController
{
    use CRUDTrait;

    protected $modelClass = GameLineupAR::class;

    /**
     * Lists all GameLineupAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember();

        $searchModel = new GameLineupAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $users = UserAR::getList();
        $tourneys = GameTourneyAR::getList();

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users' => $users,
            'tourneys' => $tourneys,
        ]);
    }

    /**
     * @return string|Response
     * @throws ExitException
     * @throws InvalidConfigException
     */
    public function actionCreate()
    {
        /** @var GameLineupAR $model */
        $model = new $this->modelClass;

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            $doing = ChangeLineUpDoing::new();

            $result = $doing->doIt([
                'model' => $model,
            ]);

            if ($result) {
                Yii::$app->session->addFlash('success', 'Запись успешно добавлена');

                return $this->redirect(['index']);
            }

            Yii::$app->session->addFlash('error', $doing->errors);
        }

        $users = UserAR::getList();
        $tourneys = GameTourneyAR::getList();

        return $this->render('create', [
            'model' => $model,
            'users' => $users,
            'tourneys' => $tourneys,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        /** @var GameLineupAR $model */
        $model = GameLineupAR::findOrFail($id);

        $dataProvider = new ActiveDataProvider([
            'query' => $model->getLineUpCells()
                ->with(['sman']),
            'sort' => [
                'defaultOrder' => [
                    'cell_id' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing AR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ExitException
     * @throws InvalidConfigException
     */
    public function actionUpdate($id)
    {
        /** @var GameLineupAR $model */
        $model = GameLineupAR::findOrFail($id);

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            $doing = ChangeLineUpDoing::new();

            $result = $doing->doIt([
                'model' => $model,
            ]);

            if ($result) {
                Yii::$app->session->addFlash('success', 'Запись успешно обновлена');

                return $this->redirect(['update', 'id' => $id]);
            }

            Yii::$app->session->addFlash('error', $doing->errors);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $model->getLineUpCells()
                ->with('sman'),
            'sort' => false,
        ]);

        $modelCells = new GameLineupCellFM();

        if ($modelCells->load(Yii::$app->request->post())) {
            if ($modelCells->saveCells($model->id)) {
                Yii::$app->session->addFlash('success', 'Ячейки успешно изменены.');

                return $this->redirect(['update', 'id' => $id]);
            }

            Yii::$app->session->addFlash('error', $modelCells->errors);
        }

        $users = UserAR::getList();
        $tourneys = GameTourneyAR::getList();

        return $this->render('update', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'modelCells' => $modelCells,
            'users' => $users,
            'tourneys' => $tourneys,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionDelete($id)
    {
        /** @var GameLineupAR $model */
        $model = GameLineupAR::findOrFail($id);

        if ($model) {
            $model->markDeleted();
            $clonedModel = clone $model;

            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Запись успешно удалена');
                $model->sendModel(GameLineupSender::SCENARIO_DEFAULT, ['clonedModel' => $clonedModel]);
            } else {
                $arrErrors = $model->getErrorSummary(true);
                Yii::$app->session->addFlash('error', empty($arrErrors) ? 'Запись НЕ удалена' : $arrErrors);
            }
        }

        return $this->goBack();
    }

    /**
     * View single cell in grid (details)
     *
     * @param int $lineup
     * @param int $cell
     * @return mixed
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionCell(int $lineup, int $cell)
    {
        $model = GameLineupCellAR::findOne(['lineup_id' => $lineup, 'cell_id' => $cell]);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post())) {
            $clonedModel = clone $model;

            if ($model->save()) {
                $model->sendModel(GameLineupCellSender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);

                return $this->redirect(['update', 'id' => $model->lineup_id, '#' => 'w3-tab1']);
            }
        }

        $smen = GameLineupCellAR::getSmenTourney($model);

        return $this->renderPartial('_cell', [
            'model' => $model,
            'smen' => $smen,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws ExitException
     * @throws InvalidConfigException
     */
    public function actionLineupSignUp($id)
    {
        $model = GameLineupAR::findOrFail($id);

        $this->performAjaxValidation($model);

        if ($model) {
            $doing = RegisterLineUpDoing::new();

            $result = $doing->doIt([
                'model' => $model,
            ]);

            if (!$result) {
                Yii::$app->session->addFlash('error', $doing->getErrors());
            }

        }

        return $this->redirect(['index']);
    }
}
