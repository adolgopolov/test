<?php

namespace common\modules\game\controllers;

use Yii;
use Yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\MethodNotAllowedHttpException;
use Yii\web\NotFoundHttpException;
use yii\web\Response;
use common\components\S3Core;
use common\doings\sport\CancelMatchDoing;
use common\doings\sport\ChangeMatchOccasionDoing;
use common\doings\sport\FinisMatchDoing;
use common\doings\sport\StartMatchDoing;
use common\models\Many2Many;
use common\modules\game\forms\SportLineupFM;
use common\modules\game\forms\SportMatchAS;
use common\modules\game\models\SportMatchAR;
use common\modules\game\senders\SportMatchSender;
use common\modules\sount\models\SouMatchAR;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SportOccaAS;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportFactAR;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportOccaAR;
use common\modules\sport\models\SportPositionAR;
use common\modules\sport\models\SportTeamAR;
use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use Exception;
use Throwable;

/**
 * MatchController implements the CRUD actions for SportMatchAR model.
 */
class MatchController extends AdminController
{
    use CRUDTrait;

    protected $modelClass = SportMatchAR::class;

    /**
     * Lists all SportMatchAR models.
     *
     * @return mixed
     * @throws Exception
     */
    public function actionIndex()
    {
        Yii::$app->user->setReturnUrl(Url::to());

        $searchModel = new SportMatchAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $leaList = SportLeagueAR::getLeaList();
        $statuses = S3Core::code2status();
        $sports = SportAR::getAsOptsList();

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'leaList' => $leaList,
            'statuses' => $statuses,
            'sports' => $sports,
        ]);
    }

    /**
     * Creates a new SportMatchAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws ExitException
     * @throws InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new SportMatchAR(['autostart' => true]);
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->r_name) {
                $model->r_name = $model->generateMatchName();
            }

            $model->autostart = !$model->autostart ? 'disable' : null;

            if ($model->save()) {
                Yii::$app->session->addFlash('Матч успешно создан.');
                $model->sendModel(SportMatchSender::SCENARIO_CREATE);

                return $this->redirect(['index']);
            }

            Yii::$app->session->setFlash('error', $model->errors);
        }

        return $this->render('create', [
            'model' => $model,
            'sports' => $sports,
        ]);
    }

    /**
     * Updates an existing SportMatchAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws ExitException
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionUpdate($id)
    {
        Url::remember();

        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($id);
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            $clonedModel = clone $model;
            $model->autostart = !$model->autostart ? 'disable' : null;

            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Матч успешно изменён.');
                $model->sendModel(SportMatchSender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);

                return $this->redirect(['view', 'id' => $model->id]);
            }

            Yii::$app->session->addFlash('error', $model->errors);
        }

        $model->autostart = $model->autostart === 'disable' ? false : true;

        return $this->render('update', [
            'model' => $model,
            'sports' => $sports,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        Url::remember();

        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($id);

        $tourneyDataProvider = new ActiveDataProvider([
            'query' => $model->getTourneys(),
            'pagination' => false,
        ]);

        return $this->render('view', [
            'model' => $model,
            'tourneyDataProvider' => $tourneyDataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionHolding($id)
    {
        Url::remember();

        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($id);

        $sportFacts = SportFactAR::getSportFacts($model->sport_id);
        $smen = $model->getSmen();
        $modelOcca = new SportOccaAR();

        $searchModel = new SportOccaAS();
        $occaDataProvider = $searchModel->search(Yii::$app->request->queryParams, $model->id);

        if ($model->sport_id !== SportAR::SOCCER) {
            $data = [];

            $smenIds = ArrayHelper::getColumn($smen, 'id');
            $sportFactsIds = ArrayHelper::getColumn($sportFacts, 'id');

            $occaModels = SportOccaAR::find()
                ->andWhere([
                    'fact_id' => $sportFactsIds,
                    'sman_id' => $smenIds,
                    'match_id' => $model->id,
                ])
                ->all();

            foreach ($smen as $sman) {
                //FIXME переделать на выборку членства из матча
                $team = $sman->lastTeamMember->team;
                $row = [
                    'sman' => $sman,
                    'team' => $team ? $team->getTitle() : null,
                ];

                foreach ($sportFacts as $fact) {
                    $key = 'fact' . $fact->id;

                    $row[$key] = null;

                    /** @var SportOccaAR[] $occaModels */
                    foreach ($occaModels as $occaModel) {
                        if ($occaModel->sman_id === $sman->id && $occaModel->fact_id === $fact->id) {
                            $row[$key] = $occaModel;

                            break;
                        }
                    }
                }

                $data[] = $row;
            }

            $arr = [
                'sman.r_name',
                'team',
            ];

            foreach ($sportFacts as $fact) {
                $arr[] = 'fact' . $fact->id;
            }

            $occaDataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => false,
                'sort' => [
                    'attributes' => $arr,
                ]
            ]);
        }

        $lineupsForm = new SportLineupFM(['match' => $model]);
        $positions = SportPositionAR::find()
            ->andWhere(['ent_value' => $model->sport_id])
            // FIXME -> whereCase()->whereSport()->orderByCase()
            ->orderByCase('id', SportPositionAR::getPositionCases($model->sport_id))
            ->all();

        return $this->render('holding', [
            'model' => $model,
            'occaDataProvider' => $occaDataProvider,
            'sportFacts' => $sportFacts,
            'smen' => $smen,
            'modelOcca' => $modelOcca,
            'lineupsForm' => $lineupsForm,
            'positions' => $positions,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws ExitException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionOccaImport($id)
    {
        throw new \yii\base\Exception('Не реализовано!!!');
        $model = SportMatchAR::findOrFail($id);

        $this->performAjaxValidation($model);
        $souMatch = SouMatchAR::find()->imported()
            ->andWhere(['match_our_id' => $id])->one();
        $manager = S3Core::mSportSou()->importManager;

        if ($souMatch && $manager->loadMatchOcca($souMatch->id)) {
            Yii::$app->session->addFlash('success', 'Импорт произошёл успешно.');
        } else {
            Yii::$app->session->addFlash('error', 'Произошла ошибка при импорте.');
        }

        return $this->goBack();
    }

    /**
     * @param $id
     * @return Response
     * @throws ExitException
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionStartMatch($id)
    {
        $model = SportMatchAR::findOrFail($id);

        $this->performAjaxValidation($model);

        if ($model) {
            $doing = StartMatchDoing::new();

            $doing->doIt([
                'model_id' => $id,
            ]);
        }

        return $this->goBack();
    }

    /**
     * @param $id
     * @return Response
     * @throws ExitException
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionFinisMatch($id)
    {
        $model = SportMatchAR::findOrFail($id);

        $this->performAjaxValidation($model);

        if ($model) {
            $doing = FinisMatchDoing::new();

            $doing->doIt([
                'model_id' => $id,
            ]);
        }

        return $this->goBack();
    }

    /**
     * @param $id
     * @return Response
     * @throws ExitException
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionNextStatus($id)
    {
        /** @var SportMatchAR $model */
        $model = SportMatchAR::findOrFail($id);

        $this->performAjaxValidation($model);

        if ($model) {
            $model->r_status = S3Core::nextStatus($model->r_status);
            $clonedModel = clone $model;

            if ($model->save()) {
                $model->sendModel(SportMatchSender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);
            }
        }

        return $this->goBack();
    }

    /**
     * @param int|null $id
     * @return array
     */
    public function actionAjaxLeagues(int $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        $selected = '';
        $sportIds = Yii::$app->request->post('depdrop_parents');

        if (is_array($sportIds) && isset($sportIds[0])) {
            $out = SportLeagueAR::find()
                ->whereSport($sportIds[0])
                ->select('id, r_name as name')
                ->asArray()
                ->orderBy('r_name ASC')
                ->all();
        }

        if ($id) {
            $model = SportMatchAR::findOne($id);
            if ($model) {
                $selected = $model->lea_id;
            }
        }

        if (!$selected && count($out) === 1) {
            $selected = $out[0]['id'];
        }

        return ['output' => $out, 'selected' => $selected];
    }

    /**
     * @param int|null $id
     * @param string|null $attr
     * @return array
     */
    public function actionAjaxTeams(int $id = null, string $attr = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        $selected = '';
        $leaIds = Yii::$app->request->post('depdrop_parents');

        if (is_array($leaIds) && isset($leaIds[0])) {
            $teamsIds = Many2Many::find()
                ->asArray()
                ->distinct()
                ->select(['id_left AS id'])
                ->andWhere([
                    'id_right' => $leaIds[0],
                    'rel_kind' => Many2Many::TYPE_TEAM_LEAGUE,
                ])
                ->column();

            $out = SportTeamAR::find()
                ->andWhere(['id' => $teamsIds])
                ->select('id, r_name as name')
                ->asArray()
                ->orderBy('r_name ASC')
                ->all();
        }

        if ($id && $attr) {
            $model = SportMatchAR::findOne($id);
            if ($model && isset($model->$attr)) {
                $selected = $model->$attr;
            }
        }

        if (!$selected && count($out) === 1) {
            $selected = $out[0]['id'];
        }

        return ['output' => $out, 'selected' => $selected];
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCalcSport($id)
    {
        $model = SportMatchAR::findOrFail($id);
        $opts = [];

        if (S3Core::mScore()->calcMatch($model, $opts)) {
            Yii::$app->session->addFlash('success', "Проведен спортивный скоринг матча №$id");
        } else {
            foreach ($opts as $name => $error) {
                Yii::$app->session->addFlash('error', $error);
            }
        }

        return $this->goBack();
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function actionCreateOcca()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new SportOccaAR();

        $error = [];

        if ($model->load(Yii::$app->request->post())) {
            $doing = ChangeMatchOccasionDoing::new();

            $result = $doing->doIt([
                'model' => $model,
            ]);

            if ($result) {
                return ['success' => true, 'message' => 'Спортивное событие успешно добавлено'];
            }

            $error = $doing->errors;
        }

        return ['success' => false, 'message' => $error];
    }

    /**
     * для summary мод (хоккей)
     * @return array
     * @throws InvalidConfigException
     */
    public function actionCreateOccaMany()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post('SportOccaAR', []);

            $facts = (array)$data['fact_id'];
            $matchId = $data['match_id'];
            $smanId = $data['sman_id'];

            $models[$smanId] = $facts;

            $doing = ChangeMatchOccasionDoing::new([
                'mode' => ChangeMatchOccasionDoing::MODE_SUMMARY,
            ]);

            $result = $doing->doIt([
                'models' => $models,
                'smanId' => $smanId,
                'matchId' => $matchId,
            ]);

            if ($result) {
                return ['success' => true, 'message' => 'Спортивные события успешно добавлены'];
            }

            return ['success' => false, 'message' => $doing->errors];
        }

        return ['success' => false, 'message' => 'Действие доступно только по методу POST'];
    }

    /**
     * @param $ids
     * @return bool|array
     * @throws InvalidConfigException
     */
    public function gridChangeStatus($ids)
    {
        /** @var SportMatchAR[] $waitingModels */
        $waitingModels = SportMatchAR::find()
            ->andWhere(['id' => $ids])
            ->isStatuses(S3Core::STATUS_WAITING)
            ->all();

        /** @var SportMatchAR[] $lifeModels */
        $lifeModels = SportMatchAR::find()
            ->andWhere(['id' => $ids])
            ->isStatuses(S3Core::STATUS_LIVE)
            ->all();

        /** @var SportMatchAR[] $otherModels */
        $otherModels = SportMatchAR::find()
            ->andWhere(['id' => $ids])
            ->isntStatuses([S3Core::STATUS_WAITING, S3Core::STATUS_LIVE])
            ->all();

        $errors = [];

        if ($waitingModels) {
            $doing = StartMatchDoing::new();

            $result = $doing->doIt([
                'models' => $waitingModels,
            ]);

            if (!$result) {
                $errors['waiting'] = $doing->errors;
            }
        }

        if ($lifeModels) {
            foreach ($lifeModels as $key => $model) {
                /** @var SportMatchAR $model */
                if ($model->getOccasions()->existFinisOcca($model->id)) {
                    $otherModels[] = $model;
                    unset($lifeModels[$key]);
                }
            }

            if ($lifeModels) {
                $doing = FinisMatchDoing::new();

                $result = $doing->doIt([
                    'models' => $lifeModels,
                ]);

                if (!$result) {
                    $errors['life'] = $doing->errors;
                }
            }
        }

        foreach ($otherModels as $model) {
            $model->r_status = S3Core::nextStatus($model->r_status);
            $clonedModel = clone $model;

            if ($model->save()) {
                $model->sendModel(SportMatchSender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);
            } else {
                $errors['other'][] = $model->errors;
            }
        }

        return $errors ?: true;
    }

    /**
     * @param array $ids
     * @return bool|array
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function gridDelete($ids)
    {
        // TODO Возможно стоит разделить списки матчей, для возможности массового удаления
        // TODO А нужна ли эта функция на матчи со статусом выще draft?

        $otherModels = SportMatchAR::find()
            ->andWhere(['id' => $ids])
            ->isntStatuses([
                S3Core::STATUS_COMPLETED,
                S3Core::STATUS_CLOSED,
                S3Core::STATUS_CANCELED,
            ])
            ->all();

        $errors = [];

        foreach ($otherModels as $model) {
            if (!$model->delete()) {
                $errors['other'][] = $model->errors;
            }
        }

        return $errors ?: true;
    }

    /**
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionGridActions()
    {
        if (Yii::$app->request->isPost) {
            $_post = Yii::$app->request->post();

            $ids = $_post['selection'] ?? [];

            $response = null;

            if (isset($_post['next-status'])) {
                $this->gridChangeStatus($ids);

            } elseif (isset($_post['delete'])) {
                $result = $this->gridDelete($ids);
                if ($result === true) {
                    Yii::$app->session->addFlash('success', 'Массовое удаление произошло успешно.');
                } else {
                    Yii::$app->session->addFlash('error', 'Произошла ошибка при удалении');
                }
            }
        }

        return $this->goBack();
    }

    /**
     * @param int $id
     * @return array
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionUpdateTotal($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isPost) {
            /** @var SportMatchAR $model */
            $model = SportMatchAR::findOrFail($id);

            if ($model->load(Yii::$app->request->post())) {
                $clonedModel = clone $model;

                if ($model->save()) {
                    $model->sendModel(SportMatchSender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);

                    return ['success' => true, 'message' => 'Счет матча успешно обновлен'];
                }
            }

            return ['success' => false, 'message' => $model->errors];
        }

        return ['success' => false, 'message' => 'Действие доступно только по методу POST'];
    }

    /**
     * @param int $id
     * @return Response
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionCloseMatch($id)
    {
        if (Yii::$app->request->isPost) {
            /** @var SportMatchAR $model */
            $model = SportMatchAR::findOrFail($id);

            $model->r_status = S3Core::STATUS_CLOSED;
            $clonedModel = clone $model;

            if ($model->save()) {
                $model->sendModel(SportMatchSender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);
            } // TODO Изменить на CloseMatchDoing

//            $doing = CloseMatchDoing::new();
//
//            $result = $doing->doIt([
//                'model' => $model,
//            ]);
//
//            if (!$result) {
//                // $doing->errors;
//            }

            return $this->goBack();
        }

        throw new MethodNotAllowedHttpException('Действие доступно только по методу POST');
    }

    /**
     * @param int $id
     * @return Response
     * @throws MethodNotAllowedHttpException
     * @throws InvalidConfigException
     */
    public function actionCancelMatch($id)
    {
        if (Yii::$app->request->isPost) {
            $doing = CancelMatchDoing::new();

            $result = $doing->doIt([
                'model_id' => $id,
            ]);

            if ($result) {
                Yii::$app->session->addFlash('success', 'Матч был успешно отменен');
            } else {
                Yii::$app->session->addFlash('error', $doing->errors);
            }

            return $this->goBack();
        }

        throw new MethodNotAllowedHttpException('Действие доступно только по методу POST');
    }

    /**
     * @param int $id
     * @return Response
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionChangeStatus($id)
    {
        if (Yii::$app->request->isPost) {
            /** @var SportMatchAR $model */
            $model = SportMatchAR::findOrFail($id);
            $model->setScenarioChangeStatus();
            $manager = S3Core::mGame()->gameManager();
            if ($model->load(Yii::$app->request->post())) {
                if (!$model->r_status) {
                    throw new Exception('Status field is required.');
                }

                $clonedModel = clone $model;

                if ($manager->changeMatch($model)) {
                    Yii::$app->session->addFlash('Матч успешно изменен');
                    $manager->getMatch()->sendModel(SportMatchSender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);
                } else {
                    Yii::$app->session->setFlash('error', $model->errors);
                }
                // TODO Изменить на ChangeMatchStatusDoing

//                $doing = ChangeMatchStatusDoing::new();
//
//                $result = $doing->doIt([
//                    'model' => $model,
//                ]);
//
//                if (!$result) {
//                    $model->addErrors($doing->errors);
//
//                    print_r($doing->errors);die;
//
//                    throw new Exception('Cannot change match status.');
//                }
            }

            return $this->goBack();
        }

        throw new MethodNotAllowedHttpException('Действие доступно только по методу POST');
    }
}
