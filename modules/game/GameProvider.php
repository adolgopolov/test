<?php

namespace common\modules\game;


use common\components\S3Component;
use common\modules\game\models\GameTourneyAR;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportMemberAR;

/**
 * FIXME убрать, устарело
 *
 * User: andrey
 * Date: 12.04.19 Time: 10:01
 * @brief класс для реализации сложных выборок турниров и составов
 */
class GameProvider extends S3Component
{

    /**
     * @brief Список турниров
     * @param callable $funcQuery
     * @param type $opts
     * @return array|null
     */
    public function listTourney(callable $funcQuery = null, $opts = null): ?array
    {
        $aq = GameTourneyAR::find();
        if (!empty($funcQuery)) $aq = $funcQuery($aq);

        return $aq->all();
    }


    public function smenTourney($tourney, callable $funcQuery = null, $opts = null): ?array
    {
        $tourney = is_object($tourney) ? $tourney : GameTourneyAR::findOne($tourney);
        $teams = [];
        foreach ($tourney->matches as $eachMatchAR) {
            $teams[$eachMatchAR->team_guest] = $eachMatchAR->team_guest;
            $teams[$eachMatchAR->team_owner] = $eachMatchAR->team_owner;
        }
        $aq = SmanAR::find()
            ->alias('s')
            ->innerJoin(SportMemberAR::tableName() . ' tm', 's.id = tm.sman_id')
            ->andWhere(['and',
                ['tm.team_id' => array_keys($teams)],
                ['<=', 'tm.first_day', $tourney->start_date],
                ['>=', 'tm.last_day', $tourney->start_date],
            ])->active();

        if (!empty($funcQuery)) {
            $aq = $funcQuery($aq);
        }

        return $aq->all();
    }
}
