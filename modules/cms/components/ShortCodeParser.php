<?php

namespace common\modules\cms\components;

use Yii;

class ShortCodeParser
{
    private static $post;
    private static $attribute;

    public static function getAvailableShortCodes()
    {
        return [
            'sman'  => [
                'class' => 'common\modules\cms\shortcodes\Sman'
            ]
        ];
    }

    public static function parse($post, $attribute)
    {
        static::$post = $post;
        static::$attribute = $attribute;

        return preg_replace_callback(
            '/\[[a-z]+[\ a-z\=0-9]+\]/',
            'self::buildShortCode',
            $post->$attribute
        );
    }

    private static function buildShortCode($matches)
    {
        $shortCodes = static::getAvailableShortCodes();
        $code = $matches[0];

        $code = str_replace(['[', ']'], '', $code);
        $codeParts = explode(' ', $code);

        if (isset($codeParts[0]) && isset($shortCodes[$codeParts[0]])) {
            $codeName = array_shift($codeParts);
            $className = $shortCodes[$codeName]['class'];
            $params = [];

            if (count($codeParts)) {
                foreach ($codeParts as $param) {
                    $paramParts = explode('=', $param);

                    if (count($paramParts) === 2) {
                        $params[$paramParts[0]] = $paramParts[1];
                    }
                }
            }

            $shortCode = new $className($params);

            if (!$shortCode->model) {
                static::$post->addError(static::$attribute . '_markdown', 'Запрашиваемая модель ' . $shortCode::MODEL_CLASS .' (id='. $shortCode->id .') не найдена.');
            } else {
                static::register($shortCode);

                return $shortCode->run();
            }

        }

        return $matches[0];
    }

    public static function register($shortCode)
    {
        if (static::$post) {
            if (!isset(static::$post->shortcodes)) {
                static::$post->shortcodes = [];
            }

            static::$post->shortcodes = array_merge(static::$post->shortcodes, [$shortCode::NAME => [$shortCode->id]]);
        }
    }
}