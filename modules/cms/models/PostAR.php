<?php

namespace common\modules\cms\models;

use Yii;
use yii\base\InvalidConfigException;
use common\behaviors\ImageUploadBehavior;
use common\components\FindOrFailTrait;
use common\components\pusher\interfaces\SenderModelInterface;
use common\components\pusher\senders\AbstractSender;
use common\components\S3Kind;
use common\extensions\ExtInfoAR;
use common\modules\cms\senders\PostSender;
use common\traits\FieldTrait;
use common\validators\JsonValidator;

/**
 * This is the model class for table "ss_cms_post".
 *
 * @property int $id
 * @property string $r_name
 * @property string $content
 * @property string $content_markdown
 * @property string $announce
 * @property string $announce_markdown
 * @property string $r_icon
 * @property int $r_status
 * @property string $publish_at
 * @property string $updated_at
 * @property string $created_at
 *
 * @property array $shortcodes
 */
class PostAR extends ExtInfoAR implements SenderModelInterface
{
    use FindOrFailTrait, FieldTrait {
        FieldTrait::urlImage as parentUrlImage;
    }

    public $sports;
    public $leagues;

    public $image_file;

    public const STATUS_DRAFT = 2;
    public const STATUS_WAITING = 4;
    public const STATUS_PUBLISHED = 8;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => ImageUploadBehavior::class,
                'kind' => self::kind(),
                'model' => $this,
                'attribute' => 'image_file',
                'image_name' => 'r_icon',
            ],
        ];
    }

    /**
     * @return int
     */
    public static function kind()
    {
        return S3Kind::KIND_NEWS;
    }

    /**
     * @param string $scenario
     * @param array $data
     * @throws InvalidConfigException
     */
    public function sendModel($scenario = AbstractSender::SCENARIO_DEFAULT, $data = [])
    {
        PostSender::sendModel($this, $scenario, $data);
    }

    /**
     * @param string $st
     * @return int
     */
    public static function status2code(string $st): int
    {
        static $cache = [
            'waiting' => self::STATUS_WAITING,
            'published' => self::STATUS_PUBLISHED,
            'draft' => self::STATUS_DRAFT,
        ];

        return $cache[$st] ?? 0;
    }

    /**
     *
     * @param string|int $st
     * @param bool $desc
     * @return string|int|array
     */
    public static function code2status($st = 'all', $desc = false)
    {
        static $cache = [
            self::STATUS_DRAFT => 'draft',
            self::STATUS_WAITING => 'waiting',
            self::STATUS_PUBLISHED => 'published',
        ];

        static $cacheDesc = [
            self::STATUS_DRAFT => 'Черновик',
            self::STATUS_WAITING => 'На утверждении',
            self::STATUS_PUBLISHED => 'Опубликовано',
        ];

        $currCache = $desc ? $cacheDesc : $cache;

        if ($st === 'all') {
            return $currCache;
        }

        return $currCache[$st] ?? 0;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cms_post}}';
    }

    public function jsonFields(): array
    {
        return ['shortcodes'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_name', 'content', 'content_markdown', 'announce', 'announce_markdown'], 'required'],
            [['r_name'], 'string', 'max' => 255],
            [['content', 'content_markdown', 'announce', 'announce_markdown'], 'string'],
            [['publish_at'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['r_status'], 'in', 'range' => [static::STATUS_DRAFT, static::STATUS_WAITING, static::STATUS_PUBLISHED]],
            [['created_at', 'updated_at'], 'safe'],
            [['ext_info'], JsonValidator::class],
            [['r_icon'], 'string', 'max' => 90],
            [['image_file'], 'file', 'extensions' => Yii::$app->images->postImage->extensions],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_name' => 'Заголовок',
            'sports' => 'Виды спорта',
            'leagues' => 'Лиги',
            'content' => 'Основной текст',
            'content_markdown' => 'Основной текст',
            'announce' => 'Текст анонса',
            'announce_markdown' => 'Текст анонса',
            'publish_at' => 'Дата публикации',
            'r_status' => 'Статус публикации',
            'r_icon' => 'Фото',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PostAQ the active query used by this AR class.
     */
    public static function find()
    {
        return (new PostAQ(static::class))->orderBy(['updated_at' => SORT_DESC]);
    }

    /**
     * @param string $attr
     * @return string|null
     */
    public function urlImage($attr = 'r_icon'): ?string
    {
        if (!$this->hasProperty($attr)) {
            return null;
        }

        if (empty($this->{$attr})) {
            return null;
        }

        return $this->parentUrlImage($attr);
    }
}
