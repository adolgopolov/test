<?php

namespace common\modules\cms\models;

use common\extensions\ExtAQ;

/**
 * This is the ActiveQuery class for [[PostAR]].
 *
 * @see PostAR
 */
class PostAQ extends ExtAQ
{
    public function published() : ExtAQ
    {
        return $this->andWhere(['>=', 'r_status', PostAR::STATUS_PUBLISHED]);
    }
}
