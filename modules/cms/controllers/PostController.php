<?php

namespace common\modules\cms\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use common\models\Many2Many;
use common\modules\cms\forms\PostFM;
use common\modules\cms\senders\PostSender;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportLeagueAR;
use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;

/**
 * PostController implements the CRUD actions for PostFM model.
 */
class PostController extends AdminController
{
    use CRUDTrait;

    protected $modelClass = PostFM::class;

    /**
     * Lists all PostFM models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PostFM::find()
                ->andWhere(['>', 'id', 999]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|Response
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new PostFM();
        $this->performAjaxValidation($model);
        $sports = SportAR::getAsOptsList();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Запись успешно добавлена');
                $model->sendModel(PostSender::SCENARIO_DEFAULT);

                return $this->redirect(['index']);
            }

            Yii::$app->session->setFlash('error', $model->errors);
        }

        return $this->render('create', [
            'model' => $model,
            'sports' => $sports,
        ]);
    }

    /**
     * @return array
     */
    public function actionAjaxLeagues()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $output = [];
        $selected = null;

        $depdropParams = Yii::$app->request->post('depdrop_all_params', []);
        $sportId = $depdropParams['sports-dd'] ?? false;

        if ($sportId) {
            $output = SportLeagueAR::getListBySport($sportId);
        }

        return [
            'output' => $output,
            'selected' => $selected,
        ];
    }

    /**
     * Updates an existing AR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var PostFM $model */
        $model = PostFM::findOrFail($id);
        $sports = SportAR::getAsOptsList();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->validate(null, false);

            return $model->getErrors();
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate(null, false)) {
            $clonedModel = clone $model;

            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
                $model->sendModel(PostSender::SCENARIO_DEFAULT, ['clonedModel' => $clonedModel]);

                return $this->redirect(['index']);
            }

            Yii::$app->session->addFlash('error', $model->errors);
        }

        $model->sports = Many2Many::m2mIDsByRel($id, Many2Many::TYPE_NEWS_SPORT);
        $model->leagues = Many2Many::m2mIDsByRel($id, Many2Many::TYPE_NEWS_LEAGUE);

        return $this->render('update', [
            'model' => $model,
            'sports' => $sports,
        ]);
    }
}
