<?php

namespace common\modules\cms\shortcodes;

use yii\base\BaseObject;

abstract class ShortCode extends BaseObject
{
    const NAME = '';
    const MODEL_CLASS = '';
    public $template;

    public $id;
    public $post;
    public $model;

    public function init()
    {
        parent::init();

        if ($this->id) {
            $class = $this::MODEL_CLASS;
            $this->model = $class::findOne($this->id);
        }
    }

    public function run()
    {
        if (!$this->template) {
            $this->template = '/views/' . $this::NAME . '.php';
        }

        $class = static::MODEL_CLASS;
        $this->model = $class::findOne($this->id);

        return \Yii::$app->view->renderPhpFile( __DIR__ . $this->template, ['model' => $this->model]);
    }
}
