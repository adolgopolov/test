<?php

namespace common\modules\cms\shortcodes;

class Team extends ShortCode
{
    const NAME = 'team';
    const MODEL_CLASS = 'common\modules\sport\models\SportTeamAR';
}