<?php

namespace common\modules\cms\shortcodes;

class Sman extends ShortCode
{
    const NAME = 'sman';
    const MODEL_CLASS = 'common\modules\sport\models\SmanAR';
}