<?php

namespace common\modules\cms\shortcodes;

use common\modules\game\models\SportMatchAR;

class Match extends ShortCode
{
    const NAME = 'match';
    const MODEL_CLASS = SportMatchAR::class;
}