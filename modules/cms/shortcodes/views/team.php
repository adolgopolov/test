<?php

/**
 * @var yii\web\View $this
 * @var common\modules\sport\models\SmanAR $model
 */
?>

<div class="row">
<div class="card col-sm-4 offset-sm-4">
    <img class="card-img-top" src="https://cdn.newsapi.com.au/image/v1/4e8544536d05c81239a513e9161ab415?width=1024"></img>
    <div class="card-body">
        <h3 class="card-title">
            <?= $model->r_name ?>
        </h3>
    </div>
</div>
</div>
