<?php

namespace common\modules\cms\senders;

use common\components\pusher\senders\AbstractSender;
use common\components\pusher\traits\BeforeSendTrait;
use common\modules\cms\models\PostAR;
use Yii;
use yii\base\InvalidConfigException;

class PostSender extends AbstractSender
{
    use BeforeSendTrait;

    /** @var PostAR */
    public $model;

    /**
     * @throws InvalidConfigException
     */
    public function defaultScenario()
    {
        /** @var PostAR $clonedModel */
        $clonedModel = $this->getData('clonedModel');
        $status = $clonedModel->r_status ?? 0;

        if (!($status & PostAR::STATUS_PUBLISHED) && $this->model->r_status & PostAR::STATUS_PUBLISHED) {
            Yii::$app->pushSender->global->update
                ->addOneRecord('news', $this->model)
                ->pushJob();
        }
    }
}
