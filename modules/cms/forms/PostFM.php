<?php

namespace common\modules\cms\forms;

use Yii;
use kartik\markdown\Markdown;
use common\models\Many2Many;
use common\modules\cms\components\ShortCodeParser;

/**
 * Class PostFM
 *
 * @package common\modules\cms\forms
 */
class PostFM extends \common\modules\cms\models\PostAR
{
    public $sports = [];
    public $leagues = [];

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'sports' => 'Виды спорта',
            'leagues' => 'Лиги',
        ]);
    }

    /**
     * @param array $data
     * @param null  $formName
     *
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $loaded = parent::load($data, $formName);

        $this->shortcodes = [];

        $this->announce = Markdown::convert($this->announce_markdown);
        $this->content = Markdown::convert($this->content_markdown);

        $this->announce = ShortCodeParser::parse($this, 'announce');
        $this->content = ShortCodeParser::parse($this, 'content');

        $scope = $formName === null ? $this->formName() : $formName;

        if (!empty($data[$scope])) {
            $this->sports = $data[$scope]['sports'];
            $this->leagues = $data[$scope]['leagues'];
        }

        return $loaded;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     *
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $success = false;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $success = parent::save($runValidation, $attributeNames);
            if ($success) {
                $relations = Many2Many::find()
                                      ->where(['id_left' => $this->id])
                                      ->andWhere([
                                          'OR',
                                          'rel_kind',
                                          Many2Many::TYPE_NEWS_LEAGUE,
                                          Many2Many::TYPE_NEWS_SPORT,
                                      ])
                                      ->asArray()
                                      ->all();

                foreach ($relations as $relation) {
                    Yii::$app->db->createCommand()
                                 ->delete(Many2Many::tableName(), [
                                     'id_left' => $relation['id_left'],
                                     'id_right' => $relation['id_right'],
                                     'rel_kind' => $relation['rel_kind'],
                                 ])->execute();
                }

                if (!empty($this->sports)) {
                    foreach ($this->sports as $sport_id) {
                        $many2many = new Many2Many([
                            'rel_kind' => Many2Many::TYPE_NEWS_SPORT,
                            'id_right' => $sport_id,
                            'id_left' => $this->id,
                        ]);

                        $success = $success && $many2many->save();
                    }
                }

                if (!empty($this->leagues)) {
                    foreach ($this->leagues as $league_id) {
                        $many2many = new Many2Many([
                            'rel_kind' => Many2Many::TYPE_NEWS_LEAGUE,
                            'id_right' => $league_id,
                            'id_left' => $this->id,
                        ]);

                        $success = $success && $many2many->save();
                    }
                }

                if ($success) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            }
        } catch (\Exception $ex) {
            \Yii::error($ex->getTraceAsString(), 'ss3');

            return false;
        }

        return $success;
    }
}
