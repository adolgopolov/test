<?php

namespace common\modules\cms;

use backend\components\aboard\MenuEntryInterface;

/**
 * cms module definition class
 */
class CmsModule extends \yii\base\Module implements MenuEntryInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\cms\controllers';

    public static function getMenuItems(): ?array
    {
        return [
            [
                'label' => 'Контент',
                'icon'  => 'files',
                'color' => 'purple-700',
                'items' => [
                    [
                        'label' => 'Новости',
                        'url'   => ['/cms/post']
                    ]
                ]
            ]
        ];
    }

}
