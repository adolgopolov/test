<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\cms\models\PostAR $model
 * @var array $sports
 */

$this->title = 'Редактирование новости: ' . $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->r_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмтор', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
?>
<div class="post-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'sports' => $sports,
        'model' => $model,
    ]) ?>
</div>
