<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\cms\models\PostAR $model
 * @var array $sports
 */

$this->title = 'Добавление новости';
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-ar-create">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'sports' => $sports,
        'model' => $model,
    ]) ?>
</div>
