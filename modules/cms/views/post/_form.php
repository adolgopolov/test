<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use kartik\markdown\MarkdownEditor;
use kartik\datetime\DateTimePicker;
use common\modules\cms\models\PostAR;
use common\widgets\image\ImageWidget;

/**
 * @var yii\web\View $this
 * @var common\modules\cms\models\PostAR $model
 * @var yii\widgets\ActiveForm $form
 * @var array $sports
 */

?>
<div class="post-ar-form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <?= $form->field($model, 'r_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'r_icon')->widget(ImageWidget::class, [
        'kind' => $model::kind(),
        'alwaysShow' => true,
    ]) ?>

    <?= $form->field($model, 'announce_markdown')->widget(MarkdownEditor::class, ['height' => 130]) ?>

    <?php if ($model->hasErrors('announce_markdown')): ?>
        <div class="row">
            <div class="col-sm-10 offset-sm-2">
                <div class="alert alert-danger">
                    <ul>
                        <?php foreach ($model->getErrors('announce_markdown') as $e): ?>
                            <li><?= $e ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'content_markdown')->widget(MarkdownEditor::class) ?>

    <?php if ($model->hasErrors('content_markdown')): ?>
        <div class="row">
            <div class="col-sm-10 offset-sm-2">
                <div class="alert alert-danger">
                    <ul>
                        <?php foreach ($model->getErrors('content_markdown') as $e): ?>
                            <li><?= $e ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'sports')->widget(Select2::class, [
        'data' => $sports,
        'options' => [
            'id' => 'sports-dd',
        ],
        'pluginOptions' => [
            'placeholder' => 'Выберите вид спорта',
            'multiple' => true,
        ],
    ]) ?>

    <?= $form->field($model, 'leagues')->widget(DepDrop::class, [
        'options' => ['id' => 'leagues-dd'],
        'pluginOptions' => [
            'depends' => ['sports-dd'],
            'url' => Url::to(['/cms/post/ajax-leagues']),
            'initialize' => !$model->isNewRecord,
        ],
        'select2Options' => [
            'pluginOptions' => [
                'placeholder' => 'Выберите лигу',
                'multiple' => true,
            ],
        ],
        'type' => DepDrop::TYPE_SELECT2,
    ]) ?>

    <div class="row">
        <label class="col-sm-2 col-form-label text-normal text-dark" for="">Публикация</label>
        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-5">
                    <?= $form->field($model, 'r_status', [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4 col-form-label text-normal text-dark',
                            'wrapper' => 'col-sm-8'
                        ]
                    ])
                        ->dropDownList(
                            PostAR::code2status('all', true)
                        )
                    ?>
                </div>
                <div class="col-sm-5 offset-sm-2">
                    <?= $form->field(
                        $model,
                        'publish_at',
                        [
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-4 col-form-label text-normal text-dark',
                                'wrapper' => 'col-sm-8'
                            ]
                        ]
                    )->widget(DateTimePicker::class)
                    ?>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
