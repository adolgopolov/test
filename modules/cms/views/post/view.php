<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\YiiAsset;

/**
 * @var $this yii\web\View
 * @var $model common\modules\cms\models\PostAR
 */

$this->title = $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Post Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];

YiiAsset::register($this);
?>
<div class="post-ar-view">
    <p class="text-right">
        <a href="<?= Url::to(['index']) ?>" class="btn btn-outline-primary pull-left"><span class="fa fa-arrow-left"></span> Вернуться к списку</a>
        <?= Html::a('<span class="fa fa-pencil"></span> Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<span class="fa fa-trash"></span> Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно желаете уделить эту новость?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <br>
    <h4>Текст анонса:</h4>
    <article><?= $model->announce ?></article>
    <br>
    <h4>Полный текст:</h4>
    <article><?= $model->content ?></article>
</div>
