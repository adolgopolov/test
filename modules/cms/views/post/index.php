<?php

use yii\widgets\Pjax;
use common\modules\cms\models\PostAR;
use common\widgets\GridActions\GridActionsWidget;
use backend\components\aboard\widgets\GridView;
use backend\components\aboard\ActionColumn;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-ar-index">
    <?= GridActionsWidget::widget([
        'title' => $this->title,
        'enableChangeStatus' => true,
        'statusesList' => PostAR::code2status('all', true),
    ]) ?>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            GridActionsWidget::$checkboxColumn,
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            'r_name',
            [
                'attribute' => 'announce',
                'format' => 'raw'
            ],
            [
                'attribute' => 'r_icon',
                'value' => static function (PostAR $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'r_status',
                'value' => static function (PostAR $model) {
                    $text = $model::code2status($model->r_status, true);
                    $class = 'badge-primary';
                    if ($model->r_status === $model::STATUS_DRAFT) {
                        $class = 'badge-warning';
                    } elseif ($model->r_status === $model::STATUS_PUBLISHED) {
                        $class = 'badge-success';
                    }

                    return '<span class="badge badge-pill ' . $class . '">' . $text . '</span>';
                },
                'format' => 'raw'
            ],
            'publish_at:date',
            ['class' => ActionColumn::class],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
