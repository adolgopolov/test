<?php

namespace common\modules\goods\controllers;

use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\modules\goods\forms\CardAS;
use common\modules\goods\models\CardAR;
use common\modules\goods\models\CardSeriesAR;
use common\modules\score\models\CalcItemAR;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportPositionAR;
use common\components\S3Core;
use Throwable;
use Yii;
use yii\base\ExitException;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CardController extends AdminController
{
    use CRUDTrait;

    public $modelClass = CardAR::class;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CardAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|Response
     * @throws ExitException
     */
    public function actionCreate()
    {
        $model = new CardAR();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->saveCard()) {
                Yii::$app->session->addFlash('success', 'Запись успешно добавлена.');

                return $this->redirect(['index']);
            }

            Yii::$app->session->addFlash('error', $model->errors);
        }

        $sportList = SportAR::getAsOptsList();
        $smenList = SmanAR::getSmenList();
        $series = CardSeriesAR::getList();

        return $this->render('create', [
            'model' => $model,
            'sportList' => $sportList,
            'smenList' => $smenList,
            'series' => $series,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = CardAR::findOne($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws ExitException
     */
    public function actionUpdate($id)
    {
        $model = CardAR::findOne($id);

        if (!$model) {
            return $this->redirect(['index']);
        }

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->saveCard()) {
                Yii::$app->session->addFlash('success', 'Запись успешно обновлена.');

                return $this->redirect(['view', 'id' => $id]);
            }

            Yii::$app->session->addFlash('error', $model->errors);
        }

        $sportList = SportAR::getAsOptsList();
        $smenList = SmanAR::getSmenList();
        $series = CardSeriesAR::getList();

        return $this->render('update', [
            'model' => $model,
            'sportList' => $sportList,
            'smenList' => $smenList,
            'series' => $series,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        /** @var CardAR $model */
        $model = CardAR::findOrFail($id);

        $seriesCards = CardAR::find()
            ->whereJO(['series' => $model->series])
            ->count();

        if (!S3Core::isAllowedDelete($model)) {
            Yii::$app->session->addFlash('info', 'Удаление запрещено');
            return $this->redirect(['index']);
        }

        if (!$model->delete()) {
            Yii::$app->session->setFlash('error', $model->errors);
        }

        // FIXME удаления связанных сущностей должны делать:
        /* 1. в расширенном методе delete объекта родителя, не beforeDelete - beforeDelete проверяет возможность удаления и возвращает true false
         * 2. статик методом в какой AR
         * 3. в обертке транзакции см пример ScoreModule
         * 4. в дуинге
         */

        if ($seriesCards == 1) {
            CardSeriesAR::deleteAll(['id' => $model->series]);
        }

        return $this->redirect(['index']);
    }

    /**
     * @param integer $sport_id
     * @param null|integer $sman_id
     * @param null|string $buffs
     * @return string
     */
    public function actionAjaxCardBuffs($sport_id, $sman_id, $buffs)
    {
        if (!Yii::$app->request->isAjax) {
            return null;
        }

        $sportModel = SportAR::findOne($sport_id);

        if (!$sportModel) {
            return null;
        }

        $smanModel = SmanAR::findOne($sman_id);

        $calcCodeList = CardAR::getCalcsBySport($sportModel->ent_code);

        $calcs = CalcItemAR::find()
            ->andWhere(['ent_code' => $calcCodeList])
            ->indexBy('ent_code')
            ->all();

        $positions = SportPositionAR::findBySmanOrSport($smanModel, $sport_id);

        return $this->renderPartial('ajax-card-buffs', [
            'buffs' => json_decode($buffs, true),
            'calcs' => $calcs,
            'positions' => $positions,
        ]);
    }

    /**
     * @param int|string $series
     * @return mixed|null|string
     */
    public function actionAjaxSeriesColor($series)
    {
        if (!Yii::$app->request->isAjax) {
            return null;
        }

        $seriesModel = CardSeriesAR::findOne($series);

        if (!$seriesModel) {
            return null;
        }

        return $seriesModel->color;
    }
}
