<?php

namespace common\modules\goods\controllers;

use Yii;
use yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\components\S3Core;
use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\modules\goods\components\S3GoodKindAR;
use common\modules\goods\forms\GivePackFM;
use common\modules\goods\forms\PackAS;
use common\modules\goods\models\CardSeriesAR;
use common\modules\goods\models\PackAR;
use common\modules\money\models\MoneyCurrencyAR;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\forms\SmanAS;
use common\modules\sport\forms\SportLeagueFM;
use common\modules\sport\models\SmanAR;
use common\modules\user\models\UserAR;
use Throwable;

/**
 * Class PackController
 * @package common\modules\goods\controllers
 */
class PackController extends AdminController
{
    use CRUDTrait;

    public $modelClass = PackAR::class;

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function actionIndex()
    {
        $searchModel = new PackAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $modelDoing = new GivePackFM();
        $userList = UserAR::getUserList();
        $packList = PackAR::getPackList();

        if ($modelDoing->givePack()) {
            return $this->redirect(['index']);
        }

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelDoing' => $modelDoing,
            'userList' => $userList,
            'packList' => $packList,
        ]);
    }

    /**
     * @return string|Response
     * @throws ExitException
     */
    public function actionCreate()
    {
        $model = new PackAR();
        $currencies = MoneyCurrencyAR::find()->all();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->savePack()) {
                Yii::$app->session->addFlash('success', 'Запись успешно добавлена.');

                return $this->redirect(['index']);
            }

            Yii::$app->session->addFlash('error', $model->errors);
        }

        return $this->render('create', [
            'model' => $model,
            'currencies' => $currencies,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        /** @var PackAR $model */
        $model = PackAR::findOrFail($id);

        $packSmen = $model->getPackSmen()
            ->select(['r_name'])
            ->indexBy('id')
            ->column();

        $seriesProvider = new ActiveDataProvider([
            'query' => $model->getPackSeries(),
        ]);

        $cardsProvider = new ArrayDataProvider([
            'allModels' => $model->getPackCards(),
        ]);

        return $this->render('view', [
            'model' => $model,
            'packSmen' => $packSmen,
            'seriesProvider' => $seriesProvider,
            'cardsProvider' => $cardsProvider,
            'cardModel' => [],
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws ExitException
     */
    public function actionUpdate($id)
    {
        $model = PackAR::findOne($id);
        $currencies = MoneyCurrencyAR::find()->all();

        if (!$model) {
            return $this->redirect(['index']);
        }

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->savePack()) {
                Yii::$app->session->addFlash('success', 'Запись успешно обновлена.');

                return $this->redirect(['update', 'id' => $id]);
            }

            Yii::$app->session->addFlash('error', $model->errors);
        }

        $anywayData = [];

        if ($model->anyway) {
            foreach ($model->anyway as $card => $count) {
                $anywayData[] = [
                    'card' => $card,
                    'count' => $count,
                ];
            }
        }

        $anywayProvider = new ArrayDataProvider([
            'allModels' => $anywayData,
        ]);

        $availableCards = $model->getAvailableCards();

        return $this->render('update', [
            'model' => $model,
            'currencies' => $currencies,
            'anywayProvider' => $anywayProvider,
            'availableCards' => $availableCards,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = PackAR::findOne($id);

        if ($model) {
            if (!S3Core::isAllowedDelete($model)) {
                Yii::$app->session->addFlash('info', 'Удаление запрещено');
                return $this->goBack();
            }

            if ($model->delete()) {
                Yii::$app->session->addFlash('success', 'Запись успешно удалена.');
                return $this->redirect(['index']);
            }
        }

        Yii::$app->session->addFlash('error', 'Произошшла ошибка.');

        return $this->goBack();
    }

    /**
     * @param null|integer $id
     * @return array
     */
    public function actionAjaxLeagues($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $output = [];
        $selected = null;

        $depdropParams = Yii::$app->request->post('depdrop_all_params', []);
        $sportId = $depdropParams['sports-dd'] ?? false;

        if ($sportId) {
            $output = SportLeagueFM::getListBySport($sportId);
        }

        if ($id) {
            $model = PackAR::findOne($id);
            if ($model) {
                $selected = $model->leagues;
            }
        }

        return [
            'output' => $output,
            'selected' => $selected,
        ];
    }

    /**
     * @param null|integer $id
     * @return array
     * @throws InvalidConfigException
     */
    public function actionAjaxTeams($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $output = [];
        $selected = null;

        $depdropParams = Yii::$app->request->post('depdrop_all_params', []);
        $leagueIds = $depdropParams['leagues-dd'] ?? [];

        foreach ($leagueIds as $leagueId) {
            $league = SportLeagueFM::findOne($leagueId);
            if ($league) {
                $output = ArrayHelper::merge($output, $league->getListTeams());
            }
        }

        if ($id) {
            $model = PackAR::findOne($id);
            if ($model) {
                $selected = $model->teams;
            }
        }

        return [
            'output' => $output,
            'selected' => $selected,
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionAjaxFormAnyway($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = ['success' => false, 'message' => 'Произошла ошибка.'];

        $model = PackAR::findOne($id);

        if (!$model) {
            return $response;
        }

        if ($model->load(Yii::$app->request->post())) {
            $anyway = $model->anyway;
            $card = $model->anywayCardUpdate ?: $model->anywayCard;

            $cardTypes = S3GoodKindAR::getCardTypesList();
            if (!isset($cardTypes[$card])) {
                return $response;
            }

            $anyway[$card] = $model->anywayCount;
            $model->anyway = $anyway;

            foreach ($anyway as $cardType => $anywayValue) {
                if (empty($anywayValue)) {
                    return ['success' => false, 'message' => 'Количество обязательных карточек не может быть нулевым или пустым.'];
                }
            }

            if ($model->savePack()) {
                $response = ['success' => true, 'message' => 'Сведения об обязательных карточках успешно обновлены.'];
            }
        }

        if ($model->hasErrors('anyway')) {
            $message = null;

            $errors = $model->getErrors('anyway');

            foreach ($errors as $error) {
                $message .= $error . '<br />';
            }

            return ['success' => false, 'message' => $message];
        }

        return $response;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionAjaxDeleteAnyway($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = ['success' => false, 'message' => 'Произошла ошибка.'];

        $model = PackAR::findOne($id);

        if (!$model) {
            return $response;
        }

        /** @var string|null $card */
        $card = Yii::$app->request->post('child_id');
        $anyway = $model->anyway;
        unset($anyway[$card]);
        $model->anyway = $anyway;

        if ($model->save(false)) {
            $response = ['success' => true, 'message' => 'Обязательная карточка успешно удалена.'];
        }

        return $response;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionAjaxUpdateProbability($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = ['success' => false, 'message' => 'Произошла ошибка.'];

        $model = PackAR::findOne($id);

        if (!$model) {
            return $response;
        }

        if ($model->load(Yii::$app->request->post())) {
            foreach ($model->probability as $cardId => $probability) {
                if (empty($probability)) {
                    return ['success' => false, 'message' => 'Вероятность не может быть пустой.'];
                }
            }

            if ($model->savePack()) {
                $response = ['success' => true, 'message' => 'Вероятности успешно обновлены.'];
            }
        }

        return $response;
    }

    /**
     * @param $series
     * @param $teams
     * @param string|null $smen
     * @param string|null $params
     * @return string
     */
    public function actionAjaxSeries($series, $teams, $smen, $params = null)
    {
        if (!$teams) {
            return null;
        }

        $paramsArray = json_decode($params, true);
        $seriesArray = json_decode($series, true);
        $teamsArray = json_decode($teams, true);
        $smenArray = json_decode($smen, true);

        $smanModels = SmanAR::getSmenListByTeam($teamsArray, true) // TODO заменить на SmanAR::find()->whereTeam()
            ->andWhere(['!=', 'sman_status', 'outall'])
            ->all();

        $smenList = ArrayHelper::map($smanModels, 'id', 'r_name');
        $seriesModels = CardSeriesAR::findAll($seriesArray);

        $smanSearch = new SmanAS();
        $smanProvider = $smanSearch->searchByTeam($teamsArray, $paramsArray);

        return $this->renderPartial('ajax-series', [
            'seriesModels' => $seriesModels,
            'smanProvider' => $smanProvider,
            'smanSearch' => $smanSearch,
            'smenArray' => $smenArray,
            'smenList' => $smenList,
        ]);
    }
}
