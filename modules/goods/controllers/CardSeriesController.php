<?php

namespace common\modules\goods\controllers;

use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\modules\goods\forms\CardSeriesAS;
use common\modules\goods\models\CardSeriesAR;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use Yii;
use common\components\S3Core;

class CardSeriesController extends AdminController
{
    use CRUDTrait;

    public $modelClass = CardSeriesAR::class;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CardSeriesAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new CardSeriesAR();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Запись успешно добавлена.');

                return $this->redirect(['index']);
            }

            Yii::$app->session->addFlash('error', $model->errors);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = CardSeriesAR::findOrFail($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = CardSeriesAR::findOrFail($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Запись успешно обновлена.');

                return $this->redirect(['index']);
            }

            Yii::$app->session->addFlash('error', $model->errors);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = CardSeriesAR::findOrFail($id);

        if ($model && !S3Core::isAllowedDelete($model)) {
            Yii::$app->session->addFlash('info', 'Удаление запрещено');
            return $this->goBack();
        }

        if ($model->delete()) {
            Yii::$app->session->addFlash('success', 'Запись успешно удалена.');
        }
        else {
            Yii::$app->session->addFlash('error', $model->errors);
        }

        return $this->redirect(['index']);
    }
}
