<?php

namespace common\modules\goods\forms;

use common\modules\goods\models\CardAR;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class CardAS
 * @package common\modules\goods\models
 */
class CardAS extends CardAR
{
    public $series_id;
    public $sman_id;

    public $columns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_name'], 'string'],
            [['id', 'r_status', 'ent_type', 'sport_id', 'series_id', 'sman_id'], 'integer'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->alias('card')
            ->select([
                'card.*',
                "JSON_VALUE(ext_info, '$.series') AS series_id",
                "JSON_VALUE(ext_info, '$.sman_id') AS sman_id"
                // DELETE FAN-529 избыточный код "JSON_UNQUOTE(JSON_EXTRACT(ext_info, '$.series')) AS series_id",
                // DELETE FAN-529 избыточный код "JSON_UNQUOTE(JSON_EXTRACT(ext_info, '$.sman_id')) AS sman_id",
            ]);
        // FAN-529 как поведет себя JSON_VALUE(ext_info, '$.series')
        // "series":["6"],"sports":["1"],"
        // {"series":6,
        // почему sports массив - могут быть одинаковые карточки для разных видов спорта?

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => ArrayHelper::merge($dataProvider->sort->attributes, [
                'series_id',
                'sman_id',
            ]),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        $query->andFilterWhere([
            'AND',
            ['id' => $id],
            ['like', 'card.r_name', $this->r_name],
            ['card.r_status' => $this->r_status],
            ['card.ent_type' => $this->ent_type],
            ['card.sport_id' => $this->sport_id],
        ]);

        $query->andFilterHaving([
            'AND',
            ['series_id' => $this->series_id],
            ['sman_id' => $this->sman_id],
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'r_name',
            'r_status',
            'ent_type',
            'sport_id',
            'series_id',
            'sman_id',
        ];
    }
}
