<?php

namespace common\modules\goods\forms;

use common\modules\goods\models\PackAR;
use common\modules\user\models\UserAR;
use Yii;
use yii\base\Model;

class GivePackFM extends Model
{
    public $user_id;
    public $pack_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'pack_id'], 'required'],
            [['user_id'], 'exist', 'targetClass' => UserAR::class, 'targetAttribute' => 'id'],
            [['pack_id'], 'exist', 'targetClass' => PackAR::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Пользователь',
            'pack_id' => 'Пак',
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function givePack()
    {
        if ($this->load(Yii::$app->request->post()) && $this->validate()) {
            return PackAR::givePack($this->user_id, $this->pack_id);
        }

        return false;
    }
}
