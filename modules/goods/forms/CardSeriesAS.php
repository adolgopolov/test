<?php

namespace common\modules\goods\forms;

use common\modules\goods\models\CardSeriesAR;
use yii\data\ActiveDataProvider;

/**
 * Class CardSeriesAS
 * @package common\modules\goods\forms
 */
class CardSeriesAS extends CardSeriesAR
{
    public $columns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'r_name'], 'string'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        $query->andFilterWhere([
            'AND',
            ['id' => $id],
            ['like', 'r_name', $this->r_name],
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'r_name',
            'color',
            'r_icon',
            'cardsCount',
        ];
    }
}
