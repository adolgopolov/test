<?php

namespace common\modules\goods\forms;

use common\modules\goods\models\PackAR;
use yii\data\ActiveDataProvider;

class PackAS extends PackAR
{
    public $columns;

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'r_name',
            'r_text',
            'r_status',
            'r_icon',
            'ent_type',
            'sports',
            'leagues',
            'teams',
        ];
    }
}
