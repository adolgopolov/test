<?php

namespace common\modules\goods\models;

use common\components\S3Kind;
use common\doings\goods\OpenPackDoing;
use common\extensions\ExtAQ;
use common\modules\goods\components\S3GoodKindAQ;
use common\modules\goods\components\S3UserGoodAR;
use common\modules\sport\models\SmanAR;
use common\modules\user\models\UserAR;

/**
 * Class UserPackAR
 * @package common\modules\goods\models
 * @property SmanAR $sman
 * @property UserAR $user
 * @property PackAR $pack
 */
class UserPackAR extends S3UserGoodAR
{
    /**
     * @return int|null
     */
    public static function kind()
    {
        return S3Kind::KIND_PACK;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function openPack()
    {
        $doing = OpenPackDoing::new();

        return $doing->doIt([
            'model_id' => $this->id,
        ]);
    }

    /**
     * @return ExtAQ|\yii\db\ActiveQuery
     */
    public function getSman()
    {
        return $this->hasOne(SmanAR::class, ['id' => 'ent_value']);
    }

    /**
     * @return S3GoodKindAQ|\yii\db\ActiveQuery
     */
    public function getPack()
    {
        return $this->hasOne(PackAR::class, ['id' => 'good_kind']);
    }
}
