<?php

namespace common\modules\goods\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use common\behaviors\ImageUploadBehavior;
use common\components\S3DictionaryAQ;
use common\components\S3Kind;
use common\components\S3Core;
use common\doings\goods\PurchasePackDoing;
use common\modules\goods\components\S3GoodKindAQ;
use common\modules\goods\components\S3GoodKindAR;
use common\modules\goods\components\S3UserGoodAR;
use common\modules\sport\models\SmanAQ;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportLeagueAQ;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportTeamAQ;
use common\modules\sport\models\SportTeamAR;
use common\validators\JsonValidator;

/**
 * Class PackAR
 * @package common\modules\goods\models
 * @property CardAR[] $packCards
 * @property CardAR[] $possibleCards
 * @property S3GoodKindAQ $packSeries
 * @property SmanAQ $packSmen
 * @property S3DictionaryAQ $sportModels
 * @property SportLeagueAQ $leagueModels
 * @property SportTeamAQ $teamModels
 * @property array $sports
 * @property array $leagues
 * @property array $teams
 * @property array $series
 * @property array $smen
 * @property array $probability
 * @property integer $count
 * @property array $anyway
 * @property array $prices
 * @property integer $event
 */
class PackAR extends S3GoodKindAR
{
    public $anywayCardUpdate;
    public $anywayCard;
    public $anywayCount;

    public $image_file;

    /**
     * @return int|null
     */
    public static function kind()
    {
        return S3Kind::KIND_PACK;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => ImageUploadBehavior::class,
                'kind' => self::kind(),
                'model' => $this,
                'attribute' => 'image_file',
                'image_name' => 'r_icon',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_name', 'r_text', 'r_status', 'ent_kind', 'ent_type', 'sports', 'leagues', 'teams', 'count', 'series'], 'required'],
            [['r_name', 'r_text'], 'string'],
            [['anywayCard', 'anywayCount', 'anywayCardUpdate'], 'integer'],
            [['anyway', 'probability'], 'safe'],
            [['anyway'], 'validateAnyway', 'skipOnEmpty' => true],
            [['r_icon'], 'string'],
            [['ext_info'], JsonValidator::class, 'rules' => [
                'smen' => 'required'
            ]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_name' => 'Название',
            'r_text' => 'Описание',
            'r_status' => 'Статус',
            'r_icon' => 'Картинка',
            'ent_type' => 'Тип пака',
            'sports' => 'Виды спорта',
            'series' => 'Серии',
            'leagues' => 'Лига',
            'teams' => 'Команды',
            'count' => 'Количество выпадаемых карточек',
            'anyway' => 'Обязательные карточки',
            'prices' => 'Цены',
            'event' => 'Событие',
            'anywayCard' => 'Тип карточки',
            'anywayCount' => 'Количество',
        ];
    }

    /**
     * @return array
     */
    protected function jsonFields(): array
    {
        return [
            'series',
            'smen',
            'sports',
            'leagues',
            'teams',
            'probability',
            'count',
            'anyway',
            'prices',
            'event',
        ];
    }

    public function beforeDelete()
    {
        if (S3Core::isAllowedDelete($this) && parent::beforeDelete()) {
            S3UserGoodAR::deleteAll(['good_kind' => $this->id]);
            return true;
        }

        return false;
    }

    /**
     * @param $attribute
     */
    public function validateAnyway($attribute)
    {
        $summary_count = array_sum($this->$attribute);

        if ($summary_count > $this->count) {
            $this->addError($attribute, 'Количество обязательных карточек не должно превышать общее количество карточек пака.');
        }
    }

    /**
     * @return array
     */
    public static function getPackList()
    {
        return self::find()
            ->select(['r_name'])
            ->indexBy('id')
            ->asArray()
            ->column();
    }

    /**
     * @return S3DictionaryAQ
     */
    public function getSportModels()
    {
        return SportAR::find()
            ->andWhere(['id' => $this->sports]);
    }

    /**
     * @return S3GoodKindAQ
     */
    public function getPackSeries()
    {
        return CardSeriesAR::find()
            ->andWhere(['id' => $this->series]);
    }

    /**
     * @param null $cardType
     * @param bool $forRead
     * @return S3GoodKindAQ
     */
    public function queryPackCards($cardType = null, $forRead = false)
    {
        $query = CardAR::find()
            ->whereJO(['series' => $this->series])
            ->andWhere(['&', 'r_status', S3GoodKindAR::STATUS_ACTIVE]);

        if ($cardType) {
            $query->andWhere(['ent_type' => $cardType]);
        }

        if ($forRead) {
            $query->forRead();
        }

        return $query;
    }

    /**
     * @param mixed|null $cardType
     * @param bool $forRead
     * @return CardAR[]
     */
    public function getPackCards($cardType = null, $forRead = false)
    {
        $packCards = $this->queryPackCards($cardType, $forRead)
            ->indexBy('id')
            ->all();

        return array_filter($packCards, function ($card) {
            /** @var CardAR $card */
            $smanIds = $this->getCardSmanList($card);

            return $smanIds && (!$card->sman_id || in_array($card->sman_id, $smanIds)) && in_array($card->sport_id, $this->sports);
        });
    }

    /**
     * @param null $cardType
     * @param bool $forRead
     * @return array
     */
    public function getPossibleCards($cardType = null, $forRead = false)
    {
        $packCards = $this->getPackCards($cardType, $forRead);
        $possibleCards = [];

        foreach ($packCards as $packCard) {
            $smanIds = $this->getCardSmanList($packCard);

            foreach ($smanIds as $smanId) {
                $packCard->sman_id = $smanId;

                $possibleCards[] = $packCard->attributes;
            }
        }

        return $possibleCards;
    }

    /**
     * @return SmanAQ
     */
    public function getPackSmen()
    {
        return SmanAR::find()
            ->andWhere(['id' => array_keys((array)$this->smen)]);
    }

    /**
     * @return SportLeagueAQ|ActiveQuery
     */
    public function getLeagueModels()
    {
        return SportLeagueAR::find()
            ->where(['id' => $this->leagues]);
    }

    /**
     * @return SportTeamAQ
     */
    public function getTeamModels()
    {
        return SportTeamAR::find()
            ->where(['id' => $this->teams]);
    }

    /**
     * @return array
     */
    public function getAvailableCards()
    {
        $cardTypes = S3GoodKindAR::getCardTypesList();

        return array_filter($cardTypes, function ($key) {
            return !isset($this->anyway[$key]);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @param CardAR $card
     * @return array
     */
    public function getCardSmanList(CardAR $card)
    {
        $smanList = [];

        foreach ($this->smen as $smanId => $series) {
            $checkSman = SmanAR::findOne($smanId);

            if (!$checkSman || $checkSman->sman_status === SmanAR::STATUS_OUTALL) {
                continue;
            }

            if ($card->ent_type == S3GoodKindAR::KIND_LEGENDARY_CARD && $card->sman_id != $smanId) {
                continue;
            }

            if (isset($series[$card->ent_type]) &&
                in_array($card->series, $series[$card->ent_type])) {
                $smanList[] = $smanId;
            }
        }

        return $smanList;
    }

    /**
     * @param array $smenItem
     * @param int $cardType
     * @return array
     */
    public static function getSmanSeries($smenItem, $cardType)
    {
        $series = [];

        foreach ($smenItem as $seriesCardType => $seriesIds) {
            if ($seriesCardType !== $cardType) {
                continue;
            }

            $series = ArrayHelper::merge($series, $seriesIds);
        }

        return $series;
    }

    /**
     * {@inheritdoc}
     */
    public function savePack($runValidation = true, $attributeNames = null)
    {
        $probabilityCards = [];

        $seriesArray = Yii::$app->request->post('smen');
        $this->smen = $seriesArray ? array_map(static function ($value) {
            return array_map('array_keys', $value);
        }, $seriesArray) : (array)$this->smen;

        if ($this->smen) {
            $packCards = $this->packCards;

            foreach ($packCards as $card) {
                /** @var CardAR $card */
                $probabilityCards[$card->id] = $this->probability[$card->id] ?? S3GoodKindAR::DEFAULT_PROBABILITY_POINT;
            }
        }

        $this->probability = $probabilityCards;

        if ($this->ent_type >= S3GoodKindAR::PACK_TYPE_NOT_CURRENCY) {
            $this->prices = [];
        } else {
            $this->event = [];
            $this->prices = array_filter($this->prices);
        }

        return $this->save($runValidation, $attributeNames);
    }

    /**
     * @param null $accId
     * @return array|int|mixed
     */
    public function getPrices($accId = null)
    {
        if ($accId !== null) {
            return $this->prices[$accId] ?? 0;
        }

        return $this->prices;
    }

    /**
     * @param int $user_id
     * @param int $pack_id
     * @return bool
     * @throws InvalidConfigException
     */
    public static function givePack($user_id, $pack_id)
    {
        /** @var PurchasePackDoing $doing */
        $doing = PurchasePackDoing::new();

        $result = $doing->unsetRules(['checkBalance'])
            ->doIt([
                'data' => [
                    [
                        'user_id' => $user_id,
                        'pack_id' => $pack_id,
                    ],
                ],
            ]);

        if ($result) {
            Yii::$app->session->addFlash('success', 'Пак успешно подарен пользователю.');

            return true;
        }

        Yii::$app->session->addFlash('error', $doing->errors);

        return false;
    }
}
