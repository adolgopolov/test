<?php

namespace common\modules\goods\models;

use common\components\S3Kind;
use common\modules\goods\components\S3GoodKindAR;
use common\validators\JsonValidator;

/**
 * Class CardTypesAR
 * @package common\modules\goods\models
 */
class CardTypesAR extends S3GoodKindAR
{
    /**
     * @return int|null
     */
    public static function kind()
    {
        return S3Kind::KIND_CARD_TYPE;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['r_name', 'ent_code'], 'string'],
            [['ent_kind'], 'integer'],
            [['ext_info'], JsonValidator::class], // FAN-529
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ent_kind' => 'Ent kind',
            'ent_code' => 'Ent code',
            'r_name' => 'Название',
        ];
    }
}
