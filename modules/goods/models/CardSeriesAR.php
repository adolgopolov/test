<?php

namespace common\modules\goods\models;

use common\behaviors\ImageUploadBehavior;
use common\components\S3DictionaryAQ;
use common\components\S3Kind;
use common\modules\goods\components\S3GoodKindAQ;
use common\modules\goods\components\S3GoodKindAR;
use common\modules\sport\models\SportAR;
use common\validators\JsonValidator;

/**
 * Class CardSeriesAR
 * @package common\modules\goods\models
 * @property CardAR $cards
 * @property int|string $cardsCount
 * @property S3GoodKindAQ $cardTypes
 * @property array $seriesSmen
 * @property S3DictionaryAQ $sportModels
 * @property string $color
 */
class CardSeriesAR extends S3GoodKindAR
{
    public $image_file;

    /**
     * {@inheritdoc}
     */
    public static function kind()
    {
        return S3Kind::KIND_CARD_SERIES;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => ImageUploadBehavior::class,
                'kind' => S3Kind::KIND_CARD_SERIES,
                'model' => $this,
                'attribute' => 'image_file',
                'image_name' => 'r_icon',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_name', 'color'], 'string'],
            [['ent_kind'], 'integer'],
            [['r_icon'], 'string'],
            [['ext_info'], JsonValidator::class] // FAN-529
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_name' => 'Название',
            'color' => 'Цвет',
            'r_icon' => 'Картинка',
            'cardsCount' => 'Количество карточек',
        ];
    }

    /**
     * @return array
     */
    protected function jsonFields(): array
    {
        return [
            'color',
        ];
    }

    /**
     * @return \common\modules\goods\components\S3GoodKindAQ
     */
    public function getCards()
    {
        return CardAR::find()
            ->whereJO(['series' => $this->id]);
    }

    /**
     * @return int|string
     */
    public function getCardsCount()
    {
        return $this->getCards()->count();
    }

    /**
     * @return \common\modules\goods\components\S3GoodKindAQ
     */
    public function getCardTypes()
    {
        $cardTypes = $this->getCards()
            ->select(['ent_type'])
            ->groupBy(['ent_type'])
            ->column();

        return CardTypesAR::find()
            ->andWhere(['id' => $cardTypes]);
    }

    /**
     * @param integer|null $cardType
     * @return array
     */
    public function getSeriesSmen($cardType = null)
    {
        $query = $this->getCards();

        if ($cardType) {
            $query->andWhere(['ent_type' => $cardType]);
        }

        $cards = $query->all();

        $smen = [];

        foreach ($cards as $card) {
            /** @var CardAR $card */
            $sman = $card->sman;

            if ($sman) {
                $smen[$sman->id] = $sman->getTitle();
            }
        }

        return $smen;
    }

    /**
     * @param integer $cardType
     * @return bool
     */
    public function hasEmptySman($cardType)
    {
        return $this->getCards()
            ->andWhere(['ent_type' => $cardType])
            ->whereJO(['IS not exists' => 'sman_id'])
            ->exists();
    }

    /**
     * @return \common\components\S3DictionaryAQ
     */
    public function getSportModels()
    {
        $sportIds = $this->getCards()
            ->select(['sport_id'])
            ->groupBy(['sport_id'])
            ->column();

        return SportAR::find()
            ->andWhere(['id' => $sportIds]);
    }

    /**
     * @param int|string $series
     * @param $color
     * @return array|bool|CardSeriesAR|null|\yii\db\ActiveRecord
     */
    public static function addSeries($series, $color)
    {
        $seriesModel = self::findOne($series);

        if ($seriesModel) {
            if ($seriesModel->color !== $color) {
                $seriesModel->color = $color;
                $seriesModel->save();
            }

            return $seriesModel;
        }

        $model = new self;
        $model->r_name = $series;
        $model->color = $color;
        $model->ent_kind = S3Kind::KIND_CARD_SERIES;
        $model->save();

        return $model;
    }

    /**
     * @return array
     */
    public static function getList()
    {
        return self::find()
            ->select(['r_name'])
            ->asArray()
            ->indexBy('id')
            ->column();
    }
}
