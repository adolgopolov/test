<?php

namespace common\modules\goods\models;

use common\components\S3DictionaryAR;
use common\components\S3Kind;
use common\modules\goods\components\S3GoodKindAR;
use common\modules\score\models\CalcItemAR;
use common\modules\sport\models\SmanAQ;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportPositionAR;
use common\validators\JsonValidator;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class CardAR
 * @package common\modules\goods\models
 * @property SportAR $sport
 * @property CardSeriesAR $seriesModel
 * @property SmanAR $sman
 * @property CalcItemAR $calcs
 * @property SportPositionAR $positions
 * @property integer $sman_id
 * @property array $buffs
 * @property string $cardName
 * @property integer $series
 */
class CardAR extends S3GoodKindAR
{
    public $series_color;

    /**
     * @return int|null
     */
    public static function kind()
    {
        return S3Kind::KIND_CARD;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ent_kind', 'ent_type', 'r_name', 'r_status', 'series', 'sport_id'], 'required'],
            [['series_color'], 'string'],
            [['r_status', 'ent_kind', 'ent_type'], 'integer'],
            [['sport_id'], 'exist', 'targetClass' => SportAR::class, 'targetAttribute' => 'id'],
            [['ext_info'], JsonValidator::class, 'rules' => [
                'sman_id' => ['validateSman'],
            ]],
        ];
    }

    /**
     * @return int
     */
    public function validateSman($value, $attribute)
    {
        if ($this->ent_type == S3GoodKindAR::KIND_LEGENDARY_CARD && empty($value)) {
            $this->addError($attribute, 'Легендарная карточка должна быть привязана к спортсмену!');
        }

        if ($this->ent_type == S3GoodKindAR::KIND_CLASSIC_CARD && $value) {
            $this->addError($attribute, 'Классическая карточка не может быть привязана к спортсмену!');
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_name' => 'Название',
            'r_status' => 'Статус',
            'ent_type' => 'Редкость',
            'series' => 'Серия',
            'series_id' => 'Серия',
            'series_color' => 'Цвет серии',
            'sman_id' => 'Спортсмен',
            'sport_id' => 'Вид спорта',
            'buffs' => 'Улучшения',
            'smen' => 'Спортсмены',
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function jsonFields(): array
    {
        return [
            'sman_id',
            'buffs',
            'series',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(SportAR::class, ['id' => 'sport_id']);
    }

    /**
     * @return CardSeriesAR
     */
    public function getSeriesModel()
    {
        return CardSeriesAR::findOne($this->series);
    }

    /**
     * @return SmanAQ|ActiveQuery
     */
    public function getSman()
    {
        return SmanAR::find()
            ->where(['id' => $this->sman_id]);
    }

    /**
     * @return S3DictionaryAR[]
     */
    public function getCalcs()
    {
        $buffs = $this->buffs;
if (empty($buffs)) { // FAN-674
return [];
}
        $calcs = array_shift($buffs);

        return CalcItemAR::find()
            ->andWhere(['ent_code' => array_keys($calcs)])
            ->indexBy('ent_code')
            ->all();
    }

    /**
     * @return S3DictionaryAR[]
     */
    public function getPositions()
    {
        return SportPositionAR::find()
            ->andWhere(['ent_code' => array_keys($this->buffs)])
            ->indexBy('ent_code')
            ->all();
    }

    /**
     * @return string
     */
    public function getCardName()
    {
        $cardTypes = S3GoodKindAR::getCardTypesList();
        $cardTypeName = $cardTypes[$this->ent_type] ?? 'Unknown type';

        if (!$this->sport) {
            return $cardTypeName;
        }

        $smenName = null;

        if ($this->sman_id) {
            $smenName = ' / ' . ($this->sman ? $this->sman->r_name : 'Unknown Sman');
        }

        $seriesName = $this->seriesModel->r_name ?? 'Unknown series';

        return $this->r_name . ' (' . $cardTypeName . ', ' . $seriesName . ', ' . $this->sport->r_name . $smenName . ')';
    }

    /**
     * @param null|array $cards
     * @return array
     */
    public static function getForList($cards = null)
    {
        $cardsList = [];

        if ($cards === null) {
            $cards = self::find()->all();
        }

        foreach ($cards as $card) {
            $cardsList[] = [
                'id' => $card->id,
                'name' => $card->cardName,
            ];
        }

        return $cardsList;
    }

    /**
     * @param $id
     * @return null|static
     */
    public static function getCard($id)
    {
        return self::findOne($id);
    }

    public function __get($name)
    {
        $result = parent::__get($name);
        if ($name == 'buffs' && empty($result)) $result = [];

        return $result;
    }


    /**
     * @param $sport
     * @return array
     */
    public static function getCalcsBySport($sport)
    {
        // FIXME добавить к расчетам флаг для карточек, заполнить, добавить к AQ карточек выбор по нему, добавить в админку управление флагом
        switch ($sport) {
            case 'csgo':
                return [
'calc.sport.cyber.defeat',
                    'calc.sport.cyber.murder',
                    'calc.sport.cyber.нelp',
                    'calc.sport.cyber.death',
'calc.sport.csgo.mvp',
                ];

            case 'dota2':
                return [
'calc.sport.cyber.defeat',
                    'calc.sport.cyber.murder',
                    'calc.sport.cyber.нelp',
                    'calc.sport.cyber.death',
'calc.sport.dota2.damage_buildings',
'calc.sport.dota2.warding',
'calc.sport.dota2.dewarding',
'calc.sport.dota2.rampage'
                ];
            case 'basket':
                return [
                    'calc.sport.basket.scored_points',
                    'calc.sport.basket.rebound',
                    'calc.sport.basket.assist',
                    'calc.sport.basket.steal',
                    'calc.sport.basket.blocked_shot',
                    'calc.sport.basket.turnover',
                    'calc.sport.basket.goal_attempted',
                ];
            case 'hockey':
                return [
                    'calc.sport.hockey.scored_goal',
                    'calc.sport.hockey.goalpass',
                    'calc.sport.hockey.removal_n_minutes',
                    'calc.sport.hockey.shot_on_target',
                    'calc.sport.hockey.missed_goal',
                    'calc.sport.any.saves',
                    'calc.sport.hockey.dry_match',
                    'calc.sport.hockey.plus_minus',
                ];
            case 'soccer':
                return [
                    'calc.sport.soccer.scored_goal',
                    'calc.sport.soccer.shot_on_target',
                    'calc.sport.soccer.goalpass',
                    'calc.sport.soccer.foul_committed',
                    'calc.sport.soccer.red_card',
                    'calc.sport.soccer.yellow_card',
                    'calc.sport.soccer.tackle_won',
                    'calc.sport.soccer.blocked_shot',
                    'calc.sport.soccer.dry_match',
                    'calc.sport.soccer.save_penalty',
                    'calc.sport.soccer.keeper_save',
                    'calc.sport.soccer.missed_goal',
                ];
        }

        return [];
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function saveCard($runValidation = true, $attributeNames = null)
    {
        $this->buffs = ArrayHelper::merge((array)$this->buffs, Yii::$app->request->post('buffs'));

        $seriesModel = CardSeriesAR::addSeries($this->series, $this->series_color);

        if ($seriesModel->hasErrors()) {
            $this->addErrors($seriesModel->errors);
        } else {
            $this->series = $seriesModel->id;
        }

        return $this->save($runValidation, $attributeNames);
    }
}
