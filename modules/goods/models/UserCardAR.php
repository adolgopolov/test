<?php

namespace common\modules\goods\models;

use common\components\S3Kind;
use common\doings\goods\ApplyNewCardDoing;
use common\modules\game\models\GameLineupAR;
use common\modules\goods\components\S3UserGoodAR;
use common\modules\sport\models\SmanAR;
use common\modules\user\models\UserAR;
use common\validators\JsonValidator;

/**
 * Class UserCardAR
 *
 * @package common\modules\goods\models
 * @property int $good_id
 * @property UserAR $user
 * @property SmanAR $sman
 *
 * @property CardAR $card
 */
class UserCardAR extends S3UserGoodAR
{
    /**
     * @return int|null
     */
    public static function kind()
    {
        return S3Kind::KIND_CARD;
    }

    /**
     * @return array
     */
    public function jsonFields(): array
    {
        return [
            'good_id',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'r_status', 'good_kind', 'ent_kind'], 'required'],
            [['user_id'], 'exist', 'targetClass' => UserAR::class, 'targetAttribute' => 'id'],
            [['r_status', 'good_kind', 'ent_kind', 'ent_value'], 'integer'],
            [['ext_info'], JsonValidator::class] // FAN-529
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserAR::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSman()
    {
        return $this->hasOne(SmanAR::class, ['id' => 'ent_value']);
    }

    public function attributeLabels()
    {
        return [
            'user_id' => 'Пользователь',
            'r_status' => 'Статус',
            'good_kind' => 'ID карточки',
            'ent_kind' => 'Тип вещи пользователя', // 45 карточка
            'ent_value' => 'ID спортсмена', //
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCard()
    {
        return $this->hasOne(CardAR::class, ['id' => 'good_kind']);
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $isNewRecord = $this->isNewRecord;

        if (parent::save($runValidation, $attributeNames)) {
            $success = true;
            if ($isNewRecord) {
                $doing = ApplyNewCardDoing::new();

                $success = $doing->doIt([
                    'data' => [
                        [
                            'user_id' => $this->user_id,
                            'user_good_id' => $this->id,
                            'good_kind' => $this->good_kind,
                            'sman_id' => $this->ent_value,
                        ],
                    ],
                ]);
            }

            return $success;
        }

        return false;
    }
}
