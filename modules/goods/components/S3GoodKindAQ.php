<?php

namespace common\modules\goods\components;

use common\extensions\ExtAQ;
use common\traits\StatusAQTrait;

/**
 * Class S3GoodKindAQ
 * @package common\modules\goods\components
 */
class S3GoodKindAQ extends ExtAQ
{
    use StatusAQTrait;

    /**
     * @param $value
     * @return $this
     */
    public function filterGood_id($value)
    {
        return $this->whereJO(['good_id' => $value]);
    }
}
