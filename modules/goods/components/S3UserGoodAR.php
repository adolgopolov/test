<?php

namespace common\modules\goods\components;

use common\components\pusher\interfaces\SenderModelInterface;
use common\components\pusher\senders\AbstractSender;
use common\extensions\ExtAQ;
use common\modules\goods\senders\UserGoodSender;
use common\modules\user\models\UserAR;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use common\validators\JsonValidator;

/**
 * Class S3UserGoodAR
 * @package common\modules\goods\components
 *
 * @property UserAR $user
 * @property int $id
 * @property int $user_id
 * @property int $r_status
 * @property int $good_kind
 * @property int $ent_kind
 * @property int $ent_value
 * @property string $created_at
 * @property string $updated_at
 * @property array $ext_info
 */
class S3UserGoodAR extends S3GoodKindAR implements SenderModelInterface
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%user_good}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'r_status', 'good_kind', 'ent_kind'], 'required'],
            [['r_status', 'good_kind', 'ent_kind'], 'integer'],
            [['user_id'], 'exist', 'targetClass' => UserAR::class, 'targetAttribute' => 'id'],
            [['r_status', 'good_kind', 'ent_kind', 'ent_value'], 'integer'],
            [['ext_info'], JsonValidator::class] // FAN-529
        ];
    }

    /**
     * @param string $scenario
     * @param array $data
     * @throws InvalidConfigException
     */
    public function sendModel($scenario = AbstractSender::SCENARIO_DEFAULT, $data = [])
    {
        UserGoodSender::sendModel($this, $scenario, $data);
    }

    /**
     * @return UserAR|ActiveQuery|ExtAQ
     */
    public function getUser()
    {
        return $this->hasOne(UserAR::class, ['id' => 'user_id']);
    }

    /**
     * @param $goodsIds
     * @return array [['id' > 1, 'good_kind' => 45]]
     */
    public static function getGoodsForFrontByIds($goodsIds)
    {
        if (!empty($goodsIds)) {
            return self::find()
                ->select(['id', 'good_kind'])
                ->andWhere(['in', 'id', $goodsIds])
                ->asArray()
                ->all();
        }
        return [];
    }
}
