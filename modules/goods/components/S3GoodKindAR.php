<?php

namespace common\modules\goods\components;

use common\extensions\ExtInfoAR;
use common\modules\goods\models\CardTypesAR;
use common\traits\FieldTrait;
use common\traits\StatusARTrait;
use common\validators\JsonValidator;

/**
 * Class S3GoodKindAR
 * @package common\modules\goods\components
 * @property $id
 * @property $r_name
 * @property $r_text
 * @property $r_status
 * @property $ent_kind
 * @property $ent_code
 * @property $ent_type
 * @property $sport_id
 * @property $ext_info
 */
class S3GoodKindAR extends ExtInfoAR
{
    use StatusARTrait, FieldTrait;

    public const KIND_CLASSIC_CARD = 2;
    public const KIND_LEGENDARY_CARD = 3;

    public const PACK_TYPE_CURRENCY = 16;
    public const PACK_TYPE_NOT_CURRENCY = 32;

    public const PACK_TYPE_CHEAP = 1;
    public const PACK_TYPE_AVERAGE = 2;
    public const PACK_TYPE_EXPENSIVE = 4;

    public const EVENT_REGISTRATION = 1;

    public const STATUS_INACTIVE = 16;
    public const STATUS_ACTIVE = 32;

    public const DEFAULT_PROBABILITY_POINT = 1;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%good_kind}}';
    }

    public function rules() // FAN-529
    {
        return [
            [['ext_info'], JsonValidator::class],
        ];
    }

    /**
     * @return null|integer
     */
    public static function kind()
    {
        return null;
    }

    /**
     * @return S3GoodKindAQ
     */
    public static function find()
    {
        $thisKind = static::kind();
        $resultAQ = new S3GoodKindAQ(static::class);

        if ($thisKind !== null) {
            $resultAQ
                ->where(['ent_kind' => $thisKind])
                ->orderBy('id');
        }

        return $resultAQ;
    }

    /**
     * @return array
     */
    public static function getPackStatusesList()
    {
        return [
            self::STATUS_INACTIVE => 'Не активен',
            self::STATUS_ACTIVE => 'Активен',
        ];
    }

    /**
     * @param string $opts
     * @return array
     */
    public static function getPackTypesList($opts = 'descripion')
    {
        static $descripions = [
//            self::PACK_TYPE_CURRENCY => 'Валютный',
            self::PACK_TYPE_CURRENCY | self::PACK_TYPE_CHEAP => 'Валютный, Дешёвый',
            self::PACK_TYPE_CURRENCY | self::PACK_TYPE_AVERAGE => 'Валютный, Средний',
            self::PACK_TYPE_CURRENCY | self::PACK_TYPE_EXPENSIVE => 'Валютный, Дорогой',
            self::PACK_TYPE_NOT_CURRENCY => 'Невалютный',
        ];

        static $codes = [
//            self::PACK_TYPE_CURRENCY => 'Валютный',
            self::PACK_TYPE_CURRENCY | self::PACK_TYPE_CHEAP => 'cheap',
            self::PACK_TYPE_CURRENCY | self::PACK_TYPE_AVERAGE => 'average',
            self::PACK_TYPE_CURRENCY | self::PACK_TYPE_EXPENSIVE => 'expensive',
            self::PACK_TYPE_NOT_CURRENCY => 'free',
        ];

        return $opts === 'descripion' ? $descripions : $codes;
    }

    /**
     * @return array
     */
    public static function getCardStatusesList()
    {
        return [
            self::STATUS_INACTIVE => 'Не активна',
            self::STATUS_ACTIVE => 'Активна',
        ];
    }

    /**
     * @param string $opts
     * @return array
     */
    public static function getCardTypesList($opts = 'descripion')
    {
        $query = CardTypesAR::find()
            ->asArray();

        if ($opts === 'descripion') {
            $query->select(['r_name']);
        } else {
            $query->select(['ent_code']);
        }

        return $query->indexBy('id')
            ->column();
    }

    /**
     * @return array
     */
    public static function getEventsList()
    {
        return [
            self::EVENT_REGISTRATION => 'Регистрация',
        ];
    }
}
