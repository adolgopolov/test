<?php

namespace common\modules\goods;

use backend\components\aboard\MenuEntryInterface;
use common\components\S3DictionaryAR;
use common\components\S3Kind;
use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameLineupCellAR;
use common\modules\goods\components\S3GoodKindAR;
use common\modules\goods\models\UserCardAR;
use common\modules\score\models\CalcTourneyAR;
use yii\base\Module;
use yii\helpers\ArrayHelper;

/**
 * Class GoodsModule
 * @package common\modules\goods
 */
class GoodsModule extends Module implements MenuEntryInterface
{
    /**
     * @return array|null
     */
    public static function getMenuItems(): ?array
    {
        return [
            [
                'label' => 'Вещи',
                'icon' => 'package',
                'color' => 'teal-500',
                'items' => [
                    [
                        'label' => 'Паки',
                        'url' => ['/goods/pack/index'],
                    ],
                    [
                        'label' => 'Карточки',
                        'url' => ['/goods/card/index'],
                    ],
                    [
                        'label' => 'Серии карточек',
                        'url' => ['/goods/card-series/index'],
                    ],
                ],
            ],
        ];
    }

    /*
     * todo сделать три метода модификации для расчета, для спортсмена, для состава
     */
    public function modifyCalc(GameLineupCellAR $cell, array $calcs): GameLineupCellAR
    {
        /** @var CalcTourneyAR[] $calcs */

        $userGoodsIds = $cell->joGet('goods', []);
        $goods = UserCardAR::find()->where([
            'user_id' => $cell->lineup->user_id,
            'ent_kind' => S3Kind::KIND_CARD,
            'r_status' => S3GoodKindAR::STATUS_ACTIVE,
            'id' => $userGoodsIds,
        ])->all();
        // взять все коды SportCalc определить id массив
        $codeCalcs = ArrayHelper::map(S3DictionaryAR::find()->where(['ent_kind' => 4])->forList()->all(), 'id', 'ent_code');

        $modifyPoints = 0;
        foreach ($goods as $eachGood) {
            //todo позицию надо брать по матчу ,а не последнее членство в команде
            $codePosition = $cell->sman->getTouTeamMember($cell->lineup->tourney)->position->ent_code;
            $modifyCalcs = $eachGood->card->buffs[$codePosition] ?? [];
            foreach ($calcs as $eachCalc) {
                // по коду проверяем наличие модифицирующих очков и суммируем их в $modifyPoints умножая на количество фактов
                $code = $codeCalcs[$eachCalc->calc_id] ?? 0;
                if (!empty($modifyCalcs[$code])) {
                    $modifyPoints += $modifyCalcs[$code] * $eachCalc->calc_amount;
                }
            }
        }
        //сумма очков всех расчетов по спортсмену

        $points = ArrayHelper::getColumn($calcs, 'calc_points');
        $cell->sman_points = array_sum($points) + $modifyPoints;
        $cell->joSet('modify_points', (int)$modifyPoints);
        return $cell;
    }

    /**
     * @param $cell
     * @return GameLineupCellAR
     */
    public function modifyCell(GameLineupCellAR $cell): GameLineupCellAR
    {
        return $cell;
    }

    /**
     * @param $lineup
     * @return GameLineupAR
     */
    public function modifyLineup(GameLineupAR $lineup): GameLineupAR
    {
        return $lineup;
    }
}
