<?php

namespace common\modules\goods\senders;

use common\components\pusher\senders\AbstractSender;
use common\components\pusher\traits\BeforeSendTrait;
use common\components\S3Kind;
use common\modules\goods\components\S3UserGoodAR;
use Yii;
use yii\base\InvalidConfigException;

class UserGoodSender extends AbstractSender
{
    use BeforeSendTrait;

    public const SCENARIO_GETTING = 'getting';

    /** @var S3UserGoodAR */
    public $model;

    /**
     * @throws InvalidConfigException
     */
    public function gettingScenario()
    {
        Yii::$app->pushSender->private->update
            ->setSubjectId($this->model->user_id)
            ->setKind(S3Kind::KIND_USER)
            ->addOneRecord('goods', $this->model)
            ->pushJob();
    }
}
