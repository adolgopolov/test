<?php

/**
 * @var $this View
 * @var $model CardAR
 */

use common\modules\goods\components\S3GoodKindAR;
use common\modules\goods\models\CardAR;
use common\modules\score\models\CalcItemAR;
use common\modules\sport\models\SportPositionAR;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;

$this->title = $model->cardName;
$this->params['breadcrumbs'][] = ['label' => 'Карточки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];

$cardCalcs = $model->calcs;
$cardPositions = SportPositionAR::findBySmanOrSport($model->sman, $model->sport_id);

$buffsData = [];
$buffsColumns = [];

foreach ($model->buffs as $position => $facts) {
    if (!isset($cardPositions[$position])) {
        continue;
    }

    $buffRow = [];

    $buffsColumnsPosition[] = [
        'attribute' => 'position',
        'label' => false,
        'format' => 'raw',
        'value' => static function ($item) use ($cardPositions) {
            /** @var bool|SportPositionAR $positionModel */
            $positionModel = $cardPositions[$item['position']] ?? false;

            return $positionModel ? $positionModel->r_name : $item['position'];
        }
    ];

    $buffRow['position'] = $position;

    foreach ((array)$facts as $fact => $buff) {
        /** @var bool|CalcItemAR $calcModel */
        $calcModel = $cardCalcs[$fact] ?? false;

        $buffRow[$fact] = $buff;
        $buffsColumnsPosition[] = [
            'attribute' => $fact,
            'format' => 'raw',
            'label' => $calcModel ? $calcModel->r_name : $fact,
        ];
    }

    if (!$buffsColumns) {
        $buffsColumns = $buffsColumnsPosition;
    }
    $buffsData[] = $buffRow;
}

$buffsProvider = new ArrayDataProvider([
    'allModels' => $buffsData,
]);
?>
<div class="card-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно желаете уделить эту карточку?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'r_name',
            [
                'attribute' => 'r_status',
                'value' => static function (CardAR $model) {
                    $statuses = S3GoodKindAR::getCardStatusesList();

                    return $statuses[$model->r_status] ?? 'Unknown';
                }
            ],
            [
                'attribute' => 'sport_id',
                'value' => static function (CardAR $model) {
                    return $model->sport ? $model->sport->r_name : 'Unknown';
                }
            ],
            [
                'attribute' => 'series',
                'value' => static function (CardAR $model) {
                    return $model->seriesModel->r_name ?? 'Unknown';
                },
            ],
            [
                'attribute' => 'series_color',
                'value' => static function (CardAR $model) {
                    if (!$model->seriesModel) {
                        return null;
                    }

                    return Html::tag('span', $model->seriesModel->color, [
                        'style' => [
                            'color' => $model->seriesModel->color,
                        ],
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'ent_type',
                'value' => static function (CardAR $model) {
                    $cardTypes = S3GoodKindAR::getCardTypesList();

                    return $cardTypes[$model->ent_type] ?? 'Unknown';
                },
            ],
            [
                'attribute' => 'sman_id',
                'enableSorting' => true,
                'value' => static function (CardAR $model) {
                    return $model->sman ? $model->sman->r_name : 'Нет';
                },
            ],
        ],
    ]) ?>

    <h4 class="c-grey-600 mT-10 mB-30">Улучшения</h4>
    <?= GridView::widget([
        'dataProvider' => $buffsProvider,
        'columns' => $buffsColumns,
    ]) ?>
</div>
