<?php

/**
 * @var $this View
 * @var $model CardAR
 * @var $sportList array
 * @var $smenList array
 * @var $series array
 */

use common\modules\goods\models\CardAR;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Создание карточки';
$this->params['breadcrumbs'][] = ['label' => 'Карточки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-ar-create">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
        'sportList' => $sportList,
        'smenList' => $smenList,
        'series' => $series,
    ]) ?>
</div>
