<?php

/**
 * @var $this View
 * @var $calcs array
 * @var $positions array
 * @var $buffs array
 */

use common\modules\score\models\CalcItemAR;
use common\modules\sport\models\SportPositionAR;
use yii\helpers\Html;
use yii\web\View;

$tableHead = null;
$tableHeadTh = Html::tag('th', null, ['scope' => 'row']);
$tableBody = null;
?>
<?= Html::beginTag('table', ['class' => 'table-wrapper-scroll-y table']) ?>
<?php foreach ($positions as $position_code => $position) {
    /** @var SportPositionAR $position */
    $tableRow = Html::tag('td', $position->r_name);
    ?>
    <?php
    foreach ($calcs as $calc_code => $calc) {
        /** @var bool|CalcItemAR $calc */
        $fieldValue = $buffs[$position_code][$calc_code] ?? null;
        $fieldInput = Html::input('number', "buffs[$position_code][$calc_code]", $fieldValue, [
            'class' => 'form-control',
        ]);

        $tableHeadTh .= Html::tag('th', $calc->r_name, ['scope' => 'row']);
        $tableRow .= Html::tag('td', $fieldInput);
    }

    $tableHead = Html::tag('tr', $tableHeadTh);
    $tableBody .= Html::tag('tr', $tableRow);
    $tableHeadTh = Html::tag('th', null, ['scope' => 'row']);
    ?>
<?php } ?>
<?php if ($tableHead && $tableBody) { ?>
    <?= Html::tag('thead', Html::tag('tr', $tableHead)) ?>
    <?= Html::tag('tbody', $tableBody) ?>
    <?= Html::endTag('table') ?>
<?php } ?>

