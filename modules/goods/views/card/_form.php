<?php

/**
 * @var $this View
 * @var $model CardAR
 * @var $sportList array
 * @var $smenList array
 * @var $series array
 */

use common\components\S3Kind;
use common\modules\goods\components\S3GoodKindAR;
use common\modules\goods\models\CardAR;
use common\widgets\ColorPicker\assets\ColorPickerAssets;
use common\widgets\DetailForm\DetailFormWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$classSmanInput = $model->ent_type === S3GoodKindAR::KIND_LEGENDARY_CARD ? 'display-table-cell' : 'display-none';

$cardBuffs = json_encode($model->buffs);

$this->registerJsVar('legendaryCard', S3GoodKindAR::KIND_LEGENDARY_CARD);
$this->registerJsVar('cardBuffs', $cardBuffs);
$this->registerJsFile('/js/goods/cards.js');

ColorPickerAssets::register($this);
?>
<div class="card-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'ent_kind')->hiddenInput([
        'value' => S3Kind::KIND_CARD,
    ])->label(false) ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'r_name',
            'r_status' => Select2::widget([
                'model' => $model,
                'attribute' => 'r_status',
                'data' => S3GoodKindAR::getCardStatusesList(),
                'options' => [
                    'value' => $model->r_status ?: S3GoodKindAR::STATUS_ACTIVE,
                ],
            ]),
            'sport_id' => Select2::widget([
                'model' => $model,
                'attribute' => 'sport_id',
                'data' => $sportList,
                'options' => [
                    'id' => 'card-sport-id-input',
                ],
                'pluginOptions' => [
                    'placeholder' => 'Выберите вид спорта',
                ],
            ]),
            'series' => Select2::widget([
                'model' => $model,
                'attribute' => 'series',
                'data' => $series,
                'options' => [
                    'id' => 'series-input',
                ],
                'pluginOptions' => [
                    'placeholder' => 'Выберите или укажите серию',
                    'tags' => true,
                ],
            ]),
            'series_color' => [
                'inputOptions' => [
                    'id' => 'color-input',
                    'maxlength' => true,
                    'role-color-picker' => true,
                    'autocomplete' => 'off',
                ],
                'inputContainer' => [
                    'class' => 'color-picker-detail-form',
                ],
            ],
            'ent_type' => Select2::widget([
                'model' => $model,
                'attribute' => 'ent_type',
                'data' => S3GoodKindAR::getCardTypesList(),
                'options' => [
                    'id' => 'card-ent-type-input',
                ],
                'pluginOptions' => [
                    'placeholder' => 'Выберите редкость карточки',
                ],
            ]),
            'sman_id' => [
                'inputValue' => Select2::widget([
                    'model' => $model,
                    'attribute' => 'sman_id',
                    'data' => $smenList,
                    'options' => [
                        'id' => 'card-sman-id-input',
                    ],
                    'pluginOptions' => [
                        'placeholder' => 'Выберите спортсмена',
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => [
                    'class' => [
                        'card-sman-id-container',
                        $classSmanInput,
                    ],
                ],
                'captionOptions' => [
                    'class' => [
                        'card-sman-id-container',
                        $classSmanInput,
                    ],
                ],
            ],
        ],
    ]) ?>
    <div id="card-buffs-container"></div>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success pull-right']) ?>
    <?php ActiveForm::end() ?>
</div>
