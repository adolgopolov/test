<?php

/**
 * @var $this View
 * @var $searchModel CardAS
 * @var $dataProvider ActiveDataProvider
 */

use backend\components\aboard\ActionColumn;
use common\modules\goods\components\S3GoodKindAR;
use common\modules\goods\forms\CardAS;
use common\modules\goods\models\CardAR;
use common\modules\goods\models\CardSeriesAR;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportAR;
use common\widgets\GridActions\GridActionsWidget;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\widgets\Pjax;

$this->title = 'Карточки';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    'r_name',
    [
        'attribute' => 'r_status',
        'filter' => S3GoodKindAR::getCardStatusesList(),
        'value' => static function (CardAR $model) {
            $statuses = S3GoodKindAR::getCardStatusesList();

            return $statuses[$model->r_status] ?? 'Unknown';
        }
    ],
    [
        'attribute' => 'ent_type',
        'filter' => S3GoodKindAR::getCardTypesList(),
        'value' => static function (CardAR $model) {
            $cardTypes = S3GoodKindAR::getCardTypesList();

            return $cardTypes[$model->ent_type] ?? 'Unknown';
        }
    ],
    [
        'attribute' => 'sport_id',
        'filter' => SportAR::getAsOptsList(),
        'value' => static function (CardAR $model) {
            return $model->sport->r_name ?? 'Unknown';
        }
    ],
    [
        'attribute' => 'series_id',
        'filter' => CardSeriesAR::getList(),
        'value' => static function (CardAR $model) {
            return $model->seriesModel->r_name ?? 'Unknown';
        }
    ],
    [
        'attribute' => 'sman_id',
        'filter' => SmanAR::getSmenList(),
        'value' => static function (CardAR $model) {
            return $model->sman->r_name ?? 'Нет';
        }
    ],
    [
        'class' => ActionColumn::class,
    ],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="card-ar-index">
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#goods-card-pjax',
        'pjaxUrl' => '/goods/card/index',
        'model' => $searchModel,
        'additionalContainerOptions' => [
            'class' => [
                'columns-select2-margined',
            ],
        ],
    ]) ?>
    <?= GridActionsWidget::widget([
        'title' => $this->title,
        'enableChangeStatus' => true,
        'statusesList' => S3GoodKindAR::getCardStatusesList(),
    ]) ?>
    <?php Pjax::begin(['id' => 'goods-card-pjax']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => ArrayHelper::merge([GridActionsWidget::$checkboxColumn], $columns),
    ]) ?>
    <?php Pjax::end() ?>
</div>
