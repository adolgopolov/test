<?php

/**
 * @var $this View
 * @var $searchModel PackAS
 * @var $dataProvider ActiveDataProvider
 * @var $modelDoing GivePackFM
 * @var $userList array
 * @var $packList array
 */

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use backend\components\aboard\ActionColumn;
use common\modules\goods\components\S3GoodKindAR;
use common\modules\goods\forms\GivePackFM;
use common\modules\goods\forms\PackAS;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\widgets\GridActions\GridActionsWidget;
use kartik\select2\Select2;

$this->title = 'Паки';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    'r_name',
    'r_text',
    [
        'attribute' => 'r_status',
        'value' => static function (PackAS $model) {
            $statuses = S3GoodKindAR::getPackStatusesList();

            return $statuses[$model->r_status] ?? 'Unknown';
        }
    ],
    [
        'attribute' => 'r_icon',
        'value' => static function (PackAS $model) {
            return $model->htmlImage();
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'ent_type',
        'value' => static function (PackAS $model) {
            $types = S3GoodKindAR::getPackTypesList();

            return $types[$model->ent_type] ?? 'Unknown';
        }
    ],
    [
        'attribute' => 'sports',
        'value' => static function (PackAS $model) {
            $sports = $model->getSportModels()
                ->select(['r_name'])
                ->indexBy('id')
                ->column();

            return implode(', ', $sports);
        }
    ],
    [
        'attribute' => 'leagues',
        'value' => static function (PackAS $model) {
            $leagues = $model->getLeagueModels()
                ->select(['r_name'])
                ->indexBy('id')
                ->column();

            foreach ($leagues as $id => $r_name) {
                $leagues[$id] = Html::a($r_name, ['/sport/league/view', 'id' => $id]);
            }

            return implode(', ', $leagues);
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'teams',
        'value' => static function (PackAS $model) {
            $teams = $model->getTeamModels()
                ->select(['r_name'])
                ->indexBy('id')
                ->column();

            foreach ($teams as $id => $r_name) {
                $teams[$id] = Html::a($r_name, ['/sport/team/view', 'id' => $id]);
            }

            return implode(', ', $teams);
        },
        'format' => 'raw',
    ],
    ['class' => ActionColumn::class]
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="pack-ar-index">
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#goods-pack-pjax',
        'pjaxUrl' => '/goods/pack/index',
        'model' => $searchModel,
        'additionalContainerOptions' => [
            'class' => [
                'columns-select2-margined',
            ],
        ],
    ]) ?>
    <?= GridActionsWidget::widget([
        'title' => $this->title,
        'enableChangeStatus' => true,
        'statusesList' => S3GoodKindAR::getPackStatusesList(),
    ]) ?>
    <?php Pjax::begin(['id' => 'goods-pack-pjax']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => ArrayHelper::merge([GridActionsWidget::$checkboxColumn], $columns),
    ]) ?>
    <?php Pjax::end() ?>
    <h4 class="c-grey-900 mT-10 mB-30">Подарить пак пользователю</h4>
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($modelDoing, 'user_id')->widget(Select2::class, [
        'data' => $userList,
        'pluginOptions' => [
            'placeholder' => 'Выберите пользователя',
        ],
    ]) ?>
    <?= $form->field($modelDoing, 'pack_id')->widget(Select2::class, [
        'data' => $packList,
        'pluginOptions' => [
            'placeholder' => 'Выберите пак',
        ],
    ]) ?>
    <?= Html::submitButton('Подарить пак', ['class' => 'btn btn-success']) ?>
    <?php ActiveForm::end() ?>
</div>
