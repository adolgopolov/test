<?php

/**
 * @var $this View
 * @var $model PackAR
 * @var $packSmen array
 * @var $seriesProvider ActiveDataProvider
 * @var $cardsProvider ArrayDataProvider
 */

use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;
use common\modules\goods\components\S3GoodKindAR;
use common\modules\goods\models\CardAR;
use common\modules\goods\models\PackAR;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Паки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];
?>
<div class="pack-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'r_name',
            'r_text',
            [
                'attribute' => 'r_status',
                'value' => static function (PackAR $model) {
                    $statuses = S3GoodKindAR::getPackStatusesList();

                    return $statuses[$model->r_status] ?? 'Unknown';
                }
            ],
            [
                'attribute' => 'r_icon',
                'value' => static function (PackAR $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'ent_type',
                'value' => static function (PackAR $model) {
                    $types = S3GoodKindAR::getPackTypesList();

                    return $types[$model->ent_type] ?? 'Unknown';
                }
            ],
            [
                'attribute' => 'sports',
                'value' => static function (PackAR $model) {
                    $sports = $model->getSportModels()
                        ->select(['r_name'])
                        ->indexBy('id')
                        ->column();

                    return implode(', ', $sports);
                }
            ],
            [
                'attribute' => 'leagues',
                'value' => static function (PackAR $model) {
                    $leagues = $model->getLeagueModels()
                        ->select(['r_name'])
                        ->indexBy('id')
                        ->column();

                    foreach ($leagues as $id => $r_name) {
                        $leagues[$id] = Html::a($r_name, ['/sport/league/view', 'id' => $id]);
                    }

                    return implode(', ', $leagues);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'teams',
                'value' => static function (PackAR $model) {
                    $teams = $model->getTeamModels()
                        ->select(['r_name'])
                        ->indexBy('id')
                        ->column();

                    foreach ($teams as $id => $r_name) {
                        $teams[$id] = Html::a($r_name, ['/sport/team/view', 'id' => $id]);
                    }

                    return implode(', ', $teams);
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

    <h3 class="c0grey-600 mT-10 mB-30">Серии</h3>
    <?= GridView::widget([
        'dataProvider' => $seriesProvider,
        'columns' => [
            'id',
            'r_name',
        ],
    ]) ?>

    <h3 class="c-grey-600 mT-10 mB-30">Карточки</h3>
    <?= GridView::widget([
        'dataProvider' => $cardsProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            'r_name',
            [
                'attribute' => 'r_status',
                'value' => static function (CardAR $model) {
                    $statuses = S3GoodKindAR::getCardStatusesList();

                    return $statuses[$model->r_status] ?? 'Unknown';
                }
            ],
            [
                'attribute' => 'ent_type',
                'value' => static function (CardAR $model) {
                    $cardTypes = S3GoodKindAR::getCardTypesList();

                    return $cardTypes[$model->ent_type] ?? 'Unknown';
                }
            ],
            [
                'attribute' => 'sport_id',
                'value' => static function (CardAR $model) {
                    return $model->sport ? $model->sport->r_name : 'Unknown';
                }
            ],
            [
                'attribute' => 'series_id',
                'value' => static function (CardAR $model) {
                    return $model->seriesModel->r_name ?? 'Unknown';
                },
            ],
            [
                'attribute' => 'smen',
                'value' => static function (CardAR $cardModel) use ($model, $packSmen) {
                    if ($cardModel->sman) {
                        return Html::a($cardModel->sman->getTitle(), ['/sport/sman/view', 'id' => $cardModel->sman->id]);
                    }

                    $smenIds = array_keys(array_filter($model->smen, static function ($value) use ($cardModel) {
                        return in_array($cardModel->series, PackAR::getSmanSeries($value, $cardModel->ent_type));
                    }));

                    $smen = [];

                    foreach ($smenIds as $smanId) {
                        $smanName = $packSmen[$smanId] ?? $smanId;
                        $smen[$smanId] = Html::a($smanName, ['/sport/sman/view', 'id' => $smanId]);
                    }

                    return implode(', ', $smen);
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>
</div>
