<?php

/**
 * @var $this View
 * @var $model PackAR|ActiveRecord
 * @var $currencies MoneyCurrencyAR
 */

use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use common\components\S3Kind;
use common\modules\goods\components\S3GoodKindAR;
use common\modules\goods\models\CardSeriesAR;
use common\modules\goods\models\PackAR;
use common\modules\money\models\MoneyCurrencyAR;
use common\modules\sport\models\SportAR;
use common\widgets\image\ImageWidget;

$this->registerJsVar('smenList', $model->smen);
$this->registerJsFile('/js/goods/goods.js', ['depends' => JqueryAsset::class]);

$params = json_encode(Yii::$app->request->queryParams);
?>
<?php $form = ActiveForm::begin([
    'id' => 'pack-edit-form',
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
]) ?>

<?= $form->field($model, 'ent_kind')->hiddenInput([
    'value' => S3Kind::KIND_PACK,
])->label(false) ?>

<?= $form->field($model, 'r_name')->textInput() ?>

<?= $form->field($model, 'r_text')->textarea() ?>

<?= $form->field($model, 'r_status')->widget(Select2::class, [
    'data' => S3GoodKindAR::getPackStatusesList(),
    'options' => [
        'value' => $model->r_status ?: S3GoodKindAR::STATUS_ACTIVE,
    ],
]) ?>

<?php if (!$model->isNewRecord) { ?>
    <?= $form->field($model, 'r_icon')->widget(ImageWidget::class, [
        'kind' => PackAR::kind(),
    ]) ?>
<?php } ?>

<?= $form->field($model, 'ent_kind')->hiddenInput([
    'value' => S3Kind::KIND_PACK,
])->label(false) ?>

<?= $form->field($model, 'ent_type')->widget(Select2::class, [
    'data' => S3GoodKindAR::getPackTypesList(),
    'options' => [
        'id' => 'pack-type',
    ],
]) ?>

<?= $form->field($model, 'series')->widget(Select2::class, [
    'data' => CardSeriesAR::getList(),
    'options' => [
        'id' => 'series-input',
    ],
    'pluginOptions' => [
        'placeholder' => 'Выберите серии',
        'multiple' => true,
    ],
]) ?>

<?= $form->field($model, 'sports')->widget(Select2::class, [
    'data' => SportAR::getAsOptsList(),
    'options' => [
        'id' => 'sports-dd',
    ],
    'pluginOptions' => [
        'placeholder' => 'Выберите вид спорта',
        'multiple' => true,
    ],
]) ?>

<?= $form->field($model, 'leagues')->widget(DepDrop::class, [
    'options' => ['id' => 'leagues-dd'],
    'pluginOptions' => [
        'depends' => ['sports-dd'],
        'url' => Url::to(['/goods/pack/ajax-leagues', 'id' => $model->id]),
        'initialize' => !$model->isNewRecord,
    ],
    'select2Options' => [
        'pluginOptions' => [
            'placeholder' => 'Выберите лигу',
            'multiple' => true,
        ],
    ],
    'type' => DepDrop::TYPE_SELECT2,
]) ?>

<?= $form->field($model, 'teams')->widget(DepDrop::class, [
    'options' => ['id' => 'teams-dd'],
    'pluginOptions' => [
        'depends' => ['leagues-dd'],
        'url' => Url::to(['/goods/pack/ajax-teams', 'id' => $model->id]),
        'initialize' => !$model->isNewRecord,
    ],
    'select2Options' => [
        'pluginOptions' => [
            'placeholder' => 'Выберите список команд',
            'multiple' => true,
        ],
    ],
    'type' => DepDrop::TYPE_SELECT2,
]) ?>

<div class="card card-body">
    <h2 class="mb-0">
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#accordionCards"
                aria-expanded="true" aria-controls="accordionCards">
            Выбор спортсменов
            <i class="fa fa-arrow-down"></i>
        </button>
    </h2>
    <div id="accordionCards" class="collapse" aria-labelledby="headingOne">
        <?php Pjax::begin([
            'enablePushState' => false,
            'enableReplaceState' => false,
            'id' => 'series-container',
        ]) ?>
        <?php Pjax::end() ?>
    </div>
</div>

<?= $form->field($model, 'count')->input('number', ['min' => 1, 'max' => 5]) ?>

<div id="prices-input">
    <h5 class="c-grey-600 mT-10 mB-20">Цены</h5>
    <?php foreach ($currencies as $currency) {
        /** @var $currency MoneyCurrencyAR */
        ?>
        <?= $form->field($model, 'prices[' . $currency->id . ']')->input('number')->label($currency->r_name) ?>
    <?php } ?>
</div>

<div id="event-input">
    <?= $form->field($model, 'event')->widget(Select2::class, [
        'data' => S3GoodKindAR::getEventsList(),
        'pluginOptions' => [
            'placeholder' => 'Выберите событие',
        ],
    ]) ?>
</div>

<?= Html::submitButton($model->isNewRecord ? 'Создать пак' : 'Сохранить изменения', [
    'class' => 'btn btn-success',
]) ?>

<?php ActiveForm::end() ?>
