<?php

/**
 * @var $this View
 * @var $model PackAR|ActiveRecord
 */

use common\modules\goods\models\CardAR;
use common\modules\goods\models\PackAR;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'action' => Url::to(['/goods/pack/ajax-update-probability', 'id' => $model->id]),
    'method' => 'POST',
    'options' => [
        'class' => 'js-form-attach-entity text-left',
    ],
]) ?>

<h4 class="c-grey-700 mT-10 mB-10">Веса вероятностей</h4>
<?php if(!empty($model->probability)):?>
<?php foreach ($model->probability as $card => $probability) {
    $cardModel = CardAR::getCard($card);
    $cardName = $cardModel ? 'ID: ' . $cardModel->id . ', ' . $cardModel->cardName : $card;
    ?>
    <?= $form->field($model, "probability[$card]")->input('number', [
        'value' => $probability
    ])->label($cardName) ?>
<?php } ?>
<?php endif;?>
<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success js-submit-attach-entity']) ?>

<div class="alert alert-primary mT-10">
    В поля весов вероятностей нужно вводить числа. Чем больше число, тем вероятнее выпадет
    карточка.
    Например, у серебряной указано - 3, а у золотой - 1; это означает, что серебряная карточка будет выпадать в 75%
    случаев, а в осставшихся 25% - золотая.
</div>

<?php ActiveForm::end() ?>
