<?php

/**
 * @var $this View
 * @var $seriesModels array
 * @var $smanProvider ActiveDataProvider
 * @var $smanSearch SmanAS
 * @var $smenList array
 * @var $smenArray array
 */

use common\modules\goods\components\S3GoodKindAQ;
use common\modules\goods\models\CardSeriesAR;
use common\modules\goods\models\CardTypesAR;
use common\modules\sport\forms\SmanAS;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

$seriesColumns = [];

foreach ($seriesModels as $seriesModel) {
    /**
     * @var CardSeriesAR $seriesModel
     * @var S3GoodKindAQ[] $cardTypes
     */
    $cardTypes = $seriesModel->getCardTypes()->all();
    $seriesAllSmanIds = array_keys($seriesModel->getSeriesSmen());

    foreach ($cardTypes as $cardType) {
        /** @var CardTypesAR $cardType */
        $seriesLabel = $seriesModel->r_name . ' (' . $cardType->r_name . ')';
        $columnClass = 'checkbox-' . $seriesModel->id . '-' . $cardType->id;

        $seriesColumns[] = [
            'label' => $seriesLabel,
            'value' => static function (SmanAS $model) use ($seriesModel, $smenArray, $cardType, $columnClass) {
                $checked = isset($smenArray[$model->id][$cardType->id]) && in_array($seriesModel->id, $smenArray[$model->id][$cardType->id]);

                $seriesSmanIds = array_keys($seriesModel->getSeriesSmen($cardType->id));
                $seriesSports = array_keys($seriesModel->getSportModels()->indexBy('id')->all());
                $hasEmptySman = $seriesModel->hasEmptySman($cardType->id);

                $fieldInput = '';
                if (($hasEmptySman xor in_array($model->id, $seriesSmanIds))
                    && in_array($model->sport->id, $seriesSports)) {
                    return Html::checkbox("smen[$model->id][$cardType->id][$seriesModel->id]", $checked, [
                        'class' => ['form-control', $columnClass],
                    ]);
                }

                return $fieldInput;
            },
            'filter' => Html::tag('label', 'Выбрать все' . Html::checkbox('smenAll[]', false, [
                    'class' => 'form-control',
                    'data' => [
                        'check-all' => '.' . $columnClass,
                    ],
                ])),
            'filterOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
        ];
    }
}
?>
<?= GridView::widget([
    'dataProvider' => $smanProvider,
    'filterModel' => $smanSearch,
    'columns' => ArrayHelper::merge([
        [
            'attribute' => 'r_name',
            'label' => 'Спортсмены',
            'enableSorting' => true,
            'filter' => Select2::widget([
                'attribute' => 'id',
                'data' => $smenList,
                'model' => $smanSearch,
                'options' => [
                    'id' => 'sman-filter-input',
                ],
                'pluginOptions' => [
                    'multiple' => true,
                    'allowClear' => true,
                ],
            ]),
            'value' => static function (SmanAS $model) {
                return Html::a($model->getTitle(), ['/sport/sman/view', 'id' => $model->id], [
                        'target' => '_blank',
                    ]) . ' (' . ($model->lastTeamMember->position->r_name ?? 'Unknown') . ')';
            },
            'format' => 'raw',
        ],
    ], $seriesColumns),
]) ?>
