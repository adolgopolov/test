<?php

/**
 * @var $this View
 * @var $model PackAR
 * @var $currencies MoneyCurrencyAR
 */

use common\modules\goods\models\PackAR;
use common\modules\money\models\MoneyCurrencyAR;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Создание пака';
$this->params['breadcrumbs'][] = ['label' => 'Паки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pack-ar-create">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
        'currencies' => $currencies,
    ]) ?>
</div>
