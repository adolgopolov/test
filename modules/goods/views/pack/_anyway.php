<?php

/**
 * @var $this View
 * @var $model PackAR
 * @var $anywayProvider ArrayDataProvider
 * @var $availableCards array
 */

use common\modules\goods\models\PackAR;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use common\modules\goods\components\S3GoodKindAR;
use kartik\select2\Select2;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

?>
<?php Pjax::begin(['id' => 'anyway-pjax']) ?>

<div class="text-right">
    <?php Modal::begin([
        'title' => '<h5 class="c-grey-900">Обязательная карточка</h5>',
        'size' => Modal::SIZE_LARGE,
        'id' => 'anyway-modal',
        'toggleButton' => [
            'label' => '<span class="fa fa-plus"></span> Добавить обязательную карточку',
            'class' => 'btn btn-outline-success mB-10',
        ],
    ]) ?>
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/goods/pack/ajax-form-anyway', 'id' => $model->id]),
        'method' => 'POST',
        'options' => [
            'class' => 'js-form-attach-entity text-left',
            'data-pjax-selector' => '#anyway-pjax',
        ],
    ]) ?>

    <?= $form->field($model, 'anywayCardUpdate')->hiddenInput([
        'id' => 'anyway-card-update',
    ])->label(false) ?>

    <div id="anyway-card-container">
        <?= $form->field($model, 'anywayCard')->widget(Select2::class, [
            'data' => $availableCards,
            'options' => [
                'id' => 'anyway-card',
            ],
        ]) ?>
    </div>

    <?= $form->field($model, 'anywayCount')->input('number', [
        'id' => 'anyway-count',
        'value' => 1,
        'min' => 1,
    ]) ?>

    <div class="text-right">
        <?= Html::submitButton('<span class="fa fa-save"></span> Сохранить', ['class' => 'btn btn-success js-submit-attach-entity']) ?>
    </div>

    <?php ActiveForm::end() ?>
    <?php Modal::end() ?>
</div>

<?= GridView::widget([
    'dataProvider' => $anywayProvider,
    'tableOptions' => [
        'class' => 'table table-bordered table-hover',
    ],
    'columns' => [
        [
            'attribute' => 'card',
            'label' => 'Тип карточки',
            'contentOptions' => [
                'class' => 'clickable-column',
            ],
            'value' => static function ($model) {
                $cardTypes = S3GoodKindAR::getCardTypesList();

                return $cardTypes[$model['card']] ?? 'Unknown';
            },
        ],
        [
            'attribute' => 'count',
            'label' => 'Количество',
            'contentOptions' => [
                'class' => 'clickable-column',
            ],
        ],
        [
            'value' => static function ($item) use ($model) {
                return Html::button(Html::tag('span', null, ['class' => 'fa fa-trash']), [
                    'class' => 'btn btn-outline-danger js-btn-detach-entity',
                    'data' => [
                        'confirm-message' => 'Вы действительно хотите удалить этту обязательную карточку из настроек пака?',
                        'child' => $item['card'],
                        'url' => Url::to(['/goods/pack/ajax-delete-anyway', 'id' => $model->id]),
                        'pjax-selector' => '#anyway-pjax',
                    ],
                ]);
            },
            'format' => 'raw'
        ],
    ],
    'rowOptions' => static function ($item) use ($model) {
        return [
            'data' => [
                'update-anyway' => $model->id,
                'card' => $item['card'],
                'count' => $item['count'],
            ],
        ];
    },
]) ?>

<?php Pjax::end() ?>
