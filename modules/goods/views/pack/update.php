<?php

/**
 * @var $this View
 * @var $model PackAR
 * @var $currencies MoneyCurrencyAR
 * @var $anywayProvider ArrayDataProvider
 * @var $availableCards array
 */

use common\modules\goods\models\PackAR;
use common\modules\money\models\MoneyCurrencyAR;
use kartik\tabs\TabsX;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Редактирование пака: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Паки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
?>
<div class="pack-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= TabsX::widget([
        'items' => [
            [
                'label' => '<span class="ti ti-pencil"></span> Основная информация',
                'active' => true,
                'content' => $this->render('_form', [
                    'model' => $model,
                    'currencies' => $currencies,
                ]),
            ],
            [
                'label' => '<span class="ti ti-layout-grid3"></span> Обязательные карточки',
                'content' => $this->render('_anyway', [
                    'model' => $model,
                    'anywayProvider' => $anywayProvider,
                    'availableCards' => $availableCards,
                ]),
            ],
            [
                'label' => '<span class="ti ti-layout-grid3"></span> Вероятности выпадения карточек',
                'content' => $this->render('_probabilities', [
                    'model' => $model,
                ]),
            ],
        ],
        'encodeLabels' => false,
    ]) ?>
</div>
