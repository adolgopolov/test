<?php

/**
 * @var $this View
 * @var $model CardSeriesAR
 */

use common\modules\goods\models\CardSeriesAR;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;

$this->title = $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Серии карточек', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];
?>
<div class="card-series-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить эту запись?',
                'method' => 'POST',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'r_name',
            [
                'attribute' => 'color',
                'value' => static function (CardSeriesAR $model) {
                    return Html::tag('span', $model->color, [
                        'style' => [
                            'color' => $model->color,
                        ],
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'r_icon',
                'value' => static function (CardSeriesAR $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
            'cardsCount',
        ],
    ]) ?>
</div>
