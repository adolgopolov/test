<?php

/**
 * @var $this View
 * @var $model CardSeriesAR
 */

use common\components\S3Kind;
use common\modules\goods\models\CardSeriesAR;
use common\widgets\ColorPicker\assets\ColorPickerAssets;
use common\widgets\DetailForm\DetailFormWidget;
use common\widgets\image\ImageWidget;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

ColorPickerAssets::register($this);
?>
<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
]) ?>
<?= DetailFormWidget::widget([
    'model' => $model,
    'inputs' => [
        'id' => [
            'inputHidden' => $model->isNewRecord,
            'inputOptions' => [
                'disabled' => true,
            ],
        ],
        'r_name',
        'color' => [
            'inputOptions' => [
                'maxlength' => true,
                'role-color-picker' => true,
                'autocomplete' => 'off',
            ],
            'inputContainer' => [
                'class' => 'color-picker-detail-form',
            ],
        ],
        'r_icon' => ImageWidget::widget([
            'model' => $model,
            'attribute' => 'r_icon',
            'kind' => CardSeriesAR::kind(),
            'alwaysShow' => true,
        ]),
    ],
]) ?>
<?= $form->field($model, 'ent_kind')->hiddenInput(['value' => S3Kind::KIND_CARD_SERIES])->label(false) ?>
<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end() ?>
