<?php

/**
 * @var $this View
 * @var $searchModel CardSeriesAS
 * @var $dataProvider ActiveDataProvider
 */

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use common\modules\goods\forms\CardSeriesAS;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\widgets\GridActions\GridActionsWidget;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

$this->title = 'Серии карточек';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    'r_name',
    [
        'attribute' => 'color',
        'value' => static function (CardSeriesAS $model) {
            return Html::tag('span', $model->color, [
                'style' => [
                    'color' => $model->color,
                ],
            ]);
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'r_icon',
        'value' => static function (CardSeriesAS $model) {
            return $model->htmlImage();
        },
        'format' => 'raw',
    ],
    'cardsCount',
    ['class' => ActionColumn::class],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="card-series-ar-index">
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#goods-card-series-pjax',
        'pjaxUrl' => '/goods/card-series/index',
        'model' => $searchModel,
    ]) ?>
    <?= GridActionsWidget::widget([
        'title' => $this->title,
    ]) ?>
    <?php Pjax::begin(['id' => 'goods-card-series-pjax']) ?>
    <?= GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => ArrayHelper::merge([GridActionsWidget::$checkboxColumn], $columns),
    ]) ?>
    <?php Pjax::end() ?>
</div>
