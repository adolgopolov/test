<?php

/**
 * @var $this View
 * @var $model CardSeriesAR
 */

use common\modules\goods\models\CardSeriesAR;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Добавление новой серии';
$this->params['breadcrumbs'][] = ['label' => 'Серии карточек', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-series-ar-create">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
