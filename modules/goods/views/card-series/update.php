<?php

/**
 * @var $this View
 * @var $model CardSeriesAR
 */

use common\modules\goods\models\CardSeriesAR;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Редактирование серии: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Серии карточек', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->r_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
?>
<div class="card-series-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
]) ?>
</div>
