<?php

namespace common\modules\money\models;

use Yii;
use common\validators\JsonValidator;

/**
 * Class OrderAR
 *
 * @package common\modules\money\models
 */
class OrderAR extends UserMoneyAccAR
{
    const ORDER_NUM_FIELDS = ['user_id', 'product_id', 'product_kind', 'product_amount', 'currency_amount'];

    protected function extInfoRules()
    {
        return [
            ['ext_info', JsonValidator::class, 'rules' => [
                'order.order_md'  => 'safe',
                'order.order_num' => 'safe',
                'order.pay_gate' => 'required',
            ]]
        ];
    }

    /**
     * @return array
     */
    protected function jsonFields(): array
    {
        return array_merge(parent::jsonFields(), [
            'order',
        ]);
    }

    /**
     * @param array $order
     * @return OrderAR
     */
    public static function new(array $order): ?OrderAR
    {
        $payment = new OrderAR([
            'user_id' => $order['user_id'],
            'r_status' => OrderAR::PAYMENT_PENDING,
            'acc_id' => $order['currency_id'],
            'acc_value' => $order['currency_amount'],
            'order' => [
                'product_id' => $order['product_id'],
                'product_kind' => $order['product_kind'],
                'product_amount' => $order['product_amount'],
                'pay_gate' => $order['pay_gate'] ?? self::PAYGATE_SBERBANK,
                'description' => $order['description'] ?? null,
                'order_md' => $order['order_md'] ?? null,
                'order_num' => $order['order_num'] ?? null,
            ]
        ]);

        if (empty($payment->acc_value)) {
            Yii::warning(['currency_amount is empty' => $order], 'ss3-money');
        }

        return $payment;
    }

    /**
     * @return UserMoneyAccAQ
     */
    public static function find()
    {
        return (new UserMoneyAccAQ(static::class))->whereJO(['order' => 'not null']);
    }

    /**
     * @param string $orderNum
     * @return array|false
     */
    public static function parserOrderNum(string $orderNum)
    {
        $array = preg_split('/[i-v]/', $orderNum);

        return array_combine(self::ORDER_NUM_FIELDS, array_map('hexdec', $array));
    }

    public function beforeSave($insert)
    {
        if ($insert) $this->createdAtTouch();
        return parent::beforeSave($insert);
    }
    /*
var order = {
    user_id: 321, product_id: 3, product_kind: 51, product_amount: 100, currency_amount: 100
};

function getOrderNum(order)
{
    var characters       = 'ijklmnopqrstuv';
    var charactersLength = characters.length;

    function hexCodeDelim(field, delim)
    {
        var res = field.toString(16);
        if (delim) {
            res += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return res;
    }

    return hexCodeDelim(order.user_id, true)
     + hexCodeDelim(order.product_id, true)
     + hexCodeDelim(order.product_kind, true)
     + hexCodeDelim(order.product_amount, true)
     + hexCodeDelim(order.currency_amount, false)
      ;
}

console.log(getOrderNum(order));
     */
}
