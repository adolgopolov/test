<?php

namespace common\modules\money\models;

use yii\data\ActiveDataProvider;
use yii\db\Expression;
use common\modules\money\forms\UserMoneyAccFM;

/**
 * Class UserMoneyAccAS
 * @package common\modules\money\models
 */
class UserMoneyAccAS extends UserMoneyAccAR
{
    /**
     * @param int $id
     * @return ActiveDataProvider
     */
    public function getUserTransactions(int $id)
    {
        return new ActiveDataProvider([
            'query' => UserMoneyAccFM::find()->where(['user_id' => $id]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    /**
     * @param int $id
     * @return ActiveDataProvider
     */
    public function getTourneyTransactionsByUser(int $id)
    {
        $query = UserMoneyAccFM::find()
            ->where(['user_id' => $id])
            ->andWhere(['IS NOT', 'tou_id', null]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    /**
     * @param $id
     * @return ActiveDataProvider
     */
    public function getTransactionsBuyCurrency($id)
    {
        $table = self::tableName();
        $query = UserMoneyAccFM::find()
            ->where(['user_id' => $id])
            ->andWhere(['IS NOT', new Expression("JSON_EXTRACT({$table}.ext_info, '$.order')"), null]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }
}
