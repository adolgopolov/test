<?php

namespace common\modules\money\models;

use common\components\{S3DictionaryAR, S3Kind};
use common\components\FindOrFailTrait;

/**
 *
 */
class MoneyAccountAR extends S3DictionaryAR
{
    use FindOrFailTrait;

    public static function kind()
    {
        return S3Kind::KIND_ACCOUNT;
    }

    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'ent_kind'  => 'Kind',
            'ent_code'  => 'Код',
            'ent_value' => 'Значение',
            'r_name'    => 'Название'
        ];
    }

    public function rules()
    {
        return [
            ['id', 'integer'],
            [['ent_code', 'r_name', 'ent_value'], 'string'],
            ['id', 'unique', 'targetAttribute' => ['id', 'ent_kind']]
        ];
    }
}
