<?php

namespace common\modules\money\models;

use Exception;
use common\extensions\ExtAQ;
use common\modules\user\senders\UserBalanceSender;

/**
 * This is the ActiveQuery class for [[UserBalanceAR]].
 *
 * @see UserBalanceAR
 */
class UserBalanceAQ extends ExtAQ
{
    public function filterUser($userId)
    {
        return $this->andWhere(['user_id' => $userId ]);
    }

    /**
     * @param $manyUsers
     * @return array
     * [account1=>summ1, account2=>summ2]
     * $manyUsers == true: [user_id=>[account1=>summ1, account2=>summ2]]
     */
    public function balances($manyUsers = false) : array
    {
        $ars = $this->all();
        $result = [];

        if (empty($ars)) return $result;

        foreach ($ars as $eachAccount)
        {
            /** @var UserBalanceAR $eachAccount */
            if ($manyUsers) {
                $result[$eachAccount->user_id][$eachAccount->acc_id] = $eachAccount->acc_value;
            }
            else {
                $result[$eachAccount->acc_id] = $eachAccount->acc_value;
            }
        }

        return $result;
    }

    private $recalcMode, $enSend;
    public function strong($reMode = true, $enableSend = true)
    {
        $this->recalcMode = $reMode;
        $this->enSend = $enableSend;
        return $this;
    }

    public function all($db = null)
    {
        if ($this->recalcMode && $this->asArray) {
            throw new Exception('asArray is disabled when recalcMode==true');
        }

        $result = parent::all($db);

        if ($this->recalcMode && !empty($result)) {
            $this->recalcBalance($result);
        }

        return $result;
    }

    public function one($db = null)
    {
        if ($this->recalcMode && $this->asArray) {
            throw new Exception('asArray is disabled when recalcMode==true');
        }

        $result = parent::one($db);

        if ($this->recalcMode && !empty($result)) {
            $this->recalcBalance([$result]);
        }

        return $result;
    }

    protected function recalcBalance(array $balances)
    {
        foreach ($balances as $eachAccount) {
            /** @var UserBalanceAR $eachAccount */
            if ($eachAccount->is_require_calc) {
                $oldValue = $eachAccount->acc_value;
                $eachAccount->updateBalance();
                $clonedModel = clone $eachAccount;
                if ($eachAccount->save() && $this->enSend && $oldValue != $eachAccount->acc_value) {
                    $eachAccount->sendModel(UserBalanceSender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);
                }
            }
        }
    }
}
