<?php

namespace common\modules\money\models;

use common\components\{S3DictionaryAR, S3Kind};
use common\components\FindOrFailTrait;

class MoneyCurrencyAR extends S3DictionaryAR
{
    use FindOrFailTrait;

    public static function kind()
    {
        return S3Kind::KIND_CURRENCY;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'ent_kind'  => 'Kind',
            'ent_code'  => 'Код',
            'ent_value' => 'Значение',
            'r_name'    => 'Название'
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [['ent_code', 'r_name', 'ent_value'], 'string'],
            ['id', 'unique', 'targetAttribute' => ['id', 'ent_kind']]
        ];
    }

    /**
     * @return array
     */
    public static function getList()
    {
        return self::find()
            ->select(['r_name'])
            ->indexBy('id')
            ->column();
    }
}
