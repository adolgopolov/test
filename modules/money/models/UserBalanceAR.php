<?php

namespace common\modules\money\models;

use Exception;
use Yii;
use common\components\pusher\interfaces\SenderModelInterface;
use common\components\pusher\senders\AbstractSender;
use common\extensions\ExtInfoAR;
use common\modules\money\MoneyModule;
use common\modules\user\models\UserAR;
use common\modules\user\senders\UserBalanceSender;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%pay_balance}}".
 *
 * @property int $user_id
 * @property int $acc_id
 * @property int $r_status
 * @property int $is_require_calc
 * @property int $acc_value
 *
 * @property UserAR $gamer
 */
class UserBalanceAR extends ExtInfoAR implements SenderModelInterface
{
    use AccountTrait;

    public const STATUS_PROCESSING = 8;
    public const STATUS_COMPLETED = 32;

    /*
     *  @brief для хранения состояния виртуального поля
     */
    private $DELETE_actual;

    protected static $statusTexts = [
        self::STATUS_PROCESSING => 'требует пересчета',
        self::STATUS_COMPLETED => 'актуальное состояние',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%pay_balance}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return $this->strongRules([
            [['acc_id', 'acc_value', 'r_status'], 'integer'],
            [['user_id', 'acc_id',], 'required'],
            [['acc_id'], 'in', 'range' => array_keys(MoneyModule::accountLabel())],
            [['r_status'], 'in', 'range' => array_keys(self::$statusTexts)],
        ], [
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserAR::class, 'targetAttribute' => ['user_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_status' => 'R Status',
            'user_id' => 'User ID',
            'acc_id' => 'Account',
            'acc_value' => 'Acc Value',
            'is_require_calc' => 'Is Require Calc',

        ];
    }

    /**
     * @param string $scenario
     * @param array $data
     * @throws InvalidConfigException
     */
    public function sendModel($scenario = AbstractSender::SCENARIO_DEFAULT, $data = [])
    {
        UserBalanceSender::sendModel($this, $scenario, $data);
    }

    /**
     * Закрываем виртуальные колонки от изменений
     * @param $name
     * @param $value
     * @throws Exception
     */
    public function __set($name, $value)
    {
        if ($name === 'is_require_calc') {
            throw new Exception("НИЗЯ писать в атрибут $name напрямую");
        }

        parent::__set($name, $value);
    }

    /**
     * @return ActiveQuery
     */
    public function getGamer()
    {
        return $this->hasOne(UserAR::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserBalanceAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new UserBalanceAQ(static::class);
    }

    /*
     * @brief Ручной пресчет баланса
     * @return int
     */
    public function updateBalance()
    {
        $oldAccValue = $this->acc_value;

        $sum = UserMoneyAccAR::find()
            ->andWhere([
                'user_id' => $this->user_id,
                'acc_id' => $this->acc_id,
                'is_actual' => 1
            ])
            ->sum('acc_value');
        $sum = $sum ?: 0;

        if ($this->isNewRecord) { // запрещенная ситуация, но мало ли
            if (empty($sum)) {
                return 0;
            }

            $this->acc_value = $sum;
            $this->r_status = self::STATUS_COMPLETED;
            $clonedModel = clone $this;

            if ($this->save() && $oldAccValue != $this->acc_value) {
                $this->sendModel(UserBalanceSender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);
            }

            return $sum;
        }

        if ($oldAccValue != $sum) {
            Yii::warning(['Increment change balance was broken, user_id' => $this->user_id],'ss3');
        }
        assert($oldAccValue == $sum, 'Increment change balance was broken ' . $this->user_id);

        $this->acc_value = $sum;
        $this->r_status = ($this->r_status & (~self::STATUS_PROCESSING)) | self::STATUS_COMPLETED;

        return $sum;
    }
}
