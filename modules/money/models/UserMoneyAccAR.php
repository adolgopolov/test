<?php

namespace common\modules\money\models;

use Yii;
use yii\db\Query;
use common\components\pusher\interfaces\SenderModelInterface;
use common\components\pusher\senders\AbstractSender;
use common\extensions\ExtInfoAR;
use common\modules\money\MoneyModule;
use common\modules\user\models\UserAR;
use common\modules\user\senders\UserMoneyAccSender;
use common\validators\JsonValidator;

/**
 * This is the model class for table "{{%payment}}".
 *
 * @property string $id
 * @property int $r_status
 * @property int $tou_id
 * @property int $user_id
 * @property int $acc_id
 * @property int $acc_value
 * @property-read string $created_at
 * @property-read string $updated_at
 * @property array $ext_info
 *
 * @property UserAR $gamer
 */
class UserMoneyAccAR extends ExtInfoAR implements SenderModelInterface
{
    use AccountTrait;

    /*
     * !ВНИМАНИЕ должны совпадать с значениями в dictionary
     * status.payment.*
     */
    /*
     * FIXME FAN-536
     * PAYMENT_PACK, PAYMENT_REGISTERED неправильно

статус это НЕ смысл, НЕ предназначение операции
статус - это технологический признак, а не аналитический

Субконто (от англ. subcount - субсчет) - термин из бухгалтерского учета, может иметь следующие значения:
1. Субконто — аналитический признак («разрез») счета бухгалтерского учёта. Низший иерархический элемент в структуре бухгалтерских счетов. Используется в бухгалтерских программах. По назначению схож с субсчетом
Субконто — объект аналитического учета. Под видом субконто понимается множество однотипных объектов аналитического учета, из которого выбирается объект.
user_id - это аналитический параметр. Объект учета
tou_id (вирт колонка)- это аналитический параметр. Объект учета
op = operation, act - разрез по предназначению, операции
а статус - это просто вкл выкл с расширенным состоянием, почему вкл, почему выкл

Для смысла операции можно использовать
1. Отдельную цифровую колонку operation, action
2. Виртуальную ext_info.op, ext_info.act и короткими названиями смысла действия
pack, buy, register, buyin
при этом можно компоновать действия, от общего к частному:
buy.pack, ...

@see PurchaseCoinDoing::doItInner
     */
    public const PAYMENT_PACK = 68;
    public const PAYMENT_REGISTERED = 66;
    public const PAYMENT_PAYED = 64;
    public const PAYMENT_REFUND = 32;
    public const PAYMENT_CHECKED = 16;
    public const PAYMENT_ERROR = 8;
    public const PAYMENT_WAITING = 4;
    public const PAYMENT_PENDING = 3;

    protected static $paymentTexts = [
        self::PAYMENT_PACK => 'пак',
        self::PAYMENT_PAYED => 'выполнено',
        self::PAYMENT_REGISTERED => 'турнир',
        self::PAYMENT_REFUND => 'возврат',
        self::PAYMENT_CHECKED => 'проверено',
        self::PAYMENT_ERROR => 'ошибка',
        self::PAYMENT_WAITING => 'ожидание',
        self::PAYMENT_PENDING => 'ожидание оплаты',
    ];


    /**
     * @param UserMoneyAccAR|null $model
     * @return array|mixed|string
     */
    public static function withdrawStatuses(UserMoneyAccAR $model = null)
    {
        static $result = [
            self::PAYMENT_WAITING => 'Ожидание ответа',
            self::PAYMENT_CHECKED => 'Подтверждение вывода',
            self::PAYMENT_PAYED => 'Деньги отправлены',
            self::PAYMENT_ERROR => 'В выводе отказано',
        ];

        if ($model === null) {
            return $result;
        }

        return $result[$model->r_status] ?? 'Неизвестный статус заявки';
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%payment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge([
            [['user_id', 'acc_id', 'acc_value', 'r_status'], 'integer'],
            [['user_id', 'acc_id', 'acc_value',], 'required'],
            [['acc_id'], 'in', 'range' => array_keys(MoneyModule::accountLabel())],
            [['r_status'], 'in', 'range' => array_keys(self::$paymentTexts)],
            ['updated_at', 'safe'],
            ['user_id', 'exist', 'skipOnError' => true, 'targetClass' => UserAR::class, 'targetAttribute' => ['user_id' => 'id']],
        ], $this->extInfoRules());
    }

    protected function extInfoRules()
    {
        return [
            ['ext_info', JsonValidator::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_status' => 'Статус',
            'user_id' => 'Пользователь',
            'acc_id' => 'Счет',
            'acc_value' => 'Оценка счета',
            'tou_id' => 'Турнир',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'ext_info' => 'E Info',
        ];
    }

    /**
     * @return array
     */
    protected function jsonFields(): array
    {
        return ['tou_id', 'is_actual', 'created_at'];
    }

    /**
     * @param string $scenario
     * @param array $data
     * @throws InvalidConfigException
     */
    public function sendModel($scenario = AbstractSender::SCENARIO_DEFAULT, $data = [])
    {
        UserMoneyAccSender::sendModel($this, $scenario, $data);
    }

    /**
     * @return ActiveQuery
     */
    public function getGamer()
    {
        return $this->hasOne(UserAR::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserMoneyAccAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new UserMoneyAccAQ(static::class);
    }

    /**
     * @brief поставить метку времени создания транзакции
     */
    public function createdAtTouch()
    {
        if (empty($this->created_at)) {
            $data = (new Query())->select('NOW()')->scalar();
            $this->joSet('created_at', $data);
        }
    }

    /*
     * сторнировать запись
     * @param array $data доп данные , которые пишутся в ext_info
     * @return bool
     */
    public function storno($data = []): bool
    {
        $payment = $this;

        if ($payment->id < 0) {
            throw new Exception('Нельзя стронировать сторно');
        }

        $attributes = $payment->getAttributes([
            'id', 'user_id', 'acc_id', 'acc_value', 'r_status'
        ]);
        $attributes['id'] = 0 - $attributes['id'];
        $attributes['acc_value'] = 0 - $attributes['acc_value'];
        $storno = self::findOne($attributes['id']);
        if ($storno) {
            $storno->r_status = $attributes['r_status'];
            $storno->user_id = $attributes['user_id'];
            $storno->acc_id = $attributes['acc_id'];
            $storno->acc_value = $attributes['acc_value'];
        } else {
            $storno = new self($attributes);
        }

        if (!empty($data)) {
            $storno->joSet($data);
        }

        $clonedModel = clone $storno;

        if ($storno->save()) {
            $storno->sendModel(UserMoneyAccSender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);

            return true;
        }

        $this->addErrors($storno->errors);

        return false;
    }

    /**
     * @param $user_id
     * @param $acc_id
     * @param $acc_value
     * @return array|bool
     * @throws InvalidConfigException
     */
    public static function increase($user_id, $acc_id, $acc_value)
    {
        $model = new self([
            'user_id' => $user_id,
            'r_status' => self::PAYMENT_PAYED,
            'acc_id' => $acc_id,
            'acc_value' => $acc_value,
        ]);

        $model->joInfo = 'ручное пополнение администратором id:' . Yii::$app->user->id;

        if (!$model->save()) {
            return $model->errors;
        }

        $model->sendModel(UserMoneyAccSender::SCENARIO_UPDATE);

        return true;
    }
}
