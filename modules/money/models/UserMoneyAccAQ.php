<?php

namespace common\modules\money\models;

use common\extensions\ExtAQ;

/**
 * This is the ActiveQuery class for [[UserMoneyAccAR]].
 *
 * @see UserPaymentAR
 */
class UserMoneyAccAQ extends ExtAQ
{
    public function whereTou($touId)
    {
        $this->andWhere(['tou_id' => $touId]);
        return $this;
    }

    public function registerPay()
    {
        return $this->andWhere(['r_status' => UserMoneyAccAR::PAYMENT_REGISTERED]);
    }

    /**
     *
     * @param string $args for strtotime
     * @return UserMoneyAccAQ
     */
    public function actualPeriod($args = '-6 week')
    {
        return $this->andWhere(['>=', 'updated_at', date('Y-m-d', strtotime($args))]);
    }
}
