<?php
/**
 * User: andrey
 * Date: 04.04.19 Time: 14:01
 */

namespace common\modules\money\models;
/*
 * @brief
 */

use common\modules\money\MoneyModule;

trait AccountTrait
{
    /*
     * @brief получить название счета для транзакции
     * @return string
     */
    public function accountName()
    {
        return MoneyModule::accountLabel($this->acc_id);
    }
}
