<?php

namespace common\modules\money\acquire;

use Yii;
use Voronkovich\SberbankAcquiring\Client;
use common\modules\money\models\UserMoneyAccAR;

/**
 * Description of Sberbank
 *
 * @author skynin <sohay_ua@yahoo.com>
 * created: 22 нояб. 2019 г.
 *
 * origin creator Artaches
 */
class Sberbank
{
    public $client;
    public $userName;
    public $password;
    public $apiUri;
    public $currency;
    public $returnUrl;
    public $failUrl;

    protected $frontend;

    public function __construct()
    {
        $this->frontend = 'https//' . Yii::$app->params['ss3_api_host'];
    }

    /**
        operation

        Тип операции, о которой пришло уведомление:

            approved - операция удержания (холдирования) суммы;
            deposited - операция завершения;
            reversed - операция отмены;
            refunded - операция возврата.

        status ($stat_operation)

        Индикатор успешности операции, указанной в параметре operation:

        1 - операция прошла успешно;
        0 - операция завершилась ошибкой.

     */

    /**
     * @param $operation
     * @param int $stat_operation
     * @return int
     */
    public static function getStatusByOperation($operation, $stat_operation = 1)
    {
        static $opst = [
            'approved' => UserMoneyAccAR::PAYMENT_PENDING,
            'deposited' => UserMoneyAccAR::PAYMENT_PAYED,
            'reversed' => UserMoneyAccAR::PAYMENT_WAITING,
            'refunded' => UserMoneyAccAR::PAYMENT_REFUND,
        ];

        if (!isset($opst[$operation]) || $stat_operation == 0) {
            return UserMoneyAccAR::PAYMENT_ERROR;
        }

        return $opst[$operation];
    }

    /**
     * @param $amount
     * @param $mdOrder
     * @param $orderNumber
     * @param $checksum
     * @param $operation
     * @param $status
     * @return bool
     * @link https://securepayments.sberbank.ru/wiki/doku.php/integration:api:callback:start Уведомления обратного вызова
     */
    public static function validateChecksum(array $orderData, string $checksum)
    {
        // на стороне продавца пары имя_параметра;значение_параметра должны быть отсортированы в прямом алфавитном порядке по имени параметров.
        ksort($orderData);

        // Из параметров и их значений генерируется строка следующего вида.
        // имя_параметра1;значение_параметра1;имя_параметра2;значение_параметра2;…;имя_параметраN;значение_параметраN;
        $data = '';
        foreach ($orderData as $dtKey => $dtVal) {
            $data .= "$dtKey;$dtVal;";
        }

        // высчитывается контрольная сумма, способ вычисления зависит от способа её формирования:
        // при использовании симметричной криптографии - с помощью алгоритма HMAC-SHA256 и общего с платёжным шлюзом закрытого ключа;
        // В получившейся строке контрольной суммы все буквы нижнего регистра заменяются на буквы верхнего регистра.
        $hmac = strtoupper(hash_hmac('sha256' , $data , Yii::$app->params['sberbank_secret_token']));

        return $hmac === $checksum;
    }

    function оrder($sum, $order_id)
    {
        $client = new Client([
            'userName' => $this->userName,
            'password' => $this->password,
            'apiUri'   => $this->apiUri
        ]);

        $params = [
            'currency' => $this->currency,
            'failUrl'  => $this->frontend . $this->failUrl
        ];

        $result = $client->registerOrder(
                $order_id,
                $sum,
                $this->frontend . $this->returnUrl,
                $params
        );
        return $result;
    }

    public function reverseOrder($data)
    {
        $client = new Client([
            'userName' => $this->userName,
            'password' => $this->password,
            'apiUri'   => $this->apiUri
        ]);

        $params = [
            'currency' => $this->currency,
            'failUrl'  => $this->failUrl
        ];

        $result = $client->reverseOrder(
                $data['id'],
                $params
        );

        return $result;
    }
}
