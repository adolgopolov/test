<?php

namespace common\modules\money\forms;

use common\modules\game\models\GameTourneyAR;

/**
 * Class UserMoneyAccFM
 * @package common\modules\money\forms
 */
class UserMoneyAccFM extends \common\modules\money\models\UserMoneyAccAR
{
    /**
     * @return string
     */
    public function getTransactionType()
    {
        if ($this->acc_value > 0) return 'Начисление';
        if ($this->acc_value < 0) return 'Списание';
        if ($this->acc_value == 0) return ' - ';
    }

    /**
     * @return string
     */
    public function getTransactionKind()
    {
        if (!empty($this->tou_id)) return 'Участие в турнире';

        // TODO будет работать до того времени пока мы не начнем покупать что то еще за реальные деньги
        if ($this->r_status == self::PAYMENT_PAYED) return 'Покупка монет';
        if ($this->joGet('pack_id')) return 'Получение пака';

        return 'Неопознанная транзакция';
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'transaction_kind' => 'Тип транзакции',
            'transaction_type' => 'Вид транзакции',
            'tou_name' => 'Название турнира',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourney()
    {
        return $this->hasOne(GameTourneyAR::class, ['id' => 'tou_id']);
    }

    /**
     * @return mixed
     */
    public function getStatusName()
    {
        return self::$paymentTexts[$this->r_status];
    }
}
