<?php

namespace common\modules\money;

use yii\base\Module;
use yii\helpers\ArrayHelper;
use backend\components\aboard\MenuEntryInterface;
use common\modules\game\models\GameLineupAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\goods\models\PackAR;
use common\modules\money\models\MoneyCurrencyAR;
use common\modules\money\models\UserMoneyAccAR;
use common\modules\money\models\UserBalanceAR;
use common\modules\user\senders\UserMoneyAccSender;

/**
 * Description of MoneyModule
 *
 * @author Skynin <sohay_ua@yahoo.com>
 */
class MoneyModule extends Module implements MenuEntryInterface
{
    /*
     * @brief Константы номеров счетов
     * !ВНИМАНИЕ должны совпадать с значениями в dictionary
     */
    const ACCOUNT_GOLD = 1;
    const ACCOUNT_GREEN = 2;
    const ACCOUNT_ENERGY = 7;
    const ACCOUNT_RUB = 100;

    /*
     * @brief Константы платежных шлюзов
     * !ВНИМАНИЕ должны совпадать с значениями в dictionary
     */
    const PAYGATE_SBERBANK = 1;

    protected static $accountTexts = [
        self::ACCOUNT_GOLD => 'желтые монеты',
        self::ACCOUNT_GREEN => 'зеленые монеты',
        self::ACCOUNT_ENERGY => 'энергия',
        self::ACCOUNT_RUB => 'рубли',
    ];

    /**
     * @param null $acc_id
     * @return array|mixed|string
     */
    public static function accountLabel($acc_id = null)
    {
        if ($acc_id) {
            return static::$accountTexts[$acc_id] ?? 'неопределенный счет';
        }

        return static::$accountTexts;
    }

    /**
     *  получаем массив проводок (пользователь => фишки), данные по операции, условия выборки транзакций
     * //TODO переделать !!!! пересчет производится следующим образом(старые записи сторнировать, если возможно), новые установить
     * //TODO, если у пользователя не хватает денег для сторнирования, то эти деньги ему не списываются
     * @param $opId
     * @param int $accId
     * @param array $data
     * @param array $touExtInfo
     * @return bool
     */
    public function transactionsByScoring($opId, $accId = 1, $data = [], $touExtInfo = [])
    {
        $allTransaction = UserMoneyAccAR::find()->actualPeriod()->where(['acc_id' => $accId, 'r_status' => UserMoneyAccAR::PAYMENT_REGISTERED])->whereJO(['op.id' => $opId])->indexBy('user_id')->all();

        $result = true;
        /** @var GameLineupAR $lineUp */
        // перебор массива пользователь - фишки, остальные транзакции обнулить
        foreach ($data as $userId => $amountAward) {
            // заполнить транзакции согласно расчитанных составов
            $transaction = $allTransaction[$userId] ?? null;

            if (empty($transaction)) {
                $transaction = new UserMoneyAccAR([
                    'acc_id' => $accId,
                    'r_status' => UserMoneyAccAR::PAYMENT_REGISTERED,
                    'user_id' => $userId,
                ]);
                $transaction->joSet($touExtInfo);
            }
            else {
                unset($allTransaction[$userId]);
            }

            /** @var UserMoneyAccAR|null $transaction */
            $oldValue = $transaction->acc_value;
            $transaction->acc_value = $amountAward['award'];
            $clonedModel = clone $transaction;
            $save = $transaction->save();
            $result = $result && $save;

            if ($save && $oldValue != $transaction->acc_value) {
                $transaction->sendModel(UserMoneyAccSender::SCENARIO_UPDATE, ['clonedModel' => $clonedModel]);
            }
        }

        foreach ($allTransaction as $eachTransaction) {
            $eachTransaction->acc_value = 0;
            $result = $result && $eachTransaction->save();
        }

        return $result;
    }

    /**
     * @return array|null
     */
    public static function getMenuItems(): ?array
    {
        return [
            [
                'label' => 'Деньги',
                'icon' => 'money',
                'color' => 'green-400',
                'items' => [
                    [
                        'label' => 'Счета',
                        'url' => ['/money/account/index']
                    ],
                    [
                        'label' => 'Валюты',
                        'url' => ['/money/currency/index']
                    ]
                ]
            ]
        ];
    }

    /** списание за участие в турнире
     * @param GameLineupAR $lineUp
     * @return UserMoneyAccAR
     * @throws \Exception
     */
    public function payForRegisterInTourney(GameLineupAR $lineUp): UserMoneyAccAR
    {
        if (empty($lineUp->tourney)) {
            throw new \Exception("Tourney for LineUp $lineUp->id not found");
        }

        $accId = $lineUp->acc_id ?? self::ACCOUNT_GOLD;
        $buyIn = $lineUp->tourney->getBuyIn($accId);

        $payment = new UserMoneyAccAR([
            'user_id' => $lineUp->user_id,
            'r_status' => UserMoneyAccAR::PAYMENT_REGISTERED,
            'acc_id' => $accId,

            'acc_value' => -abs($buyIn),

        ]);

        $payment->joSet('tou_id', $lineUp->tou_id);

        // бесплатные турниры плата не взымается
        if ($buyIn != 0 && $payment->save()) {
            $payment->sendModel(UserMoneyAccSender::SCENARIO_UPDATE);
        }

        return $payment;
    }

    /**
     * @param GameLineupAR $lineUp
     * @return bool
     * @throws \Exception
     */
    public function haveMoneyForRegisterInTourney(GameLineupAR $lineUp)
    {
        $accId = $lineUp->acc_id ?? self::ACCOUNT_GOLD;

        if (empty($lineUp->tourney)) {
            throw new \Exception("Tourney for LineUp $lineUp->id not found");
        }

        $amount = $lineUp->tourney->getBuyIn($accId);
        $balance = $this->userBalance($lineUp->user_id, $accId, 'strong');

        return isset($balance[$accId]) && ($balance[$accId] >= $amount);
    }

    /**
     * @param GameTourneyAR $tourney
     * @return bool
     * @throws \Exception
     */
    public function cancelTourneyStorno(GameTourneyAR $tourney)
    {
        $success = true;
        $payments = UserMoneyAccAR::find()->actualPeriod()->whereTou($tourney->id)->registerPay()->all();

        foreach ($payments as $eachPayment) {
            /** @var UserMoneyAccAR $eachPayment */
            $success = $eachPayment->storno();
            if (!$success) {
                break;
            }
        }

        return $success;
    }

    private static $currencies = [];

    /**
     * @param null $currencyId
     * @return array|mixed|null
     */
    public static function getCurrencies($currencyId = null)
    {
        if (empty(self::$currencies)) {
            self::$currencies = ArrayHelper::index(MoneyCurrencyAR::find()->forList()->all(), 'id');
        }

        if ($currencyId) {
            return self::$currencies[$currencyId] ?? null;
        }

        return self::$currencies;
    }

    /**
     * @param array $packBill
     * @param PackAR $pack
     * @return bool
     */
    public function haveMoneyForBuyPack(array $packBill, PackAR $pack)
    {
        $accId = $packBill['acc_id'] ?? self::ACCOUNT_GOLD;
        $price = $pack->getPrices($accId);

        $balance = $this->userBalance($packBill['user_id'], $accId, 'strong');

        return isset($balance[$accId]) && ($balance[$accId] >= $price);
    }


    /**
     * @param array $packBill
     * @param PackAR $pack
     * @return UserMoneyAccAR
     * @throws \yii\base\InvalidConfigException
     */
    public function payForPack(array $packBill, PackAR $pack): UserMoneyAccAR
    {
        $accId = $packBill['acc_id'] ?? self::ACCOUNT_GOLD;
        $price = $pack->getPrices($accId);

        $payment = new UserMoneyAccAR([
            'user_id' => $packBill['user_id'],
            'r_status' => UserMoneyAccAR::PAYMENT_PACK,
            'acc_id' => $accId,
            'acc_value' => -abs($price),
        ]);

        $payment->joSet('pack_id', $packBill['pack_id']);

        if ($price !== 0 && $payment->save()) {
            $payment->sendModel(UserMoneyAccSender::SCENARIO_UPDATE);
        }

        return $payment;
    }

    public function userBalance($userId, $accId = 'all', $mode = false) : array
    {
        $query = UserBalanceAR::find()->where(['user_id'=>$userId]);

        if ($accId != 'all') $query->andWhere(['acc_id' => $accId]);
        if ($mode) $query->strong();

        return $query->balances(is_array($userId));
    }

    public function storno($transactionId, $data = []) : bool
    {
        $transact = is_object($transactionId) ? $transactionId : UserMoneyAccAR::findOne($transactionId);

        return empty($transact) ? false : $transact->storno($data);
    }
}
