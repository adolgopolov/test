<?php

namespace common\modules\money\controllers;

use backend\controllers\AdminController;
use backend\controllers\DictCRUDTrait;
use common\components\S3Kind;
use common\modules\money\models\MoneyCurrencyAR;
use common\modules\money\models\UserMoneyAccAR;
use Yii;

/**
 * PaymentController implements the work actions for UserMoneyAccAR model.
 */
class PaymentController extends AdminController
{
    use DictCRUDTrait;

    public $modelClass = MoneyCurrencyAR::class;
    public $kind = S3Kind::KIND_CURRENCY;

    /**
     * Lists all UserMoneyAccAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /*$dataProvider = new ActiveDataProvider(['query' => MoneyCurrencyAR::find()]);
        $sports = SportAR::getAsOptsList();

        return $this->render('index', compact('dataProvider', 'sports'));*/
    }

    /**
     * @param $user_id
     * @param $acc_id
     * @param $money
     * @return \yii\web\Response
     */
    public function actionIncrease($user_id, $acc_id, $money)
    {
        $result = UserMoneyAccAR::increase($user_id, $acc_id, $money);

        if ($result === true) {
            Yii::$app->session->addFlash('success', 'Пополнение счета пользователя успешно');
        } else {
            Yii::$app->session->addFlash('error', $result);
        }

        return $this->redirect(['/user/manage/view', 'id' => $user_id]);
    }
}
