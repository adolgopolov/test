<?php

namespace common\modules\money\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use common\modules\money\models\MoneyAccountAR;
use backend\controllers\AdminController;

/**
 * AccountController implements the CRUD actions for MoneyAccountAR model.
 */
class AccountController extends AdminController
{
    /**
     * Lists all MoneyAccountAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider(['query' => MoneyAccountAR::find()]);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * Displays a single MoneyAccountAR model.
     *
     * @param integer $id
     * @param integer $ent_kind
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id, $ent_kind)
    {
        $model = MoneyAccountAR::findOrFail($id);

        return $this->render('view', compact('model'));
    }

    /**
     * Creates a new MoneyAccountAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        // $model = new MoneyAccountAR();

        // $this->performAjaxValidation($model);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     \Yii::$app->session->addFlash('success', 'Запись успешно добавлена');
        //     return $this->redirect(['index']);
        // }

        // return $this->render('create', compact('model'));
    }

    /**
     * Updates an existing MoneyAccountAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\ExitException
     */
    public function actionUpdate($id)
    {
        $model = MoneyAccountAR::findOrFail($id);

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('model'));
    }

    /**
     * Deletes an existing MoneyAccountAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // $model = MoneyAccountAR::findOrFail($id);
        // if ($model) {
        //     \Yii::$app->session->addFlash('success', 'Запись успешно удалена');
        //     $model->delete();
        // }

        Yii::$app->session->addFlash('info', 'Удаление запрещено');

        return $this->redirect(['index']);
    }
}
