<?php

namespace common\modules\money\controllers;

use common\modules\money\models\UserMoneyAccAR;
use tests\models\Payment;
use Yii;
use common\modules\money\models\MoneyCurrencyAR;
use yii\data\ActiveDataProvider;
use backend\controllers\AdminController;
use common\modules\sport\models\SportAR;
use backend\controllers\DictCRUDTrait;
use common\components\S3Kind;

/**
 * CurrencyController implements the CRUD actions for MoneyCurrencyAR model.
 */
class CurrencyController extends AdminController
{
    use DictCRUDTrait;

    public $modelClass = MoneyCurrencyAR::class;
    public $kind = S3Kind::KIND_CURRENCY;

    /**
     * Lists all MoneyCurrencyAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider(['query' => MoneyCurrencyAR::find()]);
        $sports = SportAR::getAsOptsList();

        return $this->render('index', compact('dataProvider', 'sports'));
    }

}
