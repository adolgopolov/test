<?php

use common\widgets\DetailForm\DetailFormWidget;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\modules\money\models\MoneyCurrencyAR $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<div class="money-currency-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputType' => 'number',
            ],
            'ent_kind' => [
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'ent_code',
            'ent_value',
            'r_name',
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
