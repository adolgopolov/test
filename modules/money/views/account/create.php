<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\money\models\MoneyAccountAR $model
 */

$this->title = 'Добавление счета';
$this->params['breadcrumbs'][] = ['label' => 'Счета', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="money-account-ar-create">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
