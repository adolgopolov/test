<?php

use backend\components\aboard\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Счета';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="money-account-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>
    <?php Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'ent_kind',
                'options' => [
                    'class' => 'kind-grid-column',
                ],
            ],
            [
                'attribute' => 'ent_code',
                'options' => [
                    'class' => 'code-grid-column',
                ],
            ],
            'ent_value',
            'r_name',
            [
                'class' => ActionColumn::class,
                'template' => '<div class="btn-group">{view}{update}</div>',
            ],
        ],
    ]) ?>
    <?php Pjax::end() ?>
</div>
