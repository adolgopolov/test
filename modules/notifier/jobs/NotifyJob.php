<?php

namespace common\modules\notifier\jobs;

use common\modules\notifier\components\AbstractNotifier;
use yii\base\BaseObject;
use yii\queue\JobInterface;

/**
 * Class NotifyJob
 * @package common\modules\notifier\jobs
 * @property AbstractNotifier $notifyObject
 */
class NotifyJob extends BaseObject implements JobInterface
{
    public $notifierObject;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     */
    public function execute($queue)
    {
        if ($this->notifierObject instanceof AbstractNotifier
            && $this->notifierObject->beforeSend()) {

            $this->notifierObject->send();
            $this->notifierObject->afterSend();
        }
    }
}
