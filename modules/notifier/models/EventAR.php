<?php

namespace common\modules\notifier\models;

use common\components\S3Kind;
use common\components\S3NotifyDictionaryAR;
use Yii;

/**
 * Class SubscribeTypeAR
 * @package common\modules\notifier\models
 * @property array $settings
 * @property string|array $smtp
 */
class EventAR extends S3NotifyDictionaryAR
{
    public const SMPT_SETTINGS_CUSTOM = 0;
    public const SMTP_SETTINGS_DEFAULT = 1;

    public const EVENT_ON = 1;
    public const EVENT_OFF = 0;

    public const LABEL_EVENT_ID = 'Событие';
    public const LABEL_DIRECT_ID = 'Тип уведомления';
    public const LABEL_DEFAULT_STATUS = 'Статус подписки (По умолчанию)';
    public const LABEL_READ_ONLY = 'Возможность изменять (Для пользователей)';

    public $default_status;
    public $read_only;
    public $smtp_alias;

    /**
     * {@inheritdoc}
     */
    public static function kind()
    {
        return S3Kind::KIND_EVENT;
    }

    /**
     * @return array
     */
    protected function jsonFields(): array
    {
        return [
            'settings',
            'smtp',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_name'], 'required'],
            [['r_name'], 'string'],
            [['r_status'], 'boolean'],
            [['event_id'], 'integer'],
            [['direct_id'], 'integer'],
            [['default_status', 'read_only'], 'boolean'],
            [['smtp'], 'safe'],
            [['smtp_alias'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_name' => 'Название подписки',
            'r_status' => 'Статус подписки',
            'event_id' => self::LABEL_EVENT_ID,
            'direct_id' => self::LABEL_DIRECT_ID,
            'default_status' => self::LABEL_DEFAULT_STATUS,
            'read_only' => self::LABEL_READ_ONLY,
            'smtp_alias' => 'Настройки SMTP',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplates()
    {
        return $this->hasMany(TemplateAR::class, ['event_id' => 'id']);
    }

    /**
     * @param $direct_id
     * @return $this|bool
     */
    public function getSetting($direct_id)
    {
        if (!isset($this->settings[$direct_id])) {
            return false;
        }

        $setting = $this->settings[$direct_id];

        $model = new self;

        $model->direct_id = $setting['direct_id'];
        $model->default_status = $setting['default_status'];
        $model->read_only = $setting['read_only'];

        return $model;
    }

    /**
     * @param $event_id
     * @param int $r_status
     * @return array|EventAR|null|\yii\db\ActiveRecord
     */
    public static function getEvent($event_id, $r_status = self::EVENT_ON)
    {
        return self::find()
            ->andWhere(['id' => $event_id])
            ->andWhere(['r_status' => $r_status])
            ->one();
    }

    /**
     * @param int $r_status
     * @return array
     */
    public static function getEventList($r_status = self::EVENT_ON)
    {
        return self::find()
            ->select(['r_name'])
            ->andWhere(['r_status' => $r_status])
            ->indexBy('id')
            ->asArray()
            ->column();
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            'Не активна',
            'Активна',
        ];
    }

    /**
     * @return array
     */
    public static function getSettingDefaultStatuses()
    {
        return [
            'Отключена',
            'Включена',
        ];
    }

    /**
     * @return array
     */
    public static function getSettingReadOnlyStatuses()
    {
        return [
            'Есть',
            'Нет',
        ];
    }

    /**
     * @return array
     */
    public static function getSmtpAliases()
    {
        $smtpKeys = array_keys(Yii::$app->mailer->smtpVariations);

        $result = [
            self::SMTP_SETTINGS_DEFAULT => 'По умолчанию',
            self::SMPT_SETTINGS_CUSTOM => 'Указать свои настройки',
        ];

        foreach ($smtpKeys as $smtpKey) {
            $result[$smtpKey] = $smtpKey;
        }

        return $result;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function saveSmtp($runValidation = true, $attributeNames = null)
    {
        if ($this->smtp_alias && isset(Yii::$app->mailer->smtpVariations[$this->smtp_alias])) {
            $this->smtp = $this->smtp_alias;
        }

        if ((int)$this->smtp_alias === self::SMTP_SETTINGS_DEFAULT) {
            $this->smtp = null;
        }

        return $this->save($runValidation, $attributeNames);
    }
}
