<?php

namespace common\modules\notifier\models;

use common\components\S3Kind;
use common\components\S3NotifyDictionaryAR;

/**
 * Class DirectAR
 * @package common\modules\notifier\models
 */
class DirectAR extends S3NotifyDictionaryAR
{
    /**
     * {@inheritdoc}
     */
    public static function kind()
    {
        return S3Kind::KIND_DIRECT;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [];
    }


    /**
     * @return array
     */
    public static function getDirectList()
    {
        return self::find()
            ->select(['r_name'])
            ->indexBy('id')
            ->asArray()
            ->column();
    }
}
