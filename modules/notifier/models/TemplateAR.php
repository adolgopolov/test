<?php

namespace common\modules\notifier\models;

use common\components\S3Kind;
use common\components\S3NotifyDictionaryAR;

/**
 * Class TemplateAR
 * @package common\modules\notifier\models
 * @property DirectAR $direct
 * @property EventAR $event
 */
class TemplateAR extends S3NotifyDictionaryAR
{
    /**
     * {@inheritdoc}
     */
    public static function kind()
    {
        return S3Kind::KIND_TEMPLATE;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_name', 'r_text', 'id'], 'required'],
            [['id'], 'integer'],
            [['r_name'], 'string'],
            [['r_text'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_name' => 'Заголовок',
            'r_text' => 'Контент',
            'event_id' => 'Событие',
            'direct_id' => 'Тип уведомления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(EventAR::class, ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirect()
    {
        return $this->hasOne(DirectAR::class, ['id' => 'direct_id']);
    }
}
