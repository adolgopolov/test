<?php

namespace common\modules\notifier\models;

use common\components\FindOrFailTrait;
use common\models\BaseAR;
use common\modules\user\models\UserAR;

/**
 * Class LetterAR
 * @package common\modules\notifier\models
 * @property DirectAR $direct
 * @property EventAR $event
 * @property UserAR $user
 * @property UserAR $fromUser
 * @property int $id
 * @property int $event_id
 * @property int $direct_id
 * @property int $user_id
 * @property string $uniq_stamp
 * @property int $from_user
 * @property string $r_name
 * @property string $r_text
 * @property string$created_at
 * @property string$updated_at
 * @property array $ext_info
 */
class LetterAR extends BaseAR
{
    use FindOrFailTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%letter}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'uniq_stamp', 'r_name', 'r_text'], 'required'],
            [['uniq_stamp'], 'string'],
            [['r_name'], 'string'],
            [['r_text'], 'string'],
            [['user_id', 'uniq_stamp'], 'unique', 'targetAttribute' => ['user_id', 'uniq_stamp']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Событие',
            'direct_id' => 'Тип уведомления',
            'user_id' => 'Получатель',
            'uniq_stamp' => 'Контрольная сумма',
            'from_user' => 'Отправитель',
            'r_name' => 'Заголовок',
            'r_text' => 'Содержание',
            'created_at' => 'Дата отправки',
//            'updated_at' => 'Дата изменения',
            'ext_info' => 'Ext info',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirect()
    {
        return $this->hasOne(DirectAR::class, ['id' => 'direct_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(EventAR::class, ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserAR::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(UserAR::class, ['id' => 'from_user']);
    }
}
