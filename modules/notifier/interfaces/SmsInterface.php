<?php

namespace common\modules\notifier\interfaces;

interface SmsInterface
{
    /**
     * @return bool|float
     * @throws \miserenkov\sms\exceptions\BalanceException
     */
    public function getBalance();

    /**
     * @param $id
     * @param $phone
     * @param int $all
     * @return array|bool
     * @throws \miserenkov\sms\exceptions\Exception
     * @throws \miserenkov\sms\exceptions\StatusException
     */
    public function getStatus($id, $phone, $all = 2);
}
