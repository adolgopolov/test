<?php

namespace common\modules\notifier\interfaces;

/**
 * Interface NotifierInterface
 * @package common\modules\notifier\interfaces
 */
interface NotifierInterface
{
    /**
     * @return bool
     */
    public function checkClone();

    /**
     * @return bool
     */
    public function beforeSend();

    /**
     * @return bool
     */
    public function send();

    /**
     * @return bool
     */
    public function afterSend();
}
