<?php

namespace common\modules\notifier\notifiers;

use common\modules\notifier\components\AbstractNotifier;
use Yii;

/**
 * Class EmailNotifier
 * @package common\modules\notifier
 */
class EmailNotifier extends AbstractNotifier
{
    public $toAddr;

    /**
     * @return bool
     */
    public function send()
    {
        $view = [
            'html' => 'default-html',
            'text' => 'default-text',
        ];

        $params = [
            'message' => $this->message,
        ];

        $mailer = Yii::$app->mailer;

        if (is_array($this->event->smtp)) {
            $config = $this->event->smtp;
        } else {
            $config = $mailer->smtpVariations[(string)$this->event->smtp] ?? [];
        }

        $mailer->reconfigure($config);

        $from = $mailer->getTransport()->getFrom();
        $to = $this->toAddr ?? $this->user->email;

        return $mailer
            ->compose($view, $params)
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($this->subject)
            ->send();
    }
}
