<?php

namespace common\modules\notifier\notifiers;

use common\modules\notifier\components\AbstractNotifier;
use Yii;

/**
 * Class PushNotifier
 * @package common\modules\notifier
 */
class PushNotifier extends AbstractNotifier
{
    /**
     * @return bool
     */
    public function send()
    {
        $pushSender = Yii::$app->pushSender;

        if ($this->user) {
            $pusher = $pushSender->private->message
                ->setSubjectId($this->user->id);
        } else {
            $pusher = $pushSender->global->message;
        }

        return $pusher
            ->setMessage($this->message)
            ->setTitle($this->subject)
            ->sendData();
    }
}
