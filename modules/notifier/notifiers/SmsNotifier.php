<?php

namespace common\modules\notifier\notifiers;

use common\modules\notifier\components\AbstractNotifier;
use Yii;

/**
 * Class SmsNotifier
 * @package common\modules\notifier
 */
class SmsNotifier extends AbstractNotifier
{
    /**
     * @return bool
     */
    public function send()
    {
        if (!$this->user->phone) {
            return false;
        }

        return Yii::$app->sms->send([$this->user->phone], $this->message);
    }
}
