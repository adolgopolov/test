<?php

namespace common\modules\notifier\components;

class NotifierError
{
    public const EVENT_NOT_FOUND = 1;
    public const USERS_NOT_FOUND = 2;
    public const NOTIFICATIONS_NOT_SENDED = 3;
    public const SETTING_NOT_FOUND = 4;
    public const SUBSCRIBE_IS_OFF = 5;
    public const TEMPLATE_NOT_FOUND = 6;
    public const NOTIFIER_CLASS_NOT_FOUND = 7;
}
