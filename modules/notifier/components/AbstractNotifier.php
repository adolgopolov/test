<?php

namespace common\modules\notifier\components;

use common\modules\notifier\helpers\ConjugationHelper;
use common\modules\notifier\interfaces\NotifierInterface;
use common\modules\notifier\models\DirectAR;
use common\modules\notifier\models\EventAR;
use common\modules\notifier\models\LetterAR;
use common\modules\user\models\UserAR;
use yii\base\BaseObject;

/**
 * Class AbstractNotifier
 * @package common\modules\notifier\components
 * @property UserAR|bool $user
 */
abstract class AbstractNotifier extends BaseObject implements NotifierInterface
{
    /** @var EventAR */
    public $event;
    /** @var DirectAR */
    public $direct;
    public $subject;
    public $message;
    public $varTemplate;
    public $fromUser;
    public $user;
    public $userId;
    protected $uniqStamp;

    /**
     * @return bool
     */
    public function checkClone()
    {
        $this->combineSubject($this->subject);
        $this->combineMessage($this->message);
        $this->generateUniqStamp();

        if ($this->user !== false) {
            $this->userId = $this->user->id;
        } else {
            $this->userId = UserAR::GUEST_USER;
        }

        $findDuplicate = LetterAR::find()
            ->where(['user_id' => $this->userId])
            ->andWhere(['event_id' => $this->event->id])
            ->andWhere(['direct_id' => $this->direct->id])
            ->andWhere(['uniq_stamp' => $this->uniqStamp])
            ->exists();

        if ($findDuplicate) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function beforeSend()
    {
        $model = new LetterAR();

        $model->user_id = $this->userId;
        $model->uniq_stamp = $this->uniqStamp;
        if ($this->fromUser) {
            $model->from_user = $this->fromUser;
        }
        $model->r_name = $this->subject;
        $model->r_text = $this->message;
        $model->event_id = $this->event->id;
        $model->direct_id = $this->direct->id;

        return $model->save();
    }

    /**
     * @return bool
     */
    public function afterSend()
    {
        return true;
    }

    /**
     * @param $message
     */
    public function combineMessage(&$message)
    {
        $varTemplate = $this->varTemplate;

        $message = preg_replace_callback(
            '|@.*switch.*\{(.*)\}([\s\S]*)@.*endswitch|mu',
            static function ($mathes) use ($varTemplate) {
                $mathes[1] = trim($mathes[1]);

                $switchKey = '{' . $mathes[1] . '}';
                if (empty($varTemplate[$switchKey])) {
                    return $mathes[0];
                }

                $caseValue = $varTemplate[$switchKey];

                $caseConcrete = "|@case.*$caseValue([^@]*)|mu";

                if (preg_match($caseConcrete, $mathes[2], $caseMathes)) {
                    return $caseMathes[1];
                }

                return $mathes[2];
            }, $message);

        $message = preg_replace_callback(
            '|\{([^{}]+)([\?!])([^{}]+)\}|',
            static function ($mathes) use ($varTemplate) {
                $numEnd = ConjugationHelper::numEnding($varTemplate['{' . $mathes[1] . '}'] ?? 0, $mathes[3]);

                if ($mathes[2] === '!') {
                    $numEnd = mb_strtoupper(mb_substr($numEnd, 0, 1)) . mb_substr($numEnd, 1);
                }

                return sprintf('{%s} %s', $mathes[1], $numEnd);
            }, $message);

        $message = strtr($message, $varTemplate);
    }

    /**
     * @param $subject
     */
    public function combineSubject(&$subject)
    {
        $subject = strtr($subject, $this->varTemplate);
    }

    /**
     * @param string $salt
     * @param string $algo
     * @return string
     */
    public function generateUniqStamp(string $salt = '', string $algo = 'md4'): string
    {
        return $this->uniqStamp = hash($algo, $salt . '-' . $this->event->id . '_' . $this->direct->id . ':' . $this->subject . '-' . $this->message);
    }
}
