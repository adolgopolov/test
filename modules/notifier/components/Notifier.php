<?php

namespace common\modules\notifier\components;

use common\components\S3Component;
use common\modules\notifier\notifiers\EmailNotifier;
use common\modules\notifier\jobs\NotifyJob;
use common\modules\notifier\models\EventAR;
use common\modules\notifier\models\TemplateAR;
use common\modules\notifier\notifiers\PushNotifier;
use common\modules\notifier\notifiers\SmsNotifier;
use common\modules\user\models\UserAR;
use Yii;

/**
 * Class Notifier
 * @package common\modules\notifier\components
 */
class Notifier extends S3Component
{
    public const SUBSCRIBE_ON = 1;
    public const SUBSCRIBE_OFF = 0;

    public const READ_ONLY_ON = 1;
    public const READ_ONLY_OFF = 0;

    public const PUSH_NOTIFIER = 1;
    public const SMS_NOTIFIER = 2;
    public const EMAIL_NOTIFIER = 3;

    public $jobedNotifications = 0;

    /**
     * @param array|int $user_ids
     * @param int $event_id
     * @param array $varTemplate
     * @param null $from_user
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function sendMessage($user_ids, $event_id, $varTemplate = [], $from_user = null): bool
    {
        /** @var EventAR $event */
        $event = EventAR::getEvent($event_id);

        if (!$event) {
            $this->addError(NotifierError::EVENT_NOT_FOUND, 'Event not found.');

            return false;
        }

        $users = UserAR::find()
            ->where(['id' => $user_ids])
            ->all();

        if (!$users) {
            $this->addError(NotifierError::USERS_NOT_FOUND, 'Users not found.');

            return false;
        }

        $this->jobedNotifications = 0;

        foreach ($users as $user) {
            foreach ($event->settings as $setting) {
                if ($setting['read_only'] === self::READ_ONLY_OFF) {
                    $userSubscribe = $user->ext_info['subscribe'][$event_id][$setting['direct_id']] ?? $setting['default_status'];
                } else {
                    $userSubscribe = $setting['default_status'];
                }

                if ($userSubscribe === self::SUBSCRIBE_OFF) {
                    continue;
                }

                $template = TemplateAR::find()
                    ->where(['event_id' => $event_id])
                    ->andWhere(['direct_id' => $setting['direct_id']])
                    ->one();

                if (!$template) {
                    continue;
                }

                /** @var AbstractNotifier $notifierObject */
                $notifierObject = $this->getNotifierObject($setting['direct_id'], [
                    'varTemplate' => $varTemplate,
                    'event' => $event,
                    'direct' => $template->direct,
                    'subject' => $template->r_name,
                    'message' => $template->r_text,
                    'user' => $user,
                    'fromUser' => $from_user,
                ]);

                if (!$notifierObject->checkClone()) {
                    continue;
                }

                Yii::$app->qunotify->push(new NotifyJob([
                    'notifierObject' => $notifierObject,
                ]));

                $this->jobedNotifications++; // counting jobed notifications
            }
        }

        if (!$this->jobedNotifications) {
            $this->addError(NotifierError::NOTIFICATIONS_NOT_SENDED, 'Notificatioins not sended.');
        }

        return $this->jobedNotifications > 0;
    }

    /**
     * @param $event_id
     * @param array $varTemplate
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function sendPushCommon($event_id, $varTemplate = []): bool
    {
        /** @var EventAR $event */
        $event = EventAR::getEvent($event_id);

        if (!$event) {
            $this->addError(NotifierError::EVENT_NOT_FOUND, 'Event not found.');

            return false;
        }

        /** @var EventAR $setting */
        $setting = $event->getSetting(self::PUSH_NOTIFIER);

        if (!$setting) {
            $this->addError(NotifierError::SETTING_NOT_FOUND, 'Setting for event not found.');

            return false;
        }

        if ($setting->default_status === self::SUBSCRIBE_OFF) {
            $this->addError(NotifierError::SUBSCRIBE_IS_OFF, 'Subscribe is off.');

            return false;
        }

        $template = TemplateAR::find()
            ->where(['event_id' => $event_id])
            ->andWhere(['direct_id' => $setting->direct_id])
            ->one();

        if (!$template) {
            $this->addError(NotifierError::TEMPLATE_NOT_FOUND, 'Template for event not found.');

            return false;
        }

        $notifierObject = $this->getNotifierObject($setting->direct_id, [
            'varTemplate' => $varTemplate,
            'event' => $event,
            'direct' => $template->direct,
            'subject' => $template->r_name,
            'message' => $template->r_text,
            'user' => false,
        ]);

        Yii::$app->qunotify->push(new NotifyJob([
            'notifierObject' => $notifierObject,
        ]));

        return true;
    }

    /**
     * @param $direct_id
     * @param array $params
     * @return bool|object
     * @throws \yii\base\InvalidConfigException
     */
    public function getNotifierObject($direct_id, $params = [])
    {
        $notifierClass = $this->getNotifierClass($direct_id);

        if ($notifierClass === false) {
            $this->addError(NotifierError::NOTIFIER_CLASS_NOT_FOUND, 'Notifier class not found.');

            return false;
        }

        return Yii::createObject($notifierClass, [$params]);
    }

    /**
     * @param $direct_id
     * @return bool|string
     */
    public function getNotifierClass($direct_id)
    {
        switch ($direct_id) {
            case self::PUSH_NOTIFIER:
                return PushNotifier::class;
            case self::SMS_NOTIFIER:
                return SmsNotifier::class;
            case self::EMAIL_NOTIFIER:
                return EmailNotifier::class;
        }

        return false;
    }

    /**
     * @return array
     */
    public static function getNotifiers(): array
    {
        return [
            self::PUSH_NOTIFIER => 'Push',
            self::SMS_NOTIFIER => 'Sms',
            self::EMAIL_NOTIFIER => 'Email',
        ];
    }
}
