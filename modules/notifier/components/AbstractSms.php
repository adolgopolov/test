<?php

namespace common\modules\notifier\components;

use common\modules\notifier\interfaces\SmsInterface;
use InvalidArgumentException;
use miserenkov\sms\client\SoapClient;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;

/**
 * Class AbstractSms
 * @package common\modules\notifier\components
 * @property SoapClient $_client
 */
abstract class AbstractSms extends BaseObject implements SmsInterface
{
    public $gateway;
    public $login;
    public $password;
    public $senderName;
    public $options = [];
    protected $_client;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!Yii::$app->params['sms_gateways']) {
            throw new InvalidConfigException('Sms gateways are not defined');
        }

        if (!$this->gateway) {
            $this->gateway = Yii::$app->params['sms_gateways'][0];
        }

        if (empty($this->login) || empty($this->password)) {
            throw new InvalidConfigException('Login and password must be set.');
        }

        if (!in_array($this->gateway, Yii::$app->params['sms_gateways'], true)) {
            throw new InvalidConfigException("Gateway \"$this->gateway\" doesn't support.");
        }

        if ($this->_client === null) {
            $this->_client = Yii::createObject(SoapClient::class, [
                $this->gateway,
                $this->login,
                $this->password,
                $this->senderName,
                $this->options,
            ]);
        }
    }

    /**
     * @return bool|float
     * @throws \miserenkov\sms\exceptions\BalanceException
     */
    public function getBalance()
    {
        return $this->_client->getBalance();
    }

    /**
     * @param $id
     * @param $phone
     * @param int $all
     * @return array|bool
     * @throws \miserenkov\sms\exceptions\Exception
     * @throws \miserenkov\sms\exceptions\StatusException
     */
    public function getStatus($id, $phone, $all = 2)
    {
        if (empty($id) || empty($phone)) {
            throw new InvalidArgumentException('For getting sms status, please, set id and phone');
        }

        return $this->_client->getMessageStatus($id, $phone, $all);
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    protected function smsIdGenerator()
    {
        return Yii::$app->security->generateRandomString(40);
    }
}
