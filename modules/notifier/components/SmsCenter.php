<?php

namespace common\modules\notifier\components;

use yii\base\NotSupportedException;

class SmsCenter extends AbstractSms
{
    public const TYPE_DEFAULT_MESSAGE = 0;
    public const TYPE_REGISTRATION_MESSAGE = 1;
    public const TYPE_RESET_PASSWORD_MESSAGE = 2;
    public const TYPE_LOGIN_MESSAGE = 3;
    public const TYPE_NOTIFICATION_MESSAGE = 4;

    /**
     * @return array
     */
    protected function allowedTypes()
    {
        return [
            self::TYPE_DEFAULT_MESSAGE,
            self::TYPE_REGISTRATION_MESSAGE,
            self::TYPE_RESET_PASSWORD_MESSAGE,
            self::TYPE_LOGIN_MESSAGE,
            self::TYPE_NOTIFICATION_MESSAGE,
        ];
    }

    /**
     * @param $numbers
     * @param $message
     * @param int $type
     * @return array|bool|string
     * @throws NotSupportedException
     * @throws \miserenkov\sms\exceptions\SendException
     * @throws \yii\base\Exception
     */
    public function send($numbers, $message, $type = self::TYPE_DEFAULT_MESSAGE)
    {
        if (!in_array($type, $this->allowedTypes(), true)) {
            throw new NotSupportedException("Message type \"$type\" doesn't support.");
        }

        if (empty($numbers) || empty($message)) {
            throw new \InvalidArgumentException('For sending sms, please, set phone number and message');
        }

        return $this->_client->sendMessage([
            'phones' => $numbers,
            'message' => strip_tags($message),
            'id' => $this->smsIdGenerator(),
        ]);
    }
}
