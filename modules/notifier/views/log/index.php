<?php

use backend\components\aboard\ActionColumn;
use common\modules\notifier\models\LetterAR;
use common\widgets\GridActions\GridActionsWidget;
use yii\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

$this->title = 'Логи уведомлений';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-ar-index">
    <?= GridActionsWidget::widget([
        'title' => $this->title,
        'enableCreateButton' => false,
    ]) ?>
    <?php Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            GridActionsWidget::$checkboxColumn,
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'event_id',
                'value' => static function (LetterAR $model) {
                    if ($model->event) {
                        return $model->event->r_name;
                    }

                    return 'Unknown';
                },
            ],
            [
                'attribute' => 'direct_id',
                'value' => static function (LetterAR $model) {
                    if ($model->direct) {
                        return $model->direct->r_name;
                    }

                    return 'Unknonw';
                },
            ],
            [
                'attribute' => 'user_id',
                'value' => static function (LetterAR $model) {
                    if ($model->user) {
                        return $model->user->username;
                    }

                    return $model->user_id;
                },
            ],
            [
                'attribute' => 'from_user',
                'value' => static function (LetterAR $model) {
                    if ($model->fromUser) {
                        return $model->fromUser->username;
                    }

                    return $model->from_user;
                },
            ],
            'uniq_stamp',
            'r_name',
//            [
//                'attribute' => 'r_text',
//                'value' => static function (LetterAR $model) {
//                    return StringHelper::truncate($model->r_text, 100);
//                },
//            ],
            'created_at',
//            'updated_at',
            [
                'class' => ActionColumn::class,
                'template' => '<div class="btn-group">{view}{delete}</div>',
            ],
        ],
    ]) ?>
    <?php Pjax::end() ?>
</div>
