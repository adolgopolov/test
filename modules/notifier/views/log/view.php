<?php

use common\modules\notifier\models\LetterAR;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $this \yii\web\View
 * @var $model LetterAR
 */

$this->title = $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'Логи уведомлений', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
?>
<div class="letter-ar-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p class="text-right">
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'event_id',
                'value' => static function (LetterAR $model) {
                    if ($model->event) {
                        return $model->event->r_name;
                    }

                    return 'Unknown';
                },
            ],
            [
                'attribute' => 'direct_id',
                'value' => static function (LetterAR $model) {
                    if ($model->direct) {
                        return $model->direct->r_name;
                    }

                    return 'Unknonw';
                },
            ],
            [
                'attribute' => 'user_id',
                'value' => static function (LetterAR $model) {
                    if ($model->user) {
                        return $model->user->username;
                    }

                    return $model->user_id;
                },
            ],
            [
                'attribute' => 'from_user',
                'value' => static function (LetterAR $model) {
                    if ($model->fromUser) {
                        return $model->fromUser->username;
                    }

                    return $model->from_user;
                },
            ],
            'uniq_stamp',
            'r_name',
            [
                'attribute' => 'r_text',
                'format' => 'raw',
            ],
            'created_at',
//            'updated_at',
        ],
    ]) ?>
</div>
