<?php

use common\modules\notifier\models\TemplateAR;
use kartik\markdown\MarkdownEditor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $this \yii\web\View
 * @var $model TemplateAR
 */

?>
<?php $form = ActiveForm::begin([
    'action' => Url::to(['update-template']),
    'method' => 'POST',
    'options' => [
        'class' => 'js-form-attach-entity',
        'data-pjax-selector' => '#template-pjax',
    ]
]) ?>

<?= $form->field($model, 'id')->hiddenInput([
    'id' => 'template-id',
])->label(false) ?>

<?= $form->field($model, 'r_name')->textInput([
    'id' => 'r-name',
]) ?>

<?= $form->field($model, 'r_text')->widget(MarkdownEditor::class, [
    'options' => [
        'id' => 'r-text',
    ],
]) ?>

<?php if ($model->hasErrors('r_text')) { ?>
    <div class="text-danger">
        <?php foreach ($model->getErrors('r_text') as $e) { ?>
            <div><?= $e ?></div>
        <?php } ?>
    </div>
<?php } ?>

<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success js-submit-attach-entity']) ?>

<?php ActiveForm::end() ?>
