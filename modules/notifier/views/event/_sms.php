<?php

use common\modules\notifier\models\TemplateAR;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $this \yii\web\View
 * @var $model TemplateAR
 */

?>
<?php $form = ActiveForm::begin([
    'action' => Url::to(['update-template']),
    'method' => 'POST',
    'options' => [
        'class' => 'js-form-attach-entity',
        'data-pjax-selector' => '#template-pjax',
    ]
]) ?>

<?= $form->field($model, 'id')->hiddenInput([
    'id' => 'template-id',
])->label(false) ?>

<?= $form->field($model, 'r_name')->textInput([
    'id' => 'r-name',
]) ?>

<?= $form->field($model, 'r_text')->textarea([
    'id' => 'r-text',
    'rows' => 6,
]) ?>

<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success js-submit-attach-entity']) ?>

<?php ActiveForm::end() ?>
