<?php

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use common\modules\notifier\models\EventAR;
use common\widgets\GridActions\GridActionsWidget;
use yii\widgets\Pjax;

$this->title = 'События';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifier-event-ar-index">
    <?= GridActionsWidget::widget([
        'title' => $this->title,
        'enableCreateButton' => false,
        'enableDeleteButton' => false,
        'enableChangeStatus' => true,
        'statusesList' => EventAR::getStatuses(),
    ]) ?>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            GridActionsWidget::$checkboxColumn,
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            'r_name',
            [
                'attribute' => 'r_status',
                'format' => 'raw',
                'value' => static function ($model) {
                    $statuses = EventAR::getStatuses();

                    return $statuses[$model->r_status] ?? 'Unknown';
                }
            ],
            [
                'class' => ActionColumn::class,
                'template' => '<div class="btn-group">{view}{update}</div>',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
