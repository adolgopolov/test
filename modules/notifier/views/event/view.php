<?php

use common\modules\notifier\components\Notifier;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model \common\modules\notifier\models\EventAR
 * @var $settingProvider \yii\data\ActiveDataProvider
 * @var $templateProvider \yii\data\ActiveDataProvider
 */

$this->title = $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'События', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];
?>
<div class="sport-team-ar-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'r_name',
            [
                'attribute' => 'r_status',
                'value' => static function ($model) {
                    return $model->r_status ? 'Активна' : 'Не активна';
                }
            ],
        ],
    ]) ?>

    <h4 class="c-grey-900 mT-10 mB-30">Настройки подписки</h4>
    <?= GridView::widget([
        'dataProvider' => $settingProvider,
        'columns' => [
            [
                'attribute' => 'direct_id',
                'value' => static function ($model) {
                    $notifiers = Notifier::getNotifiers();

                    return $notifiers[$model['direct_id']] ?? 'Unknown';
                },
            ],
            [
                'attribute' => 'default_status',
                'value' => static function ($model) {
                    return $model['default_status'] ? 'Включена' : 'Отключена';
                },
            ],
            [
                'attribute' => 'read_only',
                'value' => static function ($model) {
                    return $model['read_only'] ? 'Есть' : 'Нет';
                }
            ],
        ],
    ]) ?>

    <h4 class="c-grey-900 mT-10 mB-30">Шаблоны</h4>
    <?= GridView::widget([
        'dataProvider' => $templateProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'event_id',
                'value' => static function ($model) {
                    /** @var \common\modules\notifier\models\TemplateAR $model */
                    if ($model->event) {
                        return $model->event->r_name;
                    }

                    return 'Unknown';
                },
            ],
            [
                'attribute' => 'direct_id',
                'value' => static function ($model) {
                    /** @var \common\modules\notifier\models\TemplateAR $model */
                    if ($model->direct) {
                        return $model->direct->r_name;
                    }

                    return 'Unknown';
                }
            ],
            'r_name',
            'r_text',
        ],
    ]) ?>
</div>
