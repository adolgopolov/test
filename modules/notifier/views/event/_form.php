<?php

use common\modules\notifier\models\EventAR;
use common\widgets\DetailForm\DetailFormWidget;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model EventAR
 */
?>
<div class="event-type-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'r_name',
            'r_status' => Select2::widget([
                'model' => $model,
                'attribute' => 'r_status',
                'data' => EventAR::getStatuses(),
                'options' => [
                    'value' => $model->isNewRecord ? 1 : $model->r_status,
                ],
                'pluginOptions' => [
                    'placeholder' => 'Выберите статус',
                ],
            ]),
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
