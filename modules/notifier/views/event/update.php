<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model \common\modules\notifier\models\EventAR
 * @var $templateModel \common\modules\notifier\models\TemplateAR
 * @var $templateProvider \yii\data\ActiveDataProvider
 * @var $settingProvider \yii\data\ActiveDataProvider
 * @var $directs array
 * @var $smtpAliases array
 */

$this->title = 'Редактирование события : ' . $model->r_name;
$this->params['breadcrumbs'][] = ['label' => 'События', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->r_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';

$tabs = [
    [
        'label' => '<span class="ti ti-pencil"></span> Основная информация',
        'active' => true,
        'content' => $this->render('_form', [
            'model' => $model,
        ]),
    ],
    [
        'label' => '<span class="ti ti-layout-grid3"></span> Настройки подписок',
        'content' => $this->render('_setting', [
            'model' => $model,
            'settingProvider' => $settingProvider,
            'directs' => $directs,
        ]),
    ],
    [
        'label' => '<span class="ti ti-layout-grid3"></span> Шаблоны',
        'content' => $this->render('_template', [
            'templateProvider' => $templateProvider,
        ]),
    ],
    [
        'label' => '<span class="ti ti-email"></span> SMTP',
        'content' => $this->render('_smtp', [
            'model' => $model,
            'smtpAliases' => $smtpAliases,
        ]),
    ],
];
?>
<div class="sman-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= TabsX::widget(['items' => $tabs, 'encodeLabels' => false]) ?>
</div>
