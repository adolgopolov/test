<?php

use common\modules\notifier\models\EventAR;
use common\modules\notifier\models\TemplateAR;
use yii\bootstrap4\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\widgets\Pjax;

/**
 * @var $this \yii\web\View
 * @var $templateModel TemplateAR
 * @var $templateProvider \yii\data\ActiveDataProvider
 * @var $directs array
 */

$this->registerJsFile('js/event/template.js', ['depends' => JqueryAsset::class]);
?>

<?php Pjax::begin(['id' => 'template-pjax']) ?>

<?php Modal::begin([
    'title' => Html::tag('h5', 'Изменение шаблона - <span></span>', ['class' => 'c-grey-900']),
    'size' => Modal::SIZE_LARGE,
    'id' => 'template-modal',
]) ?>
<div class="template-modal-content"></div>
<?php /*= $this->render('_push', [
    'templateModel' => $templateModel,
]) /**/ ?>
<?php Modal::end() ?>

<?php /* Modal::begin([
    'title' => Html::tag('h5', 'Изменение шаблона - <span></span>', ['class' => 'c-grey-900']),
    'size' => Modal::SIZE_LARGE,
    'id' => 'sms-template-modal',
]) ?>
<?= $this->render('_sms', [
    'templateModel' => $templateModel,
]) ?>
<?php Modal::end() ?>

<?php Modal::begin([
    'title' => Html::tag('h5', 'Изменение шаблона - <span></span>', ['class' => 'c-grey-900']),
    'size' => Modal::SIZE_LARGE,
    'id' => 'email-template-modal',
]) ?>
<?= $this->render('_email', [
    'templateModel' => $templateModel,
]) ?>
<?php Modal::end() ?>

<?php Modal::begin([
    'title' => Html::tag('h5', 'Изменение шаблона - <span></span>', ['class' => 'c-grey-900']),
    'size' => Modal::SIZE_LARGE,
    'id' => 'default-template-modal',
]) ?>
<?= $this->render('_default', [
    'templateModel' => $templateModel,
]) ?>
<?php Modal::end() /**/ ?>

<?= GridView::widget([
    'dataProvider' => $templateProvider,
    'tableOptions' => [
        'class' => 'table table-bordered table-hover',
    ],
    'rowOptions' => static function (TemplateAR $model) {
        return [
            'data' => [
                'update-template' => $model->id,
                'direct-code' => $model->direct->ent_code ?? null,
            ],
        ];
    },
    'columns' => [
        [
            'attribute' => 'id',
            'options' => [
                'class' => 'id-grid-column',
            ],
        ],
        [
            'attribute' => 'direct_id',
            'label' => EventAR::LABEL_DIRECT_ID,
            'value' => static function (TemplateAR $model) {
                return $model->direct->r_name ?? 'Unknown';
            },
            'contentOptions' => [
                'data' => [
                    'direct' => true,
                ],
            ],
        ],
        [
            'attribute' => 'r_name',
            'contentOptions' => static function (TemplateAR $model) {
                return [
                    'data' => [
                        'r-name' => $model->r_name,
                    ],
                ];
            },
        ],
        [
            'attribute' => 'r_text',
            'contentOptions' => static function (TemplateAR $model) {
                return [
                    'data' => [
                        'r-text' => $model->r_text,
                    ],
                ];
            },
        ],
    ],
]) ?>

<?php Pjax::end() ?>
