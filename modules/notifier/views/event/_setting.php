<?php

use common\modules\notifier\models\EventAR;
use yii\bootstrap4\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var $this \yii\web\View
 * @var $model EventAR|\yii\db\ActiveRecord
 * @var $settingProvider \yii\data\ActiveDataProvider
 * @var $directs array
 */

$this->registerJsFile('js/event/setting.js', ['depends' => JqueryAsset::class]);
?>

<?php Pjax::begin(['id' => 'settings-pjax']) ?>

<?php Modal::begin([
    'title' => Html::tag('h5', 'Изменение настройки - <span></span>', ['class' => 'c-grey-900']),
    'size' => Modal::SIZE_LARGE,
    'id' => 'setting-modal',
]) ?>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['update-setting', 'id' => $model->id]),
    'method' => 'POST',
    'options' => [
        'class' => 'js-form-attach-entity',
        'data-pjax-selector' => '#settings-pjax',
    ]
]) ?>

<?= $form->field($model, 'direct_id')->hiddenInput([
    'id' => 'direct-id',
])->label(false) ?>

<?= $form->field($model, 'default_status')->dropDownList(EventAR::getSettingDefaultStatuses(), [
    'id' => 'default-status',
]) ?>

<?= $form->field($model, 'read_only')->dropDownList(EventAR::getSettingReadOnlyStatuses(), [
    'id' => 'read-only',
]) ?>

<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success js-submit-attach-entity']) ?>

<?php ActiveForm::end() ?>

<?php Modal::end() ?>

<?= GridView::widget([
    'dataProvider' => $settingProvider,
    'tableOptions' => [
        'class' => 'table table-bordered table-hover',
    ],
    'rowOptions' => static function () use ($model) {
        return [
            'data' => [
                'update-setting' => $model->id,
            ],
        ];
    },
    'columns' => [
        [
            'attribute' => 'direct_id',
            'label' => EventAR::LABEL_DIRECT_ID,
            'value' => static function ($model) use ($directs) {
                return $directs[$model['direct_id']] ?? 'Unknown';
            },
            'contentOptions' => static function ($model) use ($directs) {
                return [
                    'data' => [
                        'direct-title' => $directs[$model['direct_id']] ?? 'Unknown',
                        'direct-id' => $model['direct_id'],
                    ],
                ];
            },
        ],
        [
            'attribute' => 'default_status',
            'label' => EventAR::LABEL_DEFAULT_STATUS,
            'value' => static function ($model) {
                $defaultStatuses = EventAR::getSettingDefaultStatuses();

                return $defaultStatuses[$model['default_status']] ?? 'Unknown';
            },
            'contentOptions' => static function ($model) {
                return [
                    'data' => [
                        'default-status' => $model['default_status'],
                    ],
                ];
            },
        ],
        [
            'attribute' => 'read_only',
            'label' => EventAR::LABEL_READ_ONLY,
            'value' => static function ($model) {
                $readOnlyStatuses = EventAR::getSettingReadOnlyStatuses();

                return $readOnlyStatuses[$model['read_only']] ?? 'Unknown';
            },
            'contentOptions' => static function ($model) {
                return [
                    'data' => [
                        'read-only' => $model['read_only'],
                    ],
                ];
            },
        ],
    ],
]) ?>

<?php Pjax::end() ?>
