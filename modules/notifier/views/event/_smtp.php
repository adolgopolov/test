<?php

/**
 * @var $this \yii\web\View
 * @var $model EventAR
 * @var $smtpAliases array
 */

use common\modules\notifier\models\EventAR;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;

$smtpValue = EventAR::SMTP_SETTINGS_DEFAULT;
if (is_string($model->smtp)) {
    $smtpValue = $model->smtp;
}
if (is_array($model->smtp)) {
    $smtpValue = EventAR::SMPT_SETTINGS_CUSTOM;
}

$this->registerJsVar('smtpCustom', EventAR::SMPT_SETTINGS_CUSTOM);
$this->registerJsFile('/js/event/smtp.js', ['depends' => JqueryAsset::class]);
?>
<?php $form = ActiveForm::begin([
    'action' => ['update-smtp', 'id' => $model->id],
]) ?>

<?= $form->field($model, 'smtp_alias')->widget(Select2::class, [
    'data' => $smtpAliases,
    'options' => [
        'id' => 'smtp-alias',
        'value' => $smtpValue,
    ],
    'pluginOptions' => [
        'placeholder' => 'Выберите настройки из списка',
    ],
]) ?>

<div id="smtp-settings" class="display-none">
    <h5 class="c-grey-600 mT-10 mB-20">Свои настройки</h5>

    <?= $form->field($model, 'smtp[transport][host]')->textInput()->label('Host') ?>
    <?= $form->field($model, 'smtp[transport][port]')->textInput()->label('Port') ?>
    <?= $form->field($model, 'smtp[transport][encryption]')->textInput()->label('Encryption') ?>
    <?= $form->field($model, 'smtp[transport][username]')->textInput()->label('Username') ?>
    <?= $form->field($model, 'smtp[transport][password]')->textInput()->label('Password') ?>
</div>

<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>
