<?php

use common\modules\notifier\models\TemplateAR;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $this \yii\web\View
 * @var $templateModel TemplateAR
 */

?>
<?php $form = ActiveForm::begin([
    'action' => Url::to(['update-template']),
    'method' => 'POST',
    'options' => [
        'class' => 'js-form-attach-entity',
        'data-pjax-selector' => '#template-pjax',
    ]
]) ?>

<?= $form->field($templateModel, 'id')->hiddenInput([
    'id' => 'template-id',
])->label(false) ?>

<?= $form->field($templateModel, 'r_name')->textInput([
    'id' => 'r-name',
]) ?>

<?= $form->field($templateModel, 'r_text')->textarea([
    'id' => 'r-text',
    'rows' => 9,
]) ?>

<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success js-submit-attach-entity']) ?>

<?php ActiveForm::end() ?>
