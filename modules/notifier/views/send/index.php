<?php

/**
 * @var $this \yii\web\View
 */

use common\widgets\notifier\NotifierWidget;
use yii\helpers\Html;

$this->title = 'Отправить уведомление';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifier-send-index">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= NotifierWidget::widget() ?>
</div>
