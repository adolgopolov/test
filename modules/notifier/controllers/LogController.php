<?php

namespace common\modules\notifier\controllers;

use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\modules\notifier\models\LetterAR;
use yii\data\ActiveDataProvider;

class LogController extends AdminController
{
    use CRUDTrait;

    public $modelClass = LetterAR::class;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => LetterAR::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionCreate()
    {
        return $this->redirect(['index']);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionUpdate()
    {
        return $this->redirect(['index']);
    }
}
