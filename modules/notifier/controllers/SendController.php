<?php

namespace common\modules\notifier\controllers;

use backend\controllers\AdminController;
use common\widgets\notifier\actions\NotifyAction;

class SendController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'notify' => [
                'class' => NotifyAction::class,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
