<?php

namespace common\modules\notifier\controllers;

use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\modules\notifier\models\DirectAR;
use common\modules\notifier\models\EventAR;
use common\modules\notifier\models\TemplateAR;
use Yii;
use yii\base\ExitException;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class EventController extends AdminController
{
    use CRUDTrait;

    public $modelClass = EventAR::class;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => EventAR::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return Response
     */
    public function actionCreate()
    {
        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        /** @var EventAR $model */
        $model = EventAR::findOrFail($id);

        $settingProvider = new ArrayDataProvider([
            'allModels' => $model->settings,
        ]);

        $templateProvider = new ActiveDataProvider([
            'query' => $model->getTemplates(),
        ]);

        return $this->render('view', [
            'model' => $model,
            'settingProvider' => $settingProvider,
            'templateProvider' => $templateProvider,
        ]);
    }

    /**
     * @param int $id
     * @return string|Response
     * @throws ExitException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var EventAR $model */
        $model = EventAR::findOrFail($id);

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно обновлена');

            return $this->redirect(['update', 'id' => $id]);
        }

        $settings = $model->settings ?: [];

        $settingProvider = new ArrayDataProvider([
            'allModels' => $settings,
        ]);

        $templateProvider = new ActiveDataProvider([
            'query' => $model->getTemplates(),
        ]);

        $directs = DirectAR::getDirectList();
        $smtpAliases = EventAR::getSmtpAliases();

        return $this->render('update', [
            'model' => $model,
            'settingProvider' => $settingProvider,
            'templateProvider' => $templateProvider,
            'directs' => $directs,
            'smtpAliases' => $smtpAliases,
        ]);
    }

    /**
     * @param int $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateSmtp($id)
    {
        /** @var EventAR $model */
        $model = EventAR::findOrFail($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->saveSmtp()) {
                Yii::$app->session->addFlash('success', 'Настройки SMTP успешно сохранены.');
            } else {
                Yii::$app->session->addFlash('error', $model->errors);
            }
        }

        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * @return Response
     */
    public function actionDelete()
    {
        Yii::$app->session->addFlash('info', 'Удаление запрещено');

        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUpdateSetting($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var EventAR $model */
        $model = EventAR::findOrFail($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $ext_info = $model->ext_info;

            if (isset($ext_info['settings'][$model->direct_id])) {
                $ext_info['settings'][$model->direct_id]['default_status'] = $model->default_status;
                $ext_info['settings'][$model->direct_id]['read_only'] = $model->read_only;
                $model->updateAttributes([
                    'direct_id' => null,
                    'ext_info' => $ext_info,
                ]);

                if ($model->save()) {
                    return ['success' => true, 'message' => 'Настройка успешно изменена.'];
                }
            }
        }

        return ['success' => false, 'message' => 'Произошла ошибка.'];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUpdateTemplate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $error_response = ['success' => false, 'Произошла ошибка.'];

        $_post = Yii::$app->request->post('TemplateAR');

        if (!isset($_post['id'])) {
            return $error_response;
        }

        /** @var TemplateAR $model */
        $model = TemplateAR::findOrFail($_post['id']);

        unset($_post['id']);

        if ($model->load($_post, '') && $model->save()) {
            return ['success' => true, 'message' => 'Шаблон успешно изменён.'];
        }

        return $error_response;
    }

    /**
     * @param int $id
     * @param string $direct_code
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionRenderModal($id, $direct_code)
    {
        /** @var TemplateAR $model */
        $model = TemplateAR::findOrFail($id);

        if (Yii::$app->request->isPost) {
            return $this->goBack();
        }

        $view = str_replace('direct.', '_', $direct_code);

        return $this->renderAjax($view, [
            'model' => $model,
        ]);
    }
}
