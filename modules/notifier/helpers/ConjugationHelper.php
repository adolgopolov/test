<?php

namespace common\modules\notifier\helpers;

use yii\base\BaseObject;

/**
 * Class ConjugationHelper
 * @package common\modules\notifier\helpers
 */
class ConjugationHelper extends BaseObject
{
    public static $numerals = [ // 1, 2, 5
        'рубль' => ['рубль', 'рубля', 'рублей'],
        'спортсмен' => ['спортсмен', 'спортсмена', 'спортсменов'],
        'кит' => ['кит', 'кита', 'китов'],
    ];

    /**
     * @param $number
     * @param $name
     * @return string
     */
    public static function numEnding($number, $name)
    {
        $titles = self::$numerals[$name] ?? [];

        if (empty($titles)) {
            return $name;
        }

        $cases = [2, 0, 1, 1, 1, 2];
        $format = $titles[ ($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)] ];

        return sprintf($format, $number);
    }
}
