<?php

namespace common\modules\notifier;

use backend\components\aboard\MenuEntryInterface;
use yii\base\Module;

/**
 * Class NotifierModule
 * @package common\modules\notifier
 */
class NotifierModule extends Module implements MenuEntryInterface
{
    /**
     * @return array|null
     */
    public static function getMenuItems(): ?array
    {
        return [
            [
                'label' => 'Уведомления',
                'icon' => 'email',
                'color' => 'brown-500',
                'items' => [
                    [
                        'label' => 'События',
                        'url' => ['/notifier/event/index'],
                    ],
                    [
                        'label' => 'Логи уведомлений',
                        'url' => ['/notifier/log/index'],
                    ],
                    [
                        'label' => 'Отправить уведомление',
                        'url' => ['/notifier/send/index'],
                    ],
                ],
            ],
        ];
    }
}
