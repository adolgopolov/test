<?php

namespace common\modules\user;

use backend\components\aboard\MenuEntryInterface;
use backend\components\aboard\DashboardWidgetInterface;
use common\modules\user\widgets\UserCounter;

/**
 * user module definition class
 */
class Module extends \yii\base\Module implements MenuEntryInterface, DashboardWidgetInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\user\controllers';
    public $defaultRoute = 'manage';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return array
     */
    public static function getMenuItems(): ?array
    {
        return [
            [
                'label' => 'Пользователи',
                'icon'  => 'user',
                'color' => 'blue-500',
                'items' => [
                    [
                        'label'     => 'Список',
                        'url'       => ['/user/manage/index'],
                        'icon'      => 'list',
                        'color'     => 'blue-500'
                    ],
                    [
                        'label'     => 'Создать',
                        'url'       => ['/user/manage/create'],
                        'icon'      => 'plus',
                        'color'     => 'green-500'
                    ]
                ]
            ]
        ];
    }

    public static function getDashboardWidgets(): ?array
    {
        return [];
    }

    public static function getDashboardCounters(): ?array
    {
        return [
            UserCounter::widget()
        ];
    }
}
