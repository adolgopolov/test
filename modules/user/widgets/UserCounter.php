<?php

namespace common\modules\user\widgets;

use yii\base\Widget;
use common\modules\user\models\UserAQ;

class UserCounter extends Widget
{
    public function run()
    {
        $userQuery = new UserAQ('common\modules\user\models\UserAR');
        return $this->render('user-counter', ['count' => $userQuery->actual()->count()]);
    }
}
