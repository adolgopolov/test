<?php

namespace common\modules\user\senders;

use common\components\pusher\senders\AbstractSender;
use common\components\pusher\traits\BeforeSendTrait;
use common\modules\user\models\UserAR;
use Yii;
use yii\base\InvalidConfigException;

class UserSender extends AbstractSender
{
    use BeforeSendTrait;

    /** @var UserAR */
    public $model;

    /**
     * @throws InvalidConfigException
     */
    public function defaultScenario()
    {
        /** @var UserAR $clonedModel */
        $clonedModel = $this->getData('clonedModel');

        if (!$clonedModel || $clonedModel->isNewRecord) {
            return;
        }

        Yii::$app->pushSender->private->update
            ->setSubjectId($this->model->id)
            ->addOneRecord('users', $this->model)
            ->pushJob();
    }
}
