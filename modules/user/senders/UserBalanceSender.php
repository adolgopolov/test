<?php

namespace common\modules\user\senders;

use common\components\pusher\senders\AbstractSender;
use common\components\S3Kind;
use common\modules\money\models\UserBalanceAR;
use Yii;

class UserBalanceSender extends AbstractSender
{
    public const SCENARIO_UPDATE = 'update';

    /** @var UserBalanceAR */
    public $model;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function updateScenario()
    {
        Yii::$app->pushSender->private->update
            ->setSubjectId($this->model->user_id)
            ->setKind(S3Kind::KIND_USER)
            ->addOneRecord('balances', $this->model)
            ->pushJob();
    }
}
