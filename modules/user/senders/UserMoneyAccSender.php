<?php

namespace common\modules\user\senders;

use common\components\pusher\senders\AbstractSender;
use common\components\pusher\traits\BeforeSendTrait;
use common\modules\money\models\UserBalanceAR;
use common\modules\money\models\UserMoneyAccAR;
use yii\base\InvalidConfigException;

class UserMoneyAccSender extends AbstractSender
{
    use BeforeSendTrait;

    public const SCENARIO_UPDATE = 'update';

    /** @var UserMoneyAccAR */
    public $model;

    /**
     * @throws InvalidConfigException
     */
    public function updateScenario()
    {
        if (empty($this->model->acc_value)) {
            return;
        }

        $balance = UserBalanceAR::find()
            ->andWhere(['user_id' => $this->model->user_id])
            ->andWhere(['acc_id' => $this->model->acc_id])
            ->one();

        if ($balance) {
            $balance->sendModel(UserBalanceSender::SCENARIO_UPDATE);
        }
    }
}
