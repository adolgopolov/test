<?php

namespace common\modules\user\forms;

use yii\db\Expression;
use common\modules\game\models\GameLineupAR;
use common\modules\money\models\UserMoneyAccAR;
use common\modules\money\MoneyModule;

/**
 * Class UserFM
 * @package common\modules\user\forms
 */
class UserFM extends \common\modules\user\models\UserAR
{
    /**
     * @return |null
     */
    public function spentRub()
    {
        $transactions = UserMoneyAccAR::find()
            ->select(['SUM(acc_value) as spent_rub'])
            ->where(['user_id' => $this->id])
            ->andWhere(['acc_id' => MoneyModule::ACCOUNT_RUB])
            ->andWhere(['<', 'acc_value', 0])
            ->asArray()
            ->one();

        return $transactions['spent_rub'] ?? null;
    }

    /**
     * @return string
     */
    public function spentOnPacks()
    {
        $table = UserMoneyAccAR::tableName();

        return UserMoneyAccAR::find()
            ->select(['SUM(acc_value) as sum', 'acc_id'])
            ->where(['user_id' => $this->id])
            ->andWhere(['IN', 'acc_id', [MoneyModule::ACCOUNT_GREEN, MoneyModule::ACCOUNT_GOLD]])
            ->andWhere(['<', 'acc_value', 0])
            ->andWhere(['is not', new Expression("JSON_EXTRACT({$table}.ext_info, '$.pack_id')"), null])
            ->groupBy('acc_id')
            ->asArray()
            ->all();
    }

    /**
     * @return string
     */
    public function spentOnTourneys()
    {
        return UserMoneyAccAR::find()
            ->select(['SUM(acc_value) as sum', 'acc_id'])
            ->where(['user_id' => $this->id])
            ->andWhere(['IN', 'acc_id', [MoneyModule::ACCOUNT_GREEN, MoneyModule::ACCOUNT_GOLD]])
            ->andWhere(['<', 'acc_value', 0])
            ->andWhere(['is not', 'tou_id', null])
            ->groupBy('acc_id')
            ->asArray()
            ->all();
    }

    /**
     * @return mixed
     */
    public function winCoins()
    {
        return GameLineupAR::find()
            ->select('SUM(l_award) as sum')
            ->where(['user_id' => $this->id])
            ->asArray()
            ->one()['sum'];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'spent_rub' => 'Потрачено рублей',
            'spent_on_packs' => 'Потрачено на паки',
            'spent_on_tourneys' => 'Потрачено на турниры',
            'win_coins' => 'Выиграно фишек',
        ]);
    }
}
