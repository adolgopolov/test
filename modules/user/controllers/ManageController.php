<?php

namespace common\modules\user\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use backend\controllers\AdminController;
use backend\controllers\CRUDTrait;
use common\components\S3Core;
use common\modules\goods\forms\GivePackFM;
use common\modules\goods\models\PackAR;
use common\modules\money\models\{MoneyCurrencyAR, UserMoneyAccAR};
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\user\models\{UserAR, UserAS};
use common\modules\user\senders\UserSender;
use common\modules\money\models\UserMoneyAccAS;
use common\modules\user\forms\UserFM;

/**
 * ManageController implements the CRUD actions for User model.
 */
class ManageController extends AdminController
{
    use CRUDTrait;

    public $modelClass = UserAR::class;

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = UserFM::findOrFail($id);
        $accounts = S3Core::mMoney()::accountLabel();
        $currencies = MoneyCurrencyAR::find()->all();
        $balances = S3Core::mMoney()->userBalance($id);
        $userMoneySearchModel = new UserMoneyAccAS();

        return $this->render('view', [
            'model' => $model,
            'accounts' => $accounts,
            'currencies' => $currencies,
            'balances' => $balances,
            'transactions' => $userMoneySearchModel->getUserTransactions($id),
            'tourneyTransactions' => $userMoneySearchModel->getTourneyTransactionsByUser($id),
            'currencyTransactions' => $userMoneySearchModel->getTransactionsBuyCurrency($id),
        ]);
    }

    /**
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new UserAR();

        if ($model->load(Yii::$app->request->post())) {
            $model->generateAuthKey();
            $model->setPassword(Yii::$app->security->generateRandomString());
            $clonedModel = clone $model;

            if ($model->save()) {
                $model->sendModel(UserSender::SCENARIO_DEFAULT, ['clonedModel' => $clonedModel]);

                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($id)
    {
        /** @var UserAR $model */
        $model = UserAR::findOrFail($id);
        $balances = S3Core::mMoney()->userBalance($model->id, 'all', 'strong');
        $clonedModel = clone $model;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Информация о пользователе успешно изменена.');
                $model->sendModel(UserSender::SCENARIO_DEFAULT, ['clonedModel' => $clonedModel]);

                foreach ((array)$model->balances as $acc_id => $acc_value) {
                    if (empty($acc_value) && $acc_value !== 0) {
                        continue;
                    }

                    $oldValue = $balances[$acc_id] ?? 0;
                    $result = UserMoneyAccAR::increase($model->id, $acc_id, $acc_value - $oldValue);

                    if ($result !== true) {
                        Yii::$app->session->addFlash('error', $result);
                    }
                }

                return $this->redirect(['update', 'id' => $id]);
            }

            Yii::$app->session->addFlash('error', $model->errors);
        }

        $currencies = MoneyCurrencyAR::find()->all();
        $modelGivePack = new GivePackFM();
        $packList = PackAR::getPackList();

        if ($modelGivePack->givePack()) {
            return $this->redirect(['update', 'id' => $id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelGivePack' => $modelGivePack,
            'currencies' => $currencies,
            'balances' => $balances,
            'packList' => $packList,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $model = UserAR::findOrFail($id);

        if (!S3Core::isAllowedDelete($model)) {
            Yii::$app->session->addFlash('info', 'Удаление запрещено');
            return $this->redirect(['index']);
        }

        if (YII_DEBUG && $model->delete()) {
            Yii::$app->session->addFlash('success', 'Удаление пользователя прошло успешно.');
        } else {
            Yii::$app->session->addFlash('error', $model->errors);
        }

        return $this->redirect(['index']);
    }
}
