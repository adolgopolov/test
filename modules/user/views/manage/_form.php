<?php


use common\modules\user\models\UserAR;
use common\widgets\DetailForm\DetailFormWidget;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model UserAR
 * @var $currencies \common\modules\money\models\MoneyCurrencyAR[]
 * @var $balances array
 */

$disabled = !$model->isNewRecord;
$currentRole = Yii::$app->user->getIdentity()->isAdmin();
$canModifyStatus = $currentRole >= UserAR::ROLE_ADM_TALENT && $currentRole >= $model->user_role;
?>
<div class="user-form">
    <?php $form = ActiveForm::begin() ?>
    <?php
    $inputs = [
        'id' => [
            'inputHidden' => $model->isNewRecord,
            'inputOptions' => [
                'disabled' => true,
            ],
        ],
        'username' => [
            'inputOptions' => [
                'disabled' => !$model->isNewRecord,
            ],
        ],
        'email' => [
            'inputType' => 'email',
        ],
        'status' => Select2::widget([
            'model' => $model,
            'attribute' => 'status',
            'data' => UserAR::getStatuses(),
            'options' => [
                'disabled' => !$canModifyStatus,
            ],
            'pluginOptions' => [
                'placeholder' => 'Выберите статус',
            ],
        ]),
        'user_role' => Select2::widget([
            'model' => $model,
            'attribute' => 'user_role',
            'data' => UserAR::getRoles($canModifyStatus ? $currentRole : 0xFF),
            'options' => [
                'disabled' => !$canModifyStatus,
            ],
            'pluginOptions' => [
                'placeholder' => 'Выберите статус',
            ],
        ]),
        'created_at' => [
            'inputHidden' => $model->isNewRecord,
            'inputOptions' => [
                'disabled' => true,
            ],
        ],
        'updated_at' => [
            'inputHidden' => $model->isNewRecord,
            'inputOptions' => [
                'disabled' => true,
            ],
        ],
    ];

    if (!$model->isNewRecord) {
        foreach ($currencies as $currency) {
            $inputs[$currency->ent_code] = [
                'label' => $currency->r_name,
                'inputValue' => Html::activeInput('number', $model, "balances[$currency->id]", [
                    'class' => 'form-control',
                    'value' => $balances[$currency->id] ?? null,
                ]),
            ];
        }

        $inputs['user_packs'] = [
            'label' => 'Все паки',
            'inputValue' => $this->render('_packs', [
                'packs' => $model->getPackLinks(),
                'accordionId' => 'accordionUserPacks',
                'forRead' => true,
            ]),
        ];

        $inputs['active_user_packs'] = [
            'label' => 'Неоткрытые паки',
            'inputValue' => $this->render('_packs', [
                'packs' => $model->getPackLinks(true),
                'accordionId' => 'accordionActiveUserPacks',
                'forRead' => true,
            ]),
        ];
    }
    ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => $inputs,
    ]) ?>

    <div class="form-group text-right">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end() ?>
</div>
