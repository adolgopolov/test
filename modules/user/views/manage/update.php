<?php

/**
 * @var $this yii\web\View
 * @var $model \common\modules\user\models\UserAR
 * @var $modelGivePack \common\modules\goods\forms\GivePackFM
 * @var $currencies \common\modules\money\models\MoneyCurrencyAR[]
 * @var $balances array
 * @var $packList array
 */

use kartik\select2\Select2;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Редактирование пользователя: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
?>
<?php Modal::begin([
    'title' => Html::tag('h4', 'Подарить пак пользователю', ['class' => 'c-grey-900']),
    'id' => 'pack-modal',
]) ?>
<?php $form = ActiveForm::begin() ?>

<?= $form->field($modelGivePack, 'user_id')->hiddenInput(['value' => $model->id])->label(false) ?>

<?= $form->field($modelGivePack, 'pack_id')->widget(Select2::class, [
    'data' => $packList,
    'pluginOptions' => [
        'placeholder' => 'Выберите пак',
    ],
]) ?>

<?= Html::submitButton('Подарить пак', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>
<?php Modal::end() ?>
<div class="user-update">
    <h3 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h3>
    <p class="text-right">
        <?= Html::a('Отправить уведомление', '#', [
            'class' => 'btn btn-primary',
        ]) ?>
        <?= Html::button('Подарить пак', [
            'class' => 'btn btn-primary',
            'data' => [
                'toggle' => 'modal',
                'target' => '#pack-modal',
            ],
        ]) ?>
    </p>
    <?= $this->render('_form', [
        'model' => $model,
        'currencies' => $currencies,
        'balances' => $balances,
    ]) ?>
</div>
