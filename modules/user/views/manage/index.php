<?php

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\user\models\UserAR;
use common\modules\user\models\UserAS;
use common\widgets\GridActions\GridActionsWidget;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $searchModel UserAS
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    'username',
    'email:email',
    ['attribute' => 'status',
        'label' => 'Статус',
        'filter' => UserAR::getStatuses(),
        'value' => static function (UserAR $data) {
            return $data->statusLabel();
        },
    ],
    ['attribute' => 'user_role',
        'label' => 'Роль',
        'filter' => UserAR::getRoles(),
        'value' => static function (UserAR $data) {
            return $data->roleLabel();
        },
    ],
    'created_at',
    'updated_at',
    ['class' => ActionColumn::class],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="user-index">
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#user-pjax',
        'pjaxUrl' => '/user/manage/index',
        'model' => $searchModel,
        'additionalContainerOptions' => [
            'class' => [
                'columns-select2-margined',
            ],
        ],
    ]) ?>
    <?= GridActionsWidget::widget([
        'title' => $this->title,
    ]) ?>
    <?php Pjax::begin(['id' => 'user-pjax']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => ArrayHelper::merge([GridActionsWidget::$checkboxColumn], $columns),
    ]) ?>
    <?php Pjax::end(); ?>
</div>
