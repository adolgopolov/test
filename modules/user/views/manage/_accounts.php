<?php

use common\modules\money\MoneyModule;

?>

<h3 class="c-grey-900 mT-10 mB-30">Счета</h3>

<table id="w0" class="table table-striped table-bordered detail-view">
    <thead>
    <tr>
        <th></th>
        <th>
            Золотые
        </th>
        <th>
            Зеленые
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>Текущий счет</th>
        <?php foreach ($currencies as $currency): ?>
            <td>
                <?php if (in_array($currency->id, [MoneyModule::ACCOUNT_GOLD, MoneyModule::ACCOUNT_GREEN])) {
                    echo $balances[$currency->id] ?? null;
                } ?>
            </td>
        <?php endforeach; ?>
    </tr>
    <tr>
        <th>Участие в турнирах</th>
        <?php foreach ($model->spentOnPacks() as $account): ?>
            <td><?= $account['sum'] ?></td>
        <?php endforeach; ?>
    </tr>
    <tr>
        <th>Покупка паков</th>
        <?php foreach ($model->spentOnTourneys() as $account): ?>
            <td><?= $account['sum'] ?></td>
        <?php endforeach; ?>
    </tr>
    </tbody>
</table>
