<?php

/**
 * @var $this \yii\web\View
 * @var $packs array
 * @var $accordionId string
 * @var $forRead bool
 */

use yii\helpers\Html;

?>
<?= Html::button('<i class="fa fa-arrow-down"></i> Развернуть перечень', [
    'class' => 'btn btn-primary',
    'type' => 'button',
    'data' => [
        'toggle' => 'collapse',
        'target' => '#' . $accordionId,
    ],
    'aria' => [
        'expanded' => 'true',
        'controls' => $accordionId,
    ],
]) ?>
<div id="<?= $accordionId ?>" class="collapse mT-5" aria-labelledby="headingOne">
    <?php if (!$forRead) { ?>
    <?= Html::submitButton('Удалить из вещей', [
        'class' => 'btn btn-danger btn-sm',
    ]) ?>
    <?php } ?>
    <ul>
        <?php foreach ($packs as $pack) { ?>
            <li><?= (!$forRead ? Html::checkbox('good_id') : '') ?> <?= $pack ?></li>
        <?php } ?>
    </ul>
</div>
