<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use backend\components\aboard\widgets\GridView;
use common\modules\money\forms\UserMoneyAccFM;
use common\modules\money\MoneyModule;

$defaultColumn = [
    'id',
    [
        'attribute' => 'acc_id',
        'value' => static function (UserMoneyAccFM $model) {
            return MoneyModule::accountLabel($model->acc_id);
        },
        'format' => 'raw',
    ],
    'acc_value',
    'updated_at',
];

?>

<h3 class="c-grey-900 mT-10 mB-30">
    Покупка монет

    <?= Html::button('<i class="fa fa-arrow-down"></i> Развернуть таблицу', [
        'class' => 'btn btn-primary',
        'type' => 'button',
        'data' => [
            'toggle' => 'collapse',
            'target' => '#currencyTransactions',
        ],
    ]) ?>
</h3>

<div id="currencyTransactions" class="collapse mT-5" aria-labelledby="headingOne">
    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $currencyTransactions,
        'columns' => array_merge($defaultColumn, [
            [
                'attribute' => 'r_status',
                'value' => static function (UserMoneyAccFM $model) {
                    return $model->getStatusName();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'order_',
                'value' => static function (UserMoneyAccFM $model) {
                    return $model->getStatusName();
                },
                'format' => 'raw',
            ],
        ])
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>

<hr>

<h3 class="c-grey-900 mT-10 mB-30">
    Участие в турнирах

    <?= Html::button('<i class="fa fa-arrow-down"></i> Развернуть таблицу', [
        'class' => 'btn btn-primary',
        'type' => 'button',
        'data' => [
            'toggle' => 'collapse',
            'target' => '#tourneyTransactions',
        ],
    ]) ?>
</h3>

<div id="tourneyTransactions" class="collapse mT-5" aria-labelledby="headingOne">
    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $tourneyTransactions,
        'columns' => array_merge($defaultColumn, [
            'tou_id',
            [
                'attribute' => 'tou_name',
                'value' => static function (UserMoneyAccFM $model) {
                    return $model->tourney->r_name ?? null;
                },
                'format' => 'raw',
            ],
        ])
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>

<hr>

<h3 class="c-grey-900 mT-10 mB-30">
    Все транзакции

    <?= Html::button('<i class="fa fa-arrow-down"></i> Развернуть таблицу', [
        'class' => 'btn btn-primary',
        'type' => 'button',
        'data' => [
            'toggle' => 'collapse',
            'target' => '#transactions',
        ],
    ]) ?>
</h3>

<div id="transactions" class="collapse mT-5" aria-labelledby="headingOne">
    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $transactions,
        'columns' => array_merge($defaultColumn, [
            [
                'attribute' => 'transaction_kind',
                'value' => static function (UserMoneyAccFM $model) {
                    return $model->getTransactionKind();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'transaction_type',
                'value' => static function (UserMoneyAccFM $model) {
                    return $model->getTransactionType();
                },
                'format' => 'raw',
            ],
        ])
    ])
    ?>
    <?php Pjax::end(); ?>
</div>
