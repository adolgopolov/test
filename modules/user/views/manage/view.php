<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\user\forms\UserFM;

/**
 * @var $this yii\web\View
 * @var $model UserFM
 * @var $moneyModule \common\modules\money\MoneyModule
 * @var $accounts array
 * @var $currencies \common\modules\money\models\MoneyCurrencyAR[]
 */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];

$attributes = [
    'id',
    'username',
    'email',
    [
        'attribute' => 'status',
        'value' => static function (UserFM $model) {
            return $model->statusLabel();
        }
    ],
    [
        'attribute' => 'user_role',
        'value' => static function (UserFM $model) {
            return $model->roleLabel();
        }
    ],
    'exp_points',
    'rank_id',
    'rank_progress',
    'elite_id',
    'updated_at',
    [
        'attribute' => 'spent_rub',
        'value' => static function (UserFM $model) {
            return $model->spentRub();
        }
    ],
    [
        'attribute' => 'win_coins',
        'value' => static function (UserFM $model) {
            return $model->winCoins();
        }
    ],
];

foreach ($currencies as $currency) {
    $attributes[] = [
        'attribute' => $currency->ent_code,
        'label' => $currency->r_name,
        'value' => static function () use ($balances, $currency) {
            return $balances[$currency->id] ?? null;
        },
    ];
}

$attributes[] = [
    'attribute' => 'user_packs',
    'label' => 'Все паки',
    'value' => function (UserFM $model) {
        $packs = $model->getPackLinks();

        return $this->render('_packs', [
            'packs' => $packs,
            'accordionId' => 'accordionUserPacks',
            'forRead' => false,
        ]);
    },
    'format' => 'raw',
];

$attributes[] = [
    'attribute' => 'active_user_packs',
    'label' => 'Неоткрытые паки',
    'value' => function (UserFM $model) {
        $packs = $model->getPackLinks(true);

        return $this->render('_packs', [
            'packs' => $packs,
            'accordionId' => 'accordionActiveUserPacks',
            'forRead' => false,
        ]);
    },
    'format' => 'raw',
];

?>
<div class="user view">
    <h3 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h3>
    <p class="text-right">
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этого пользователя?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

    <?= $this->render('_accounts', [
        'currencies' => $currencies,
        'model' => $model,
        'balances' => $balances,
    ]); ?>

    <h3 class="c-grey-900 mT-10 mB-30"></h3>
    <?= Html::beginForm(['/money/payment/increase'], 'get', ['class' => 'form-inline']); ?>
    <?= Html::hiddenInput('user_id', $model->id) ?>
    <?= Html::dropDownList('acc_id', null, $accounts, ['class' => 'form-control mL-5']) ?>
    <?= Html::input('money', 'money', null, ['class' => 'form-control mL-5']) ?>
    <?= Html::submitButton('Зачислить на счёт', ['class' => 'btn btn-warning mL-5']) ?>
    <?= Html::endForm() ?>

    <?= $this->render('_transactions', [
        'currencyTransactions' => $currencyTransactions,
        'tourneyTransactions' => $tourneyTransactions,
        'transactions' => $transactions,
    ]); ?>

</div>
