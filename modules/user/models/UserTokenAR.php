<?php

namespace common\modules\user\models;


use common\extensions\ExtAQ;
use common\extensions\ExtInfoAR;

/**
 * Class UserTokenAR
 * @property int id
 * @property string updated_at
 * @property string password_reset_token
 * @property string verification_token
 * @property string change_email_token
 * @property string change_phone_token
 * @property string $email_advanced
 * @property string $phone_advanced
 *
 * @package common\modules\user\models
 */
class UserTokenAR extends ExtInfoAR
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%user_token}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['password_reset_token', 'verification_token', 'change_email_token', 'change_phone_token'], 'safe'],
            [['email_advanced'], 'email'],
        ];
    }

    /**
     * @return ExtAQ|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new ExtAQ(static::class);
    }

    /**
     * @return array
     */
    protected function jsonFields(): array
    {
        return [
            'email_advanced',
            'phone_advanced',
        ];
    }

    /**
     * @param int $user_id
     * @return array
     */
    public static function getTokens($user_id): array
    {
        $model = self::find()
            ->forRead()
            ->asArray()
            ->andWhere(['id' => $user_id])
            ->one();

        $tokens = [];

        if ($model) {
            unset($model['id'], $model['updated_at']);

            foreach ($model as $key => $value) {
                $tokens[$key] = $value;
            }
        }

        return $tokens;
    }

    /**
     * @param $user_id
     * @param array $tokens
     * @return bool
     */
    public static function setTokens($user_id, array $tokens)
    {
        $model = self::find()
            ->forUpdate()
            ->andWhere(['id' => $user_id])
            ->one();

        if (!$model) {
            $model = new self([
                'id' => $user_id,
            ]);
        }

        $model->setAttributes($tokens);

        return $model->save();
    }
}
