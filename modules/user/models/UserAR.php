<?php

namespace common\modules\user\models;

use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\helpers\Html;
use yii\web\IdentityInterface;
use common\components\pusher\interfaces\SenderModelInterface;
use common\components\pusher\senders\AbstractSender;
use common\doings\user\ChangeEmailDoing;
use common\doings\user\ChangePhoneDoing;
use common\extensions\ExtAQ;
use common\extensions\ExtInfoAR;
use common\components\S3Core;
use common\models\AuthAR;
use common\modules\game\models\GameLineupAR;
use common\modules\goods\components\S3GoodKindAR;
use common\modules\goods\models\UserPackAR;
use common\modules\money\models\UserMoneyAccAR;
use common\modules\user\senders\UserSender;
use common\traits\StatusARTrait;
use common\validators\JsonValidator;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status read-only
 * @property integer $joStatus
 * @property string $created_at
 * @property string $updated_at
 * @property integer $user_role
 * @property integer $avatar_id
 * @property string $phone
 * @property integer $exp_points
 * @property integer $elite_id
 * @property integer $rank_id
 * @property integer $rank_progress
 * @property array $ext_info
 * @property string $password write-only password
 * @property UserTokenAR $userToken
 * @property UserPackAR[] $userPacks
 */
class UserAR extends ExtInfoAR implements IdentityInterface, SenderModelInterface
{
    use StatusARTrait;

    public const STATUS_DELETED = 0;
    public const STATUS_BANNED = 2;
    public const STATUS_ACTIVE = 128;

    public const STATUS_CREATED = 2;
    public const STATUS_CONFIRM_EMAIL = 4;

    public const ROLE_GAMER = 2;
    public const ROLE_BRAND = 4;
    public const ROLE_TALENT = 8;
    public const ROLE_SUPER = 32 - 8; // 24

    public const ROLE_ADMIN = 32;
    public const ROLE_ADM_BRAND = 32 + 4; // 36
    public const ROLE_ADM_TALENT = 32 + 8; // 40
    public const ROLE_ADM_SUPER = 64 - 8; // 56
    public const ROLE_ROOT = 64;

    public const ANY_USER = 1;
    public const GUEST_USER = 2;

    protected $tokens;
    protected $shouldUpdateTokens = false;

    public $old_password;
    public $new_password;
    public $repeat_new_password;
    public $balances; // ТОЛЬКО для админки FIXME перенести в FM

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     * @return UserAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new UserAQ(static::class);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['status', 'user_role', 'exp_points', 'elite_id', 'rank_id', 'rank_progress'], 'integer'],
            [['username', 'email'], 'string'],
            [['email'], 'email'],
            [['created_at', 'updated_at'], 'safe'],
            [['balances'], 'safe'],
            [['ext_info'], JsonValidator::class, 'rules' => [
                'status' => function($value) {return $value == self::STATUS_ACTIVE ? null : $value;},
            ]],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя пользователя',
            'email' => 'Email',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'user_role' => 'Роль',
            'exp_points' => 'Опыт',
            'elite_id' => 'Уровень элиты',
            'rank_id' => 'Ранг',
            'rank_progress' => 'Прогресс ранга',
            'avatar_id' => 'Аватар',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    /**
     * @param string $scenario
     * @param array $data
     * @throws \yii\base\InvalidConfigException
     */
    public function sendModel($scenario = AbstractSender::SCENARIO_DEFAULT, $data = [])
    {
        UserSender::sendModel($this, $scenario, $data);
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        if (S3Core::isAllowedDelete($this) && parent::beforeDelete()) {
            $lineups = GameLineupAR::find()->where(['user_id' => $this->id])->all();
            foreach ($lineups as $lineup) {
                $lineup->delete();
            }

            UserPackAR::deleteAll(['user_id' => $this->id]);
            UserMoneyAccAR::deleteAll(['user_id' => $this->id]);

            return true;
        }

        return false;
    }

    /**
     * @return void
     */
    /* TODO пока костыль ниже   protected function jsonFields(): array
        {
            return [
                'status',
            ];
        }*/

    /* костыль */
    public function __set($name, $value)
    {
        if ($name === 'status') {
            $this->joSet($name,
                empty($value) || $value == UserAR::STATUS_ACTIVE ? null : $value,
                'clean');
            return;
        }

        parent::__set($name, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->shouldUpdateTokens) {
            $this->updateTokens();
        }
    }

    /**
     * @return bool|false|int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateTokens()
    {
        if ($this->tokens) {
            return UserTokenAR::setTokens($this->id, $this->tokens);
        }

        $userToken = UserTokenAR::findOne(['id' => $this->id]);

        if ($userToken) {
            return $userToken->delete();
        }

        return false;
    }

    /**
     * @param int $maxRole
     * @return array
     */
    public static function getRoles($maxRole = 0xFF)
    {
        static $roles;

        if (empty($roles)) {
            $roles = [
                self::ROLE_GAMER => 'ROLE_GAMER',
                self::ROLE_BRAND => 'ROLE_BRAND',
                self::ROLE_TALENT => 'ROLE_TALENT',
                self::ROLE_SUPER => 'ROLE_SUPER',
                self::ROLE_ADMIN => 'ROLE_ADMIN',
                self::ROLE_ADM_BRAND => 'ROLE_ADM_BRAND',
                self::ROLE_ADM_TALENT => 'ROLE_ADM_TALENT',
                self::ROLE_ADM_SUPER => 'ROLE_ADM_SUPER',
                self::ROLE_ROOT => 'ROLE_ROOT',
            ];
        }

        if ($maxRole === 0xFF) {
            return $roles;
        }

        $tR = [];
        foreach ($roles as $key => $value) {
            if ($key > $maxRole) {
                continue;
            }
            $tR[$key] = $value;
        }

        return $tR;
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        static $sts;

        if (isset($sts)) {
            return $sts;
        }

        return $sts = [
            self::STATUS_DELETED => 'STATUS_DELETED',
            self::STATUS_BANNED => 'STATUS_BANNED',
            self::STATUS_ACTIVE => 'STATUS_ACTIVE',
        ];
    }

    /**
     * @return mixed|string
     */
    public function statusLabel()
    {
        $arr = self::getStatuses();
        return $arr[$this->status] ?? ('? ' . print_r($this->status, TRUE));
    }

    /**
     * @return mixed|string
     */
    public function roleLabel()
    {
        $arr = self::getRoles();
        return $arr[$this->user_role] ?? '?';
    }

    /**
     * @return int
     */
    public function isAdmin(): int
    {
        return $this->user_role >= self::ROLE_ADMIN ? $this->user_role : 0;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status == UserAR::STATUS_ACTIVE;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);  // TODO FAS-72
//        return static::findOne(['id' => $id, 'status' => static::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]); // TODO FAS-72
//        return static::findOne(['username' => $username, 'status' => static::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return array|ActiveRecord|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::find()->rightJoin('{{%user_token}} `token`', '`token`.`id` = {{%user}}.id')
            ->andWhere([
                '`token`.`password_reset_token`' => $token,
                'status' => self::STATUS_ACTIVE,
            ])
            ->one();
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return ExtAQ|ActiveQuery
     */
    public function getUserToken()
    {
        return $this->hasOne(UserTokenAR::class, ['id' => 'id']);
    }

    /**
     * @return null|string
     */
    public function getPassword_reset_token(): ?string
    {
        return $this->getToken('password_reset_token');
    }

    /**
     * @param string|null $value
     * @return void
     */
    public function setPassword_reset_token(?string $value): void
    {
        $this->setToken('password_reset_token', $value);
    }

    /**
     * Verification-token getter
     * @return null|string
     */
    public function getVerification_token(): ?string
    {
        return $this->getToken('verification_token');
    }

    /**
     * @param string|null $value
     * @return void
     */
    public function setVerification_token(?string $value): void
    {
        $this->setToken('verification_token', $value);
    }

    /**
     * @param string $name
     * @param int $length
     * @return string
     * @throws Exception
     */
    public function generateToken(string $name, int $length = 32): string
    {
        $value = Yii::$app->security->generateRandomString($length);

        $this->setToken($name, $value);

        return $value;
    }

    /**
     * @param string $name
     */
    public function removeToken(string $name)
    {
        $this->setToken($name, null);
    }

    /**
     * @param string $name
     * @param string|null $value
     * @return void
     */
    public function setToken(string $name, ?string $value): void
    {
        $this->tokens[$name] = $value;
        $this->shouldUpdateTokens = true;
    }

    /**
     * @param string $name
     * @return string|null
     */
    public function getToken(string $name): ?string
    {
        $tokens = $this->getTokens();

        return $tokens[$name] ?? null;
    }

    /**
     * @return array
     */
    protected function getTokens(): array
    {
        if ($this->tokens === null) {
            $this->tokens = UserTokenAR::getTokens($this->id);
        }

        return $this->tokens;
    }

    /**
     * {@inheritDoc}
     */
    public function attributeHints()
    {
        return [
            'user_role' => 'Роль пользователя. Админ может изменить роль(статус) только ниже или равную своей'
        ];
    }

    /**
     * FIXME выдаем всех пользователей???
     *
     * @return array
     */
    public static function getUserList(): array
    {
        return self::find()
            ->select(['username'])
            ->indexBy('id')
            ->asArray()
            ->column();
    }

    /**
     * @param array $post_data
     * @return bool
     * @throws Throwable
     */
    public function dataUpdate($post_data): bool
    {
        unset($post_data['data']['password_hash']);

        if (isset($post_data['data']['old_password'], $post_data['data']['new_password'], $post_data['data']['repeat_new_password'])) {
            if (!Yii::$app->security->validatePassword($post_data['data']['old_password'], $this->password_hash)) {
                $this->addError('old_password', 'Старый пароль введён неверно!');

                return false;
            }

            if ($post_data['data']['new_password'] !== $post_data['data']['repeat_new_password']) {
                $this->addError('repeat_password', 'Неправильно введён повтор нового пароля!');

                return false;
            }

            $post_data['data']['password_hash'] = Yii::$app->security->generatePasswordHash($post_data['data']['new_password']);
        }

        unset($post_data['data']['old_password'], $post_data['data']['new_password'], $post_data['data']['repeat_new_password']);

        if (isset($post_data['data']['email'])) {
            /** @var ChangeEmailDoing $emailDoing */
            $emailDoing = ChangeEmailDoing::new();

            $result = $emailDoing->unsetRules(['checkToken'])
                ->doIt([
                    'data' => [
                        [
                            'id' => $this->id,
                            'email' => $post_data['data']['email'],
                        ],
                    ],
                ]);

            if (!$result) {
                $this->addError('email', 'Произошла ошибка при попытке начать процесс смены почты.');
                $this->addErrors($emailDoing->errors);

                return false;
            }

            unset($post_data['data']['email']);
        }

        if (isset($post_data['data']['phone'])) {
            /** @var ChangePhoneDoing $phoneDoing */
            $phoneDoing = ChangePhoneDoing::new();

            $result = $phoneDoing->unsetRules(['checkToken'])
                ->doIt([
                    'data' => [
                        [
                            'id' => $this->id,
                            'phone' => $post_data['data']['phone'],
                        ],
                    ],
                ]);

            if (!$result) {
                $this->addError('phone', 'Произошла ошибка при попытке начать процесс смены телефона.');
                $this->addErrors($phoneDoing->errors);

                return false;
            }

            unset($post_data['data']['phone']);
        }

        $this->setAttributes($post_data['data'], false);
        $clonedModel = clone $this;
        $save = $this->save();

        if ($save) {
            $this->sendModel(UserSender::SCENARIO_DEFAULT, ['clonedModel' => $clonedModel]);
        }

        return $save;
    }

    /**
     * @param null $field
     * @return mixed|bool|UserAR
     */
    public static function authorizedUser($field = null)
    {
        static $userAR = false;

        if ($userAR) {
            if ($field) {
                return $userAR->$field;
            }

            return $userAR;
        }

        if (Yii::$app->session->has('user_id')) {
            $user = self::findOne(Yii::$app->session->get('user_id'));

            if ($user) {
                $userAR = $user;
                if ($field && $userAR) {
                    return $userAR->$field;
                }

                return $userAR;
            }
        }

        return false;
    }

    /**
     * @return bool|false|int
     * @throws Throwable
     */
    public function delete()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $this->clearAuth();
            $result = parent::delete();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            $this->addError('id', $e->getMessage());
            $result = false;
        }

        return $result; // TODO: Change the autogenerated stub
    }

    /**
     * @return void
     */
    protected function clearAuth()
    {
        AuthAR::deleteAll(['user_id' => $this->id]);
    }

    /**
     * FIXME выдаем всех пользователей???
     * @return array
     */
    public static function getList()
    {
        return self::find()
            ->actual()
            ->select(['username'])
            ->whereGamer()
            ->indexBy('id')
            ->column();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPacks()
    {
        return $this->hasMany(UserPackAR::class, ['user_id' => 'id']);
    }

    /**
     * FIXME этот метод должен быть только в FM
     * @param bool $onlyActive
     * @return array
     */
    public function getPackLinks($onlyActive = false)
    {
        $query = $this->getUserPacks();

        if ($onlyActive) {
            $query->andWhere(['&', 'r_status', S3GoodKindAR::STATUS_ACTIVE]);
        }

        $userPacks = $query->all();

        $packs = [];

        foreach ($userPacks as $userPack) {
            $packTitle = $userPack->id . '-' . ($userPack->pack->r_name ?? null);
            $packs[] = Html::a($packTitle, ['/goods/pack/view', 'id' => $userPack->good_kind]);
        }

        return $packs;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLineups()
    {
        return $this->hasMany(GameLineupAR::class, ['user_id' => 'id']);
    }
}
