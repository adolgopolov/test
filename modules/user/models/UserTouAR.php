<?php

namespace common\modules\user\models;



/**
 * Расширение для "турнирного спортсмена"
 *
 * @property int $lup_id
 * @property int $lup_points
 * @property int $lup_place
 * @property int $lup_award

 */
class UserTouAR extends UserAR
{
    /**
     * {@inheritdoc}
     * @return UserAQ the active query used by this AR class.
     */
    public static function find()
    {
        return (new UserTouAQ(static::class))->select('ss_user.*')

        /*->select([
            'u.*',
            'lup_id' => 'tbl_lup.id',
            'lup_points' => 'tbl_lup.l_points',
            'lup_place' => 'tbl_lup.l_place',
            'lup_award' => 'tbl_lup.l_award',
        ])*/
            ;
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['lup_id', 'lup_points', 'lup_place','lup_award' ], 'safe'];
        return $rules;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        throw new \Exception('SmanTouAR read only');
    }
}
