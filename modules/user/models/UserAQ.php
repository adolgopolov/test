<?php

namespace common\modules\user\models;

use yii\db\Expression;
use yii\helpers\StringHelper;
use common\extensions\ExtAQ;
use common\modules\game\models\GameLineupAR;

/**
 * Class UserAQ
 *
 * @package common\modules\user\models
 */
class UserAQ extends ExtAQ
{
    /**
     * @return UserAQ
     */
	function actual() {
		return $this->andWhere(['>=', 'id', 99])->andWhere(['status' => UserAR::STATUS_ACTIVE]);
	}
    /**
     * @return UserAQ
     */
    function whereGamer() {
        return $this->andWhere(['user_role' => UserAR::ROLE_GAMER]);
    }

    /**
     * основная часть должна делаться в whereTourney
     * filterTourney для типичного вызова из ss3api
     *
     * @param $touList
     *
     * @return $this
     */
    function filterTourney($touList)
    {
        if (is_numeric($touList)) {
           $touList = (int) $touList;
        }
        elseif (is_string($touList)) {
            $touList = StringHelper::explode(',', $touList);
        }
        //todo переделать на костыль
        /*$result = $this->alias('u')
            ->innerJoin(GameLineupAR::tableName()." tbl_lup", "tbl_lup.user_id = u.id")
                ->andWhere(['tbl_lup.tou_id' => $touList, 'tbl_lup.isnt_reg' => 0])
                ->andWhere(['>=', 'u.id', 99])
            ->andWhere(['u.status' => UserAR::STATUS_ACTIVE])
            ->andWhere(['u.user_role' => UserAR::ROLE_GAMER]);

        return $result;*/

        $tableLup = GameLineupAR::tableName();
        $tableUser = $this->getTableNameAndAlias()[1]; // $this->modelClass::tableName();

        $idAlias = "$tableUser.id";
        $this->columnAliases(['id' => $idAlias, 'exp_points' => "$tableUser.exp_points"]);

        $this->innerJoin("$tableLup tbl_lup", "tbl_lup.user_id = $idAlias")
            ->andWhere(['tbl_lup.tou_id' => $touList, 'tbl_lup.isnt_reg' => 0])
            ->andWhere(['>=', "$tableUser.id", 99])
            ->andWhere(["$tableUser.status" => UserAR::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * @param $arg
     * @return $this
     */
    public function filterTop($arg)
    {
        $this->select(['r.*', new Expression('@rank:= @rank + 1 AS place')])->from(['{{%user}} r', '(SELECT @rank:=0) b']);
        $this->orderBy('exp_points DESC');
        $this->limit($arg);

        return $this;
    }
}
