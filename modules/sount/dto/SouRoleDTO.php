<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 31.05.19 9:36
 */

namespace common\modules\sount\dto;


class SouRoleDTO extends BaseDTO
{
    public $type;
    public $active;
    public $team_id;
    public $start_date;
    public $end_date;




    public function __construct($array)
    {
        $this->type = $array['type'];
        $this->active = $array['active'];
        $this->team_id = $array['team']['id'] ?? null;
        $this->start_date = $array['start_date'] ?? null;
        $this->end_date = $array['end_date'] ?? null;

    }

    public function getIterator() {
        return new \ArrayIterator($this->roles);
    }

    public function isActive()
    {
        return $this->active === 'true';
    }

}