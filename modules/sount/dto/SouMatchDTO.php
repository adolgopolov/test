<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 31.05.19 9:36
 */

namespace common\modules\sount\dto;


use yii\helpers\ArrayHelper;

class SouMatchDTO extends BaseDTO implements \IteratorAggregate
{
    /*   */
    public $id;
    /*   */
    public $start_time_tbd;
    /*   */
    public $scheduled;
    /*   */
    public $status;
    /*   */
    public $tournament_id;

    public $tournament_round;
    /*   */
    public $team_home_id;
    /*   */
    public $team_away_id;

    public $home_score;

    public $away_score;

    public $winner;

    public $match_status;

    private $games = [];

    public function __construct($array)
    {
        $this->id = $array['id'];
        $this->start_time_tbd = $array['start_time_tbd'];
        $this->scheduled = $this->convertDateTime($array['scheduled']);
        $this->status = $array['status'] ?? null;
        $this->tournament_id = $array['tournament']['id'] ?? null;
        $this->setTeams($array['competitors'] ?? null);
        $this->tournament_round = $array['tournament_round'] ?? null;

    }

    public function setMatchRezult($array)
    {
        if (!empty($array)) {
            $this->status = $array['status']??null;
            $this->home_score = $array['home_score']?? null;
            $this->away_score = $array['away_score']?? null;
            $this->winner = $array['winner_id']?? null;
            $this->match_status = $array['match_status']?? null;
        }
    }

    private function setTeams($teams)
    {
        if (is_array($teams)) {
            $competitors = ArrayHelper::index($teams, 'qualifier');
            $this->team_home_id = $competitors['home']['id'] ?? $teams[0]['id'];
            $this->team_away_id = ['away']['id'] ?? $teams[1]['id'];
        }
    }

    public function addGames($array)
    {
        if(is_array($array)){
            foreach ($array as $item) {
                $this->games[] = new SouGameDTO($item);
            }
        }
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->games);
    }
}