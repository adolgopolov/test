<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 31.05.19 9:36
 */

namespace common\modules\sount\dto;


class SouTeamDTO extends BaseDTO implements \IteratorAggregate
{
    /*  ключ */
    public $id;
    /*  название */
    public $name;
    /*  код страны */
    public $country_code;
    /*  абревиатура */
    public $abbreviation;
    /*  урл картинки */
    public $logo;

    /* @var  SouSmanDTO[] */
    public $smens = [];

    public function __construct($array)
    {
        $this->id = $array['id'];
        $this->name = $array['name'];
        $this->country_code = $array['country_code']??null;
        $this->abbreviation = $array['abbreviation'];

    }

    public function addSmens($array)
    {
        if(!is_array($array))
        {
            return;
        }
        foreach ($array as $item) {
            $sman = new SouSmanDTO($item);
            if(!empty($item['statistics'])){
                $sman->addStatistic($item['statistics']);
            }
            $this->smens[] =$sman;
        }
    }

    public function getIterator() {
        return new \ArrayIterator($this->smens);
    }

}