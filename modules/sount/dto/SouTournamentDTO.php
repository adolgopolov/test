<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 31.05.19 9:36
 */

namespace common\modules\sount\dto;


class SouTournamentDTO extends BaseDTO implements \IteratorAggregate
{
    /* @var string */
    public $id;
    /*  @var string */
    public $name;
    /*  @var string */
    public $current_season_id;
    /*  @var string */
    public $start_date;
    /*  @var string */
    public $end_date;

    /*  @var array */
    public $category;

    /*  @var SouMatchDTO[] */
    private $matches = [];

    private $clientClass;

    public function __construct($array) {
        $this->id = $array['id'];
        $this->name = $array['name'];
        $this->current_season_id = $array['current_season']['id']??null;
        $this->start_date = $array['current_season']['start_date']??null;
        $this->end_date = $array['current_season']['end_date']??null;
        $this->category = $array['category']??null;
    }

    public function addMatches($array)
    {
        foreach ($array as $item) {
            $this->matches[] = new SouMatchDTO($item);
        }
    }

    public function getMatches()
    {
        return $this->matches;
    }


    public function getIterator() {
        return new \ArrayIterator($this->matches);
    }


}