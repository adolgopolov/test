<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 31.05.19 9:36
 */

namespace common\modules\sount\dto;


use common\modules\sount\models\SouOccaAR;

class SouSmanPointDTO extends BaseDTO implements \IteratorAggregate
{

    /*  номер спортсмена */
    public $sman_id;
    /*  номер матча */
    public $match_id;
    /*  тип клиента */
    public $sou_id;
    /* номер амплуа */
    public $position_name;
    /* события спортсмена в матче */
    protected $occa = [];


    public function __construct(SouOccaAR $occaAR)
    {
        $this->sman_id = $occaAR->sman_sou_id;
        $this->match_id = $occaAR->match_sou_id;
        $this->sou_id = $occaAR->sou_id;
        $this->position_name = $occaAR->sman->joPosition;
    }

    public function addOcca(SouOccaAR $occaAR)
    {
        $previos = $this->occa[$occaAR->fact_sou_id] ?? 0;
        if(is_numeric($occaAR->original_value)) {
            $this->occa[$occaAR->fact_sou_id] = $occaAR->original_value + $previos;
        }
    }

    public function getIterator() {
        return new \ArrayIterator($this->occa);
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->sou_id."-".$this->match_id."-".$this->sman_id;
    }

}