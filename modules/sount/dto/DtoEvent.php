<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 12.06.19 11:03
 */

namespace common\modules\sount\dto;


use yii\base\Event;

class DtoEvent extends Event
{
    public $dto;
    public $type;
}