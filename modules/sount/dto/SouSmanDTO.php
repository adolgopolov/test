<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 31.05.19 9:36
 */

namespace common\modules\sount\dto;


class SouSmanDTO extends BaseDTO implements \IteratorAggregate
{
    public $id;
    public $name;
    public $date_of_birth;
    public $country_code;
    public $nickname;
    public $gender;
    public $type;
    public $logo;
    /* @var  SouRoleDTO[] */
    protected $roles = [];

    protected $statistic = [];

    public function __construct($array)
    {
        $this->id = $array['id'];
        $this->name = $array['name'];
        $this->date_of_birth = $array['date_of_birth'] ?? null;
        $this->country_code = $array['country_code'] ?? null;
        $this->nickname = $array['nickname'] ?? null;
        $this->gender = $array['gender'] ?? null;
        $this->type = $array['type'] ?? null;
    }

    public function addRoles($array)
    {

        foreach ($array as $item) {
            $this->roles[] = new SouRoleDTO($item);
        }
    }

    public function addStatistic($array)
    {
        foreach ($array as $key => $item) {
            $this->statistic[$key] = $item;
        }
    }

    public function getStatistic()
    {
        return $this->statistic;
    }

    public function getIterator() {
        return new \ArrayIterator($this->roles);
    }

    public function getActiveRole()
    {
        foreach ($this->roles as $item) {
            if($item->active === 'true') return $item;
        }
        return null;
    }

}