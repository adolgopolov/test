<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 31.05.19 9:36
 */

namespace common\modules\sount\dto;


use common\modules\game\models\SportMatchAR;
use common\modules\sport\models\SportOccaAR;

/**
 * Class MatchOccaDTO
 * @package common\modules\sport\dto
 */
class MatchOccaDTO  implements \IteratorAggregate
{
    /*  ключ */
    public $id;
    /*  название */
    public $name;
    /*  вид спорта */
    public $sport_id;
    /*  количество раундов матча */
    protected $countGame = 0;

    /*  агрегировать одинаковые события матча */
    public $is_aggregate = true;

    /* @var  SportOccaAR[] */
    public $occa = [];

    public function __construct(SportMatchAR $array)
    {
        $this->id = $array->id;
        $this->name = $array->r_name;
        $this->sport_id = $array->sport_id;
    }

    public function addOcca($array)
    {
        //todo если массив ни имеет ключей ['match_id'] ['sman_id'] ['fact_id'] ['occa_amount']
        // вернуть ошибку
        if($this->isBadOccaKeys($array))
        {
            return 'ошибка ключей массива для записи';
        }
        $key = $this->genKey($array);

        if($this->countGame < $array['game']) {
            $this->countGame = $array['game'];
        }
        unset($array['game']);
        $this->occa[$key] = $array;

    }

    public function getIterator() {
        return new \ArrayIterator($this->occa);
    }

    /**
     * генерирует ключ для колекции спортивных событий матча
     * @param $array
     * @return int|string
     */
    public function genKey($array)
    {
        if($this->is_aggregate) {
            return $array['match_id']."-".$array['sman_id']."-".$array['fact_id'];
        }else {
            //инкрементальный ключ
            return count($this->occa);
        }


    }

    /**
     * @param $array
     * @return bool
     */
    protected function isBadOccaKeys($array): bool
    {
        return !is_array($array)
            && !array_key_exists('match_id', $array)
            && !array_key_exists('sman_id', $array)
            && !array_key_exists('fact_id', $array)
            && !array_key_exists('occa_amount', $array)
            && !array_key_exists('after_start', $array)
            && !array_key_exists('game', $array);
    }

    public function getCountGame()
    {
        return $this->countGame;
    }

}