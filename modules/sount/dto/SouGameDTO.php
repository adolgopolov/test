<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 31.05.19 9:36
 */

namespace common\modules\sount\dto;


class SouGameDTO extends BaseDTO
{
    public $number;

    private $teams = [];


    public function __construct($array)
    {
        $this->number = $array['number'];
        foreach ($array['teams'] as $team) {
            if(array_key_exists('players',$team) && !empty($team['players'])) {
                $souTeam = new SouTeamDTO($team);
                $souTeam->addSmens($team['players']);
                $this->teams[] = $souTeam;
            }

        }
    }

    /**
     * @return SouTeamDTO[]
     */
    public function getTeams(): array
    {
        return $this->teams;
    }

}