<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 31.05.19 14:28
 */

namespace common\modules\sount\dto;


class BaseDTO
{
    protected function convertDateTime($dateTime)
    {
        if ($dateTime === true ) {
            var_dump($dateTime);
            die;
        }

        $timeZone = new \DateTimeZone('Europe/Moscow');
        $date = new \DateTime($dateTime, $timeZone);
        return $date->format('Y-m-d H:i:s');
    }
}