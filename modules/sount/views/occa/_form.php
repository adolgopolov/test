<?php

use common\widgets\DetailForm\DetailFormWidget;
use common\widgets\JsonEditor;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\modules\sount\models\SouOccaAR $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<div class="sou-occa-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'sou_id',
            'match_sou_id',
            'sman_sou_id',
            'game',
            'fact_sou_id',
            'fact_value',
            'original_value',
            'after_start',
            'ext_info' => JsonEditor::widget([
                'model' => $model,
                'attribute' => 'ext_info',
            ]),
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
