<?php

use backend\components\aboard\ActionColumn;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var $searchModel common\modules\sount\models\SouOccaAS
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Sou Occa Ars';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    'sou_id',
    'match_sou_id',
    'sman_sou_id',
    'game',
    //'fact_sou_id',
    //'fact_value',
    //'original_value',
    //'after_start',
    //'ext_info',
    ['class' => ActionColumn::class],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sou-occa-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#sount-occa-pjax',
        'pjaxUrl' => '/sount/occa/index',
        'model' => $searchModel,
    ]) ?>
    <?= Html::a('<span class="ti-plus"></span> Добавить', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    <?php Pjax::begin(['id' => 'sount-occa-pjax']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
    <?php Pjax::end() ?>
</div>
