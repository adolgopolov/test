<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\sount\models\SouOccaAS */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sou-occa-ar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sou_id') ?>

    <?= $form->field($model, 'match_sou_id') ?>

    <?= $form->field($model, 'sman_sou_id') ?>

    <?= $form->field($model, 'game') ?>

    <?php // echo $form->field($model, 'fact_sou_id') ?>

    <?php // echo $form->field($model, 'fact_value') ?>

    <?php // echo $form->field($model, 'original_value') ?>

    <?php // echo $form->field($model, 'after_start') ?>

    <?php // echo $form->field($model, 'ext_info') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
