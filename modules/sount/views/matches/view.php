<?php

use common\modules\sount\models\SouMatchAR;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\sount\models\SouMatchAR */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sou Match Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];

YiiAsset::register($this);
?>
<div class="sou-match-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-pencil"></span> Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<span class="fa fa-trash"></span> Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно желаете удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<span class="fa fa-pencil"></span> скоринг расширенной статистики', ['calc-ext', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<span class="fa fa-pencil"></span> импорт', ['import-match', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>

    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sou_key',
            'sou_id',
            [
                'attribute' => 'r_status',
                'value' => static function (SouMatchAR $model) {
                    return $model->statusName;
                },
            ],
            'tou_sou_id',
            'match_our_id',
            'th_sou_id',
            'ta_sou_id',
            'date_start',
        ],
    ]) ?>
</div>
