<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\sount\models\SouMatchAS */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sou-match-ar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sou_key') ?>

    <?= $form->field($model, 'sou_id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'tou_sou_id') ?>

    <?php // echo $form->field($model, 'match_our_id') ?>

    <?php // echo $form->field($model, 'th_sou_id') ?>

    <?php // echo $form->field($model, 'ta_sou_id') ?>

    <?php // echo $form->field($model, 'date_start') ?>

    <?php // echo $form->field($model, 'is_unity') ?>

    <?php // echo $form->field($model, 'ext_info') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
