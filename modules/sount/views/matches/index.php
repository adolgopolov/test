<?php

use backend\components\aboard\ActionColumn;
use common\modules\sount\models\SouMatchAS;
use common\modules\sount\SportSouModule;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var $searchModel common\modules\sount\models\SouMatchAS
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Sou Match Ars';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'contentOptions' => static function (SouMatchAS $model) {
            return ['class' => $model->isGoodForImport() ? 'good bg-success' : 'bad bg-warning'];
        },
    ],
    'sou_key',
    [
        'attribute' => 'sou_id',
        'filter' => SportSouModule::getTypeClient(),

        'content' => static function (SouMatchAS $model) {
            return $model->clientName;
        },
    ],
    [
        'attribute' => 'r_status',
        'filter' => SportSouModule::getSouStatus(),
        'content' => static function (SouMatchAS $model) {
            return $model->statusName;
        },
    ],
    [
        'attribute' => 'tou_sou_id',
        'content' => static function (SouMatchAS $model) {
            return $model->tournament->name;
        },
    ],
    'match_our_id',
    [
        'attribute' => 'th_sou_id',
        'filter' => false,
        'content' => static function (SouMatchAS $model) {
            return $model->teamOwner->name;
        },
    ],
    [
        'attribute' => 'ta_sou_id',
        'filter' => false,
        'content' => static function (SouMatchAS $model) {
            return $model->teamGuest->name;
        },
    ],
    'date_start',
    ['class' => ActionColumn::class,
        'template' => '<div class="btn-group">{view}{update}{delete}{import-match}{load-occa}</div>',
        'buttons' => [
            'load-occa' => static function ($url) {
                $options = [
                    'title' => 'load-occa',
                    'aria-label' => 'load-occa',
                    'data-pjax' => '0',
                    'class' => 'btn btn-outline-primary btn-info',
                    'data-confirm' => false,
                    'data-toggle' => 'tooltip',
                    'data-title' => 'load-occa'
                ];

                $icon = Html::tag('em', '', ['class' => 'fas fa-gift']);

                return Html::a($icon, $url, $options);
            },
            'import-match' => static function ($url) {
                $options = [
                    'title' => 'import-match',
                    'aria-label' => 'import-match',
                    'data-pjax' => '0',
                    'class' => 'btn btn-outline-primary btn-danger',
                    'data-confirm' => false,
                    'data-toggle' => 'tooltip',
                    'data-title' => 'occa-import'
                ];
                $icon = Html::tag('em', '', ['class' => "fas fa-fire"]);

                return Html::a($icon, $url, $options);
            },
        ],
    ],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sou-match-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#sount-matches-pjax',
        'pjaxUrl' => '/sount/matches/index',
        'model' => $searchModel,
    ]) ?>
    <?= Html::a('<span class="ti-plus"></span> посчитать расширенную статистику', ['ext-scoring'], ['class' => 'btn btn-success pull-right mL-5']) ?>
    <?= Html::a('<span class="ti-plus"></span> загрузить события матчей', ['grab-summary'], ['class' => 'btn btn-danger right pull-right']) ?>
    <?php Pjax::begin(['id' => 'sount-matches-pjax']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => $columns,
    ]); ?>
    <?php Pjax::end(); ?>
</div>
