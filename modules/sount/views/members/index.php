<?php

use backend\components\aboard\ActionColumn;
use common\modules\sount\models\SouTeamMemberAS;
use common\modules\sount\SportSouModule;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var $searchModel common\modules\sount\models\SouTeamMemberAS
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Sou Team Member Ars';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    [
        'attribute' => 'team_sou_id',
        'content' => static function (SouTeamMemberAS $model) {
            return $model->team->name;
        },
    ],
    [
        'attribute' => 'sman_sou_id',

        'content' => static function (SouTeamMemberAS $model) {
            return $model->sman->nickname ?? $model->sman->name;
        },
    ],
    [
        'attribute' => 'sou_id',
        'filter' => SportSouModule::getTypeClient(),
        'content' => static function (SouTeamMemberAS $model) {
            return $model->clientName;
        },
    ],
    [
        'attribute' => 'r_status',
        'filter' => SportSouModule::getSouStatus(),
        'content' => static function (SouTeamMemberAS $model) {
            return $model->statusName;
        },
    ],
    'tm_our_id',
    'first_day:date',
    'last_day:date',
    [
        'attribute' => 'pos_id',
        'filter' => false,//SportSouModule::getSouStatus(),
        'content' => static function (SouTeamMemberAS $model) {
            return $model->positionName();
        },
    ],
    ['class' => ActionColumn::class],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sou-team-member-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#sount-members-pjax',
        'pjaxUrl' => '/sount/members/index',
        'model' => $searchModel,
    ]) ?>
    <?= Html::a('<span class="ti-plus"></span> Добавить', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    <?php Pjax::begin(['id' => 'sount-members-pjax']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
    <?php Pjax::end() ?>
</div>
