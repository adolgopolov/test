<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\sount\models\SouTeamMemberAS */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sou-team-member-ar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'team_sou_id') ?>

    <?= $form->field($model, 'sman_sou_id') ?>

    <?= $form->field($model, 'sou_id') ?>

    <?= $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'tm_our_id') ?>

    <?php // echo $form->field($model, 'first_day') ?>

    <?php // echo $form->field($model, 'last_day') ?>

    <?php // echo $form->field($model, 'pos_id') ?>

    <?php // echo $form->field($model, 'ext_info') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
