<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\sount\models\SouTeamMemberAR $model
 */

$this->title = 'Update Sou Team Member Ar: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sou Team Member Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
?>
<div class="sou-team-member-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
