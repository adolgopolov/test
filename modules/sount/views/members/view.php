<?php

use common\modules\sount\models\SouTeamMemberAR;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model common\modules\sount\models\SouTeamMemberAR
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sou Team Member Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['crud-breadcrumbs'][] = 'Просмотр';
$this->params['crud-breadcrumbs'][] = ['label' => 'Редактирование', 'url' => ['update', 'id' => $model->id]];

YiiAsset::register($this);
?>
<div class="sou-team-member-ar-view">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <p class="text-right">
        <?= Html::a('<span class="fa fa-pencil"></span> Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<span class="fa fa-trash"></span> Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно желаете уделить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'team_sou_id',
            'sman_sou_id',
            'sou_id',
            [
                'attribute' => 'r_status',
                'value' => static function (SouTeamMemberAR $model) {
                    return $model->statusName;
                },
            ],
            'tm_our_id',
            'first_day',
            'last_day',
            'pos_id',
        ],
    ]) ?>
</div>
