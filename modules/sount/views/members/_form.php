<?php

use common\widgets\DetailForm\DetailFormWidget;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\modules\sount\models\SouTeamMemberAR $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<div class="sou-team-member-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'team_sou_id',
            'sman_sou_id',
            'sou_id',
            'first_day' => DateTimePicker::widget([
                'model' => $model,
                'attribute' => 'first_day',
            ]),
            'last_day' => DateTimePicker::widget([
                'model' => $model,
                'attribute' => 'last_day',
            ]),
            'pos_id',
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
