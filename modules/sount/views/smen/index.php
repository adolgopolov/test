<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\components\aboard\ActionColumn;
use common\modules\sount\models\SouSmanAS;
use common\modules\sount\SportSouModule;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sount\widgets\buffer\MatchedFilterWidget;

/**
 * @var yii\web\View $this
 * @var common\modules\sount\models\SouSmanAS $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var array $queryParams
 */

$this->title = 'Sou Sman Ars';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    'name',
    'sou_key',
    [
        'attribute' => 'sou_id',
        'filter' => SportSouModule::getTypeClient(),
        'content' => static function (SouSmanAS $model){
            return $model->clientName;
        },
    ],
    [
        'attribute' => 'r_status',
        'filter' => SportSouModule::getSouStatus(),
        'content' => static function (SouSmanAS $model){
            return $model->statusName;
        },
    ],
    [
        'attribute' => 'logo',
        'filter' => false,
        'content' => static function (SouSmanAS $model){
            return Html::img($model->joLogo,['width'=>100,'height'=> 100]);
        },
    ],
    [
        'attribute' => 'logo_sman',
        'format' => 'raw',
        'value' => static function (SouSmanAS $model) {
            $sman = $model->innerSman;

            if (!$sman) {
                return null;
            }

            return $sman->htmlImage();
        }
    ],
    'sman_our_id',
    'sman_our_name',
    'nickname',
    [
        'class' => ActionColumn::class,
        'template' => '{create}{update}{delete}{clear}{create-our-team}',
        'buttons' => [
            'clear' => static function ($url, SouSmanAS $model, $key) use ($queryParams) {
                $options = [
                    'title' => 'clear',
                    'aria-label' => 'clear',
                    'data-pjax' => 1,
                    'class' => 'btn btn-outline-primary btn-danger',
                    'data-confirm' => false,
                    'data-toggle' => 'tooltip',
                    'data-title' => 'clear'
                ];
                $icon = Html::tag('em', '', ['class' => 'fas fa-fire']);
                $url = array_merge(['smen/clear', 'id' => $model->id], $queryParams);

                return Html::a($icon, $url, $options);
            },
            'create-our-team' => static function ($url, SouSmanAS $model, $key) use ($queryParams) {
                $options = [
                    'title' => 'create-our-team',
                    'aria-label' => 'create-our-team',
                    'data-pjax' => 0,
                    'class' => 'btn btn-outline-primary btn-info',
                    'data-confirm' => false,
                    'data-toggle' => 'tooltip',
                    'data-title' => 'create-our-team'
                ];
                $icon = Html::tag('em', '', ['class' => 'fas fa-gift']);
                $url = array_merge(['/sport/sman/create'], $model->getAttributes([
                    'id', 'joLogo', 'name', 'nickname']));

                return Html::a($icon, $url, $options);
            },
        ],
        'visibleButtons' => [
            'clear' => static function (SouSmanAS $model, $key, $index) {
                return (bool)$model->sman_our_id;
            },
            'create-our-team' => static function (SouSmanAS $model, $key, $index) {
                return !$model->sman_our_id;
            },
        ],
    ],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sou-sman-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#sount-smen-pjax',
        'pjaxUrl' => '/sount/smen/index',
        'model' => $searchModel,
    ]) ?>

    <?php Pjax::begin(['id' => 'sount-smen-pjax']) ?>
    <?= Html::a('<span class="ti-plus"></span> Добавить', ['create'], ['class' => 'btn btn-success pull-right']) ?>

    <?= MatchedFilterWidget::widget() ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
    <?php Pjax::end(); ?>
</div>
