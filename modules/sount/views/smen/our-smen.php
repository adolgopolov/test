<?php

use backend\components\aboard\ActionColumn;
use backend\components\aboard\widgets\GridView;
use common\modules\sount\models\SmanOurAS;
use common\modules\sount\models\SouSmanAR;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel \common\modules\sport\forms\SmanAS
 */

$this->title = 'Спортсмены';
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$(document).on("click",".show-team",function() {
    $(this).closest("td").find(".team-data").toggle();

    return false;
});
JS;

$this->registerJs($js);
?>
<div class="sman-ar-index">

    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>

    <a href="javascript:void(0)" data-url="<?= Url::to(['paste-all']) ?>"
       class="paste-data btn btn-warning right pull-right mT-10">
        <span class="ti-plus"></span>
        привязать все
    </a>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'id' => 'list-paste-data',
        'rowClickAction' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterUrl' => ['/sount/smen/our-smen'],
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            'r_name',
            [
                'attribute' => 'team',
                'value' => static function (SmanOurAS $model) {
                    $team = $model->lastTeamMember->team ?? null;

//                    if ($team) {
//                        $smen = [];
//                        $souTeam = SouTeamAR::findOne(['team_our_id' => $team->id]);
//                        if ($souTeam) {
//                            foreach ($souTeam->smen as $sman) {
//                                $smen[] = $sman->id . " - " . $sman->name;
//                            }
//                        }
//                        if (empty($smen)) $smen[] = 'Команда не сопоставлена';
//                        $ul = Html::ul($smen);
//                        return Html::tag('span', $team->getTitle(), ['class' => 'show-team']) . Html::tag('div', $ul, ['style' => 'display:none', 'class' => 'team-data']);
//                    }

                    if (!$team) {
                        return null;
                    }

                    return $team->getTitle();
                },
                'format' => 'raw',

            ],
            [
                'attribute' => 'r_icon',
                'value' => static function (SmanOurAS $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'sou_sman',
                'value' => static function (SmanOurAS $model) {
                    $list = [];
                    $smanIds = explode(',', $model->sou_sman);
                    $souSmen = SouSmanAR::find()
                        ->select(['name'])
                        ->where(['id' => $smanIds])
                        ->indexBy('id')
                        ->column();

                    $smen = SouSmanAR::compareSman($model);
                    foreach ($smen as $sman) {
                        $smanName = $sman->name . ' - ' . $sman->contry_code . ' - ' . $sman->id;
                        $smanName .= Html::checkbox('paste', false, ['class' => 'check-paste-flag', 'data-id' => $sman->id]);
                        Html::tag('p', $smanName);
                        $list[] = $smanName;
                    }

                    $html = Select2::widget([
                        'attribute' => 'sou_sman[' . $model->id . ']',
                        'model' => $model,
                        'data' => $souSmen,
                        'options' => [
                            'data' => [
                                'id' => $model->id,
                            ],
                        ],
                        'pluginOptions' => [
                            'placeholder' => 'Выберите спортсмена из списка',
                            'allowClear' => true,
                            'tags' => true,
                            'minimumInputLength' => 3,
                        ],
                    ]);

                    $html .= Html::ul($list, ['encode' => false]);

                    return $html;
                },
                'format' => 'raw',
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{paste}',
                'buttons' => [
                    'paste' => static function ($url, SmanOurAS $model, $key) {
                        $options = [
                            'title' => 'paste',
                            'aria-label' => 'paste',
                            'data-pjax' => 1,
                            'data-method' => 'post',
                            'class' => 'btn btn-outline-primary btn-danger paste-id',
                            'data-confirm' => false,
                            'data-toggle' => 'tooltip',
                            'data-title' => 'paste',
                            'data-params' => [
                                'sou_id' => '',
                            ],
                        ];

                        $icon = Html::tag('em', '', ['class' => 'fas fa-fire']);
                        $urlPaste = ['smen/paste', 'id' => $model->id];

                        return Html::a($icon, $urlPaste, $options);
                    },
                ],
                'visibleButtons' => [
                    'paste' => static function ($model) {
                        return !SouSmanAR::find()->where(['sman_our_id' => $model->id])->exists();
                    },
                ],
            ],
        ],
    ]) ?>
    <?php Pjax::end(); ?>
    <div class="bd bgc-white p-20">
        <div class="layers">
            <a href="javascript:void(0)" data-url="<?= Url::to(['paste-all']) ?>"
               class="paste-data btn btn-warning right pull-right mT-10">
                <span class="ti-plus"></span>
                привязать все
            </a>
        </div>

    </div>
</div>
