<?php

use yii\helpers\Html;

/** @var yii\web\View $this*/
/** @var common\modules\sount\models\SouSmanAR $model */

$this->title = 'Добавление Sou Sman Ar';
$this->params['breadcrumbs'][] = ['label' => 'Sou Sman Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sou-sman-ar-create">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
