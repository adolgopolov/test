<?php

use common\modules\sount\models\SouTournamentAR;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var SouTournamentAR $model
 */

$this->title = 'Добавление Sou Tournament Ar';
$this->params['breadcrumbs'][] = ['label' => 'Sou Tournament Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sou-tournament-ar-create">

    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
