<?php

use backend\components\aboard\ActionColumn;
use common\modules\sount\models\SouTournamentAS;
use common\modules\sount\SportSouModule;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var $searchModel common\modules\sount\models\SouTournamentAS
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Sou Tournament Ars';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    'sou_key',
    [
        'attribute' => 'sou_id',
        'filter' => SportSouModule::getTypeClient(),
        'content' => static function (SouTournamentAS $model) {
            return $model->clientName;
        },
    ],
    [
        'attribute' => 'r_status',
        'filter' => SportSouModule::getSouStatus(),
        'content' => static function (SouTournamentAS $model) {
            return $model->statusName;
        },
    ],
    'date_start:date',
    'date_end:date',
    'name',
    [
        'attribute' => 'country',
        'filter' => false,
        'content' => static function (SouTournamentAS $model) {
            return $model->joGet('category.name');
        },
    ],
    ['class' => ActionColumn::class],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sou-tournament-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#sount-tournaments-pjax',
        'pjaxUrl' => '/sount/tournaments/index',
        'model' => $searchModel,
    ]) ?>
    <?= Html::a('<span class="ti-plus"></span> загрузить турниры', ['upload-tournaments'], ['class' => 'btn btn-warning right pull-right']) ?>
    <?= Html::a('<span class="ti-plus"></span> загрузить матчи', ['grab-matches'], ['class' => 'btn btn-danger right pull-right']) ?>
    <?php Pjax::begin(['id' => 'sount-tournaments-pjax']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
    <?php Pjax::end(); ?>
</div>
