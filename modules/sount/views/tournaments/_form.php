<?php

use common\modules\sount\SportSouModule;
use common\widgets\DetailForm\DetailFormWidget;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\modules\sount\models\SouTournamentAR $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<div class="sou-tournament-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'status' => Select2::widget([
                'model' => $model,
                'attribute' => 'status',
                'data' => SportSouModule::getSouStatus(),
            ]),
            'statusList' => Html::activeCheckboxList($model, 'statusList', SportSouModule::getSouStatus()),
            'date_start' => DateTimePicker::widget([
                'model' => $model,
                'attribute' => 'date_start',
            ]),
            'date_end' => DateTimePicker::widget([
                'model' => $model,
                'attribute' => 'date_end',
            ]),
            'name',
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
