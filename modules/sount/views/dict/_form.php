<?php

use common\widgets\DetailForm\DetailFormWidget;
use common\widgets\JsonEditor;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\modules\sount\models\SouDictAR $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<div class="sou-dict-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
                'inputOptions' => [
                    'disabled' => true,
                ],
            ],
            'sou_key',
            'sou_id',
            'ent_id',
            'ent_kind',
            'ext_info' => JsonEditor::widget([
                'model' => $model,
                'attribute' => 'ext_info',
            ]),
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
