<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\sount\models\SouDictAR $model
 */

$this->title = 'Update Sou Dict Ar: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sou Dict Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$this->params['crud-breadcrumbs'][] = ['label' => 'Просмотр', 'url' => ['view', 'id' => $model->id]];
$this->params['crud-breadcrumbs'][] = 'Редактирование';
?>
<div class="sou-dict-ar-update">
    <h4 class="c-grey-900 mT-10 mB-30"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
