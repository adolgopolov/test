<?php

use backend\components\aboard\ActionColumn;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var $searchModel common\modules\sount\models\SouDictAS
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Sou Dict Ars';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    'sou_key',
    'sou_id',
    'ent_id',
    [
        'attribute' => 'ent_kind',
        'options' => [
            'class' => 'kind-grid-column',
        ],
    ],
    //'ext_info',
    ['class' => ActionColumn::class],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sou-dict-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>
    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#sount-dict-pjax',
        'pjaxUrl' => '/sount/dict/index',
        'model' => $searchModel,
    ]) ?>
    <?= Html::a('<span class="ti-plus"></span> Добавить', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    <?php Pjax::begin(['id' => 'sount-dict-pjax']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
    <?php Pjax::end(); ?>
</div>
