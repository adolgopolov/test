<?php

use backend\components\aboard\ActionColumn;
use common\modules\sount\models\SouTeamAR;
use common\modules\sount\models\SportTeamOurAS;
use common\modules\sport\models\SportAR;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var SportTeamOurAS $searchModel
 * @var array $sports
 */

$this->title = 'Команды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sport-team-ar-index">

    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>

    <a href="javascript:void(0)" data-url="<?= Url::to(['paste-all']) ?>"
       class="paste-data btn btn-warning right pull-right mT-10">
        <span class="ti-plus"></span>
        привязать все
    </a>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'id' => 'list-paste-data',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterUrl' => ['/sount/teams/our-team'],
        'columns' => [
            [
                'attribute' => 'id',
                'options' => [
                    'class' => 'id-grid-column',
                ],
            ],
            [
                'attribute' => 'sport_id',

                'filter' => SportAR::getAsOptsList(),
                'value' => static function (SportTeamOurAS $model) {
                    return $model->sport->r_name;
                },
            ],
            [
                'attribute' => 'r_name',
                'value' => static function (SportTeamOurAS $model) {
                    return $model->getTitle();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'league',
                'value' => static function (SportTeamOurAS $model) {
                    $list = ArrayHelper::getColumn($model->leagues, 'r_name');
                    return Html::ul($list);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'r_icon',
                'value' => static function (SportTeamOurAS $model) {
                    return $model->htmlImage();
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'sou_team',
                'value' => static function (SportTeamOurAS $model) {
                    $teamIds = explode(',', $model->sou_team);
                    $souTeams = SouTeamAR::find()
                        ->select(['name'])
                        ->where(['id' => $teamIds])
                        ->indexBy('id')
                        ->column();

                    $list = [];
                    $teams = SouTeamAR::compareTeam($model);

                    foreach ($teams as $team) {
                        $teamName = $team->name . ' - ' . $team->contry_code . ' - ' . $team->id;
                        $teamName .= Html::checkbox('paste', false, ['class' => 'check-paste-flag uncheckable', 'data-id' => $team->id]);
                        Html::tag('p', $teamName);
                        $list[] = $teamName;
                    }

                    $html = Select2::widget([
                        'attribute' => 'sou_team[' . $model->id . ']',
                        'model' => $model,
                        'data' => $souTeams,
                        'options' => [
                            'data' => [
                                'id' => $model->id,
                            ],
                        ],
                        'pluginOptions' => [
                            'placeholder' => 'Выберите команду из списка',
                            'allowClear' => true,
                            'tags' => true,
                            'minimumInputLength' => 3,
                        ],
                    ]);

                    $html .= Html::ul($list, ['encode' => false]);

                    return $html;
                },
                'format' => 'raw',
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{paste}',
                'buttons' => [
                    'paste' => static function ($url, SportTeamOurAS $model, $key) {
                        $options = [
                            'title' => 'paste',
                            'aria-label' => 'paste',
                            'data-pjax' => 1,
                            'data-method' => 'post',
                            'class' => 'btn btn-outline-primary btn-danger paste-id',
                            'data-confirm' => false,
                            'data-toggle' => 'tooltip',
                            'data-title' => 'paste',
                            'data-params' => [
                                'sou_id' => '',
                            ],
                        ];
                        $icon = Html::tag('em', '', ['class' => 'fas fa-fire']);
                        $urlPaste = ['teams/paste', 'id' => $model->id];

                        return Html::a($icon, $urlPaste, $options);
                    },
                ],
                'visibleButtons' => [
                    'paste' => static function ($model, $key, $index) {
                        return !SouTeamAR::find()->where(['team_our_id' => $model->id])->exists();
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    <div class="bd bgc-white p-20">
        <div class="layers">
            <a href="javascript:void(0)" data-url="<?= Url::to(['paste-all']) ?>"
               class="paste-data btn btn-warning right pull-right mT-10">
                <span class="ti-plus"></span>
                привязать все
            </a>
        </div>
    </div>
</div>
