<?php

use backend\components\aboard\ActionColumn;
use common\modules\sount\models\SouTeamAS;
use common\modules\sount\SportSouModule;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sount\widgets\buffer\MatchedFilterWidget;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var common\modules\sount\models\SouTeamAS $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var array $queryParams
 */

$this->title = 'Sou Team Ars';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'class' => 'id-grid-column',
        ],
    ],
    'name',
    'sou_key',
    [
        'attribute' => 'sou_id',
        'filter' => SportSouModule::getTypeClient(),
        'content' => static function (SouTeamAS $model) {
            return $model->clientName;
        },
    ],
    [
        'attribute' => 'r_status',
        'filter' => SportSouModule::getSouStatus(),
        'content' => static function (SouTeamAS $model) {
            return $model->statusName;
        },
    ],
    [
        'attribute' => 'logo',
        'filter' => false,
        'content' => static function (SouTeamAS $model) {
            return Html::img($model->joLogo, ['width' => 100, 'height' => 100]);
        },
    ],
    [
        'attribute' => 'logo_team',
        'content' => static function (SouTeamAS $model) {
            $team = $model->innerTeam;

            if (!$team) {
                return null;
            }

            return $team->htmlImage();
        },
    ],
    'team_our_id',
    'team_our_name',
    'abbr',
    'contry_code',
    [
        'class' => ActionColumn::class,
        'template' => '{view}{update}{delete}{clear}{create-our-team}',
        'buttons' => [
            'clear' => static function ($url, SouTeamAS $model) use ($queryParams) {
                $options = [
                    'title' => 'clear',
                    'aria-label' => 'clear',
                    'data-pjax' => 1,
                    'class' => 'btn btn-outline-primary btn-danger',
                    'data-confirm' => false,
                    'data-toggle' => 'tooltip',
                    'data-title' => 'clear'
                ];
                $icon = Html::tag('em', '', ['class' => 'fas fa-fire']);
                $urlNew = array_merge(['teams/clear', 'id' => $model->id], $queryParams);

                return Html::a($icon, $urlNew, $options);
            },
            'create-our-team' => static function ($url, SouTeamAS $model) {
                $options = [
                    'title' => 'create-our-team',
                    'aria-label' => 'create-our-team',
                    'data-pjax' => 0,
                    'class' => 'btn btn-outline-primary btn-info',
                    'data-confirm' => false,
                    'data-toggle' => 'tooltip',
                    'data-title' => 'create-our-team'
                ];
                $icon = Html::tag('em', '', ['class' => 'fas fa-gift']);
                $urlNew = array_merge(['/sport/team/create'], $model->getAttributes([
                    'id', 'joLogo', 'name', 'abbr'
                ]));

                return Html::a($icon, $urlNew, $options);
            },
        ],
        'visibleButtons' => [
            'clear' => static function (SouTeamAS $model) {
                return (bool)$model->team_our_id;
            },
            'create-our-team' => static function (SouTeamAS $model) {
                return !$model->team_our_id;
            },
        ],
    ],
];

ColumnHiderWidget::calculateArrayColumns($columns);
?>
<div class="sou-team-ar-index">
    <h4 class="c-grey-900 mT-10 mB-30 pull-left"><?= Html::encode($this->title) ?></h4>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= ColumnHiderWidget::widget([
        'pjaxSelector' => '#sount-teams-pjax',
        'pjaxUrl' => '/sount/teams/index',
        'model' => $searchModel,
    ]) ?>

    <?php Pjax::begin(['id' => 'sount-teams-pjax']); ?>

    <?= Html::a('<span class="ti-plus"></span> Добавить', ['create'], ['class' => 'btn btn-success pull-right']) ?>

    <?= MatchedFilterWidget::widget() ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
    <?php Pjax::end(); ?>
</div>
