<?php

use common\widgets\DetailForm\DetailFormWidget;
use common\widgets\JsonEditor;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\modules\sount\models\SouTeamAR $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<div class="sou-team-ar-form">
    <?php $form = ActiveForm::begin() ?>
    <?= DetailFormWidget::widget([
        'model' => $model,
        'inputs' => [
            'id' => [
                'inputHidden' => $model->isNewRecord,
            ],
            'sou_key',
            'sou_id',
            'team_our_id',
            'name',
            'abbr',
            'contry_code',
            'ext_info' => JsonEditor::widget([
                'model' => $model,
                'attribute' => 'ext_info',
            ]),
        ],
    ]) ?>
    <hr>
    <div class="form-group text-right">
        <?= Html::submitButton('<span class="ti-save"></span> Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
