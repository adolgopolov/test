<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 13.04.19 11:55
 */

namespace common\modules\sount\doings;

use common\components\S3Component;
use common\components\S3Core;
use common\modules\sount\models\SouMatchAR;

/*
 *  @brief класс для реализации действий импорта
 *  создание , перевод в разные состояния и т.д.
 */

class ImportDoing extends S3Component
{
    public static function importOcca($matchId)
    {
        $souMatch = SouMatchAR::find()->imported()
            ->andWhere(['match_our_id' => $matchId])->one();
        $manager = S3Core::mSportSou()->importManager;
        return $souMatch ? $manager->loadMatchOcca($souMatch->id) : false;
    }
}