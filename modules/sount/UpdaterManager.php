<?php
/**
 * !!!не закончена разработка
 * Для обновления связанных данных из буферных таблиц во внутриигровые
 * Created by: adolgopolov0@gmail.com
 * Date: 16.07.19 19:58
 * @package common\modules\sount
 */

namespace common\modules\sount;


use common\components\S3Component;
use common\doings\sport\ChangeMatchOccasionDoing;
use common\modules\sount\models\SouMatchAR;
use common\modules\sount\models\SouOccaAR;
use common\modules\sport\doings\OccaDoing;
use common\modules\sount\dto\MatchOccaDTO;

class UpdaterManager extends S3Component
{
    /**
     * @var массив событий
     */
    protected $poolOcca;


    /**
     * загрузка событий по матчу
     * @param $souMatchId
     * @return bool
     */
    public function loadMatchOcca($souMatchId)
    {
        $souMatch = SouMatchAR::findOne($souMatchId);
        if (empty($souMatch)) {
            return false;
        }
        if (empty($souMatch->innerMatch)) {
            return false;
        }
        $occaARs = $this->getSouOccaForMatch($souMatch);
        //todo составить коллекцию событий для записи через sport load occa for match doing
        $matchOcca = new MatchOccaDTO($souMatch->innerMatch);
        $matchOcca->is_aggregate = true;
        foreach ($occaARs as $occaAR) {
            if(isset($occaAR->sman->sman_our_id) && isset($occaAR->fact->ent_id) && $occaAR->match->match_our_id) {
                $matchOcca->addOcca([
                    'match_id' => $occaAR->match->match_our_id,
                    'sman_id' => $occaAR->sman->sman_our_id,
                    'fact_id' => $occaAR->fact->ent_id,
                    'occa_amount' => $occaAR->fact_value,
                    'game' => $occaAR->game,
                    'after_start' => $occaAR->after_start,
                ]);
            }
        }
        $doing = new ChangeMatchOccasionDoing();
        //OccaDoing::loadOccaForMatch($matchOcca);
        return $doing->doLoadMatchOcca($matchOcca);
    }

    /**
     * получение буферных событий по матчу
     * @param SouMatchAR $match
     * @return array|SouOccaAR[]
     */
    protected function getSouOccaForMatch(SouMatchAR $match)
    {
        return SouOccaAR::find()->whereMatch($match->id)->all();
    }
}