<?php
/**
 * @deprecated отказались от функционала
 * Created by: adolgopolov0@gmail.com
 * Date: 16.07.19 19:58
 */

namespace common\modules\sount;


use common\components\S3Component;
use common\components\S3Core;
use common\doings\sport\ChangeMatchDoing;
use common\modules\game\models\GameTouMatchAR;
use common\modules\game\models\GameTourneyAR;
use common\modules\game\models\SportMatchAR;
use common\modules\game\senders\GameTourneySender;
use common\modules\sount\dto\MatchOccaDTO;
use common\modules\sount\models\SouMatchAR;
use common\modules\sount\models\SouOccaAR;
use common\modules\sount\models\SouSmanAR;
use common\modules\sount\models\SouTeamAR;
use common\modules\sount\models\SouTeamMemberAR;
use common\modules\sount\models\SouTournamentAR;
use common\modules\sport\doings\OccaDoing;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportMemberAR;
use common\modules\sport\models\SportTeamAR;
use yii\base\Exception;

class ImportManager extends S3Component
{
    protected $matches;
    protected $smen;
    protected $teams;
    protected $poolOcca;

    /**todo сделать создание турниров на лету, с набора импортируемых матчей
     * !!! draft
     * создание турнира по набору матчей, с импортом спортсменов, команд, матчей.
     * @param $tournament
     * @param $matchesIds
     * @return bool
     * @throws Exception
     */
    public function createTourney($tournament, $matchesIds)
    {
        $firstMatchDate = null;
        $tournament = is_numeric($tournament) ? SouTournamentAR::findOne($tournament) : $tournament;
        // матчи должны быть с целостными данными
        $matches = SouMatchAR::find()->andWhere([
            'id' => $matchesIds,
            'sou_id' => $tournament->sou_id,
            //'status' => SportSouModule::STATUS_PERFECT,
        ])->all();
        if (count($matches) == 0) {
            $this->addError('matches', 'в буферных таблицах нету таких матчей');
            return !$this->hasErrors();
        }
        foreach ($matches as $eachSouMatch) {
            /** @var SportMatchAR $innermatch */
            $innermatch = $this->updateMatch($eachSouMatch);
            $this->matches[] = $innermatch;
            if (($firstMatchDate === null) || ($innermatch->start_date < $firstMatchDate)) {
                $firstMatchDate = $innermatch->start_date;
            }
        }
        //todo игровому турниру прописать дефолтные значения
        $tourney = new GameTourneyAR([
            'sport_id' => SportSouModule::getSportByClientType($tournament->sou_id),
            'lea_id' => 10,
            'r_status' => S3Core::STATUS_DRAFT,
            'r_name' => $tournament->name,
//            'r_icon' => 'Лого турнира',
            'start_date' => $firstMatchDate,
//            'buyIn' => 'Байн-ин',
//            'prizefund' => 'Призовой фонд',
//            'lup_budget' => 'Лайнап-бюджет',
//            'max_smen' => 'Максимальное число спортсменов',
//            'max_gamers' => 'Максимальное число ?игроков?',
//            'reg_lineups' => 'Reg Lineups',
//            'trophy_kind' => 'Trophy Kind',
//            'score_kind'

        ]);

        $clonedTourney = clone $tourney;

        if (!$tourney->save()) {
            $this->addErrors($tourney->errors);

            return !$this->hasErrors();
        }

        $tourney->sendModel(GameTourneySender::SCENARIO_UPDATE, ['clonedModel' => $clonedTourney]);

        foreach ($this->matches as $eachSportMatch) {
            $touMatch = new GameTouMatchAR([
                'match_id' => $eachSportMatch->id,
                'tou_id' => $tourney->id,
            ]);
            if (!$touMatch->save()) {
                $this->addErrors($touMatch->errors);
            }
        }
        return !$this->hasErrors();
    }

    /**
     * @param SouMatchAR $souMatch
     * @return SportMatchAR|null
     * @throws Exception
     */
    public function updateMatch(SouMatchAR $souMatch)
    {
        foreach ($souMatch->teams() as $team) {
            $this->teams[] = $this->updateTeam($team);
        }
        foreach ($souMatch->smen() as $sman) {
            $this->smen[] = $this->updateSman($sman);
        }

        if ($sportMatch = $souMatch->innerMatch) {
            $doing = new ChangeMatchDoing(['matchAr'=>$sportMatch]);
        } else {
            $doing = new ChangeMatchDoing();
        }
        $doing->loadAttributes([
            'r_status' => S3Core::STATUS_DRAFT,
            'sport_id' => SportSouModule::getSportByClientType($souMatch->sou_id),
            'lea_id' => 1,
            'r_name' => $souMatch->teamGuest->name . '-' . $souMatch->teamOwner->name,
            'start_date' => $souMatch->date_start,
            'team_owner' => $souMatch->teamGuest->team_our_id,
            'team_guest' => $souMatch->teamOwner->team_our_id,
        ]);

        if (!$doing->createOrUpdate()) {
            $this->addErrors($doing->errors);
            return null;
        }

        $souMatch->setImportStatus($doing->getMatchAr()->id);

        foreach ($souMatch->teamGuestMembers as $eachGuestMember) {
            $this->updateMember($eachGuestMember);
        }
        foreach ($souMatch->teamOwnerMembers as $eachOwnerMember) {
            $this->updateMember($eachOwnerMember);
        }
        return $doing->getMatchAr();
    }

    public function updateSman(SouSmanAR $souSman)
    {
        if ($souSman->sman_our_id) {
            $sman = SmanAR::findOne($souSman->sman_our_id);
            return $sman;
        }

        throw new Exception('Спорстмен не сопоставлен');
    }

    public function updateTeam(SouTeamAR $souTeam)
    {
        if ($souTeam->team_our_id) {

            $team = SportTeamAR::findOne($souTeam->team_our_id);
            return $team;
        }

        throw new Exception('Команда не сопоставлена');
    }

    public function updateMember(SouTeamMemberAR $souMember)
    {
        if ($souMember->tm_our_id) {
            $member = SportMemberAR::findOne($souMember->tm_our_id);
        } else {
            $member = new SportMemberAR([
                'team_id' => $souMember->team->team_our_id,
                'sman_id' => $souMember->sman->sman_our_id,
                'first_day' => $souMember->first_day,
                'last_day' => $souMember->last_day,
            ]);
            if ($souMember->positionId()) $member->pos_id = $souMember->positionId();

        }
        if (!$member->save()) {
            $this->addErrors($member->errors);
            return null;
        }
        $souMember->updateAttributes([
            'tm_our_id' => $member->id,
        ]);

        return $member;
    }

    public function loadMatchOcca($souMatchId)
    {
        $souMatch = SouMatchAR::findOne($souMatchId);
        if (empty($souMatch)) {
            return false;
        }
        if (empty($souMatch->innerMatch)) {
            return false;
        }
        $occaARs = $this->getSouOccaForMatch($souMatch);
        //todo составить коллекцию событий для записи через sport load occa for match doing
        $matchOcca = new MatchOccaDTO($souMatch->innerMatch);
        $matchOcca->is_aggregate = true;
        foreach ($occaARs as $occaAR) {
            if(isset($occaAR->sman->sman_our_id) && isset($occaAR->fact->ent_id) && $occaAR->match->match_our_id) {
                $matchOcca->addOcca([
                    'match_id' => $occaAR->match->match_our_id,
                    'sman_id' => $occaAR->sman->sman_our_id,
                    'fact_id' => $occaAR->fact->ent_id,
                    'occa_amount' => $occaAR->fact_value,
                    'game' => $occaAR->game,
                    'after_start' => $occaAR->after_start,
                ]);
            }
        }
        OccaDoing::loadOccaForMatch($matchOcca);
        return true;
    }

    protected function getSouOccaForMatch(SouMatchAR $match)
    {
        return SouOccaAR::find()->whereMatch($match->id)->all();
    }
}