<?php
/**
 * трейт для работы с двичными статусами ActiveQuery методы
 * Created by: adolgopolov0@gmail.com
 * Date: 15.08.2019 16:40
 * @package common\modules\sount
 */

namespace common\modules\sount;


use common\components\S3Core;
use yii\db\ActiveQuery;

trait StatusAQTrait
{
    /**
     * неотслежываемые
     * @return self
     */
    public function disallow()
    {
        return $this->andWhere(['&', 'r_status' , S3Core::mSportSou()::STATUS_DISALLOW]);
    }
    /**
     * черновики
     * @return self
     */
    public function draft()
    {
        return $this->andWhere(['&', 'r_status' , S3Core::mSportSou()::STATUS_DRAFT]);
    }
    /**
     * подготовленные
     * @return self
     */
    public function prepared()
    {
        return $this->andWhere(['&', 'r_status' , S3Core::mSportSou()::STATUS_PREPARED]);
    }
    /**
     * записи с багами при загрузке
     * @return self
     */
    public function hasBugs()
    {
        return $this->andWhere(['&', 'r_status' , S3Core::mSportSou()::STATUS_BUGS]);
    }
    /**
     * связанные с игровыми таблицами записями
     * @return self
     */
    public function imported()
    {
        return $this->andWhere(['&', 'r_status' , S3Core::mSportSou()::STATUS_IMPORTED]);
    }

    /**
     * закрытые для дальнейшего скачивания
     *
     * например матчи с завершенной датой проведения и полученными событиями,
     * дальнейшее получение информации бесполезно
     * @return self
     */
    public function closed()
    {
        return $this->andWhere(['&', 'r_status' , S3Core::mSportSou()::STATUS_CLOSED]);
    }

    /**
     * @see  self::disallow()
     * @return self
     */
    public function notDisallow()
    {
        return $this->andWhere(['not',['&', 'r_status' , S3Core::mSportSou()::STATUS_DISALLOW]]);
    }

    /**
     * @see  self::draft()
     * @return self
     */
    public function notDraft()
    {
        return $this->andWhere(['not',['&', 'r_status' , S3Core::mSportSou()::STATUS_DRAFT]]);
    }

    /**
     * @see  self::prepared()
     * @return self
     */
    public function notPrepared()
    {
        return $this->andWhere(['not',['&', 'r_status' , S3Core::mSportSou()::STATUS_PREPARED]]);
    }

    /**
     * @see  self::hasBugs()
     * @return self
     */
    public function notHasBugs()
    {
        return $this->andWhere(['not',['&', 'r_status' , S3Core::mSportSou()::STATUS_BUGS]]);
    }

    /**
     * @see  self::imported()
     * @return self
     */
    public function notImported()
    {
        return $this->andWhere(['not',['&', 'r_status' , S3Core::mSportSou()::STATUS_IMPORTED]]);
    }

    /**
     * @see  self::closed()
     * @return self
     */
    public function notClosed()
    {
        return $this->andWhere(['not',['&', 'r_status' , S3Core::mSportSou()::STATUS_CLOSED]]);
    }
}