<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 10.06.19 15:02
 */

namespace common\modules\sount\jobs;

use common\components\S3Core;
use yii\base\Exception;

class MatchSummaryJob extends SountJob
{
    public $type;
    public $matchId;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     * @throws Exception
     */
    protected function executeInner($queue)
    {
        $client = S3Core::mSportSou()->getClient($this->type);
        $this->description = "получение статистики матча ".$this->matchId;
        if($client) {
            $result = $client->getMatchStatistic($this->matchId);
            if (!$result) {
                $this->error = $client->getErrors();
                return false;
            }

            return true;
        }
        $this->error = ['souClient'=>'wrong sou_id client'];
        return false;
    }
}