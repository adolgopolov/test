<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 10.06.19 15:02
 */

namespace common\modules\sount\jobs;

use common\components\BaseJob;
use common\components\S3Core;
use common\modules\sount\clients\SportClient;
use common\modules\sount\clients\SportClientInterface;
use yii\base\BaseObject;
use yii\base\Exception;

class SouScoringJob extends SountJob
{
    public $matchId;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|string|void
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function executeInner($queue)
    {
        $result = S3Core::mSportSou()->matchScoreStatistic($this->matchId,$errors);
        $this->description = "расчет статистики матча ".$this->matchId;
        if (!$result) {
            $this->error = $errors;
            return false;
        }
        return true;
    }
}