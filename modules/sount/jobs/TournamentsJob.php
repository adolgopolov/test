<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 10.06.19 15:02
 */

namespace common\modules\sount\jobs;

use common\components\BaseJob;
use common\components\S3Core;
use common\modules\sount\clients\SportClient;
use common\modules\sount\clients\SportClientInterface;
use yii\base\BaseObject;

class TournamentsJob extends SountJob
{
    public $type;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     */
    public function executeInner($queue)
    {   /** @var SportClientInterface $client */
        $client = S3Core::mSportSou()->getClient($this->type);
        $this->description = "новые турниры";
        if($client) {
            $result = $client->getTournaments();
            if (!$result) {
                $this->error = $client->getErrors();
                return false;
            }
            return true;
        }
        $this->error = ['souClient'=>'wrong sou_id client'];
        return false;
    }
}