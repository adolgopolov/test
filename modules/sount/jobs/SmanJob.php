<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 10.06.19 15:02
 */

namespace common\modules\sount\jobs;

use common\components\S3Core;
use common\modules\sount\clients\SportClient;
use common\modules\sount\clients\SportClientInterface;
use yii\base\BaseObject;

class SmanJob extends SountJob
{
    public $type;
    public $smanId;
    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     */
    public function executeInner($queue)
    {
        $client = S3Core::mSportSou()->getClient($this->type,[]);
        $this->description = "данные спортсмена ".$this->smanId;
        if($client) {
            $result = $client->getSman($this->smanId);
            if (!$result) {
                $this->error = $client->getErrors();
                return false;
            }
            return true;
        }
        $this->error = ['souClient'=>'wrong sou_id client'];
        return false;
    }
}