<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 10.06.19 15:02
 */

namespace common\modules\sount\jobs;

use common\components\S3Core;
use common\modules\sount\clients\SportClient;
use common\modules\sount\clients\SportClientInterface;
use yii\base\BaseObject;

class TeamJob extends SountJob
{
    public $type;
    public $teamId;
    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     */
    protected function executeInner($queue)
    {
        $client = S3Core::mSportSou()->getClient($this->type);
        $this->description = "данные команды ".$this->teamId;
        if($client) {
            if(!$client->getTeam($this->teamId)) {
                $this->error = $client->getErrors();
                return false;
            }
            return true;
        }
        $this->error = ['souClient'=>'wrong sou_id client'];
        return false;
    }
}
