<?php

namespace common\modules\sount\widgets\buffer\interfaces;

interface ColumnHiderModelInterface
{
    /**
     * @return array
     */
    public function getColumnsList();
}
