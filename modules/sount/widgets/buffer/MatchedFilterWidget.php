<?php

namespace common\modules\sount\widgets\buffer;

use Yii;
use yii\base\Widget;
use yii\db\ActiveQuery;

/**
 * Class MatchedFilterWidget
 * @package common\modules\sount\widgets\buffer
 */
class MatchedFilterWidget extends Widget
{
    public const DEFAULT_PARAM_NAME = 'only_matched';

    public $paramName = self::DEFAULT_PARAM_NAME;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $onlyMatched = Yii::$app->request->get($this->paramName);

        return $this->render('matched-filter', [
            'onlyMatched' => $onlyMatched,
        ]);
    }

    /**
     * @param ActiveQuery $query
     * @param array $params
     * @param string $columnName
     * @param string $paramName
     */
    public static function addQueryWhere(&$query, &$params, $columnName, $paramName = self::DEFAULT_PARAM_NAME)
    {
        $onlyMatched = null;

        if (isset($params[$paramName])) {
            $onlyMatched = $params[$paramName];

            unset($params[$paramName]);
        }

        if ($onlyMatched !== null) {
            if ($onlyMatched) {
                $query->andWhere(['not', [$columnName => null]]);
            } else {
                $query->andWhere([$columnName => null]);
            }
        }
    }
}
