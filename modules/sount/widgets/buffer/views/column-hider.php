<?php

/**
 * @var $this \yii\web\View
 * @var $model \yii\db\ActiveRecord|\common\modules\sount\widgets\buffer\interfaces\ColumnHiderModelInterface
 * @var $pjaxSelector string
 * @var $pjaxUrl string
 * @var $inputSelector string
 * @var $columns array
 * @var $containerOptions array
 */

use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$js = "
$('$inputSelector').on('change', function () {
    $.post({
        url: '$pjaxUrl',
        data: {
            columns: $(this).val(),
        },
        success: function () {
            $.pjax.reload({container: '$pjaxSelector'});
        }
    });
});
";

$this->registerJs($js);
?>
<?= Html::beginTag('div', $containerOptions) ?>
<?php $form = ActiveForm::begin([
    'id' => 'columns-form',
    'options' => [
        'data-pjax' => true,
        'class' => 'columns-select2-form',
    ],
]) ?>
<?= $form->field($model, ColumnHiderWidget::$inputColumns)->widget(Select2::class, [
    'data' => ColumnHiderWidget::prepareColumnsList($model),
    'theme' => Select2::THEME_BOOTSTRAP,
    'options' => [
        'id' => ltrim($inputSelector, '#'),
        'value' => Yii::$app->session->get(ColumnHiderWidget::getSessionParamName()),
    ],
    'pluginOptions' => [
        'tags' => true,
        'multiple' => true,
        'placeholder' => 'Скрыть колонки',
    ],
])->label(false) ?>
<?php ActiveForm::end() ?>
<?= Html::endTag('div') ?>