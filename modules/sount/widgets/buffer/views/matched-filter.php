<?php

/**
 * @var $this \yii\web\View
 * @var integer|null $onlyMatched
 */

use yii\helpers\Html;

?>
<div class="btn-group pull-right mR-5">
    <?= Html::a('Только сопоставленные', [
        'index',
        'only_matched' => 1
    ], ['class' => 'btn btn-primary' . ($onlyMatched ? ' active' : '')]) ?>
    <?= Html::a('Только несопоставленные', [
        'index',
        'only_matched' => 0
    ], ['class' => 'btn btn-primary' . ($onlyMatched !== null && !$onlyMatched ? ' active' : '')]) ?>
</div>

<?php if ($onlyMatched !== null) { ?>
    <?= Html::a('&times;', ['index'], ['class' => 'btn btn-danger pull-right mT-10 mR-5']) ?>
<?php } ?>
