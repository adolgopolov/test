<?php

namespace common\modules\sount\widgets\buffer;

use common\modules\sount\widgets\buffer\interfaces\ColumnHiderModelInterface;
use Yii;
use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class ColumnHiderWidget
 * @package common\modules\sount\widgets\buffer
 */
class ColumnHiderWidget extends Widget
{
    public static $inputColumns = 'columns';
    public $inputSelector = '#columns-select2';
    public $pjaxSelector;
    public $pjaxUrl;
    public $model;
    public $containerOptions = [
        'class' => [
            'form-group',
            'pull-right',
            'mL-5',
            'columns-select2-input',
        ],
    ];
    public $additionalContainerOptions = [];

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $containerOptions = ArrayHelper::merge($this->containerOptions, $this->additionalContainerOptions);

        return $this->render('column-hider', [
            'pjaxSelector' => $this->pjaxSelector,
            'pjaxUrl' => $this->pjaxUrl,
            'inputSelector' => $this->inputSelector,
            'model' => $this->model,
            'containerOptions' => $containerOptions,
        ]);
    }

    /**
     * @param array $columns
     * @param array|null $hiddenColumns
     */
    public static function calculateArrayColumns(&$columns, $hiddenColumns = null)
    {
        if ($hiddenColumns === null) {
            $hiddenColumns = Yii::$app->session->get(self::getSessionParamName());
        }

        $columns = array_filter($columns, static function ($value) use ($hiddenColumns) {
            $column = $value;
            if (is_array($value)) {
                $column = $value['attribute'] ?? false;
            }
            $explodedColumn = explode(':', $column);
            $column = $explodedColumn[0] ?? null;

            return !in_array($column, (array)$hiddenColumns);
        });
    }

    /**
     * @return void
     */
    public static function setColumnsSession()
    {
        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            $columnsData = Yii::$app->request->post(self::$inputColumns);

            Yii::$app->session->set(self::getSessionParamName(), $columnsData);
        }
    }

    /**
     * @return string
     */
    public static function getSessionParamName()
    {
        return 'columns-' . Yii::$app->controller->id . '-' . Yii::$app->controller->action->id;
    }

    /**
     * @param ActiveRecord|ColumnHiderModelInterface $model
     * @return array
     */
    public static function prepareColumnsList($model)
    {
        $columnsList = $model->getColumnsList();
        $attributeLabels = $model->attributeLabels();

        $result = [];

        foreach ($columnsList as $columnName) {
            $result[$columnName] = $attributeLabels[$columnName] ?? $columnName;
        }

        return $result;
    }
}
