<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 28.05.19 12:57
 */

namespace common\modules\sount\grabbers;


use common\components\S3Component;
use common\components\S3Core;
use common\modules\sount\models\SouMatchSmanAR;
use common\modules\sport\models\SportPositionAR;
use common\modules\sount\clients\BaseClient;
use common\modules\sount\clients\SportRadarDota;
use common\modules\sount\dto\SouGameDTO;
use common\modules\sount\dto\SouMatchDTO;
use common\modules\sount\dto\SouRoleDTO;
use common\modules\sount\dto\SouSmanDTO;
use common\modules\sount\dto\SouTeamDTO;
use common\modules\sount\dto\SouTournamentDTO;
use common\modules\sount\models\SouDictAR;
use common\modules\sount\models\SouMatchAR;
use common\modules\sount\models\SouOccaAR;
use common\modules\sount\models\SouSmanAR;
use common\modules\sount\models\SouTeamAR;
use common\modules\sount\models\SouTeamMemberAR;
use common\modules\sount\models\SouTournamentAR;
use common\modules\sount\SportSouModule;
use common\modules\sount\dto\DtoEvent as Event;
use yii\helpers\ArrayHelper;

class SportGrabber extends S3Component
{
    /** @var SouTeamAR[]| null */
    private $teams;
    /** @var SouSmanAR[]| null */
    private $smens;
    private $dict_entities = [];
    /** @var SouOccaAR[]| null */
    private $occa;

    public function __construct($config = [])
    {
        parent::__construct($config);
    }


    /**
     * загрузить все турниры, с датами текущего сезона
     * @param Event $event
     * @return void
     */
    public function loadNewTournamets(Event $event)
    {
        $souTournaments = $event->dto;
        $tournaments = SouTournamentAR::findAll(['sou_id' => $event->type]);
        $tournaments = ArrayHelper::index($tournaments, 'sou_key');
        /** @var SouTournamentDTO $eachSouTournament */
        foreach ($souTournaments as $eachSouTournament) {
            if (array_key_exists($eachSouTournament->id, $tournaments)) {
                $touRec = $tournaments[$eachSouTournament->id];
                $touRec->ext_info = ['current_season' => $eachSouTournament->current_season_id,
                    'category' => $eachSouTournament->category];
                $touRec->save();
                //TODO сравнить даты, c текущим сезоном и обновить, если сменился текущий сезон
            } else {
                $touRec = new SouTournamentAR([
                    'sou_key' => $eachSouTournament->id,
                    'sou_id' => $event->type,
                    'name' => $eachSouTournament->name,
                    'date_start' => $eachSouTournament->start_date,
                    'date_end' => $eachSouTournament->end_date,
                    'ext_info' => [
                        'current_season' => $eachSouTournament->current_season_id,
                        'category' => $eachSouTournament->category,
                    ],
                ]);
                $touRec->markDraft();
                if (!$touRec->save()) {
                    $this->saveToLog($touRec->errors,'sportGrabber');
                }
            }
        }
    }

    /**
     * загрузить матчи турнира
     * @return bool
     * @throws \ReflectionException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function loadNewMatches(Event $event)
    {
        $teamsInMatches = [];
        /** @var SouTournamentDTO $souTou */
        $souTou = $event->dto;

        $matchKeys = ArrayHelper::getColumn($souTou->getMatches(), 'id');
        $matches = SouMatchAR::findAll(['sou_id' => $event->type, 'sou_key' => $matchKeys]);
        $matches = ArrayHelper::index($matches, 'sou_key');
        $touRec = $this->updateTourney($souTou);
        foreach ($souTou as $eachSouMatch) {
            /** @var SouMatchDTO $eachSouMatch */
            if (array_key_exists($eachSouMatch->id, $matches)) {
                //TODO матч уже записан (проверка даты начала, перезапись)
            } else {
                $teamHome = $this->touchTeam($eachSouMatch->team_home_id,$event->type);
                $teamAway = $this->touchTeam($eachSouMatch->team_away_id,$event->type);
                $matchRec = new SouMatchAR([
                    'sou_key' => $eachSouMatch->id,
                    'sou_id' => $event->type,
                    'tou_sou_id' => $touRec->id,
                    'date_start' => $eachSouMatch->scheduled,
                    'th_sou_id' => $teamHome->id,
                    'ta_sou_id' => $teamAway->id,
                    'ext_info' => [
                        'status' => $eachSouMatch->status,
                        'start_time_tbd' => $eachSouMatch->start_time_tbd,
                        'tournament_round' => $eachSouMatch->tournament_round,
                    ],
                ]);
                //todo если матч отменен то маркировать его как отмененный
                if($eachSouMatch->status == 'cancelled') $matchRec->markHasBugs();
                $matchRec->markDraft();
                if ($matchRec->save()) {
                    $matches[$matchRec->sou_key] = $matchRec;
                    // отсеивать повторные запросы по командам в матчах турнира
                    if (!in_array($teamHome->id, $teamsInMatches)) {
                        S3Core::mSportSou()->createJobTeam($event->type, $teamHome,0);
                        $teamsInMatches[] = $teamHome->id;
                    }
                    if (!in_array($teamAway->id, $teamsInMatches)) {
                        S3Core::mSportSou()->createJobTeam($event->type, $teamAway,0);
                        $teamsInMatches[] = $teamAway->id;
                    }


                } else {
                    $this->saveToLog($matchRec->errors,'sportGrabber');
                }
            }
        }
        //todo вызвать updater буферные матчи которые обновились (для действий над внутренними матчами)
    }

    public function updateTourney(SouTournamentDTO $souTou)
    {
        $touRec = SouTournamentAR::findOne(['sou_key' => $souTou->id]);
        if($touRec){
            $touRec->markPrepared();
            $touRec->save();
        }
        return $touRec;
    }
    /**
     * загрузка по профилю команды
     * @param Event $event
     */
    public function updateTeam(Event $event)
    {
        /** @var SouTeamDTO $souTeam */
        $smen=[];
        $souTeam = $event->dto;
        $teamRec = $this->getArTeam($souTeam, $event->type);
        if($teamRec === null) return;
        foreach ($souTeam as $eachSouSman) {
            $smen[$eachSouSman->id] = ArrayHelper::toArray($eachSouSman);
            $smanRec = $this->updateSman($eachSouSman, $event->type);
            S3Core::mSportSou()->createJobSman($event->type, $smanRec->sou_key,0);
        }
        $teamRec->joSmen = $smen;
        //todo вызвать updater буферные команды которые обновились (для действий над внутренними командами)
    }


    /**
     * загрузка по профилю спортсмена
     *
     * @param Event $event
     */
    public function updateMember(Event $event)
    {
        /** @var SouSmanDTO $smanProfile */
        $smanProfile = $event->dto;
        $smanRec = $this->updateSman($smanProfile, $event->type);

        $teamIds = ArrayHelper::getColumn($smanProfile, 'team_id');
        $roles = ArrayHelper::index($smanProfile, 'team_id');
        $teams = SouTeamAR::findAll(['sou_id' => $event->type, 'sou_key' => $teamIds]);
        $memberRecs = SouTeamMemberAR::find()->where([
            'sman_sou_id' => $smanRec->id,
            'team_sou_id' => ArrayHelper::getColumn($teams, 'id'),
            'sou_id' => $event->type,
        ])->all();
        /** @var  SouTeamMemberAR[] $memberRecs */
        $memberRecs = ArrayHelper::index($memberRecs, 'team_sou_id');
        foreach ($teams as $key => $eachTeamRec) {
            /** @var SouRoleDTO $role */
            $role = $roles[$eachTeamRec->sou_key];
            if (!isset($memberRecs[$eachTeamRec->id])) {
                // создать новую запись, если есть команда в буферной таблице(для всех ролей и команд)
                $positionAR = $this->getArForDict($event->type, $smanProfile->type ?? 'Unknown', SouDictAR::KIND_POSITIONS);
                /** @var SouTeamMemberAR $memberAr */
                $memberAr = new SouTeamMemberAR([
                    'sman_sou_id' => $smanRec->id,
                    'team_sou_id' => $eachTeamRec->id,
                    'sou_id' => $event->type,

                    'pos_id' => $positionAR->id,//TODO писать как факты (через таблицу соответствий)
                    'ext_info' => [
                        'position' => $smanProfile->type ?? 'Unknown',
                    ]
                ]);
                $memberAr->markPrepared();
                if($role->start_date)$memberAr->first_day = $role->start_date;
                if($role->end_date)$memberAr->last_day = $role->end_date;
                $memberAr->save();

            } else {
                $memberAr = $memberRecs[$eachTeamRec->id];
                // закрыть старую запись, если есть

                if ($memberAr->last_day == S3Core::MAX_DATE && !$role->isActive()) {
                    $memberAr->last_day = $role->end_date;
                    if (!$memberAr->save()) {
                        $this->saveToLog($memberAr->errors,'sportGrabber');
                    }
                }

            }

        }
        //todo вызвать updater (для действий над внутренними мемберами)

    }

    /*
    * по всем запланированным матчам, получить статистику
    * @return bool
    */
    public function loadMatchSummary(Event $event)
    {

        /* @var SouMatchDTO $souMatch */
        $souMatch = $event->dto;
        //todo получить слушателя (накопитель значений матч-спортсмен) $listner = SouMatchSmanAR::getListner()
        $listner = new SouMatchSmanAR();
        \yii\base\Event::on(SouOccaAR::class, SouOccaAR::EVENT_AFTER_INSERT, [$listner, 'grabNewSouOcca']);
        $matchRec = SouMatchAR::findOne(['sou_key' => $souMatch->id]);
        if ($matchRec === null) {
            $this->addError('sou_match', "Этот матч не записан в буферную таблицу:$souMatch->id");
            $this->saveToLog($this->errors,'sportGrabber');
            return;
        }
        $matchRec->joSet('status',$souMatch->status);
        $matchRec->joSet('match_status',$souMatch->match_status);
        $matchRec->joSet('home_score',$souMatch->home_score);
        $matchRec->joSet('away_score',$souMatch->away_score);

        if($matchRec->joStatus == 'cancelled') {
            $matchRec->markHasBugs();
        } else {
            $matchRec->markPrepared();
        }
        //todo отмечать матч как завершенный, после скачивания статистики, и завершения самого матча, когда вся итоговая статистика скачана
        if($souMatch->status == 'closed') {
            $matchRec->markClosed();
        }
        $matchRec->save();
        /* @var SouGameDTO $game */
        $rezult = true;
        foreach ($souMatch as $game) {
            //todo добавить фейковые события для всех спортсменов команды
            // -- для футбола и доты все спортсмены вышедшие на поле имеют события
            foreach ($game->getTeams() as $souTeam) {
                foreach ($souTeam as $souSman) {
                    $smanRec = $this->updateSman($souSman, $event->type);
                    foreach ($souSman->getStatistic() as $souFact => $souVal) {
                        $souFactRec = $this->getArForDict($event->type, $souFact, SouDictAR::KIND_FACTS);
                        $souOcca = $this->getArForOcca($event->type, $matchRec, $smanRec, $souFactRec, $game);
                        $souOcca->setAttributes([
                            'fact_value' => is_numeric($souVal) ? (int) $souVal : 0,
                            'original_value' => (string)$souVal,
                            'ext_info' => [
                                'game' => $game->number
                            ],
                        ]);

                        if (!$souOcca->save()) { //todo $listner должен слушать aftersave() и накапливать уникальные пары ключей
                            $rezult = false;
                            $this->saveToLog($souOcca->errors,'sportGrabber');
                        }
                    }
                }
            }
        }

        $this->deleteOldOcca();
        $listner->flushOcca();
        if($matchRec->isClosed() && !$matchRec->isHasBugs()) {
            S3Core::mSportSou()->createJobSouScoring($matchRec->id,0);
        }
        //todo событие окончания загрузки событий матча через UpdaterManager обновить внутренние события матча

    }

    /**
     * @param SouSmanDTO $souSman
     * @param $type
     * @return SouSmanAR
     */
    protected function updateSman(SouSmanDTO $souSman, $type)
    {
        $this->getSmans($type);
        $roles = ArrayHelper::index($souSman, 'team_id');
        if (array_key_exists($souSman->id, $this->smens)) {
            $smanRec = $this->smens[$souSman->id];
            if(!empty($roles)){
                $smanRec->joRoles = $roles;
            }
            if(!empty($souSman->logo)) {
                $smanRec->joLogo = $souSman->logo;
            }
            $smanRec->markPrepared();
            $smanRec->save();
        } else {
            $smanRec = new SouSmanAR([
                'sou_key' => $souSman->id,
                'sou_id' => $type,
                'name' => $souSman->name,
                'nickname' => $souSman->nickname,
                'contry_code' => $souSman->country_code,
                'ext_info' => [
                    'date_of_birth' => $souSman->date_of_birth,
                    'gender' => $souSman->gender,
                    'position' => $souSman->type,
                    'roles' => $roles,
                    'logo' => $souSman->logo,
                ],
            ]);
            $smanRec->markPrepared();
            if ($smanRec->save()) {
                $this->smens[$smanRec->sou_key] = $smanRec;
            } else {
                $this->saveToLog($smanRec->errors,'sportGrabber');
            }
        }
        //todo вызвать updater буферные матчи спортсмены (для действий над внутренними спортсменами)
        return $smanRec;
    }

    /**
     * @return SouTeamAR[]
     */
    private function getTeams($type): array
    {
        if (empty($this->teams)) {
            $teams = SouTeamAR::findAll(['sou_id' => $type]);
            $this->teams = ArrayHelper::index($teams, 'sou_key');
        }
        return $this->teams;
    }

    private function getArTeam(SouTeamDTO $souTeam, $type)
    {
        $this->getTeams($type);
        if (array_key_exists($souTeam->id, $this->teams)) {
            $teamRec = $this->teams[$souTeam->id];

            $teamRec->setAttributes([
                'name' => $souTeam->name,
                'abbr' => $souTeam->abbreviation,
                'contry_code' => $souTeam->country_code,
                'ext_info' => [
                    'logo' => $souTeam->logo,
                ],
            ]);
            $teamRec->markPrepared();
            $teamRec->save();
            return $teamRec;
        } else {
            $teamRec = new SouTeamAR([
                'sou_key' => $souTeam->id,
                'sou_id' => $type,
                'name' => $souTeam->name,
                'abbr' => $souTeam->abbreviation,
                'contry_code' => $souTeam->country_code,
                'ext_info' => [
                    'logo' => $souTeam->logo,
                ],
            ]);
            $teamRec->markPrepared();
            if ($teamRec->save()) {
                $this->teams[$teamRec->sou_key] = $teamRec;
                return $teamRec;
            } else {
                $this->saveToLog($teamRec->errors,'sportGrabber');
            }
            return null;
        }

    }

    private function getSmans($type)
    {
        if (empty($this->smens)) {
            $smans = SouSmanAR::findAll(['sou_id' => $type]);
            $this->smens = ArrayHelper::index($smans, 'sou_key');
        }
        return $this->smens;
    }


    /**
     * @param Event $event
     * @param static|null $matchRec
     * @param SouSmanAR $smanRec
     * @param SouDictAR $souFactRec
     * @param SouGameDTO $game
     * @return SouOccaAR|null
     */
    protected function getArForOcca(int $type, SouMatchAR $matchRec, SouSmanAR $smanRec, SouDictAR $souFactRec, SouGameDTO $game)
    {
        $this->getOccaForMatch($matchRec);

        $rec = new SouOccaAR([
            'sou_id' => $type,
            'match_sou_id' => $matchRec->id,
            'sman_sou_id' => $smanRec->id,
            'fact_sou_id' => $souFactRec->id,
            'game' => $game->number,
        ]);
        if (isset($this->occa[$rec->getKeyForCash()])) {
            $oldRec = $this->occa[$rec->getKeyForCash()];
            unset($this->occa[$rec->getKeyForCash()]);
            return $oldRec;
        } else {
            $rec->clearVal();
            return $rec;
        }
    }

    /**
     * @param SouMatchAR $matchRec
     */
    protected function getOccaForMatch(SouMatchAR $matchRec): void
    {
        if (empty($this->occa)) {
            foreach (SouOccaAR::find()->where([
                'match_sou_id' => $matchRec->id,
            ])->all() as $occa) {
                $occa->clearVal();
                $this->occa[$occa->getKeyForCash()] = $occa;
            }

        }
    }

    protected function deleteOldOcca(): void
    {
        if (!empty($this->occa)) {
            SouOccaAR::deleteAll(['id' => ArrayHelper::getColumn($this->occa, 'id')]);
        }

    }

    /**
     * @param int $type
     * @param mixed $souDict
     * @param int $entKind
     * @return SouDictAR
     */
    protected function getArForDict(int $type, string $souDict, int $entKind): SouDictAR
    {
        if (empty($this->dict_entities)) {
            $entities = SouDictAR::findAll(['sou_id' => $type]);
            foreach ($entities as $entRec) {
                $this->dict_entities[$entRec->uniKey] = $entRec;
            }
            //$this->dict_entities = ArrayHelper::index($facts, 'sou_key');
        }

        if (!array_key_exists(SouDictAR::genUniKey($entKind, $souDict), $this->dict_entities)) {
            $entRec = new SouDictAR([
                'sou_key' => $souDict,
                'sou_id' => $type,
                'ent_kind' => $entKind,
            ]);
            $entRec->save();
            $this->dict_entities[$entRec->uniKey] = $entRec;
        } else {
            $entRec = $this->dict_entities[SouDictAR::genUniKey($entKind, $souDict)];
        }
        return $entRec;
    }

    /**
     * @param string $key
     * @return SouTeamAR
     */
    protected function touchTeam($key,$type): SouTeamAR
    {
        if (!$team = SouTeamAR::find()->where(['sou_key' => $key])->one()) {
            $team = new SouTeamAR([
                'sou_key' => $key,
                'sou_id' => $type,
            ]);
            $team->markDraft();
            $team->save();
        }
        return $team;
    }


}