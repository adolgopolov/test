<?php

namespace common\modules\sount\operations;

use common\components\S3Component;
use common\modules\score\models\ScoreKindAR;
use common\modules\sount\dto\SouSmanPointDTO;
use common\modules\sount\models\SouDictAR;
use common\modules\sount\models\SouMatchAR;
use common\modules\sount\models\SouOccaAR;
use common\modules\sount\models\SouSmanPointAR;
use common\modules\sount\SportSouModule;
use yii\helpers\ArrayHelper;

/**
 * класс для скоринга расширенной статистики
 * считает события в матче по дефолтному скорингу для вида спорта
 *
 * @author Skynin <sohay_ua@yahoo.com>
 * created: 19-Apr-2019
 *
 * @package common\modules\sount\scoring
 * @property SouMatchAR $match
 */
class MatchScoring extends S3Component
{
    /** @var SouSmanPointAR[]  */
    protected $poolSmanPoints = [];
    /** @var SouSmanPointAR[] */
    protected $newResults = [];
    /** @var SouSmanPointDTO[] */
    protected $matchSmanOcca = [];
    /** @var ScoreKindAR[] */
    protected $scoreKinds = [];

    protected $matchIds;


    public function calculate($matchId)
    {
        $this->matchIds = $matchId;
        // выбрать выбрать матч  все собыя
        $souOcca = $this->getOccaWithSmanPos($matchId);

        foreach ($souOcca as $eachSouOcca) {
            //сгрупировать все события по матчу и спортсмену, расчитать спортсмена
            $this->getScoreKindForOcca($eachSouOcca);
            if (!isset($this->matchSmanOcca[$eachSouOcca->getKeyMatchSman()])) {
                $this->matchSmanOcca[$eachSouOcca->getKeyMatchSman()] = new SouSmanPointDTO($eachSouOcca);
            }
            $this->matchSmanOcca[$eachSouOcca->getKeyMatchSman()]->addOcca($eachSouOcca);
        }

        foreach ($this->matchSmanOcca as $matchSman) {
            $this->newResults[] = $this->calculateSman($matchSman);
        }
        return true;
    }

    /**
     * собрать все скоринги для событий по виду спорта в массив ключ клиент
     * @param SouOccaAR $occaAR
     */
    public function getScoreKindForOcca(SouOccaAR $occaAR)
    {
        if (!isset($this->scoreKinds[$occaAR->sou_id])) {
            $this->scoreKinds[$occaAR->sou_id] = ScoreKindAR::getDefaultKind(SportSouModule::getSportByClientType($occaAR->sou_id));

        }
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return mixed
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function save($runValidation = true, $attributeNames = null)
    {

        $this->clearPrevARs();
        // TODO реальная запись в БД
        foreach ($this->newResults as $eachScoring) {
            if (!$eachScoring->save()) {
                $this->addErrors($eachScoring->errors);
            }
        }
        return !$this->hasErrors();
    }

    /**
     * @brief расчет событий матча по набору правил
     * @param SouSmanPointDTO $matchSman
     * @return SouSmanPointAR
     */
    protected function calculateSman(SouSmanPointDTO $matchSman)
    {
        $smanPointsAR = $this->getARfor($matchSman);
        $smanPointsAR->setAttributes([
            'sman_sou_id' => $matchSman->sman_id,
            'match_sou_id' => $matchSman->match_id,
            'sou_id' => $matchSman->sou_id,
        ]);
        $match = SouMatchAR::findOne($matchSman->match_id);

        $smanPointsAR->joScoreMatch = [
            $match->th_sou_id => $match->joGet('home_score',0),
            $match->ta_sou_id => $match->joGet('away_score',0),
        ];
        $smanPointsAR->joOcca = ArrayHelper::toArray($matchSman);
        $points = 0;
        foreach ($matchSman as $factId => $occaVal) {
            // по каждому виду расчета произвести расчет события
            $souFactAR = SouDictAR::findOne($factId);
            if($matchSman->position_name){
                $query = ['sou_id' => $matchSman->sou_id,'sou_key' => $matchSman->position_name];
            }else {
                $query = ['sou_id' => $matchSman->sou_id,'sou_key' => 'Unknown'];
            }
            $souPositionAR = SouDictAR::findOne($query);
            $scoreKind = $this->scoreKinds[$matchSman->sou_id];
            $occaPoints = $this->calculateOcca($occaVal, $souFactAR, $souPositionAR, $scoreKind);
            $smanPointsAR->addPoints($occaPoints);
            // TODO вернуть успешность или ошибку
        }
        return $smanPointsAR;
    }

    /**
     * @brief расчет события по скорингу учитывая тип скоринга
     * @param $occaVal
     * @param SouDictAR $factAR
     * @param SouDictAR $positionAR
     * @param ScoreKindAR $scoreKind
     * @return float|int
     */
    protected function calculateOcca($occaVal, SouDictAR $factAR, SouDictAR $positionAR, ScoreKindAR $scoreKind)
    {
        $result = 0;
        if ($factAR->innerDict && $positionAR->innerDict) {
            $points = $scoreKind->pointsFor($positionAR->ent_id,$factAR->ent_id);
            $result = $occaVal * $points;
        }
        return $result;
    }

    /**
     * @param $occa
     * @param $scoreKind
     * @return SouSmanPointAR
     */
    protected function getARfor(SouSmanPointDTO $smanPoints): SouSmanPointAR
    {
        return $this->grabPool($smanPoints);
    }

    /**
     * Очищает неиспользованные записи
     *
     */
    protected function clearPrevARs()
    {
        foreach ($this->poolSmanPoints as $freeAr) {
            $freeAr->delete();
        }
    }

    /**
     * return SportMatchAQ
     */
    public function getMatch()
    {
        return $this->hasOne(SouMatchAR::class, ['id' => 'ent_id']);
    }

    /**
     * @return SouSmanPointAR
     */
    protected function createNewCalcResult(): SouSmanPointAR
    {
        $result = new SouSmanPointAR();
        return $result;
    }

    /**
     * @return SouSmanPointAR
     */
    protected function grabPool(SouSmanPointDTO $smanPoints): SouSmanPointAR
    {
        if (empty($this->poolSmanPoints)) {
            foreach (SouSmanPointAR::findAll(['match_sou_id' => $this->matchIds]) as $result) {
                $this->poolSmanPoints[$result->getKeyMatchSman()] = $result;
            }
        }
        // вернуть запись , убрать ее из пула
        if (isset($this->poolSmanPoints[$smanPoints->getKey()])) {
            $smanPointAR = $this->poolSmanPoints[$smanPoints->getKey()];
            unset($this->poolSmanPoints[$smanPoints->getKey()]);
            return $smanPointAR;
        }
        // создаем новую запись под расчет
        return $this->createNewCalcResult();
    }

    /**
     * @param $matchId
     * @return array|SouOccaAR[]
     */
    protected function getOccaWithSmanPos($matchId)
    {
        return SouOccaAR::find()->where(['match_sou_id' => $matchId])->with('sman')->all();
    }

}
