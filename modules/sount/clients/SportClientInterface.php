<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 27.05.19 11:06
 *
 */

namespace common\modules\sount\clients;

use common\modules\sount\dto\DtoEvent;
use common\modules\sount\dto\SouMatchDTO;
use common\modules\sount\dto\SouSmanDTO;
use common\modules\sount\dto\SouTeamDTO;
use common\modules\sount\dto\SouTournamentDTO;

/**
 * Итерфейс для набора методов для получения данных по виду спорта
 *
 * турниры матчи команды спортсмены события сотавы итд
 * @package common\modules\sount\clients
 */
interface SportClientInterface
{
    CONST EVENT_TOURNAMENTS = 'tournaments';
    CONST EVENT_MATCHES = 'matches';
    CONST EVENT_MATCH_STAT = 'match-statistic';
    CONST EVENT_TEAM = 'team';
    CONST EVENT_SMAN = 'sman';

    /**
     * формирует DtoEvent для списка турниров
     * @see DtoEvent
     * @see SouTournamentDTO
     * @return bool
     */
    function getTournaments():bool;

    /**
     * формирует DtoEvent для списка матчей турнира
     * @see DtoEvent
     * @see SouTournamentDTO::$matches
     * @return bool
     */
    function getMatches($touId):bool;
    /**
     * формирует DtoEvent для профиля команды
     * @see DtoEvent
     * @see SouTeamDTO
     * @return bool
     */
    public function getTeam($teamId):bool;
    /**
     * формирует DtoEvent для профиля спортсмена
     * @see DtoEvent
     * @see SouSmanDTO
     * @return bool
     */
    public function getSman($smanId):bool;
    /**
     * формирует DtoEvent для списка событий матча
     * @see DtoEvent
     * @see SouMatchDTO
     * @return bool
     */
    public function getMatchStatistic($matchId):bool;

    public function getErrors();



}