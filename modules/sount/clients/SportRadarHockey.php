<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 18.05.19 14:40
 */

namespace common\modules\sount\clients;

use common\components\S3Component;

/**
 * Class SportRadarHockey
 * все методы получения данных хокея загружают данные в свойство $source
 *  получение данных через методы Source*()
 * @package common\components\sport_source
 */
class SportRadarHockey extends SportRadar implements SportClientInterface
{

    public function __construct($config = [])
    {
        $this->nameConfig = 'sportRadarHockey';
        parent::__construct($config);
    }

    /**
     * все методы должны возвращать истину при правильной работе и ложь при наличии ошибок
     * @return bool
     */
    function getTournaments(): bool
    {
        // TODO: Implement getTournaments() method.
    }

    /**
     * все методы должны возвращать истину при правильной работе и ложь при наличии ошибок
     * @return bool
     */
    function getMatches($touId): bool
    {
        // TODO: Implement getMatches() method.
    }

    /**
     * все методы должны возвращать истину при правильной работе и ложь при наличии ошибок
     * @return bool
     */
    public function getTeam($teamId): bool
    {
        // TODO: Implement getTeam() method.
    }

    /**
     * все методы должны возвращать истину при правильной работе и ложь при наличии ошибок
     * @return bool
     */
    public function getSman($smanId): bool
    {
        // TODO: Implement getSman() method.
    }

    /**
     * все методы должны возвращать истину при правильной работе и ложь при наличии ошибок
     * @return bool
     */
    public function getMatchStatistic($matchId): bool
    {
        // TODO: Implement getMatchStatistic() method.
    }

    public function loadDailyResults($date = null)
    {
        $date = $date ?? date('Y-m-d');
        return $this->load("schedules/$date/results");
    }

    public function loadDailySchedule($date = null)
    {
        $date = $date ?? date('Y-m-d');
        return $this->load("schedules/$date/schedule");
    }

    public function loadMatchSummary($matchId)
    {
        return $this->load("matches/$matchId/summary");
    }

    public function loadMatchTimeline($matchId)
    {
        return $this->load("matches/$matchId/timeline");
    }

    public function loadPlayerProfile($playerId)
    {
        return $this->load("players/$playerId/profile");
    }

    public function loadTeamProfile($teamId)
    {
        return $this->load("teams/$teamId/profile");
    }

    public function loadTournaments()
    {
        return $this->load('tournaments');
    }

    public function loadTournamentInfo($touId)
    {
        return $this->load("tournaments/$touId/info");
    }

    public function loadTournamentSchedule($touId)
    {
        return $this->load("tournaments/$touId/schedule");
    }

    public function loadTournamentResults($touId)
    {
        return $this->load("tournaments/$touId/results");
    }

    public function loadTournamentLiveStandings($touId)
    {
        return $this->load("tournaments/$touId/live_standings");
    }


}