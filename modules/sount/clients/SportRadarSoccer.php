<?php
/**
 * Описние клиента по футболу версии 3 спортадар
 * Created by: adolgopolov0@gmail.com
 * Date: 18.05.19 14:40
 *
 */
namespace common\modules\sount\clients;


use common\modules\sount\dto\DtoEvent;
use common\modules\sount\dto\SouMatchDTO;
use common\modules\sount\dto\SouSmanDTO;
use common\modules\sount\dto\SouTeamDTO;
use common\modules\sount\dto\SouTournamentDTO;
use common\modules\sount\SportSouModule;
use yii\base\Exception;

/**
 * Описние клиента по футболу версии 3 спортадар
 *
 * все методы получения данных футбола загружают данные в свойство $source
 * @package common\modules\sount\clients
 */
class SportRadarSoccer extends SportRadar implements SportClientInterface
{

    /**
     * SportRadarSoccer constructor.
     * @param array $config
     * @throws Exception
     */
    public function __construct($config = [])
    {
        $this->type = SportSouModule::SOU_ID_SR_SOCCER;
        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function getTournaments():bool
    {
        if($tournamentsDTO = $this->loadTournaments()){
            $this->trigger(self::EVENT_TOURNAMENTS,new DtoEvent(['dto' => $tournamentsDTO,'type' => $this->type]));
            return true;
        }else {
            return  false;
        }
    }
    /**
     * @inheritDoc
     */
    public function getMatches($touId):bool
    {
        if(!empty($touDTO = $this->loadTournamentSchedule($touId))){
            $this->trigger(self::EVENT_MATCHES,new DtoEvent(['dto' => $touDTO,'type' => $this->type]));
            return true;
        }else {
            return  false;
        }
    }

    /**
     * @inheritDoc
     */
    public function getTeam($teamId):bool
    {
        if($teamDTO = $this->loadTeamProfile($teamId)){
            $this->trigger(self::EVENT_TEAM,new DtoEvent(['dto' => $teamDTO,'type' => $this->type]));
            return true;
        }else {
            return  false;
        }
    }
    /**
     * @inheritDoc
     */
    public function getSman($smanId):bool
    {
        if($smanDTO = $this->loadPlayerProfile($smanId)){
            $this->trigger(self::EVENT_SMAN,new DtoEvent(['dto' => $smanDTO,'type' => $this->type]));
            return true;
        }else {
            return  false;
        }
    }

    /**
     * @param $matchId
     * @return bool
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function getMatchStatistic($matchId):bool
    {
        if($matchDTO = $this->loadMatchSummary($matchId)){
            $this->trigger(self::EVENT_MATCH_STAT,new DtoEvent(['dto' => $matchDTO,'type' => $this->type]));
            return true;
        }else {
            return  false;
        }
    }


    /**
     * не используется
     * @param null $date
     * @return bool
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadDailyResults($date = null)
    {
        $date = $date??date('Y-m-d');
        return $this->load("schedules/$date/results");
    }

    /**
     * не используется
     * @param null $date
     * @return bool
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadDailySchedule($date = null)
    {
        $date = $date??date('Y-m-d');
        return $this->load("schedules/$date/schedule");
    }

    /**
     * не используется
     * @return bool
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadLiveResults()
    {
        return $this->load("schedules/live/results");
    }

    /**
     * получение событий в summary моде
     * @param $matchId
     * @return SouMatchDTO|null
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadMatchSummary($matchId)
    {
        $this->clearErrors();

        if ($this->load("matches/$matchId/summary")) {
            $match = new SouMatchDTO($this->sourceByPath('sport_event'));
            $match->setMatchRezult($this->sourceByPath('sport_event_status'));
            if($statistics = $this->sourceByPath('statistics.maps')) {
                $match->addGames($statistics);
            }else if($statistics = $this->sourceByPath('statistics')) {
                foreach($statistics['teams'] as $keyTeam => $eachTeam) {
                    foreach ($eachTeam['players'] as $keyPlayer => $eachPlayer) {
                        $statAr = $eachPlayer;
                        unset($statAr['id'],$statAr['name']);
                        $statistics['teams'][$keyTeam]['players'][$keyPlayer]['statistics'] = $statAr;
                    }
                }
                $game[0] = $statistics;
                $game[0]['number'] = 1;
                $match->addGames($game);
            } else {
                $game = [];
                //TODO пустая статистика по матчу сделать логирование
                throw new Exception("Пустая статистика матча: $matchId ");
            }


            return $match;
        }
        return null;
    }

    /**
     * получение событий в timeline моде
     * @param $matchId
     * @return bool
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadMatchTimeline($matchId)
    {
        return $this->load("matches/$matchId/timeline");
    }

    /**
     * составы матча (стартовые)
     * @param $matchId
     * @return bool
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadMatchLineups($matchId)
    {
        return $this->load("matches/$matchId/lineups");
    }

    /**
     * получения данных по спортсмену
     * @param $playerId
     * @return SouSmanDTO|null
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadPlayerProfile($playerId)
    {
        $this->clearErrors();
        if ($this->load("players/$playerId/profile")) {
            $sman = new SouSmanDTO($this->sourceByPath('player'));
            $sman->logo = $this->sourceByPath('image.url');
            $sman->addRoles($this->sourceByPath('roles'));
            return $sman;
        }
        return null;
    }

    /**
     * получение данных по команде
     * @param $teamId
     * @return SouTeamDTO|null
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadTeamProfile($teamId)
    {
        $this->clearErrors();
        if ($this->load("teams/$teamId/profile")) {
            $team = new SouTeamDTO($this->sourceByPath('team'));
            $team->logo = $this->sourceByPath('logo.url');
            if($players = $this->sourceByPath('players')) {
                $team->addSmens($players);
            }
            return $team;
        }
        return null;
    }

    /**
     * список турниров и их текущих сезонов
     * @return array|null
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadTournaments()
    {
        $this->clearErrors();
        if ($this->load('tournaments')) {
            $array = [];
            foreach ($this->sourceByPath('tournaments') as $tournament) {
                $array[] = new SouTournamentDTO($tournament);
            }
            return $array;
        }
        return null;
    }

    /**
     * не используется
     * @param $touId
     * @return bool
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadTournamentInfo($touId)
    {
        return $this->load("tournaments/$touId/info");
    }

    /**
     * расписание текущего сезона турнира(список матчей)
     * @param $touId
     * @return SouTournamentDTO|null
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadTournamentSchedule($touId)
    {
        $this->clearErrors();
        if ($this->load("tournaments/$touId/schedule")) {
            $tou = new SouTournamentDTO($this->sourceByPath('tournament'));
            $tou->addMatches($this->sourceByPath('sport_events'));
            return $tou;
        }
        return null;
    }

    /**
     * не используется
     * @param $touId
     * @return bool
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadTournamentResults ($touId)
    {
        return $this->load("tournaments/$touId/results");
    }


}