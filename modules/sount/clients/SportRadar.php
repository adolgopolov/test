<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 18.05.19 14:40
 */
namespace common\modules\sount\clients;

use Yii;
use yii\base\Exception;

/**
 * Class SportRadar
 * @package common\modules\sount\clients
 */
abstract class SportRadar extends SportClient
{
    /**
     * параметры конфигураци спортрадара
     */
    public $url;
    public $apiKey;
    public $languageCode;
    public $format;

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * формировние ссылки пути и сохранения файла для загрузки
     * @see \common\components\yii\caching\FileCache::saveFile
     * @param array $url
     * @return array|string
     */
    protected function getUrl($url)
    {
        return [
            0 => 'source',
            1 => "$this->url/$this->languageCode/$url.$this->format?api_key=$this->apiKey",
            2 => $this->getPathToFile($url),
        ];
    }

    /**
     * @inheritDoc
     * @return void|null
     */
    protected function initConfig()
    {

        $params = Yii::$app->params[$this->nameConfig];
        $this->url = $params['url'];
        $this->apiKey = $params['apiKey'];
        $this->languageCode = $params['languageCode'];
        $this->format = $params['format'];
    }


    /**
     * @param string $dateTime
     * @return int timestamp
     * @throws \Exception
     */
    protected function convertDateTime($dateTime)
    {
        if ($dateTime === true ) {
            var_dump($dateTime);
            die;
        }

        $timeZone = new \DateTimeZone('Europe/Moscow');
        $date = new \DateTime($dateTime, $timeZone);
        $timestamp = $date->format('U');
        return $timestamp;
    }

    /**
     * возвращает количество секунд для хранения кеша по урлу
     * @param string $key полный урл запроса
     * @return int
     */
    public static function detectDuration($key)
    {
        $params = Yii::$app->params;
        $durationData = $params['durations']??[];
        $default = $durationData['default']??60*60;
        unset($durationData['default']);
        foreach ($durationData as $pattern => $time) {
            if(preg_match("/".$pattern."/",$key)){
                return $time;
            }
        }
        return $default;
    }

}