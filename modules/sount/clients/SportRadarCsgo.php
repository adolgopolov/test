<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 18.05.19 14:40
 */
namespace common\modules\sount\clients;

use common\components\S3Component;
use common\modules\sount\dto\DtoEvent;
use common\modules\sount\dto\SouMatchDTO;
use common\modules\sount\dto\SouSmanDTO;
use common\modules\sount\dto\SouTeamDTO;
use common\modules\sount\dto\SouTournamentDTO;
use common\modules\sount\SportSouModule;
use yii\base\Exception;

/**
 * Class SportRadarCsgo
 * все методы получения данных CSGO загружают данные в свойство $source
 *  получение данных через методы Source*()
 * @package common\components\sport_source
 */
class SportRadarCsgo extends SportRadar implements SportClientInterface
{


    /**
     * SportRadarCsgo constructor.
     * @param array $config
     * @throws Exception
     */
    public function __construct($config = [])
    {

        $this->type = SportSouModule::SOU_ID_SR_CSGO;
        parent::__construct($config);
    }

    /**
     * @return array
     * @throws \ReflectionException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function getTournaments():bool
    {
        if($tournamentsDTO = $this->loadTournaments()){
            $this->trigger(self::EVENT_TOURNAMENTS,new DtoEvent(['dto' => $tournamentsDTO,'type' => $this->type]));
            return true;
        }else {
            return  false;
        }
    }

    public function getMatches($touId):bool
    {
        if(!empty($touDTO = $this->loadTournamentSchedule($touId))){
            $this->trigger(self::EVENT_MATCHES,new DtoEvent(['dto' => $touDTO,'type' => $this->type]));
            return true;
        }else {
            return  false;
        }
    }

    public function getTeam($teamId):bool
    {
        if($teamDTO = $this->loadTeamProfile($teamId)){
            $this->trigger(self::EVENT_TEAM,new DtoEvent(['dto' => $teamDTO,'type' => $this->type]));
            return true;
        }else {
            return  false;
        }
    }

    public function getSman($smanId):bool
    {
        if($smanDTO = $this->loadPlayerProfile($smanId)){
            $this->trigger(self::EVENT_SMAN,new DtoEvent(['dto' => $smanDTO,'type' => $this->type]));
            return true;
        }else {
            return  false;
        }
    }

    public function getMatchStatistic($matchId):bool
    {
        if($matchDTO = $this->loadMatchSummary($matchId)){
            $this->trigger(self::EVENT_MATCH_STAT,new DtoEvent(['dto' => $matchDTO,'type' => $this->type]));
            return true;
        }else {
            return  false;
        }
    }

    public function loadDailyResults($date = null)
    {
        $date = $date??date('Y-m-d');
        return $this->load("schedules/$date/results");
    }

    public function loadDailySchedule($date = null)
    {
        $date = $date??date('Y-m-d');
        return $this->load("schedules/$date/schedule");
    }

    public function loadMatchLineups($matchId)
    {
        return $this->load("matches/$matchId/lineups");
    }

    public function loadMatchProbabilities($matchId)
    {
        return $this->load("matches/$matchId/probabilities");
    }

    public function loadMatchSummary($matchId)
    {
        $this->clearErrors();

        if ($this->load("matches/$matchId/summary")) {
            $match = new SouMatchDTO($this->sourceByPath('sport_event'));
            $match->setMatchRezult($this->sourceByPath('sport_event_status'));
            if($statistics = $this->sourceByPath('statistics.maps')) {
                $match->addGames($statistics);
            }else if($statistics = $this->sourceByPath('statistics')) {
                $game[0] = $statistics;
                $game[0]['number'] = 1;
                $match->addGames($game);
            } else {
                $game = [];
                //TODO пустая статистика по матчу сделать логирование
                throw new Exception("Пустая статистика матча: $matchId ");
            }


            return $match;
        }
        return null;
    }

    public function loadPlayerProfile($playerId)
    {
        $this->clearErrors();
        if ($this->load("players/$playerId/profile")) {
            $sman = new SouSmanDTO($this->sourceByPath('player'));
            $sman->logo = $this->sourceByPath('image.url');
            $sman->addRoles($this->sourceByPath('roles'));
            return $sman;
        }
        return null;
    }

    public function loadTeamProfile($teamId)
    {
        $this->clearErrors();
        if ($this->load("teams/$teamId/profile")) {
            $team = new SouTeamDTO($this->sourceByPath('team'));
            $team->logo = $this->sourceByPath('logo.url');
            $team->addSmens($this->sourceByPath('players'));
            return $team;
        }
        return null;
    }

    /**
     * @return array|bool|null
     * @throws \ReflectionException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function loadTournaments()
    {

        $this->clearErrors();
        if ($this->load('tournaments')) {
            $array = [];
            foreach ($this->sourceByPath('tournaments') as $tournament) {
                $array[] = new SouTournamentDTO($tournament);
            }
            return $array;
        }
        return null;
    }

    public function loadTournamentInfo($touId)
    {
        return $this->load("tournaments/$touId/info");
    }

    public function loadTournamentSchedule($touId)
    {
        $this->clearErrors();
        if ($this->load("tournaments/$touId/schedule")) {
            $tou = new SouTournamentDTO($this->sourceByPath('tournament'));
            $tou->addMatches($this->sourceByPath('sport_events'));
            return $tou;
        }
        return null;
    }

    public function loadTournamentResults ($touId)
    {
        return $this->load("tournaments/$touId/results");
    }

    public function loadTournamentLiveSummary()
    {
        return $this->load("schedules/live/summaries");
    }

    public function loadTournamentSummary($touId)
    {
        return $this->load("tournaments/$touId/summaries");
    }






}