<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 18.05.19 14:40
 */

namespace common\modules\sount\clients;

use common\components\S3Component;
use common\modules\sount\SportSouModule;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

abstract class BaseClient extends S3Component
{



    public $type;
    public $loadFromFile = false;
    public $saveFile = false;
    /**
     * @var HttpRequest
     */
    protected $httpClient;
    protected $nameConfig;
    protected $source;
    protected $loaderClass;

    protected $path;
    public $dir = '@common/tests/_data/sport_source/files';

    protected $timeZone;

    /**
     * BaseClient constructor.
     * @param array $config
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function __construct($config = [])
    {
        $this->nameConfig = SportSouModule::getTypeClient($this->type);
        $loader = null;
        if(isset($config['loader'])) {
            $loader = $config['loader'];
            unset ($config['loader']);
        } else {
            $loader = HttpRequest::class;
        }
        $this->httpClient = Yii::createObject($loader);
        parent::__construct($config);

        //$this->timeZone = new \DateTimeZone('Europe/Moscow');
        if (!$this->type) throw new Exception('Не указан тип ресурса в классе переопределить
         свойство $type в конструкторе конечного класса клиента, список типов константами в модуле указать
        ');
    }

    public function init()
    {
        $this->initConfig();
        parent::init();
    }

    /**
     * забрать конфигурационные данные из параметров и заполнить ими переменные
     * @return null
     */
    abstract protected function initConfig();

    /**
     * конвертация времени спортивного источника в timestamp
     * @param $dateTime
     * @return int timestamp
     */
    abstract protected function convertDateTime($dateTime);

    /**
     * преобразовать данные из  строки json как массив
     * @param mixed $source
     * @return array
     * @throws Exception
     */

    public function sourceAsArray($source)
    {
        if ($this->format == 'json') {
            $source = json_decode($source, true);
        } elseif ($this->format == 'xml') {
            throw new Exception('Не реализовано преоборазование xml в массив');

        }

        return $source;
    }

    /**
     * @deprecated не используется
     * дерево ключей ресурса
     * @param string $path
     * @return array
     */

    public function sourceKeys($path = null)
    {
        return array_keys($this->sourceByPath($path));
    }


    /**
     *  возвращает массив данных ответа или часть массива ответа по
     * @param string $path
     * @return mixed
     */
    public function sourceByPath($path = null)
    {
        if ($path) {
            return ArrayHelper::getValue($this->source, $path);
        }
        return $this->source;
    }

    /**
     * функция формирования строки адреса запроса
     * @param array $url
     * [
     *  0 => 'source',
     *  1 => "$this->url/$this->languageCode/$url.$this->format?api_key=$this->apiKey",
        2 => $this->getPathToFile($url),
     * ]
     * @return string
     */
    abstract protected function getUrl($url);


    /**
     * метод загрузки данных от стороннего источника
     * @param array $url
     * @param array $params
     * @return bool
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function load($url, $params = [])
    {
        $this->source = null;

        if ($source = Yii::$app->cache->get($this->getUrl($url))) {
            $this->source = $this->sourceAsArray($source);
            return true;
        }

        $this->source = $this->httpClient->load($this->getUrl($url));

        if ($this->httpClient->hasErrors()) {
            $this->addErrors($this->httpClient->getErrors());
        }
        if ($this->hasErrors()) {
            return false;
        }
        Yii::$app->cache->set($this->getUrl($url),$this->source);

        $this->source = $this->sourceAsArray($this->source);

        return true;
    }

    /**
     * служебный метод для формирования названия файла для хранения данных
     * @param $url
     * @return string
     */
    protected function getFileName($url)
    {
        return str_replace(['/', '.', ':'], '_', $url) . ".json";
    }

    /**
     * служебный метод для формирования названия директории для хранения данных
     * @return string
     */
    protected function getDirName(): string
    {
        return Yii::getAlias($this->dir . DIRECTORY_SEPARATOR . $this->nameConfig);
    }

    /**
     * служебный метод для формирования полного пути к файлу для хранения данных
     * @param $url
     * @return string
     */
    protected function getPathToFile($url): string
    {
        return $this->getDirName() . DIRECTORY_SEPARATOR . $this->getFileName($url);
    }

}