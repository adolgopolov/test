<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 18.05.19 14:40
 */
namespace common\modules\sount\clients;

use common\components\S3Component;
use yii\httpclient\Client;
use yii\log\Logger;

class HttpRequest extends S3Component implements LoaderInterface
{
    /**
     * @param mixed $url
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function load($url)
    {
        if(is_array($url)){
            list($type,$url,$fileName) = $url;
        }

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->send();
        if ($response->isOk) {
            return $response->content;

        } else {
            $content = $response->content;
            if($response->format == Client::FORMAT_JSON){
                $content = json_decode($response->content);
                $this->addError('http-client',$content['message']." : ".$url);

            }
            if($response->format == Client::FORMAT_XML){
                $content = strip_tags($response->content);
                $this->addError('http-client',$content." : ".$url);

            }
            if(!$this->hasErrors()){
                $this->addError('http-client',"ошибка неизвестного формата"." : ".$url);
            }
            return false;
        }

    }



}