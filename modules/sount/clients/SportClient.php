<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 11.06.19 10:11
 */

namespace common\modules\sount\clients;


use common\components\S3Component;
use common\modules\sount\events\TournamentsEvent;
use common\modules\sount\SportSouModule;
use yii\base\Event;

abstract class SportClient extends BaseClient implements SportClientInterface
{

}