<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 27.05.19 11:06
 */

namespace common\modules\sount\clients;


interface LoaderInterface
{
    /**
     * загрузка данных по урлу с удаленного источника
     * @param $url
     * @return mixed
     */
    function load($url);
}