<?php
/**
 * Created by: adolgopolov0@gmail.com
 * Date: 18.05.19 14:40
 */
namespace common\modules\sount\clients;

use common\components\S3Component;

/**
 * Class SportRadarBasketball
 * все методы получения данных баскетбола загружают данные в свойство $source
 *  получение данных через методы Source*()
 * @package common\modules\sount\clients
 */
class SportRadarBasketball extends SportRadar
{

    public function __construct($config = [])
    {
        $this->nameConfig = 'sportRadarBasketball';
        parent::__construct($config);
    }

    /**  Лиги
     * @return bool
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadCompetitions()
    {
        return $this->load("competitions");
    }
    /**  Лига номера сезонов
     * @return bool
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function loadCompetitionSeasons($compId)
    {
        return $this->load("competitions/$compId/seasons");
    }

    /**
     *  Live summaries
     */
    public function loadCompetitorSummaries($compId)
    {
        return $this->load("competitors/$compId/summaries");
    }

    public function loadDailySchedule($date = null)
    {
        $date = $date??date('Y-m-d');
        return $this->load("schedules/$date/summaries");
    }

    public function loadLiveTimelines()
    {
        return $this->load("schedules/live/timelines");
    }

    public function loadLiveTimelinesDelta()
    {
        return $this->load("schedules/live/timelines_delta");
    }

    public function loadPlayerProfile($playerId)
    {
        return $this->load("players/$playerId/profile");
    }

    public function loadSeasonLineups($seasonId)
    {
        return $this->load("seasons/$seasonId/lineups");
    }

    public function loadSportEventLineups($matchId)
    {
        return $this->load("sport_events/$matchId/lineups");
    }

    public function loadSportEventSummary($matchId)
    {
        return $this->load("sport_events/$matchId/summary");
    }

    public function loadSportEventTimeline($matchId)
    {
        return $this->load("sport_events/$matchId/timeline");
    }

}