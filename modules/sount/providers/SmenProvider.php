<?php

namespace common\modules\sount\providers;

use common\components\S3Component;
use common\components\S3Kind;
use common\modules\sount\models\SouDictAR;
use common\modules\sount\models\SouMatchAR;
use common\modules\sount\models\SouSmanAR;
use common\modules\sount\models\SouSmanPointAR;
use common\modules\sount\models\SouTeamAR;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportTeamAR;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Планировался для получения статистики по спортсменам
 * !!!не реализован
 * @package sount-module
 */
class SmenProvider extends S3Component
{
    /**
     * @param $sman_id
     * @return array|bool|SouSmanPointAR[]
     */
    public static function getStats($smanIds)
    {
        $facts = $sou_sman_points = [];

        $souFacts = SouDictAR::find()->where(['ent_kind' => 3])->all();
        foreach ($souFacts as $souFact) {
            if ($souFact->ent_id) {
                $facts[$souFact->id] = [
                    'title' => $souFact->innerDict->r_name,
                    'title_short' => $souFact->innerDict->title ?? '',
                    'value' => 0,
                ];
            }
        }
        $smanMatches = $query = (new Query())
            ->select([
                'sman_id' => 's.id',
                'match_id' => 'sou_m.match_our_id',
                'match_date' => 'sou_m.date_start',
                'match_title' => 'CONCAT(coalesce(json_extract(sth.ext_info, \'$.rus_short\'), sou_th.abbr,"")," - ",coalesce(json_extract(sta.ext_info, \'$.rus_short\'), sou_ta.abbr,""))',
                'th_abbr' => 'coalesce(json_extract(sth.ext_info, \'$.rus_short\'), sou_th.abbr,"")',
                'ta_abbr' => 'coalesce(json_extract(sta.ext_info, \'$.rus_short\'), sou_ta.abbr,"")',
                'th' => 'sou_th.id',
                'ta' => 'sou_ta.id',
                'points' => 'sou_sp.points',
                'stats' => 'sou_sp.ext_info',


            ])
            ->from(['s' => SmanAR::tableName()])
            ->innerJoin(SouSmanAR::tableName() . " sou_s", 'sou_s.sman_our_id=s.id')
            ->innerJoin(SouSmanPointAR::tableName() . " sou_sp", 'sou_sp.sman_sou_id=sou_s.id')
            ->innerJoin(SouMatchAR::tableName() . " sou_m", 'sou_sp.match_sou_id=sou_m.id')
            ->innerJoin(SouTeamAR::tableName() . " sou_th", 'sou_m.th_sou_id=sou_th.id')
            ->innerJoin(SouTeamAR::tableName() . " sou_ta", 'sou_m.ta_sou_id=sou_ta.id')
            ->leftJoin(SportTeamAR::tableName() . " sth", 'sth.id=sou_th.team_our_id')
            ->leftJoin(SportTeamAR::tableName() . " sta", 'sta.id=sou_ta.team_our_id')
            ->where(['sou_s.sman_our_id' => $smanIds])->all();
        $extPoints = new SouSmanPointAR();
        foreach ($smanMatches as $smanMatch) {
            $extPoints->ext_info = [];
            $extPoints->ext_info = json_decode($smanMatch['stats'], true);
            $occa = $score = [];
            if (!empty($extPoints->occa)) {
                foreach ($extPoints->occa as $eachId => $eachValue) {
                    if (isset($facts[$eachId])) {
                        $fact = $facts[$eachId];
                        $fact['value'] = $eachValue;
                        $occa[] = $fact;
                    }
                }
            }
            if (!empty($extPoints->scoreMatch)) {
                foreach ($extPoints->scoreMatch as $id => $value) {

                    if ($smanMatch['th'] == $id) {
                        $score['team_owner'] = $smanMatch['th_abbr'];
                        $score['total_owner'] = $value;
                    } else {
                        $score['team_guest'] = $smanMatch['ta_abbr'];
                        $score['total_guest'] = $value;
                    }
                }
            }


            $sou_sman_points[] = [
                'kind' => S3Kind::KIND_SPORT_STAT,
                'sman_id' => $smanMatch['sman_id'],
                'match_id' => $smanMatch['match_id'] ?? null,// только для матчей спортивного в игровых турнирах
                'match_date' => $smanMatch['match_date'],
                'match_title' => $smanMatch['match_title'],
                'match_total' => $score,
                'points' => $smanMatch['points'],
                'stats' => $occa,
            ];
        }
        return $sou_sman_points;
    }
}
