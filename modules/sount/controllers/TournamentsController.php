<?php

namespace common\modules\sount\controllers;

use backend\controllers\AdminController;
use common\components\S3Core;
use common\modules\sount\models\SouTournamentAR;
use common\modules\sount\models\SouTournamentAS;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\models\SportAR;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * TournamentsController implements the CRUD actions for SouTournamentAR model.
 */
class TournamentsController extends AdminController
{
    /**
     * Lists all SouTournamentAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SouTournamentAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SouTournamentAR model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = SouTournamentAR::findOrFail($id);

        return $this->render('view', compact('model'));
    }

    /**
     * Creates a new SouTournamentAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function actionCreate()
    {
        $model = new SouTournamentAR();
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->addFlash('success', 'Запись успешно добавлена');
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('model', 'sports'));
    }

    /**
     * Updates an existing SouTournamentAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\ExitException
     */
    public function actionUpdate($id)
    {
        $model = SouTournamentAR::findOrFail($id);
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('model', 'sports'));
    }

    /**
     * Deletes an existing SouTournamentAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = SouTournamentAR::findOrFail($id);
        if ($model) {
            \Yii::$app->session->addFlash('success', 'Запись успешно удалена');
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionUploadTournaments()
    {
        $log = S3Core::mSportSou()->getTypeClient();
        foreach (S3Core::mSportSou()->getTypeClient() as $keySource => $nameSource) {
            $log[] = S3Core::mSportSou()->createJobTournaments($keySource);
        }

        return $this->redirect(['index']);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionGrabMatches()
    {
        $log = 'пустой';
        $log = S3Core::mSportSou()->getTypeClient();
        foreach (S3Core::mSportSou()->getTypeClient() as $keySource => $nameSource) {
            $log[] = S3Core::mSportSou()->createJobMatchesForAllTou($keySource, date('Y-m-d', time() - 12 * 3600 * 24 * 30));
        }
        return $this->redirect(['index']);
    }
}
