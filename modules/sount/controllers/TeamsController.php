<?php

namespace common\modules\sount\controllers;

use backend\controllers\AdminController;
use common\modules\sount\models\SouTeamAR;
use common\modules\sount\models\SouTeamAS;
use common\modules\sount\models\SportTeamOurAS;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportTeamAR;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * TeamsController implements the CRUD actions for SouTeamAR model.
 */
class TeamsController extends AdminController
{
    /**
     * Lists all SouTeamAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SouTeamAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $queryParams = Yii::$app->request->queryParams;

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'queryParams' => $queryParams,
        ]);
    }

    /**
     * @return string
     */
    public function actionOurTeam()
    {
        $searchModel = new SportTeamOurAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('our-team', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionClear($id)
    {
        /** @var SouTeamAR $model */
        $model = SouTeamAR::findOrFail($id);
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model->clearImportStatus();
            return $this->actionSouTeam();
        }

        return $this->redirect(['sou-team']);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionPaste($id)
    {
        /** @var SouTeamAR $model */
        $sou_id = Yii::$app->request->post('sou_id');
        $model = SouTeamAR::findOrFail($sou_id);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->setImportStatus($id);

            return $this->actionOurTeam();
        }

        return $this->redirect(['our-team']);
    }

    /**
     * @return Response
     */
    public function actionPasteAll()
    {
        /** @var SouTeamAR $souModel */

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $data = Yii::$app->request->post('data');
            $souIds = ArrayHelper::getColumn($data, 'sou_id');
            $souModels = SouTeamAR::find()->where(['id' => $souIds])->all();
            $souModels = ArrayHelper::index($souModels, 'id');
            foreach ($data as $item) {
                $souTeamId = $item['sou_id'];
                $teamId = $item['our_id'];
                if (!$souTeamId) {
                    continue;
                }
                $existsOurRecord = SportTeamAR::find()->where(['id' => $teamId])->exists();
                if (!$existsOurRecord) {
                    Yii::$app->session->addFlash('error', "Комманда № $teamId не существует");
                    continue;
                }
                $existsRecord = SouTeamAR::find()->where(['team_our_id' => $teamId])->exists();
                if ($existsRecord) {
                    Yii::$app->session->addFlash('error', "Комманда № $teamId уже сопоставлен");
                    continue;
                }
                if (isset($souModels[$souTeamId])) {
                    $souModel = $souModels[$souTeamId];
                    $souModel->team_our_id = $teamId;
                    if ($souModel->validate()) {
                        $souModel->setImportStatus($teamId);
                        Yii::$app->session->addFlash('success', "Запись $souTeamId успешно добавлена");
                    } else {
                        Yii::$app->session->addFlash('error', "Запись $souTeamId имеет ошибки");
                    }
                } else {
                    Yii::$app->session->addFlash('error', "Буферная Комманда № $souTeamId не существует");
                    continue;
                }
            }
        }

        return $this->redirect(['our-team']);
    }

    /**
     * Displays a single SouTeamAR model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = SouTeamAR::findOrFail($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new SouTeamAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function actionCreate()
    {
        $model = new SouTeamAR();
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно добавлена');

            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'sports' => $sports,
        ]);
    }

    /**
     * Updates an existing SouTeamAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = SouTeamAR::findOrFail($id);
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно обновлена');

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'sports' => $sports,
        ]);
    }

    /**
     * Deletes an existing SouTeamAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        throw new Exception('Не реализовано!!!');
        return $this->redirect(['index']);
        $model = SouTeamAR::findOrFail($id);
        if ($model) {
            Yii::$app->session->addFlash('success', 'Запись успешно удалена');

            $model->delete();
        }

        return $this->redirect(['index']);
    }
}
