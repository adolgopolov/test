<?php

namespace common\modules\sount\controllers;

use backend\controllers\AdminController;
use common\components\S3Core;
use common\modules\sount\models\SouMatchAR;
use common\modules\sount\models\SouMatchAS;
use common\modules\sount\models\SouTournamentAR;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\models\SportAR;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * MatchesController implements the CRUD actions for SouMatchAR model.
 */
class MatchesController extends AdminController
{
    /**
     * Lists all SouMatchAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SouMatchAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SouMatchAR model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = SouMatchAR::findOrFail($id);

        return $this->render('view', compact('model'));
    }

    /**
     * Creates a new SouMatchAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function actionCreate()
    {
        $model = new SouMatchAR();
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно добавлена');

            return $this->redirect(['index']);
        }

        return $this->render('create', compact('model', 'sports'));
    }

    /**
     * Updates an existing SouMatchAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\ExitException
     */
    public function actionUpdate($id)
    {
        $model = SouMatchAR::findOrFail($id);
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('model', 'sports'));
    }

    /**
     * Deletes an existing SouMatchAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = SouMatchAR::findOrFail($id);
        if ($model) {
            \Yii::$app->session->addFlash('success', 'Запись успешно удалена');
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCalcExt($id)
    {
        $model = SouMatchAR::findOrFail($id);

        if (!S3Core::mSportSou()->matchScoreStatistic($model->id,$errors)) {
            //todo вернуть ошибку
        }

        return $this->redirect(['view','id' => $model->id]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionImportMatch($id)
    {
        $model = SouMatchAR::findOrFail($id);

        if (!S3Core::mSportSou()->importManager->updateMatch($model)) {
            //todo вернуть ошибку
        }

        return $this->redirect(['view','id' => $model->id]);
    }

    /**
     * @param null $client
     * @return \yii\web\Response
     */
    public function actionGrabSummary($client = null)
    {
        $log = ['пустой'];
        $query = SouTournamentAR::find()->prepared();
        if($client) {
            $query->andWhere(['sou_id' => $client]);
        }
        $tournaments = $query->all();
        foreach ($tournaments as $eachTournament) {
            $log[$eachTournament->id] = S3Core::mSportSou()->createJobMatchesSumForTou($eachTournament->id) ;

        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionLoadOcca($id)
    {
        $log = ['пустой'];
        $match = SouMatchAR::find()
            ->notHasBugs()->notClosed()->notDisallow()
            ->where(['id' => $id])->one();
        S3Core::mSportSou()->createJobMatchSum($match->sou_id, $match->sou_key, 0);

        return $this->redirect(['index']);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionExtScoring()
    {
        $log = ['пустой'];
        $matches = SouMatchAR::find()->prepared()->all();

        foreach ($matches as $eachMatch) {
            $log[$eachMatch->id] = S3Core::mSportSou()->createJobSouScoring($eachMatch->id,0) ;
        }

        return $this->redirect(['index']);
    }
}
