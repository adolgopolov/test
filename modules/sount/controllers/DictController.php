<?php

namespace common\modules\sount\controllers;

use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use Yii;
use common\modules\sount\models\SouDictAR;
use common\modules\sount\models\SouDictAS;
use backend\controllers\AdminController;
use common\modules\sport\models\SportAR;

/**
 * DictController implements the CRUD actions for SouDictAR model.
 */
class DictController extends AdminController
{
    /**
     * Lists all SouDictAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SouDictAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SouDictAR model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = SouDictAR::findOrFail($id);

        return $this->render('view', compact('model'));
    }

    /**
     * Creates a new SouDictAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function actionCreate()
    {
        $model = new SouDictAR();
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->addFlash('success', 'Запись успешно добавлена');
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('model', 'sports'));
    }

    /**
     * Updates an existing SouDictAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = SouDictAR::findOrFail($id);
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('model', 'sports'));
    }

    /**
     * Deletes an existing SouDictAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = SouDictAR::findOrFail($id);
        if ($model) {
            \Yii::$app->session->addFlash('success', 'Запись успешно удалена');
            $model->delete();
        }

        return $this->redirect(['index']);
    }
}
