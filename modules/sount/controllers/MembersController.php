<?php

namespace common\modules\sount\controllers;

use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use Yii;
use common\modules\sount\models\SouTeamMemberAR;
use common\modules\sount\models\SouTeamMemberAS;
use backend\controllers\AdminController;
use common\modules\sport\models\SportAR;
use yii\web\NotFoundHttpException;

/**
 * MembersController implements the CRUD actions for SouTeamMemberAR model.
 */
class MembersController extends AdminController
{
    /**
     * Lists all SouTeamMemberAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SouTeamMemberAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SouTeamMemberAR model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = SouTeamMemberAR::findOrFail($id);

        return $this->render('view', compact('model'));
    }

    /**
     * Creates a new SouTeamMemberAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function actionCreate()
    {
        $model = new SouTeamMemberAR();
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->addFlash('success', 'Запись успешно добавлена');
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('model', 'sports'));
    }

    /**
     * Updates an existing SouTeamMemberAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\ExitException
     */
    public function actionUpdate($id)
    {
        $model = SouTeamMemberAR::findOrFail($id);
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('model', 'sports'));
    }

    /**
     * Deletes an existing SouTeamMemberAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = SouTeamMemberAR::findOrFail($id);
        if ($model) {
            \Yii::$app->session->addFlash('success', 'Запись успешно удалена');
            $model->delete();
        }

        return $this->redirect(['index']);
    }
}
