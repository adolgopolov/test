<?php

namespace common\modules\sount\controllers;

use common\modules\sount\models\SmanOurAS;
use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use common\modules\sport\models\SmanAR;
use Yii;
use common\modules\sount\models\SouSmanAR;
use common\modules\sount\models\SouSmanAS;
use backend\controllers\AdminController;
use common\modules\sport\models\SportAR;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * SmenController implements the CRUD actions for SouSmanAR model.
 */
class SmenController extends AdminController
{
    /**
     * Lists all SouSmanAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SouSmanAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $queryParams = Yii::$app->request->queryParams;

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'queryParams' => $queryParams,
        ]);
    }

    /**
     * @return string
     */
    public function actionOurSmen()
    {
        $searchModel = new SmanOurAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('our-smen', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionClear($id)
    {
        /** @var SouSmanAR $model */
        $model = SouSmanAR::findOrFail($id);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model->clearImportStatus();
            return $this->actionSouSmen();
        }

        return $this->redirect(['sou-smen']);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionPaste($id)
    {
        /** @var SouSmanAR $model */
        $sou_id = Yii::$app->request->post('sou_id');
        $model = SouSmanAR::findOrFail($sou_id);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->setImportStatus($id);

            return $this->actionOurSmen();
        }

        return $this->redirect(['our-smen']);
    }

    /**
     * @return Response
     */
    public function actionPasteAll()
    {
        /** @var SouSmanAR $souModel */

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $data = Yii::$app->request->post('data');
            $souIds = ArrayHelper::getColumn($data, 'sou_id');
            $souModels = SouSmanAR::find()->where(['id' => $souIds])->all();
            $souModels = ArrayHelper::index($souModels, 'id');
            foreach ($data as $item) {
                $souSmanId = $item['sou_id'];
                $smanId = $item['our_id'];
                if (!$souSmanId) {
                    continue;
                }
                $existsOurRecord = SmanAR::find()->where(['id' => $smanId])->exists();
                if (!$existsOurRecord) {
                    Yii::$app->session->addFlash('error', "Спортсмен № $smanId не существует");

                    continue;
                }
                $existsRecord = SouSmanAR::find()->where(['sman_our_id' => $smanId])->exists();
                if ($existsRecord) {
                    Yii::$app->session->addFlash('error', "Спортсмен № $smanId уже сопоставлен");

                    continue;
                }
                if (isset($souModels[$souSmanId])) {
                    $souModel = $souModels[$souSmanId];
                    $souModel->sman_our_id = $smanId;
                    if ($souModel->validate()) {
                        $souModel->setImportStatus($smanId);

                        Yii::$app->session->addFlash('success', "Запись $souSmanId успешно добавлена");
                    } else {
                        Yii::$app->session->addFlash('error', "Запись $souSmanId имеет ошибки");
                    }
                } else {
                    Yii::$app->session->addFlash('error', "Буферный Спортсмен № $souSmanId не существует");

                    continue;
                }
            }
        }

        return $this->redirect(['our-smen']);
    }

    /**
     * Displays a single SouSmanAR model.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = SouSmanAR::findOrFail($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new SouSmanAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function actionCreate()
    {
        $model = new SouSmanAR();
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно добавлена');

            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'sports' => $sports,
        ]);
    }

    /**
     * Updates an existing SouSmanAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\ExitException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = SouSmanAR::findOrFail($id);
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись успешно обновлена');

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'sports' => $sports
        ]);
    }

    /**
     * Deletes an existing SouSmanAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        throw new Exception('Не реализовано!!!');
        return $this->redirect(['index']);
        $model = SouSmanAR::findOrFail($id);

        if ($model) {
            Yii::$app->session->addFlash('success', 'Запись успешно удалена');

            $model->delete();
        }

        return $this->redirect(['index']);
    }
}
