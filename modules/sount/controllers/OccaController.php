<?php

namespace common\modules\sount\controllers;

use common\modules\sount\widgets\buffer\ColumnHiderWidget;
use Yii;
use common\modules\sount\models\SouOccaAR;
use common\modules\sount\models\SouOccaAS;
use backend\controllers\AdminController;
use common\modules\sport\models\SportAR;
use yii\web\NotFoundHttpException;

/**
 * OccaController implements the CRUD actions for SouOccaAR model.
 */
class OccaController extends AdminController
{
    /**
     * Lists all SouOccaAR models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SouOccaAS();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        ColumnHiderWidget::setColumnsSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SouOccaAR model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $model = SouOccaAR::findOrFail($id);

        return $this->render('view', compact('model'));
    }

    /**
     * Creates a new SouOccaAR model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function actionCreate()
    {
        $model = new SouOccaAR();
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->addFlash('success', 'Запись успешно добавлена');
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('model', 'sports'));
    }

    /**
     * Updates an existing SouOccaAR model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\ExitException
     */
    public function actionUpdate($id)
    {
        $model = SouOccaAR::findOrFail($id);
        $sports = SportAR::getAsOptsList();

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('model', 'sports'));
    }

    /**
     * Deletes an existing SouOccaAR model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = SouOccaAR::findOrFail($id);
        if ($model) {
            \Yii::$app->session->addFlash('success', 'Запись успешно удалена');
            $model->delete();
        }

        return $this->redirect(['index']);
    }
}
