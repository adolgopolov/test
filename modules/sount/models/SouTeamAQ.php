<?php

namespace common\modules\sount\models;

use common\modules\sount\StatusAQTrait;

/**
 * This is the ActiveQuery class for [[SouTeamAR]].
 *
 * @see SouTeamAR
 */
class SouTeamAQ extends \common\extensions\ExtAQ
{
    use StatusAQTrait;

    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return SouTeamAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SouTeamAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
