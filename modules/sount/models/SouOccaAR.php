<?php

namespace common\modules\sount\models;

use common\extensions\ExtInfoAR;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "ss_sou_occasion".
 *
 * @property int $id
 * @property int $sou_id
 * @property int $match_sou_id
 * @property int $sman_sou_id
 * @property int $game
 * @property int $fact_sou_id
 * @property int $fact_value
 * @property string $original_value
 * @property int $after_start minutes after start match
 * @property array $ext_info
 *
 * @property SouSmanAR $sman
 * @property SouMatchAR $match
 * @property SouDictAR $fact
 */
class SouOccaAR extends ExtInfoAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sou_occasion}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sou_id', 'match_sou_id', 'sman_sou_id', 'fact_sou_id', 'game'], 'required'],
            [['sou_id', 'match_sou_id', 'sman_sou_id', 'fact_sou_id', 'fact_value', 'after_start'], 'integer'],
            [['ext_info'], 'safe'],
            [['original_value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sou_id' => 'Sou ID',
            'match_sou_id' => 'Match Sou ID',
            'sman_sou_id' => 'Sman Sou ID',
            'game' => 'Game',
            'fact_sou_id' => 'Fact Sou ID',
            'fact_value' => 'Fact Value',
            'original_value' => 'Original Value',
            'after_start' => 'After Start',
            'ext_info' => 'Ext Info',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SouOccaAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SouOccaAQ(get_called_class());
    }
    
    public function getKeyForCash()
    {
        return $this->sou_id."-".$this->match_sou_id."-".$this->sman_sou_id."-".$this->fact_sou_id."-".$this->game;
    }

    public function getKeyMatchSman()
    {
        return $this->sou_id."-".$this->match_sou_id."-".$this->sman_sou_id;
    }

    public function clearVal()
    {
        $this->fact_value = 0;
        $this->original_value = 0;
        $this->after_start = 1;
        $this->ext_info = [];
    }

    /**
     * @return ActiveQuery
     */
    public function getSman() : ActiveQuery
    {
        return $this->hasOne(SouSmanAR::class,['id' => 'sman_sou_id']);
    }

    public function getMatch() : ActiveQuery
    {
        return $this->hasOne(SouMatchAR::class,['id' => 'match_sou_id']);
    }

    public function getFact() : ActiveQuery
    {
        return $this->hasOne(SouDictAR::class,['id' => 'fact_sou_id'])->onCondition(['ent_kind'=>SouDictAR::KIND_FACTS]);
    }
}
