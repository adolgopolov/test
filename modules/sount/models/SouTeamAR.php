<?php

namespace common\modules\sount\models;

use common\modules\sount\SportSouModule;
use common\modules\sport\models\SportAR;
use common\modules\sport\models\SportTeamAR;

/**
 * This is the model class for table "ss_sou_team".
 *
 * @property int $id
 * @property string $sou_key
 * @property int $sou_id
 * @property int $team_our_id
 * @property string $name
 * @property string $abbr
 * @property string $contry_code
 * @property array $ext_info
 * @property int $r_status
 * @property SportAR $sport
 *
 * @property SportTeamAR $innerTeam
 * @property SouSmanAR[] $smen
 * @property $joLogo
 */
class SouTeamAR extends SouAR
{
    public static $countryCodes = [];

    public function init()
    {
        $this->columnImportId = 'team_our_id';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sou_team}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sou_key', 'sou_id'], 'required'],
            [['sou_id', 'team_our_id'], 'integer'],
            [['ext_info'], 'safe'],
            [['sou_key', 'name', 'abbr', 'contry_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'sou_key' => 'Ключ',
            'sou_id' => 'Клиент',
            'r_status' => 'Статус',
            'logo' => 'Фото (Буферная)',
            'team_our_id' => 'Наша команда, ! все сопоставленные, + несопоставленные',
            'name' => 'Название',

            'abbr' => 'Аббревиатура',
            'contry_code' => 'Код страны',
        ];
    }

    /**
     * @return SouSmanAQ|\yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getSmen()
    {
        return $this->hasMany(SouSmanAR::class, ['id' => 'sman_sou_id'])
            ->viaTable(SouTeamMemberAR::tableName(), ['team_sou_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return SouTeamAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SouTeamAQ(static::class);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInnerTeam()
    {
        return $this->hasOne(SportTeamAR::class, ['id' => 'team_our_id']);
    }

    /**
     * @param SportTeamAR $model
     * @return array|SouTeamAR[]
     */
    public static function compareTeam(SportTeamAR $model)
    {
        $arrayClients = SportSouModule::getSportByClientType();

        $sou_id = array_search($model->sport_id, $arrayClients);

        $title = $model->getTitle();
        $words = explode(' ', $title);
        if (!$words) {
            return [];
        }
        if (count($words) > 1) {
            $query = ['OR'];
            foreach ($words as $word) {
                $query[] = ['like', 'name', $word];
            }
            $query[] = ['and',
                $query,
                ['sou_id' => $sou_id],
                ['team_our_id' => null],
            ];
        } else {
            $query = ['and',
                ['like', 'name', $words[0]],
                ['sou_id' => $sou_id],
                ['team_our_id' => null],
            ];
        }
        $equalRezult = self::find()
            ->where(['and',
                ['like', 'name', $title],
                ['sou_id' => $sou_id],
                ['team_our_id' => null],
            ])
            ->limit(2)
            ->indexBy('id')
            ->all();

        $rezult = self::find()
            ->where($query)
            ->limit(3)
            ->indexBy('id')
            ->all();

        return $rezult + $equalRezult;
    }

    /**
     * @return array
     */
    public static function countryCodes()
    {
        if (empty(self::$countryCodes)) {
            $codes = self::find()->select('contry_code')->groupBy(['contry_code'])->column();
            foreach ($codes as $code) {
                if (!empty($code)) {
                    self::$countryCodes[$code] = $code;
                }
            }
        }
        return self::$countryCodes;
    }

    /**
     * @return void
     */
    public function getSport()
    {
        SportAR::findOne(SportSouModule::getSportByClientType($this->sou_id));
    }
}
