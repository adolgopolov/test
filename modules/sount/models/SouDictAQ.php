<?php

namespace common\modules\sount\models;

/**
 * This is the ActiveQuery class for [[SouDictAR]].
 *
 * @see SouDictAR
 */
class SouDictAQ extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return SouDictAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SouDictAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
