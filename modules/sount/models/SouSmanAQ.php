<?php

namespace common\modules\sount\models;

use common\modules\sount\StatusAQTrait;

/**
 * This is the ActiveQuery class for [[SouSmanAR]].
 *
 * @see SouSmanAR
 */
class SouSmanAQ extends \common\extensions\ExtAQ
{
    use StatusAQTrait;

    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return SouSmanAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SouSmanAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
