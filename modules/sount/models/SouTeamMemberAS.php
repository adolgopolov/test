<?php

namespace common\modules\sount\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SouTeamMemberAS represents the model behind the search form of `common\modules\sount\models\SouTeamMemberAR`.
 */
class SouTeamMemberAS extends SouTeamMemberAR
{
    public $columns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'team_sou_id', 'sman_sou_id', 'sou_id', 'r_status', 'tm_our_id', 'pos_id'], 'integer'],
            [['first_day', 'last_day', 'ext_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'team_sou_id' => $this->team_sou_id,
            'sman_sou_id' => $this->sman_sou_id,
            'sou_id' => $this->sou_id,
            'tm_our_id' => $this->tm_our_id,
            'first_day' => $this->first_day,
            'last_day' => $this->last_day,
            'pos_id' => $this->pos_id,
        ]);

        $query->andFilterWhere(['&', 'r_status', $this->r_status]);

        $query->andFilterWhere(['like', 'ext_info', $this->ext_info]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'team_sou_id',
            'sman_sou_id',
            'sou_id',
            'r_status',
            'tm_our_id',
            'first_day',
            'last_day',
            'pos_id',
        ];
    }
}
