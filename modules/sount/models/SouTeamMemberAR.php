<?php

namespace common\modules\sount\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "ss_sou_team_member".
 *
 * @property int $id
 * @property int $team_sou_id
 * @property int $sman_sou_id
 * @property int $sou_id
 * @property int $r_status
 * @property int $tm_our_id
 * @property string $first_day
 * @property string $last_day
 * @property int $pos_id position
 * @property array $ext_info
 *
 * @property SouDictAR $position
 * @property SouSmanAR $sman
 * @property SouTeamAR $team
 *
 */
class SouTeamMemberAR extends SouAR
{

    public function init()
    {
        $this->columnImportId = 'tm_our_id';
        parent::init();
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sou_team_member}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_sou_id', 'sman_sou_id', 'sou_id'], 'required'],
            [['team_sou_id', 'sman_sou_id', 'sou_id', 'pos_id','tm_our_id'], 'integer'],
            [['first_day', 'last_day', 'ext_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'team_sou_id' => 'Команда',
            'sman_sou_id' => 'Спортсмен',
            'sou_id' => 'Клиент',
            'r_status' => 'Статус',
            'tm_our_id' => 'Внутренняя запись',
            'first_day' => 'Первый день',
            'last_day' => 'Последний день',
            'pos_id' => 'Амплуа',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SouTeamMemberAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SouTeamMemberAQ(get_called_class());
    }

    public function getPosition(): ActiveQuery
    {
        return $this->hasOne(SouDictAR::class, ['id' => 'pos_id'])->onCondition(['ent_kind' => SouDictAR::KIND_POSITIONS]);
    }

    public function positionId()
    {
        return $this->position->ent_id??null;
    }

    public function positionName()
    {
        return $this->position->sou_key;
    }
    /**
     *
     */
    public function getSman()
    {
        return $this->hasOne(SouSmanAR::class, ['id' => 'sman_sou_id']);
    }

    /**
     *
     */
    public function getTeam()
    {
        return $this->hasOne(SouTeamAR::class, ['id' => 'team_sou_id']);
    }
}
