<?php

namespace common\modules\sount\models;

use common\extensions\ExtInfoAR;
use common\modules\sount\SportSouModule;
use Yii;

/**
 * Class SouAR
 * @package common\modules\sount\models
 * @property string $statusName
 * @property string $clientName
 *
 */
class SouAR extends ExtInfoAR
{
    /**
     * Название колонки, где хранится внутренний  ID
     * @var string
     */
    protected $columnImportId;

    public $status;

    public function getStatusName()
    {
        return SportSouModule::getSouStatus($this->r_status);
    }

    public function getStatusList()
    {
        $rezult = [];

        $souStatuses = SportSouModule::getSouStatus();

        foreach($souStatuses as $key => $name){
            if($this->r_status & $key) {
                $rezult[]=$key;
            }
        }

        return $rezult;
    }

    public function setStatusList($array)
    {
        if(empty($array)) {
            $this->markDraft();
        } else {
            $this->r_status = 0;
            foreach($array as $key){
                $this->markStatus($key);
            }
        }

    }

    public function getClientName()
    {
        return SportSouModule::getTypeClient($this->sou_id);
    }

    /**
     * маркирование указанных битов
     * @param $status
     */
    public function markStatus($status)
    {
        if($this->r_status === null) {
            $this->r_status = 0;
        }
        $this->r_status = $this->r_status | $status;
    }

    /**
     * стереть указанные биты
     * @param $status
     */
    public function clearStatus($status)
    {
        $this->r_status = $this->r_status & ~$status;
    }

    public function markDraft()
    {
        $this->clearStatus(SportSouModule::STATUS_PREPARED);
        $this->markStatus(SportSouModule::STATUS_DRAFT);
    }

    public function markDisallow()
    {
        $this->markStatus(SportSouModule::STATUS_DISALLOW);
    }

    public function markPrepared()
    {
        $this->clearStatus(SportSouModule::STATUS_DRAFT);
        $this->markStatus(SportSouModule::STATUS_PREPARED);
    }

    public function markHasBugs()
    {
        $this->markStatus(SportSouModule::STATUS_BUGS);
    }

    public function markImported()
    {
        $this->markStatus(SportSouModule::STATUS_IMPORTED);
    }

    public function markClosed()
    {
        $this->markStatus(SportSouModule::STATUS_CLOSED);
    }

    public function clearDraft()
    {
        $this->clearStatus(SportSouModule::STATUS_DRAFT);
    }

    public function clearDisallow()
    {
        $this->clearStatus(SportSouModule::STATUS_DISALLOW);
    }

    public function clearPrepared()
    {
        $this->clearStatus(SportSouModule::STATUS_PREPARED);
    }

    public function clearHasBugs()
    {
        $this->clearStatus(SportSouModule::STATUS_BUGS);
    }

    public function clearImported()
    {
        $this->clearStatus(SportSouModule::STATUS_IMPORTED);
    }

    public function clearClosed()
    {
        $this->clearStatus(SportSouModule::STATUS_CLOSED);
    }

    public function isDisallow():bool
    {
        return (bool) ($this->r_status & SportSouModule::STATUS_DISALLOW);
    }

    public function isDraft():bool
    {
        return (bool) ($this->r_status & SportSouModule::STATUS_DRAFT);
    }

    public function isPrepared():bool
    {
        return (bool) ($this->r_status & SportSouModule::STATUS_PREPARED);
    }

    public function isHasBugs():bool
    {
        return (bool) ($this->r_status & SportSouModule::STATUS_BUGS);
    }

    public function isImported():bool
    {
        return (bool) ($this->r_status & SportSouModule::STATUS_IMPORTED);
    }

    public function isClosed():bool
    {
        return (bool) ($this->r_status & SportSouModule::STATUS_CLOSED);
    }

    /**
     * указание записи внутреннего ИД , установка статуса загружен во внутреннюю таблицу
     * @param $innerId
     */
    public function setImportStatus($innerId)
    {
        $this->markImported();
        $this->updateAttributes([
            $this->columnImportId => $innerId,
            'r_status' => $this->r_status,
        ]);
        return ;
    }

    public function clearImportStatus()
    {
        $this->clearImported();
        $this->{$this->columnImportId} = null;
        return $this->save();
    }
}
