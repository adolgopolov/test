<?php

namespace common\modules\sount\models;

use common\modules\game\models\SportMatchAR;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ss_sou_match".
 *
 * @property int $id
 * @property string $sou_key
 * @property int $sou_id
 * @property int $r_status
 * @property int $tou_sou_id
 * @property int $match_our_id
 * @property int $th_sou_id team home
 * @property int $ta_sou_id team away
 * @property string $date_start
 * @property array $ext_info
 * @property int $status [smallint(6) unsigned]
 *
 * @property SouTeamAR $teamGuest
 * @property SouTeamAR $teamOwner
 * @property SportMatchAR $innerMatch
 * @property SouTournamentAR $tournament
 * SouTeamMemberAR[]  $teamGuestMembers
 * @property SouTeamMemberAR[]  $teamOwnerMembers
 */
class SouMatchAR extends SouAR
{

    public function init()
    {
        $this->columnImportId = 'match_our_id';
        parent::init();
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sou_match}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sou_key', 'sou_id', 'tou_sou_id', 'th_sou_id', 'ta_sou_id'], 'required'],
            [['sou_id', 'tou_sou_id', 'match_our_id', 'th_sou_id', 'ta_sou_id'], 'integer'],
            [['date_start', 'ext_info'], 'safe'],
            [['sou_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'sou_key' => 'Ключ',
            'sou_id' => 'Клиент',
            'tou_sou_id' => 'Турнир',
            'match_our_id' => 'Внутренний матч',
            'th_sou_id' => 'Команда дом',
            'ta_sou_id' => 'Команда гость',
            'date_start' => 'Дата начала',
            'r_status' => 'Статус',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SouMatchAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SouMatchAQ(get_called_class());
    }

    public function getTournament()
    {
        return $this->hasOne(SouTournamentAR::class, ['id' => 'tou_sou_id']);
    }

    public function getInnerMatch()
    {
        return $this->hasOne(SportMatchAR::class, ['id' => 'match_our_id']);
    }

    public function getTeamGuest()
    {
        return $this->hasOne(SouTeamAR::class, ['id' => 'ta_sou_id']);
    }


    public function getTeamOwner()
    {
        return $this->hasOne(SouTeamAR::class, ['id' => 'th_sou_id']);
    }

    public function teams()
    {
        return [$this->teamGuest, $this->teamOwner];
    }

    public function smen()
    {
        $smanIdsG = ArrayHelper::getColumn($this->teamGuestMembers,'sman_sou_id');
        $smanIdsO = ArrayHelper::getColumn($this->teamOwnerMembers,'sman_sou_id');
        $smanIds = array_merge($smanIdsG,$smanIdsO);

        //todo список спортсменов из обеих команд , сделать выборку через ключи, записывать в ext_info из DTO список спортсменов
        return SouSmanAR::find()->where(['id' => $smanIds])->all();
    }

    /**
     * @param $smanId
     * @return SouTeamMemberAR|null
     */
    public function findPositionOfMan($smanId)
    {
        if(($member = $this->getTeamOwnerMembers()->byMan($smanId)->one()) === null) {
            $member = $this->getTeamGuestMembers()->byMan($smanId)->one();
        }
        return $member;
    }
    /**
     * {@inheritdoc}
     * @return SouTeamMemberAQ.
     */
    public function getTeamGuestMembers()
    {
        return $this->hasMany(SouTeamMemberAR::class, ['team_sou_id' => 'ta_sou_id'])->byDate($this->date_start);
    }
    /**
     * {@inheritdoc}
     * @return SouTeamMemberAQ.
     */
    public function getTeamOwnerMembers()
    {
        return $this->hasMany(SouTeamMemberAR::class, ['team_sou_id' => 'th_sou_id'])->byDate($this->date_start);
    }

    public function isGoodForImport() :bool
    {
        return (bool) ($this->teamGuest->isImported() && $this->teamOwner->isImported())??false;
    }
}
