<?php

namespace common\modules\sount\models;

use common\components\S3Core;
use common\modules\sount\StatusAQTrait;

/**
 * This is the ActiveQuery class for [[SouMatchAR]].
 *
 * @see SouMatchAR
 */
class SouMatchAQ extends \common\extensions\ExtAQ
{
    use StatusAQTrait;

    /**
     * {@inheritdoc}
     * @return SouMatchAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SouMatchAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
