<?php

namespace common\modules\sount\models;

use common\modules\sount\SportSouModule;
use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportAR;

/**
 * This is the model class for table "ss_sou_sman".
 *
 * @property int $id
 * @property string $sou_key
 * @property int $sou_id
 * @property int $r_status
 * @property int $sman_our_id
 * @property string $name
 * @property string $contry_code
 * @property string $nickname
 * @property array $ext_info
 *
 * @property string $joPosition
 * @property SmanAR $innerSman
 */
class SouSmanAR extends SouAR
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->columnImportId = 'sman_our_id';

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sou_sman}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sou_key', 'sou_id'], 'required'],
            [['sou_id', 'sman_our_id', 'r_status'], 'integer'],
            [['ext_info'], 'safe'],
            [['sou_key', 'name', 'contry_code', 'nickname'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'sou_key' => 'Ключ',
            'sou_id' => 'Клиент',
            'r_status' => 'Статус',
            'logo' => 'Фото',
            'sman_our_id' => 'Наш спортсмен, ! все сопоставленные, + несопоставленные',
            'name' => 'Имя',
            'contry_code' => 'Код страны',
            'nickname' => 'Никнейм',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SouSmanAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SouSmanAQ(static::class);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInnerSman()
    {
        return $this->hasOne(SmanAR::class, ['id' => 'sman_our_id']);
    }

    /**
     * @return SportAR|null
     */
    public function getSport()
    {
        return SportAR::findOne(SportSouModule::getSportByClientType($this->sou_id));
    }

    /**
     * @param SmanAR $model
     * @return array|SouSmanAR[]
     */
    public static function compareSman(SmanAR $model)
    {

        $team_id = $model->lastTeamMember->team_id;

        $title = $model->getTitle();
        $words = explode(' ', $title);
        if (!$words) {
            return [];
        }
        if (count($words) > 1) {
            $orQuery[] = 'OR';
            foreach ($words as $word) {
                $orQuery[] = ['like', 's.name', $word];
            }
            $query = ['and',
                $orQuery,
                ['t.team_our_id' => $team_id],
            ];
        } else {
            $query = ['and',
                ['like', 's.name', $words[0]],
                ['sman_our_id' => null],
                ['t.team_our_id' => $team_id],
            ];
        }
        $equalRezult = self::find()
            ->alias('s')
            ->innerJoin(SouTeamMemberAR::tableName() . ' tm', 'tm.sman_sou_id=s.id')
            ->innerJoin(SouTeamAR::tableName() . ' t', 'tm.team_sou_id=t.id')
            ->where(['and',
                ['like', 's.name', $title],
                ['sman_our_id' => null],
                ['t.team_our_id' => $team_id],
            ])
            ->limit(2)
            ->indexBy('id')
            ->all();

        $rezult = self::find()
            ->alias('s')
            ->innerJoin(SouTeamMemberAR::tableName() . ' tm', 'tm.sman_sou_id=s.id')
            ->innerJoin(SouTeamAR::tableName() . ' t', 'tm.team_sou_id=t.id')
            ->where($query)
            ->limit(3)
            ->indexBy('id')
            ->all();

        return $rezult + $equalRezult;
    }
}
