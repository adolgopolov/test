<?php

namespace common\modules\sount\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SouDictAS represents the model behind the search form of `common\modules\sount\models\SouDictAR`.
 */
class SouDictAS extends SouDictAR
{
    public $columns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sou_id', 'ent_id', 'ent_kind'], 'integer'],
            [['sou_key', 'ext_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SouDictAR::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sou_id' => $this->sou_id,
            'ent_id' => $this->ent_id,
            'ent_kind' => $this->ent_kind,
        ]);

        $query->andFilterWhere(['like', 'sou_key', $this->sou_key])
            ->andFilterWhere(['like', 'ext_info', $this->ext_info]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'sou_key',
            'sou_id',
            'ent_id',
            'ent_kind',
        ];
    }
}
