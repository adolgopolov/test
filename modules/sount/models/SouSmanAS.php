<?php

namespace common\modules\sount\models;

use common\modules\sount\widgets\buffer\interfaces\ColumnHiderModelInterface;
use common\modules\sount\widgets\buffer\MatchedFilterWidget;
use common\modules\sport\models\SmanAR;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * SouSmanAS represents the model behind the search form of `common\modules\sount\models\SouSmanAR`.
 */
class SouSmanAS extends SouSmanAR implements ColumnHiderModelInterface
{
    /** @var array */
    public $columns;

    /** @var string */
    public $logo_sman;

    /** @var string */
    public $sman_our_name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sou_id', 'r_status'], 'integer'],
            [['sman_our_name'], 'string'],
            [['sou_key', 'name', 'contry_code', 'nickname', 'ext_info', 'sman_our_id', 'team'], 'safe'],
            [['columns'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'logo_sman' => 'Фото (С базы)',
            'sman_our_id' => 'ID',
            'sman_our_name' => 'Наш спортсмен',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->alias('sou_sman')
            ->leftJoin(SmanAR::tableName() . ' sman', 'sman.id = sou_sman.sman_our_id')
            ->select(['sou_sman.*', 'sman.r_name as sman_our_name']);

        MatchedFilterWidget::addQueryWhere($query, $params, 'sou_sman.sman_our_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 60,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => ArrayHelper::merge($dataProvider->sort->attributes, [
                'sman_our_name',
            ]),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sou_id' => $this->sou_id,
        ]);

        $query->andFilterWhere(['&', 'sou_sman.r_status', $this->r_status])
            ->andFilterWhere(['like', 'sou_sman.sou_key', $this->sou_key])
            ->andFilterWhere(['like', 'sou_sman.name', $this->name])
            ->andFilterWhere(['like', 'sou_sman.contry_code', $this->contry_code])
            ->andFilterWhere(['like', 'sou_sman.nickname', $this->nickname])
            ->andFilterWhere(['like', 'sou_sman.ext_info', $this->ext_info])
            ->andFilterWhere(['like', 'sou_sman.sman_our_id', $this->sman_our_id])
            ->andFilterHaving(['like', 'sman_our_name', $this->sman_our_name]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'id',
            'sou_key',
            'sou_id',
            'r_status',
            'name',
            'logo',
            'logo_sman',
            'nickname',
        ];
    }
}
