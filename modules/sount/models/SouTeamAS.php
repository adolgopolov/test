<?php

namespace common\modules\sount\models;

use common\modules\sount\widgets\buffer\interfaces\ColumnHiderModelInterface;
use common\modules\sount\widgets\buffer\MatchedFilterWidget;
use common\modules\sport\models\SportTeamAR;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * SouTeamAS represents the model behind the search form of `common\modules\sount\models\SouTeamAR`.
 */
class SouTeamAS extends SouTeamAR implements ColumnHiderModelInterface
{
    /** @var string */
    public $team_our_name;

    /** @var array */
    public $columns;

    /** @var string */
    public $logo_team;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sou_id', 'r_status'], 'integer'],
            [['team_our_name'], 'string'],
            [['sou_key', 'name', 'abbr', 'contry_code', 'ext_info', 'team_our_id'], 'safe'],
            [['columns'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'team_our_id' => 'ID',
            'team_our_name' => 'Наша команда',
            'logo_team' => 'Фото (С базы)',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->alias('sou_team')
            ->leftJoin(SportTeamAR::tableName() . ' team', 'team.id = sou_team.team_our_id')
            ->select(['sou_team.*', 'team.r_name as team_our_name']);

        MatchedFilterWidget::addQueryWhere($query, $params, 'sou_team.team_our_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 60,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => ArrayHelper::merge($dataProvider->sort->attributes, [
                'team_our_name',
            ]),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sou_team.id' => $this->id,
            'sou_team.sou_id' => $this->sou_id,
        ]);

        $query->andFilterWhere(['&', 'sou_team.r_status', $this->r_status])
            ->andFilterWhere(['like', 'sou_team.sou_key', $this->sou_key])
            ->andFilterWhere(['like', 'sou_team.name', $this->name])
            ->andFilterWhere(['like', 'sou_team.abbr', $this->abbr])
            ->andFilterWhere(['like', 'sou_team.contry_code', $this->contry_code])
            ->andFilterWhere(['like', 'sou_team.team_our_id', $this->team_our_id])
            ->andFilterHaving(['like', 'team_our_name', $this->team_our_name]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'id',
            'sou_key',
            'sou_id',
            'r_status',
            'logo',
            'logo_team',
            'abbr',
            'contry_code',
        ];
    }
}
