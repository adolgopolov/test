<?php

namespace common\modules\sount\models;

use common\models\Many2Many;
use common\modules\sport\models\SportLeagueAR;
use common\modules\sport\models\SportTeamAR;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class SportTeamOurAS
 * @package common\modules\sount\models
 *
 * @property $league
 */
class SportTeamOurAS extends SportTeamAR
{
    public $sou_team;
    public $league;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sport_id'], 'integer'],
            [['id', 'r_name', 'r_icon', 'team_jersey', 'league'], 'string'],
            [['ext_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'sou_team' => 'Буферные команды',
        ]);
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $souTeamQuery = SouTeamAR::find()
            ->alias('sou_team')
            ->select(["GROUP_CONCAT(' ', sou_team.id)"])
            ->andWhere(['and',
                "sou_team.name RLIKE REGEXP_REPLACE(team.r_name, '[\\\s,]+', '|')",
                ['sou_team.team_our_id' => null],
            ])
            ->limit(2);

        $query = self::find()
            ->alias('team')
            ->select(['team.*', '(' . $souTeamQuery->createCommand()->rawSql . ') as sou_team'])
            ->leftJoin(Many2Many::tableName() . ' m2m', 'team.id = m2m.id_left AND rel_kind=' . Many2Many::TYPE_TEAM_LEAGUE)
            ->leftJoin(SportLeagueAR::tableName() . ' league', 'league.id = m2m.id_right')
            ->leftJoin(SouTeamAR::tableName(), 'team.id = team_our_id')
            ->andWhere(['team_our_id' => null]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 60,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AND',
            ['team.id' => $id],
            ['like', 'league.r_name', $this->league],
            ['like', 'team.r_name', $this->r_name],
            ['like', 'team.r_icon', $this->r_icon],
            ['like', 'team.team_jersey', $this->team_jersey],
            ['team.sport_id' => $this->sport_id],
        ]);

        $query->andFilterWhere(['like', 'ext_info', $this->ext_info]);

        return $dataProvider;
    }
}
