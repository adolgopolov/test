<?php

namespace common\modules\sount\models;

/**
 * This is the ActiveQuery class for [[SouOccaAR]].
 *
 * @see SouOccaAR
 */
class SouOccaAQ extends \yii\db\ActiveQuery
{
    public function whereMatch($matchId)
    {
        return $this->andWhere(['match_sou_id' => $matchId]);
    }

    /**
     * {@inheritdoc}
     * @return SouOccaAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SouOccaAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
