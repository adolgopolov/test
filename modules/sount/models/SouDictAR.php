<?php

namespace common\modules\sount\models;

use common\components\S3DictionaryAR;
use common\extensions\ExtInfoAR;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "ss_sou_dict".
 *
 * @property int $id
 * @property string $sou_key
 * @property int $sou_id
 * @property int $ent_id внутренний ключ
 * @property int $ent_kind внутренний тип
 * @property array $ext_info
 *
 * @property string $uniKey
 * @property S3DictionaryAR $innerDict
 */
class SouDictAR extends ExtInfoAR
{
    const KIND_POSITIONS = 2;
    const KIND_FACTS = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sou_dict}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return $this->strongRules([
            [['sou_key', 'sou_id', 'ent_kind'], 'required'],
            [['sou_id', 'ent_id', 'ent_kind'], 'integer'],
            [['ext_info'], 'safe'],
            [['sou_key'], 'string', 'max' => 255]
            ],
            [
            [['sou_id', 'sou_key'], 'unique', 'targetAttribute' => ['sou_id', 'sou_key']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sou_key' => 'Sou Key',
            'sou_id' => 'Sou ID',
            'ent_id' => 'Ent ID',
            'ent_kind' => 'Ent Kind',
            'ext_info' => 'Ext Info',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SouDictAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SouDictAQ(get_called_class());
    }

    public function getUniKey()
    {
        return self::genUniKey($this->ent_kind,$this->sou_key);
    }

    public static function genUniKey($kind,$key)
    {
        return $kind."-".$key;
    }

    public function getInnerDict() : ActiveQuery
    {
        return $this->hasOne(S3DictionaryAR::class,['id' => 'ent_id'])->onCondition(['ent_kind'=>$this->ent_kind]);
    }
}
