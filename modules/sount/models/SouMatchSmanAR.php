<?php

namespace common\modules\sount\models;

use Yii;
use yii\db\AfterSaveEvent;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ss_sou_match_sman".
 *
 * @property int $match_sou_id
 * @property int $sman_sou_id
 */
class SouMatchSmanAR extends \common\extensions\ExtInfoAR
{
    protected static $newRecords = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sou_match_sman}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['match_sou_id', 'sman_sou_id'], 'required'],
            [['match_sou_id', 'sman_sou_id'], 'integer'],
            //[['match_sou_id', 'sman_sou_id'], 'unique', 'targetAttribute' => ['match_sou_id', 'sman_sou_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'match_sou_id' => 'Match Sou ID',
            'sman_sou_id' => 'Sman Sou ID',
        ];
    }

    //todo создать слушателя для накопления массива для записи

    public function grabNewSouOcca(AfterSaveEvent $event)
    {
        $rec = $event->sender->getAttributes(['match_sou_id','sman_sou_id']);
        $key = $rec['match_sou_id'].'-'.$rec['sman_sou_id'];
        self::$newRecords[$key] = $rec;

    }

    public function flushOcca()
    {
        // выбрать посторяющиеся матчи
        $smans = ArrayHelper::getColumn(self::$newRecords,'sman_sou_id');
        $smans = array_unique($smans);
        $oldRecs = self::find()->where(['sman_sou_id' => $smans])->all();
        $oldKeys = [];
        foreach ($oldRecs as $oldRec) {
            $oldKeys[$oldRec['match_sou_id'].'-'.$oldRec['sman_sou_id']] = $oldRec;
        }
        // убрать из массива уже записанные
        foreach (self::$newRecords as $key => $occa) {
            if(!array_key_exists($key,$oldKeys)) {
                $newRecs[] = $occa;
                $oldKeys[$key] = $occa;
            }
        }
        if(!empty($newRecs)) {
            $this->insertAll($newRecs);

            $this->deleteOldMatches($oldKeys);
        }
        self::$newRecords = [];
    }

    public function insertAll($rows)
    {
        Yii::$app->db->createCommand()->batchInsert(self::tableName(), ['match_sou_id','sman_sou_id'], $rows)->execute();
    }

    public function deleteOldMatches($heapOfRecs)
    {
        $smans = [];
        if(!empty($heapOfRecs)){
            foreach ($heapOfRecs as $rec) {
                $smans[$rec['sman_sou_id']][$rec['match_sou_id']] = $rec['match_sou_id'];
            }
            foreach ($smans as $sman) {
                krsort($sman);
                //todo 50 заменить на число максимального периода
                $last50Recs = array_slice($sman, 0, 50);
                $RecsforDelete = array_diff($sman,$last50Recs);
                //todo сделать удаление блока записей для спортсмена одним запросом
                foreach ($RecsforDelete as $data) {
                    self::deleteAll(['match_sou_id'=>$data['match_sou_id'], 'sman_sou_id' => $data['sman_sou_id']]);
                }
            }
        }
    }
}
