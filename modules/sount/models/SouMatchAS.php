<?php

namespace common\modules\sount\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SouMatchAS represents the model behind the search form of `common\modules\sount\models\SouMatchAR`.
 */
class SouMatchAS extends SouMatchAR
{
    public $columns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sou_id', 'r_status', 'match_our_id', 'th_sou_id', 'ta_sou_id'], 'integer'],
            [['tou_sou_id',], 'string'],
            [['sou_key', 'date_start', 'ext_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();//->with('tournament');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sou_id' => $this->sou_id,
            'match_our_id' => $this->match_our_id,
            'tou_sou_id' => $this->tou_sou_id,
            'th_sou_id' => $this->th_sou_id,
            'ta_sou_id' => $this->ta_sou_id,
            'date_start' => $this->date_start,
        ]);
        $query->andFilterWhere(['&', 'r_status', $this->r_status]);

        $query->andFilterWhere(['like', 'sou_key', $this->sou_key])
            //->andFilterWhere(['like', 'tournament.name', $this->tou_sou_id])
            ->andFilterWhere(['like', 'ext_info', $this->ext_info]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'sou_key',
            'sou_id',
            'r_status',
            'tou_sou_id',
            'match_our_id',
            'th_sou_id',
            'ta_sou_id',
            'date_start',
        ];
    }
}
