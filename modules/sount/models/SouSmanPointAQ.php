<?php

namespace common\modules\sount\models;

/**
 * This is the ActiveQuery class for [[SouSmanPointAR]].
 *
 * @see SouSmanPointAR
 */
class SouSmanPointAQ extends \common\extensions\ExtAQ
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return SouSmanPointAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SouSmanPointAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
