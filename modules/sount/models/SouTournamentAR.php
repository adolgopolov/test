<?php

namespace common\modules\sount\models;

use common\modules\sount\SportSouModule;
use Yii;

/**
 * This is the model class for table "ss_sou_tournament".
 *
 * @property int $id
 * @property string $sou_key
 * @property int $sou_id
 * @property string $date_start
 * @property string $date_end
 * @property string $name
 * @property array $ext_info
 * @property bool $r_status [tinyint(3) unsigned]
 * @property int $status [smallint(6) unsigned]
 */
class SouTournamentAR extends SouAR
{
    public function init()
    {
        $this->columnImportId = null;
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ss_sou_tournament';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sou_key', 'sou_id', 'name'], 'required'],
            [['sou_id','r_status'], 'integer'],
            ['statusList', 'in', 'range' => array_keys(SportSouModule::getSouStatus()), 'allowArray' => true],
            //['x', 'each', 'rule' => ['in', 'range' => [2, 4, 6, 8]]],
            [['date_start', 'date_end', 'ext_info'], 'safe'],
            [['sou_key', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'sou_key' => 'Ключ',
            'sou_id' => 'Клиент',
            'date_start' => 'Дата старта',
            'date_end' => 'Дата окончания',
            'name' => 'Название',
            'country' => 'Страна',
            'r_status' => 'Состояние',
            'statusList' => 'Статус',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SouTournamentAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SouTournamentAQ(get_called_class());
    }
}
