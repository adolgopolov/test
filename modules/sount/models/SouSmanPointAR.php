<?php

namespace common\modules\sount\models;

use Yii;

/**
 * This is the model class for table "ss_sou_sman_point".
 *
 * @property int $sman_sou_id
 * @property int $match_sou_id
 * @property int $sou_id
 * @property int $points
 * @property array $scoreMatch
 * @property array $occa
 * @property array $ext_info
 */
class SouSmanPointAR extends \common\extensions\ExtInfoAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sou_sman_point}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return $this->strongRules([
            [['sman_sou_id', 'match_sou_id', 'sou_id'], 'required'],
            [['sman_sou_id', 'match_sou_id', 'sou_id', 'points'], 'integer'],
            [['ext_info'], 'safe']
            ],[
            [['sman_sou_id', 'match_sou_id'], 'unique', 'targetAttribute' => ['sman_sou_id', 'match_sou_id']],
        ]);
    }

    protected function jsonFields(): array
    {
        return array_merge(parent::jsonFields(), [
            'scoreMatch',
            'occa',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sman_sou_id' => 'Sman Sou ID',
            'match_sou_id' => 'Match Sou ID',
            'sou_id' => 'Sou ID',
            'points' => 'Points',
            'ext_info' => 'Ext Info',
        ];
    }

    /**
     * @return string
     */
    public function getKeyMatchSman()
    {
        return $this->sou_id."-".$this->match_sou_id."-".$this->sman_sou_id;
    }
    /**
     * {@inheritdoc}
     * @return SouSmanPointAQ the active query used by this AR class.
     */
    public static function find()
    {
        return new SouSmanPointAQ(get_called_class());
    }

    /**
     * @param $occaPoints
     */
    public function addPoints($occaPoints)
    {
        $this->points += $occaPoints??0;
    }
}
