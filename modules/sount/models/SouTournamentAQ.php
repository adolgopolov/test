<?php

namespace common\modules\sount\models;

use common\modules\sount\StatusAQTrait;

/**
 * This is the ActiveQuery class for [[SouTournament]].
 *
 * @see SouTournamentAR
 */
class SouTournamentAQ extends \yii\db\ActiveQuery
{
    use StatusAQTrait;

    /**
     *  турниры которые скоро начнутся(через неделю) или проходят в данный момент
     * @return SouTournamentAQ
     */
    public function actual()
    {
        return $this->andWhere(['<=','date_start',date('Y-m-d H:i:s',time()-24*3600*7)])->andWhere(['>=','date_end',time()]);
    }

    /**
     * {@inheritdoc}
     * @return SouTournamentAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SouTournamentAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
