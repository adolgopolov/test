<?php

namespace common\modules\sount\models;

use common\modules\sount\StatusAQTrait;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[SouTeamMemberAR]].
 *
 * @see SouTeamMemberAR
 */
class SouTeamMemberAQ extends \common\extensions\ExtAQ
{

    use StatusAQTrait;
    /**
     * {@inheritdoc}
     * @return SouTeamMemberAR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SouTeamMemberAR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $date
     * @return SouTeamMemberAQ
     */
    public function byDate($date)
    {
        return $this->andWhere(['<=', 'first_day', new Expression("DATE(:date)", [':date' => $date])])
            ->andWhere(['>=', 'last_day', new Expression("DATE(:date)", [':date' => $date])]);
    }

    public function byMan($manId)
    {
        return $this->andWhere(['sman_sou_id' => $manId]);
    }
}
