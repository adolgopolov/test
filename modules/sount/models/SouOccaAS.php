<?php

namespace common\modules\sount\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SouOccaAS represents the model behind the search form of `common\modules\sount\models\SouOccaAR`.
 */
class SouOccaAS extends SouOccaAR
{
    public $columns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sou_id', 'match_sou_id', 'sman_sou_id', 'game', 'fact_sou_id', 'fact_value', 'after_start'], 'integer'],
            [['original_value', 'ext_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SouOccaAR::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sou_id' => $this->sou_id,
            'match_sou_id' => $this->match_sou_id,
            'sman_sou_id' => $this->sman_sou_id,
            'game' => $this->game,
            'fact_sou_id' => $this->fact_sou_id,
            'fact_value' => $this->fact_value,
            'after_start' => $this->after_start,
        ]);

        $query->andFilterWhere(['like', 'original_value', $this->original_value])
            ->andFilterWhere(['like', 'ext_info', $this->ext_info]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'sou_id',
            'match_sou_id',
            'sman_sou_id',
            'game',
        ];
    }
}
