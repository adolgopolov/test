<?php

namespace common\modules\sount\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SouTournamentAS represents the model behind the search form of `common\modules\sount\models\SouTournamentAR`.
 */
class SouTournamentAS extends SouTournamentAR
{
    public $columns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sou_id', 'r_status'], 'integer'],
            [['sou_key', 'date_start', 'date_end', 'name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sou_id' => $this->sou_id,
            'r_status' => $this->r_status,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
        ]);

        $query->andFilterWhere(['like', 'sou_key', $this->sou_key])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getColumnsList()
    {
        return [
            'sou_key',
            'sou_id',
            'r_status',
            'date_start',
            'date_end',
            'name',
            'country',
        ];
    }
}
