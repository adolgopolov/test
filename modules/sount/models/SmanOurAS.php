<?php

namespace common\modules\sount\models;

use common\modules\sport\models\SmanAR;
use common\modules\sport\models\SportMemberAR;
use common\modules\sport\models\SportTeamAR;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class SmanOurAS extends SmanAR
{
    public $sou_sman;
    public $team;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'r_name', 'sman_status', 'r_icon', 'r_name', 'team'], 'string'],
            [['ext_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'sou_sman' => 'Буферные спортсмены',
        ]);
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $souSmanQuery = SouSmanAR::find()
            ->alias('sou_sman')
            ->select(["GROUP_CONCAT(' ', sou_sman.id)"])
            ->andWhere(['and',
                "sou_sman.name RLIKE REGEXP_REPLACE(sman.r_name, '[\\\s,]+', '|')",
                ['sou_sman.sman_our_id' => null],
            ])
            ->limit(2);

        $query = self::find()
            ->alias('sman')
            ->select(['sman.*', '(' . $souSmanQuery->createCommand()->rawSql . ') as sou_sman'])
            ->leftJoin(SouSmanAR::tableName() . ' sou_sman', 'sman.id = sou_sman.sman_our_id')
            ->leftJoin(SportMemberAR::tableName() . ' team_member', 'sman.id = team_member.sman_id')
            ->leftJoin(SportTeamAR::tableName() . ' team', 'team_member.team_id = team.id')
            ->andWhere(['sou_sman.sman_our_id' => null]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 60,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $id = str_replace(' ', '', trim($this->id));
        if (strpos($id, ',')) {
            $id = explode(',', $id);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AND',
            ['sman.id' => $id],
            ['like', 'sman.r_name', $this->r_name],
            ['like', 'sman.r_icon', $this->r_icon],
        ]);

        $query->andFilterWhere(['like', 'team.r_name', $this->team]);

        return $dataProvider;
    }
}
