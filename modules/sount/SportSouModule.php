<?php

namespace common\modules\sount;

use backend\components\aboard\MenuEntryInterface;
use common\modules\sount\clients\SportClientInterface;
use common\modules\sount\clients\SportRadarDota;
use common\modules\sount\grabbers\SportGrabber;
use common\modules\sount\jobs\MatchSummaryJob;
use common\modules\sount\jobs\SmanJob;
use common\modules\sount\jobs\SouScoringJob;
use common\modules\sount\jobs\TeamJob;
use common\modules\sount\jobs\TournamentScheduleJob;
use common\modules\sount\jobs\TournamentsJob;
use common\modules\sount\models\SouMatchAR;
use common\modules\sount\models\SouTeamAR;
use common\modules\sount\models\SouTournamentAR;
use common\modules\sount\operations\MatchScoring;
use Yii;
use yii\base\Event;
use yii\queue\JobInterface;

/**
 * Главный класс SountModule
 *
 * Инициализация клиентов грабера набор всех используемых групп констант необходимых для статусов типов и прочего
 * все методы публичного апи модуля для работы с ним
 *
 * @package common\modules\sount
 */
class SportSouModule extends \yii\base\Module implements MenuEntryInterface
{
    /**
     *  названия клиентов типы
     * @see
     */
    CONST TYPE_SR_DOTA = 'sr_dota';
    CONST TYPE_SR_CSGO = 'sr_csgo';
    CONST TYPE_SR_SOCCER = 'sr_soccer';
    CONST TYPE_SR_HOCKEY = 'sr_hockey';
    CONST TYPE_SR_BASKETBALL = 'sr_basketball';
    /**
     *  названия клиентов типы
     * @see
     */
    CONST SOU_ID_SR_DOTA = 1;
    CONST SOU_ID_SR_CSGO = 2;
    CONST SOU_ID_SR_SOCCER = 3;
    CONST SOU_ID_SR_HOCKEY = 4;
    CONST SOU_ID_SR_BASKETBALL = 5;
    /**
     *  статусы записей в двичной реализации для буферных таблиц
     * @see StatusAQTrait
     * @see models/SouAR
     */
    CONST STATUS_DISALLOW = 1;
    CONST STATUS_DRAFT = 2;
    CONST STATUS_PREPARED = 4;
    CONST STATUS_BUGS = 8;
    CONST STATUS_IMPORTED = 16;
    CONST STATUS_CLOSED = 32;

    /**
     * массив соответствия SOU_ID и типов(строковых названий)
     * @var array
     */
    private static $types = [
        self::SOU_ID_SR_DOTA => self::TYPE_SR_DOTA,
        self::SOU_ID_SR_CSGO => self::TYPE_SR_CSGO,
        self::SOU_ID_SR_SOCCER => self::TYPE_SR_SOCCER,
//        self::SOU_ID_SR_HOCKEY => self::TYPE_SR_HOCKEY,
//        self::SOU_ID_SR_BASKETBALL => self::TYPE_SR_BASKETBALL,
    ];
    /**
     * массив соответствия SOU_ID и id видов спорта(dictionary)
     * @var array
     */
    private static $sports = [
        self::SOU_ID_SR_DOTA => 100,
        self::SOU_ID_SR_CSGO => 101,
        self::SOU_ID_SR_SOCCER => 1,
        self::SOU_ID_SR_HOCKEY => 2,
        self::SOU_ID_SR_BASKETBALL => 3,
    ];
    /**
     * список текстовых названий для статусов
     * @var array
     */
    private static $souStatuses = [
        self::STATUS_DISALLOW => 'Не отслеживаемый',
        self::STATUS_DRAFT => 'Черновик',
        self::STATUS_PREPARED => 'Загружен',
        self::STATUS_BUGS => 'Имеет ошибки',
        self::STATUS_IMPORTED => 'Импортирован',
        self::STATUS_CLOSED => 'Закрыт',
    ];
    /**
     * инициализированные клиенты
     * @var array
     */
    public $clients;
    /**
     * инициализированные грабберы
     * @var array
     */
    public $grabbers;

    /**
     * todo функционал переключить с импорта на обновление уже связанных записей
     * @deprecated отказались от реализации импорта, заменить UpdaterManager
     * @var ImportManager
     */
    public $importManager;

    /**
     * инициализация зависимых контейнеров
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->importManager = \Yii::createObject($this->importManager);
        $this->initGrabberHandlers();

    }

    /**
     * список пунктов меню для админки "Буферные таблицы"
     * @see backend/views/layouts/sidebar.php
     * @return array
     */
    public static function getMenuItems(): ?array
    {
        return [
            [
                'label' => 'Буферные данные',
                'icon' => 'user',
                'color' => 'blue-500',
                'items' => [
                    [
                        'label' => 'Турниры',
                        'url' => ['/sount/tournaments/index'],
                        'icon' => 'list',
                        'color' => 'blue-500'
                    ],
                    [
                        'label' => 'Матчи',
                        'url' => ['/sount/matches/index'],
                        'icon' => 'list',
                        'color' => 'blue-500'
                    ],
                    [
                        'label' => 'Команды',
                        'url' => ['/sount/teams/index'],
                        'icon' => 'list',
                        'color' => 'blue-500'
                    ],
                    [
                        'label' => 'Спортсмены',
                        'url' => ['/sount/smen/index'],
                        'icon' => 'list',
                        'color' => 'blue-500'
                    ],
                    [
                        'label' => 'Cоп. нашей команды',
                        'url' => ['/sount/teams/our-team'],
                        'icon' => 'list',
                        'color' => 'blue-500'
                    ],
                    [
                        'label' => 'Cоп. нашего спортсмена',
                        'url' => ['/sount/smen/our-smen'],
                        'icon' => 'list',
                        'color' => 'blue-500'
                    ],
                    [
                        'label' => 'Членство',
                        'url' => ['/sount/members/index'],
                        'icon' => 'list',
                        'color' => 'blue-500'
                    ],
                    [
                        'label' => 'События',
                        'url' => ['/sount/occa/index'],
                        'icon' => 'list',
                        'color' => 'blue-500'
                    ],
                    [
                        'label' => 'Справочник',
                        'url' => ['/sount/dict/index'],
                        'icon' => 'list',
                        'color' => 'blue-500'
                    ],
                ]
            ]
        ];
    }

    /**
     * метод апи модуля для сбора расширенной статистики и скоринга очков по матчу
     * @param $matchSouId
     * @return bool
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function matchScoreStatistic($matchSouId, &$errors)
    {
        $scoring = new MatchScoring();
        $scoring->calculate($matchSouId);
        if ($scoring->save()) {
            return true;
        }
        $errors = $scoring->getErrors();
        return false;

    }

    /**
     * test method!!!!
     * @param $apiMethod
     * @param array $params
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function getDotaRequest($apiMethod, $params = [])
    {
        $loadFromFile = $params['saveFile'] ?? false;
        $param = $params['id'] ?? $params['date'] ?? null;
        $client = $this->getClient($params['type'] ?? 1, [
            'saveFile' => $params['saveFile'] ?? false,
            'loadFromFile' => $params['loadFromFile'] ?? false,
        ]);
        $client->$apiMethod($param);

        $path = $params['path'] ?? null;
        if ($client->hasErrors()) {
            return $client->getErrors();
        }
        if ($params['keys'] ?? null) {
            return $client->sourceKeys($path);
        }
        return $client->sourceByPath($path);

    }

    /**
     * создание задачи для очереди для получения списка турниров
     * @param int $typeClient
     * @param int $taskId
     */
    public function createJobTournaments(int $typeClient,$taskId = 0)
    {
        $id = $this->getPushQueue(new TournamentsJob([
            'type' => $typeClient,
            'taskId' => $taskId,
        ]));
    }

    /**
     * создание задачи  для скоринга по матчу для расширенной статистики
     * @param $matchId
     * @param int $taskId
     */
    public function createJobSouScoring($matchId,$taskId = 0)
    {
        $id = $this->getPushQueue(new SouScoringJob([
            'matchId' => $matchId,
            'taskId' => $taskId,
        ]));
    }

    /**
     * задача для получения распиания по турниру
     *
     * матчи с их временем проведения и статусами вданном турнире
     *
     * @param int $typeClient
     * @param string $souTouKey
     * @param int $taskId
     */
    public function createJobTouSchedule(int $typeClient, string $souTouKey, $taskId = 0)
    {
        $id = $this->getPushQueue(new TournamentScheduleJob([
            'type' => $typeClient,
            'touId' => $souTouKey,
            'taskId' => $taskId,
        ]));
    }

    /**
     * создание задачи для получения профиля спортсмена
     * @param int $typeClient
     * @param string $souSmanKey
     * @param int $taskId
     * @return string|null
     */
    public function createJobSman(int $typeClient, string $souSmanKey, $taskId = 0)
    {
        $id = $this->getPushQueue(new SmanJob([
            'type' => $typeClient,
            'smanId' => $souSmanKey,
            'taskId' => $taskId,
        ]));
        return $id;
    }

    /**
     * оздание задачи для получения профиля команды
     * @param int $typeClient
     * @param SouTeamAR $buffTeam
     * @param int $taskId
     */
    public function createJobTeam(int $typeClient, SouTeamAR $buffTeam, $taskId = 0)
    {

        $id = $this->getPushQueue(new TeamJob([
            'type' => $typeClient,
            'teamId' => $buffTeam->sou_key,
            'taskId' => $taskId,
        ]));
    }

    /**
     * создание задачи для получения событий по матчу
     *
     * тип получаемых событий определяется для каждого вида спорта в клиенте
     * todo переименовать потому что в клиентах для получения списка событий могут использоваться
     * todo и Summary и Timeline mode
     * @param int $typeClient
     * @param string $souMatchKey
     * @param int $taskId
     */
    public function createJobMatchSum(int $typeClient, string $souMatchKey, $taskId = 0)
    {
        $id = $this->getPushQueue(new MatchSummaryJob([
            'type' => $typeClient,
            'matchId' => $souMatchKey,
            'taskId' => $taskId,
        ]));
    }

    /**
     *  создание списком задач по всем турнирам с незавершенной датой окончания, забрать все матчи
     *
     * метод для кнопки в админке, возможно надо переделать
     * @param int $typeClient
     * @param null $date
     * @return array
     */
    public function createJobMatchesForAllTou(int $typeClient, $date = null)
    {
        $date = $date ?? date('Y-01-01');
        $tous = SouTournamentAR::find()->draft()->notDisallow()->andWhere(['AND',
            ['sou_id' => $typeClient],
            ['>=', 'date_end', $date],
        ])->all();
        $ids = [];
        foreach ($tous as $tou) {
            $ids[] = $this->createJobTouSchedule($typeClient, $tou->sou_key, 0);
        }
        return $ids;
    }

    /**
     *  задачи списком по турниру с прошедшей датой начала , забрать все occa матчей
     * @param int| string $touId
     * @return array | error
     */
    public function createJobMatchesSumForTou($touId)
    {
        if (is_numeric($touId)) {
            $tou = SouTournamentAR::findOne(['id' => $touId]);
        } else {
            $tou = SouTournamentAR::findOne(['sou_key' => $touId]);
        }

        if ($tou) {
            $typeClient = $tou->sou_id;
            $matches = SouMatchAR::find()
                ->notHasBugs()->notClosed()->notDisallow()
                ->where(['and',
                ['tou_sou_id' => $tou->id],
                ['<=', 'date_start', date('Y-m-d')],
            ])->limit(10)->all();
            $ids = [];
            foreach ($matches as $match) {
                $ids[] = $this->createJobMatchSum($typeClient, $match->sou_key, 0);
            }
            return $ids;
        }
        return 'Нету такого турнира';
    }

    /**
     * получение клиента на текстовому названию
     * @param int $type
     * @param array $config
     * @return SportClientInterface|null
     * @throws \yii\base\InvalidConfigException
     *
     * @see self::getTypeClient self::$types
     */
    public function getClient($type, $config = []): ?SportClientInterface
    {
        $config = [
            'saveFile' => $params['saveFile'] ?? false,
            'loadFromFile' => $params['loadFromFile'] ?? false,
        ];
        if (array_key_exists($type, $this->clients)) {
            return $client = \Yii::createObject(array_merge([
                'class' => $this->clients[$type],
            ], $config));
        }
        return null;
    }

    /**
     * @param null $souId
     * @return array|bool|string
     */
    public static function getTypeClient($souId = null)
    {
        if ($souId !== null) return self::$types[$souId] ?? false;
        return self::$types;
    }

    /**
     * получить номер вида спорта по номеру клиента
     * @param null $souId
     * @return array|bool|mixed
     *
     * @see self::$sports
     */
    public static function getSportByClientType($souId = null)
    {
        if ($souId !== null) return self::$sports[$souId] ?? false;
        return self::$sports;
    }

    /**
     * получить название статуса по номеру или список id=>name по умолчанию
     * @param null $statusId
     * @return array|bool|string
     */
    public static function getSouStatus($statusId = null)
    {
        if ($statusId !== null) {
            $str = '';
            foreach (self::$souStatuses as $key => $name) {
                if ($key & $statusId) {
                    $str .= $name . " ";
                }
            }
            return $str ?? false;
        }
        return self::$souStatuses;
    }

    /**
     * инициализация граббера и навешивание событий клиентов
     * @throws \yii\base\InvalidConfigException
     */
    protected function initGrabberHandlers(): void
    {
        foreach ($this->grabbers as $key => $grabber) {

            if ($key === 0) {
                /** @var SportGrabber $grabber */
                $grabber = \Yii::createObject($grabber, [
                    'test' => true,
                ]);

                Event::on(SportClientInterface::class, SportClientInterface::EVENT_TOURNAMENTS, [$grabber, 'loadNewTournamets']);
                Event::on(SportClientInterface::class, SportClientInterface::EVENT_MATCHES, [$grabber, 'loadNewMatches']);
                Event::on(SportClientInterface::class, SportClientInterface::EVENT_SMAN, [$grabber, 'updateMember']);
                Event::on(SportClientInterface::class, SportClientInterface::EVENT_TEAM, [$grabber, 'updateTeam']);
                Event::on(SportClientInterface::class, SportClientInterface::EVENT_MATCH_STAT, [$grabber, 'loadMatchSummary']);
                //todo
            } else if ($key === 1) {
                //TODO инициализация другого граббера (новости)
            }
        }
    }

    /**
     * служебный метод по добавлению задачи в очередь для исполнения
     *
     * @param JobInterface $job
     * @return string|null
     */
    protected function getPushQueue(JobInterface $job)
    {
        return Yii::$app->qusount->push($job);
    }

}
