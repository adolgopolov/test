<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\TaskAR */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-ar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'priority')->dropDownList($model->listTasks) ?>

    <?= $form->field($model, 'status')->checkbox(['uncheck' => 1, 'value' => 2,'label'=> 'Отметка о выполнении']) ?>

    <?= $form->field($model, 'date_done')->widget(DatePicker::class, [

    ]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
