<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\TaskAR;
/* @var $this yii\web\View */
/* @var $model app\models\TaskAR */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'To Do List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="task-ar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description:ntext',
            'priority',
            [
                'attribute' => 'status',
                'value' => function(TaskAR $model) {
                    return TaskAR::getStatuses($model->status);
                }
            ],

            'date_done:date',
            'created_at:datetime',
        ],
    ]) ?>

</div>
