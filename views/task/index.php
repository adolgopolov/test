<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\TaskAR;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskAS */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'To Do List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-ar-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать задачу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',
            'priority',
            [
                'attribute' => 'status',
                'filter' => TaskAR::getStatuses(),
                'value' => function (TaskAR $model) {
                    return TaskAR::getStatuses($model->status);
                }
            ],
            'date_done:date',
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {check} {delete}',
                'buttons' => [
                    'check' => function ($url, $model, $key) {
                        $done = $model->isDone() ? 'ok' : 'off';
                        return Html::a('<span class="glyphicon glyphicon-' . $done . '"></span>', $url);
                    },
                ]
            ],
        ],
    ]); ?>


</div>
