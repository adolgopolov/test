<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskAR */

$this->title = 'Create Task Ar';
$this->params['breadcrumbs'][] = ['label' => 'Task Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-ar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
