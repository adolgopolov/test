<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $models app\models\TaskAR[] */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $form yii\widgets\ActiveForm */


$this->title = 'Task1-2-3';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/task1.js');
?>
<div class="task-ar-index">

    <a href="<?= \yii\helpers\Url::to(['task-one/triangle']) ?>">Треугольник</a>
    <br/>

    <a href="<?= \yii\helpers\Url::to(['task-one/generate', 'u' => 30, 'p' => 300]) ?>">Сгенерировать юзеров и посты</a>

    <?php if (!empty($models)): ?>
        <?= Html::ul(
            \yii\helpers\ArrayHelper::getColumn($models,
                function ($model) {
                    return $model['name'] . "(" . $model['p_count'] . ")";
                }
            )
        ) ?>
    <?php endif; ?>

    <h1>Блокировка окна браузером</h1>
    <button id="popupButton">Click me!</button>

</div>
