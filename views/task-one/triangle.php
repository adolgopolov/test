<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskOne */
/* @var $form ActiveForm */
/* @var string $result */
$this->title = 'triangle';
$this->params['breadcrumbs'][] = ['label' => 'Task-1-2-3', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-one-triangle">

    <?php $form = ActiveForm::begin(); ?>
    <?php if($result): print_r($result) ; endif;?>
    <?= $form->field($model, 'line1')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'line2')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'line3')->textInput(['maxlength' => true]) ?>
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- task-one-triangle -->
